#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import QPointF, QRectF, QVariant
from PyQt4.QtGui import QMainWindow, QSpacerItem
from PyQt4.QtGui import QColor, QPainter, QPen, QGraphicsItem, QGraphicsScene

# Imports (Plugins and Resources)
import ui_xycontroller
from shared import *

# XY Controller Scene
class XYGraphicsScene(QGraphicsScene):
    def __init__(self, parent=None):
        super(XYGraphicsScene, self).__init__(parent)

        self.setBackgroundBrush(QColor(0,0,0))

        cursor_pen = QPen(QColor(255,255,255), 2)
        cursor_brush = QColor(255,255,255,50)
        self.xy_cursor = self.addEllipse(QRectF(-10, -10, 20, 20), cursor_pen, cursor_brush)

        line_pen = QPen(QColor(200,200,200,100), 1, Qt.DashLine)
        self.hline = self.addLine(-9999, 0, 9999, 0, line_pen)
        self.vline = self.addLine(0, -9999, 0, 9999, line_pen)

        self.rect_size = QRectF(-100, -100, 100, 100)
        self.bounds = self.addRect(self.rect_size, Qt.white)

        self.x_cc = self.parent().x_cc
        self.y_cc = self.parent().y_cc

        self.lock = False

    def setPosX(self, x, forward=True):
        if not self.lock:
          pos_x = x*(self.rect_size.x()+self.rect_size.width())
          self.xy_cursor.setPos(pos_x, self.xy_cursor.y())
          self.vline.setX(pos_x)

          if (forward):
            self.handleMIDI(pos_x/(self.rect_size.x()+self.rect_size.width()))

    def setPosY(self, y, forward=True):
        if not self.lock:
          pos_y = y*(self.rect_size.y()+self.rect_size.height())
          self.xy_cursor.setPos(self.xy_cursor.x(), pos_y)
          self.hline.setY(pos_y)

          if (forward):
            self.handleMIDI(None, pos_y/(self.rect_size.y()+self.rect_size.height()))

    def handleJackParam(self, param, value):
        if (param == self.x_cc):
          xp = (float(value)/63)-1
          yp = self.xy_cursor.y()/(self.rect_size.y()+self.rect_size.height())
          self.setPosX(xp, False)

        elif (param == self.y_cc):
          xp = self.xy_cursor.x()/(self.rect_size.x()+self.rect_size.width())
          yp = (float(value)/63)-1
          self.setPosY(yp, False)

        self.emit(SIGNAL("cursorMoved(float, float)"), xp, yp)

    def handleMousePos(self, pos):
        if (not self.rect_size.contains(pos)):
          if (pos.x() < self.rect_size.x()):
            pos.setX(self.rect_size.x())
          elif (pos.x() > self.rect_size.x()+self.rect_size.width()):
            pos.setX(self.rect_size.x()+self.rect_size.width())

          if (pos.y() < self.rect_size.y()):
            pos.setY(self.rect_size.y())
          elif (pos.y() > self.rect_size.y()+self.rect_size.height()):
            pos.setY(self.rect_size.y()+self.rect_size.height())

        self.xy_cursor.setPos(pos)
        self.hline.setY(pos.y())
        self.vline.setX(pos.x())

        xp = pos.x()/(self.rect_size.x()+self.rect_size.width())
        yp = pos.y()/(self.rect_size.y()+self.rect_size.height())

        self.emit(SIGNAL("cursorMoved(float, float)"), xp, yp)
        self.handleMIDI(xp, yp)

    def handleMIDI(self, xp=None, yp=None):
        rate = 0xff/4

        if (xp != None):
          value = int((xp*rate)+rate)
          for i in range(15): #FIXME
            jack.midi_out_data.append((0xB0+i, self.x_cc, value))

        if (yp != None):
          value = int((yp*rate)+rate)
          for i in range(15): #FIXME
            jack.midi_out_data.append((0xB0+i, self.y_cc, value))

    def keyPressEvent(self, event):
        return event.accept()

    def wheelEvent(self, event):
        return event.accept()

    def mousePressEvent(self, event):
        self.lock = True
        self.handleMousePos(event.scenePos())
        return QGraphicsScene.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        self.handleMousePos(event.scenePos())
        return QGraphicsScene.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        self.lock = False
        return QGraphicsScene.mouseReleaseEvent(self, event)

    def updateSize(self, size):
        self.rect_size.setRect(-(size.width()/2), -(size.height()/2), size.width(), size.height())
        self.bounds.setRect(self.rect_size)

# XY Controller Window
class XYControllerW(QMainWindow, ui_xycontroller.Ui_XYControllerW):
    def __init__(self, parent=None):
        super(XYControllerW, self).__init__(parent)
        self.setupUi(self)

        # Pre-Initial setup
        self.dial_x.setPixmap(6)
        self.dial_y.setPixmap(6)
        self.keyboard.setOctaves(5)

        # Load App Settings
        self.settings = QSettings()
        self.loadSettings()

        self.keyboard.enableOut(jack.midi_out_data)
        self.params_from_jack = []

        self.x_cc = 0x0A #Pan
        self.y_cc = 0x01 #Modulation

        self.timer = QTimer()
        self.timer.setInterval(100)

        self.scene = XYGraphicsScene(self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setRenderHints(QPainter.Antialiasing)

        # Connect actions to functions
        self.connect(self.act_show_keyboard, SIGNAL("triggered(bool)"), self.showKeyboard)
        self.connect(self.act_about, SIGNAL("triggered()"), self.aboutXYController)

        self.connect(self.dial_x, SIGNAL("valueChanged(int)"), self.updateX)
        self.connect(self.dial_y, SIGNAL("valueChanged(int)"), self.updateY)

        self.connect(self.timer, SIGNAL("timeout()"), self.checkJackParams)

        self.connect(self.scene, SIGNAL("cursorMoved(float, float)"), self.updateDials)

    def showKeyboard(self, show):
        self.keyboard.setVisible(show)

        for i in range(self.keyboard_layout.count()):
          item = self.keyboard_layout.itemAt(i)
          if (type(item) == QSpacerItem):
            item.changeSize(0, 64 if (show) else 0)

    def updateDials(self, xp, yp):
        self.dial_x.setValue(xp*100)
        self.dial_y.setValue(yp*100)

    def updateX(self, x):
        self.scene.setPosX(float(x)/100)

    def updateY(self, y):
        self.scene.setPosY(float(y)/100)

    def paramFromJack(self, param, value):
        if (param == self.x_cc or param == self.y_cc):
          self.params_from_jack.append((param, value))
          self.timer.start()

    def checkJackParams(self):
        h = 0
        for i in range(len(self.params_from_jack)):
          self.scene.handleJackParam(self.params_from_jack[i-h][0], self.params_from_jack[i-h][1])
          self.params_from_jack.pop(i-h)
          h += 1

        self.timer.stop()

    def aboutXYController(self):
        QMessageBox.about(self, self.tr("About XY Controller"), self.tr("<h3>XY Controller</h3>"
            "<br>Version %1"
            "<br>XY Controller is a simple XY widget that send and receives data from Jack MIDI.<br>"
            "<br>Copyright (C) 2011 falkTX").arg(VERSION))

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))
        self.settings.setValue("ShowKeyboard", self.act_show_keyboard.isChecked())
        self.settings.setValue("DialX", self.dial_x.value())
        self.settings.setValue("DialY", self.dial_y.value())

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())
        self.act_show_keyboard.setChecked(self.settings.value("ShowKeyboard", False).toBool())
        self.showKeyboard(self.settings.value("ShowKeyboard", False).toBool())
        self.dial_x.setValue(self.settings.value("DialX", 50).toInt()[0])
        self.dial_y.setValue(self.settings.value("DialY", 50).toInt()[0])

    def closeEvent(self, event):
        self.saveSettings()
        return QMainWindow.closeEvent(self, event)

    def resizeEvent(self, event):
        QTimer.singleShot(0, lambda: self.scene.updateSize(self.graphicsView.size()))
        QTimer.singleShot(0, lambda: self.graphicsView.centerOn(0, 0))
        # TODO - Manage key octaves number
        return QMainWindow.resizeEvent(self, event)


def process_callback(nframes, arg):
    # MIDI In
    midi_in_buffer = jacklib.port_get_buffer(jack.midi_in_port, nframes)

    event_count = jacklib.midi_get_event_count(midi_in_buffer)
    event_index = 0
    event = jacklib.jack_midi_event_t

    jacklib.midi_event_get(event, midi_in_buffer, event_index)

    for i in range(nframes):
      if (event.time == i and event_index < event_count):

        if (len(event.buffer) < 3):
          continue

        mode = ord(event.buffer[0])
        note = ord(event.buffer[1])
        velo = ord(event.buffer[2])

        if (0x90 <= mode <= 0x9f):
          gui.keyboard.noteOn(note)
        elif (0x80 <= mode <= 0x8f):
          gui.keyboard.noteOff(note)
        elif (0xB0 <= mode <= 0xBf):
          gui.paramFromJack(note, velo)

        event_index += 1

        if (event_index < event_count):
          jacklib.midi_event_get(event, midi_in_buffer, event_index)

    # MIDI Out
    midi_out_buffer = jacklib.port_get_buffer(jack.midi_out_port, nframes)
    jacklib.midi_clear_buffer(midi_out_buffer)

    h = 0

    for i in range(len(jack.midi_out_data)):
      mode = jack.midi_out_data[i-h][0]
      note = jack.midi_out_data[i-h][1]
      velo = jack.midi_out_data[i-h][2]

      data = "%s%s%s" % (chr(mode), chr(note), chr(velo))
      jacklib.midi_event_write(midi_out_buffer, i, data, 3)

      jack.midi_out_data.pop(i-h)
      h += 1

    return 0


#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QRasterApplication(sys.argv)
    app.setApplicationName("XY-Controller")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    #app.setWindowIcon(QIcon(":/48/xy-controller.png"))

    # Start jack
    jack.client = jacklib.client_open("XY-Controller", jacklib.NullOption, None)
    jack.midi_in_port = jacklib.port_register(jack.client, "midi_in", jacklib.DEFAULT_MIDI_TYPE, jacklib.PortIsInput, 0)
    jack.midi_out_port = jacklib.port_register(jack.client, "midi_out", jacklib.DEFAULT_MIDI_TYPE, jacklib.PortIsOutput, 0)
    jack.midi_out_data = []
    jacklib.set_process_callback(jack.client, process_callback, 0)

    # Show GUI
    gui = XYControllerW()
    gui.show()

    jacklib.activate(jack.client)

    # App-Loop
    ret = app.exec_()

    # Close Jack
    if (jack.client):
      jacklib.deactivate(jack.client)
      jacklib.client_close(jack.client)

    # Exit properly
    sys.exit(ret)

