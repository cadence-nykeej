#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import os
from PyQt4.QtCore import Qt, QFile, QIODevice, QTextStream, QTimer, SIGNAL
from PyQt4.QtGui import QDialog, QIcon, QPalette, QSyntaxHighlighter

# Imports (Custom Stuff)
import icons_rc, ui_logs

# Get Icon from user theme, using our own as backup (Oxygen)
def getIcon(icon, size=16):
    return QIcon.fromTheme(icon, QIcon(":/%ix%i/.png" % (size,size)))

# Jack Syntax Highlighter
class JackSyntaxH(QSyntaxHighlighter):
    def __init__(self, parent=None):
        super(JackSyntaxH, self).__init__(parent)

        self.palette = self.parent().palette()

    def highlightBlock(self, text):
      if ("ERROR: " in text):
        self.setFormat(text.indexOf("ERROR: "), text.count(), Qt.red)
      elif ("WARNING: " in text):
        self.setFormat(text.indexOf("WARNING: "), text.count(), Qt.darkRed)
      elif ("------------------" in text):
        self.setFormat(text.indexOf("------------------"), text.count(), self.palette.color(QPalette.Active, QPalette.Mid))
      elif ("Connecting " in text):
        self.setFormat(text.indexOf("Connecting "), text.count(), self.palette.color(QPalette.Active, QPalette.Link))
      elif ("Disconnecting " in text):
        self.setFormat(text.indexOf("Disconnecting "), text.count(), self.palette.color(QPalette.Active, QPalette.LinkVisited))

# LADISH Syntax Highlighter
class LadishSyntaxH(QSyntaxHighlighter):
    def __init__(self, parent=None):
        super(LadishSyntaxH, self).__init__(parent)

        self.palette = self.parent().palette()

    def highlightBlock(self, text):
      if ("ERROR: " in text):
        self.setFormat(text.indexOf("ERROR: "), text.count(), Qt.red)
      elif ("WARNING: " in text):
        self.setFormat(text.indexOf("WARNING: "), text.count(), Qt.darkRed)
      elif ("------------------" in text):
        self.setFormat(text.indexOf("------------------"), text.count(), self.palette.color(QPalette.Active, QPalette.Mid))
      #elif ("Connecting " in text):
        #self.setFormat(text.indexOf("Connecting "), text.count(), self.palette.color(QPalette.Active, QPalette.Link))
      #elif ("Disconnecting " in text):
        #self.setFormat(text.indexOf("Disconnecting "), text.count(), self.palette.color(QPalette.Active, QPalette.LinkVisited))

# A2J Syntax Highlighter
class A2JSyntaxH(QSyntaxHighlighter):
    def __init__(self, parent=None):
        super(A2JSyntaxH, self).__init__(parent)

        self.palette = self.parent().palette()

    def highlightBlock(self, text):
      if ("error: " in text):
        self.setFormat(text.indexOf("error: "), text.count(), Qt.red)
      elif ("WARNING: " in text):
        self.setFormat(text.indexOf("WARNING: "), text.count(), Qt.darkRed)
      elif ("----------------------------" in text):
        self.setFormat(text.indexOf("----------------------------"), text.count(), self.palette.color(QPalette.Active, QPalette.Mid))
      elif ("port created: " in text):
        self.setFormat(text.indexOf("port created: "), text.count(), self.palette.color(QPalette.Active, QPalette.Link))
      elif ("port deleted: " in text):
        self.setFormat(text.indexOf("port deleted: "), text.count(), self.palette.color(QPalette.Active, QPalette.LinkVisited))


# Render Window
class LogsW(QDialog, ui_logs.Ui_LogsW):
    def __init__(self, parent=None):
        super(LogsW, self).__init__(parent)
        self.setupUi(self)

        self.b_close.setIcon(getIcon("dialog-close"))
        self.b_purge.setIcon(getIcon("edit-delete"))

        self.timer = QTimer()
        self.timer.setInterval(1000)
        self.timer.start()

        self.jack_syh = JackSyntaxH(self.pte_jack)
        self.jack_syh.setDocument(self.pte_jack.document())

        self.ladish_syh = LadishSyntaxH(self.pte_ladish)
        self.ladish_syh.setDocument(self.pte_ladish.document())

        self.a2j_syh = A2JSyntaxH(self.pte_a2j)
        self.a2j_syh.setDocument(self.pte_a2j.document())

        self.connect(self.timer, SIGNAL("timeout()"), self.updateLogs)
        self.connect(self.b_purge, SIGNAL("clicked()"), self.purgeLogs)

        self.initLogs()
        self.updateLogs()

    def initLogs(self):
        HOME = os.getenv("HOME")

        self.log_jack = {
            'valid': False,
            'file': QFile(os.path.join(HOME, ".log", "jack", "jackdbus.log")),
            'stream': None,
            'last_line': 0
        }

        self.log_ladish = {
            'valid': False,
            'file': QFile(os.path.join(HOME, ".log", "ladish", "ladish.log")),
            'stream': None,
            'last_line': 0
        }

        self.log_a2j = {
            'valid': False,
            'file': QFile(os.path.join(HOME, ".log", "a2j", "a2j.log")),
            'stream': None,
            'last_line': 0
        }

        if (self.log_jack['file'].exists()):
          self.log_jack['valid'] = True
          self.log_jack['file'].open(QIODevice.ReadOnly)
          self.log_jack['stream'] = QTextStream(self.log_jack['file'])
          self.log_jack['stream'].setCodec("UTF-8")

        if (self.log_ladish['file'].exists()):
          self.log_ladish['valid'] = True
          self.log_ladish['file'].open(QIODevice.ReadOnly)
          self.log_ladish['stream'] = QTextStream(self.log_ladish['file'])
          self.log_ladish['stream'].setCodec("UTF-8")

        if (self.log_a2j['file'].exists()):
          self.log_a2j['valid'] = True
          self.log_a2j['file'].open(QIODevice.ReadOnly)
          self.log_a2j['stream'] = QTextStream(self.log_a2j['file'])
          self.log_a2j['stream'].setCodec("UTF-8")

    def purgeLogs(self):
        self.timer.stop()

        if (self.log_jack['valid']):
          self.log_jack['stream'].flush()
          self.pte_jack.clear()
          self.log_jack['file'].close()
          self.log_jack['file'].open(QIODevice.WriteOnly)
          self.log_jack['file'].close()

        if (self.log_ladish['valid']):
          self.log_ladish['stream'].flush()
          self.pte_ladish.clear()
          self.log_ladish['file'].close()
          self.log_ladish['file'].open(QIODevice.WriteOnly)
          self.log_ladish['file'].close()

        if (self.log_a2j['valid']):
          self.log_a2j['stream'].flush()
          self.pte_a2j.clear()
          self.log_a2j['file'].close()
          self.log_a2j['file'].open(QIODevice.WriteOnly)
          self.log_a2j['file'].close()

        self.initLogs()
        self.timer.start()

    def updateLogs(self):
        if (self.log_jack['valid']):
          text = self.log_jack['stream'].readAll().replace("[1m[31m","").replace("[1m[33m","").replace("[0m","")
          text.remove(text.count()-1, text.count())
          if not text.isEmpty():
            self.pte_jack.appendPlainText(text)

        if (self.log_ladish['valid']):
          text = self.log_ladish['stream'].readAll().replace("[31m","").replace("[33m","").replace("[0m","")
          text.remove(text.count()-1, text.count())
          if not text.isEmpty():
            self.pte_ladish.appendPlainText(text)

        if (self.log_a2j['valid']):
          text = self.log_a2j['stream'].readAll().replace("[31m","").replace("[33m","").replace("[0m","")
          text.remove(text.count()-1, text.count())
          if not text.isEmpty():
            self.pte_a2j.appendPlainText(text)

    def closeEvent(self, event):
        self.timer.stop()

        if (self.log_jack['valid']):
          self.log_jack['file'].close()

        if (self.log_ladish['valid']):
          self.log_ladish['file'].close()

        if (self.log_a2j['valid']):
          self.log_a2j['file'].close()

        return QDialog.closeEvent(self, event)


# Allow to use this as a standalone app
if __name__ == '__main__':

    # Additional imports
    import sys
    from PyQt4.QtGui import QApplication

    # App initialization
    app = QApplication(sys.argv)

    # Show GUI
    gui = LogsW()
    gui.show()

    # App-Loop
    sys.exit(app.exec_())

