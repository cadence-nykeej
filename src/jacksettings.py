#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, SIGNAL
from PyQt4.QtGui import QDialog, QDialogButtonBox, QMessageBox

# Imports (Custom Stuff)
import ui_settings_jack

# Properly convert QString to str
def QStringStr(string):
    return str(unicode(string).encode('utf-8'))

global jackctl
jackctl = None

# Connect to DBus
def initBus(bus):
  global jackctl
  try:
    jackctl = dbus.Interface(bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller"), "org.jackaudio.Configure")
    return 0
  except:
    jackctl = None
    return 1

# Helper functions to use outside jacksettings
def getBufferSize():
  return getDriverParameter("period", -1)

def getSampleRate():
  return getDriverParameter("rate", -1)

def isRealtime():
  return getEngineParameter("realtime", False)

def setBufferSize(bsize):
  return setDriverParameter("period", dbus.UInt32(bsize))

def setSampleRate(srate):
  return setDriverParameter("rate", dbus.UInt32(srate))

def engineHasFeature(feature):
    feature_list = jackctl.ReadContainer(["engine"])[1]
    if (dbus.String(feature) in feature_list):
      return True
    else:
      return False

def getEngineParameter(parameter, error=False):
    if (not engineHasFeature(parameter)):
      return error
    else:
      return jackctl.GetParameterValue(["engine",parameter])[2]

def setEngineParameter(parameter, value, optional=True):
    if (not engineHasFeature(parameter)):
      return False
    elif (optional and value != jackctl.GetParameterValue(["engine",parameter])[2]):
      return jackctl.SetParameterValue(["engine",parameter],value)
    else:
      return jackctl.SetParameterValue(["engine",parameter],value)

def driverHasFeature(feature):
    feature_list = jackctl.ReadContainer(["driver"])[1]
    if (dbus.String(feature) in feature_list):
      return True
    else:
      return False

def getDriverParameter(parameter, error=True):
    if (not driverHasFeature(parameter)):
      return error
    else:
      return jackctl.GetParameterValue(["driver",parameter])[2]

def setDriverParameter(parameter, value, optional=True):
    if (not driverHasFeature(parameter)):
      return False
    elif (optional and value != jackctl.GetParameterValue(["driver",parameter])[2]):
      return jackctl.SetParameterValue(["driver",parameter],value)
    else:
      return jackctl.SetParameterValue(["driver",parameter],value)

# Jack Settings Dialog
class JackSettingsW(QDialog, ui_settings_jack.Ui_JackSettingsW):
    def __init__(self, parent=None):
        super(JackSettingsW, self).__init__(parent)
        self.setupUi(self)

        if not jackctl:
          QMessageBox.critical(self, self.tr("Error"), self.tr("jackdbus is not available!\nIs not possible to configure JACK at this point."))
          self.close()
          return

        # Hide non available drivers
        driver_list = jackctl.ReadContainer(["drivers"])[1]
        for i in range(self.obj_server_driver.rowCount()):
          self.obj_server_driver.item(0, i).setTextAlignment(Qt.AlignCenter)
          if (not dbus.String(self.obj_server_driver.item(0, i).text().toLower()) in driver_list):
            self.obj_server_driver.hideRow(i)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL("clicked()"), self.resetJackSettings)
        self.connect(self, SIGNAL("accepted()"), self.saveJackSettings)

        self.connect(self.obj_driver_duplex, SIGNAL("clicked(bool)"), self.checkDuplexSelection)
        self.connect(self.obj_server_driver, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkDriverSelection)

        self.checkEngine()
        self.loadServerSettings()
        self.loadDriverSettings(True)

        # Load selected Jack driver
        self.driver = dbus.String(jackctl.GetParameterValue(["engine","driver"])[2])
        for i in range(self.obj_server_driver.rowCount()):
          if (self.obj_server_driver.item(i, 0).text().toLower() == self.driver):
            self.obj_server_driver.setCurrentCell(i, 0)
            break

    def resetJackSettings(self):
        if (self.tabWidget.currentIndex() == 0):
          self.loadServerSettings(True, True)
        elif (self.tabWidget.currentIndex() == 1):
          self.loadDriverSettings(True, True)

    def saveJackSettings(self):
        self.saveServerSettings()
        self.saveDriverSettings()

    #def loadJackSettings(self):
        #self.loadServerSettings()
        #self.loadDriverSettings()

    def saveServerSettings(self):
        if (self.obj_server_name.isEnabled()):
          value = dbus.String(self.obj_server_name.text())
          setEngineParameter("name", value, True)

        if (self.obj_server_realtime.isEnabled()):
          value = dbus.Boolean(self.obj_server_realtime.isChecked())
          setEngineParameter("realtime", value, True)

        if (self.obj_server_realtime_priority.isEnabled()):
          value = dbus.Int32(self.obj_server_realtime_priority.value())
          setEngineParameter("realtime-priority", value, True)

        if (self.obj_server_temporary.isEnabled()):
          value = dbus.Boolean(self.obj_server_temporary.isChecked())
          setEngineParameter("temporary", value, True)

        if (self.obj_server_verbose.isEnabled()):
          value = dbus.Boolean(self.obj_server_verbose.isChecked())
          setEngineParameter("verbose", value, True)

        if (self.obj_server_client_timeout.isEnabled()):
          value = dbus.Int32(self.obj_server_client_timeout.currentText().toInt()[0])
          setEngineParameter("client-timeout", value, True)

        if (self.obj_server_clock_source.isEnabled()):
          if (self.obj_server_clock_source_cycle.isChecked()):
            value = dbus.Byte("c")
          elif (self.obj_server_clock_source_hpet.isChecked()):
            value = dbus.Byte("h")
          elif (self.obj_server_clock_source_system.isChecked()):
            value = dbus.Byte("s")
          else:
            print "saveServerSettings: Cannot save clock-source value"
            return

          setEngineParameter("clock-source", value, True)

        if (self.obj_server_port_max.isEnabled()):
          value = dbus.UInt32(self.obj_server_port_max.currentText().toInt()[0])
          setEngineParameter("port-max", value, True)

        if (self.obj_server_replace_registry.isEnabled()):
          value = dbus.Boolean(self.obj_server_replace_registry.isChecked())
          setEngineParameter("replace-registry", value, True)

        if (self.obj_server_sync.isEnabled()):
          value = dbus.Boolean(self.obj_server_sync.isChecked())
          setEngineParameter("sync", value, True)

        if (self.obj_server_self_connect_mode.isEnabled()):
          if (self.obj_server_self_connect_mode_0.isChecked()):
            value = dbus.Byte(" ")
          elif (self.obj_server_self_connect_mode_1.isChecked()):
            value = dbus.Byte("E")
          elif (self.obj_server_self_connect_mode_2.isChecked()):
            value = dbus.Byte("e")
          elif (self.obj_server_self_connect_mode_3.isChecked()):
            value = dbus.Byte("A")
          elif (self.obj_server_self_connect_mode_4.isChecked()):
            value = dbus.Byte("a")
          else:
            print "saveServerSettings: Cannot save self-connect-mode value"
            return

          setEngineParameter("self-connect-mode", value, True)

    def loadServerSettings(self, reset=False, force_reset=False):
        settings = jackctl.ReadContainer(["engine"])
        for i in range(len(settings[1])):
          attribute = settings[1][i]
          if (reset):
            value = jackctl.GetParameterValue(["engine",attribute])[1]
            if (force_reset and attribute != "driver"):
              jackctl.ResetParameterValue(["engine",attribute])
          else:
            value = jackctl.GetParameterValue(["engine",attribute])[2]

          if (attribute == "name"):
            self.obj_server_name.setText(QStringStr(value))
          elif (attribute == "realtime"):
            self.obj_server_realtime.setChecked(bool(value))
          elif (attribute == "realtime-priority"):
            self.obj_server_realtime_priority.setValue(int(value))
          elif (attribute == "temporary"):
            self.obj_server_temporary.setChecked(bool(value))
          elif (attribute == "verbose"):
            self.obj_server_verbose.setChecked(bool(value))
          elif (attribute == "client-timeout"):
            self.setComboBoxValue(self.obj_server_client_timeout, str(value))
          elif (attribute == "clock-source"):
            value = str(value)
            if (value == "h"):
              self.obj_server_clock_source_hpet.setChecked(True)
            elif (value == "s"):
              self.obj_server_clock_source_system.setChecked(True)
            elif (value == "c"):
              self.obj_server_clock_source_cycle.setChecked(True)
            else:
              print "loadServerSettings: Invalid clock-source value:", value
          elif (attribute == "port-max"):
            self.setComboBoxValue(self.obj_server_port_max, str(value))
          elif (attribute == "replace-registry"):
            self.obj_server_replace_registry.setChecked(bool(value))
          elif (attribute == "sync"):
            self.obj_server_sync.setChecked(bool(value))
          elif (attribute == "self-connect-mode"):
            value = str(value)
            if (value == " "):
              self.obj_server_self_connect_mode_0.setChecked(True)
            elif (value == "E"):
              self.obj_server_self_connect_mode_1.setChecked(True)
            elif (value == "e"):
              self.obj_server_self_connect_mode_2.setChecked(True)
            elif (value == "A"):
              self.obj_server_self_connect_mode_3.setChecked(True)
            elif (value == "a"):
              self.obj_server_self_connect_mode_4.setChecked(True)
            else:
              print "loadServerSettings: Invalid self-connect-mode value:", value
          elif (attribute == "driver"):
            pass
          else:
            print "loadServerSettings: Got a non implemented server attribute:", attribute

    def checkEngine(self):
        # Enable widgets according to available engine options
        self.obj_server_name.setEnabled(engineHasFeature("name"))
        self.obj_server_realtime.setEnabled(engineHasFeature("realtime"))
        self.obj_server_realtime_priority.setEnabled(engineHasFeature("realtime-priority"))
        self.obj_server_temporary.setEnabled(engineHasFeature("temporary"))
        self.obj_server_verbose.setEnabled(engineHasFeature("verbose"))
        self.obj_server_client_timeout.setEnabled(engineHasFeature("client-timeout"))
        self.obj_server_clock_source.setEnabled(engineHasFeature("clock-source") and False) #FIXME - broken in JACK
        self.obj_server_port_max.setEnabled(engineHasFeature("port-max"))
        self.obj_server_replace_registry.setEnabled(engineHasFeature("replace-registry"))
        self.obj_server_sync.setEnabled(engineHasFeature("sync"))
        self.obj_server_self_connect_mode.setEnabled(engineHasFeature("self-connect-mode"))

    def saveDriverSettings(self):
        if (self.obj_driver_device.isEnabled()):
          value = dbus.String(self.obj_driver_device.currentText().split(" ")[0])
          if (value != jackctl.GetParameterValue(["driver","device"])[2]):
            jackctl.SetParameterValue(["driver","device"],value)

        if (self.obj_driver_capture.isEnabled()):
          if (self.driver == "alsa"):
            value = dbus.String(self.obj_driver_capture.currentText().split(" ")[0])
          elif (self.driver == "dummy"):
            value = dbus.UInt32(self.obj_driver_capture.currentText().toInt()[0])
          elif (self.driver == "firewire"):
            value = dbus.Boolean(self.obj_driver_capture.currentIndex() == 1)
          else:
            print "saveDriverSettings: Cannot save capture value"
            return

          setDriverParameter("capture", value, True)

        if (self.obj_driver_playback.isEnabled()):
          if (self.driver == "alsa"):
            value = dbus.String(self.obj_driver_playback.currentText().split(" ")[0])
          elif (self.driver == "dummy"):
            value = dbus.UInt32(self.obj_driver_capture.currentText().toInt()[0])
          elif (self.driver == "firewire"):
            value = dbus.Boolean(self.obj_driver_playback.currentIndex() == 1)
          else:
            print "saveDriverSettings: Cannot save playback value"
            return

          setDriverParameter("playback", value, True)

        if (self.obj_driver_rate.isEnabled()):
          value = dbus.UInt32(self.obj_driver_rate.currentText().toInt()[0])
          setDriverParameter("rate", value, True)

        if (self.obj_driver_period.isEnabled()):
          value = dbus.UInt32(self.obj_driver_period.currentText().toInt()[0])
          setDriverParameter("period", value, True)

        if (self.obj_driver_nperiods.isEnabled()):
          value = dbus.UInt32(self.obj_driver_nperiods.value())
          setDriverParameter("nperiods", value, True)

        if (self.obj_driver_hwmon.isEnabled()):
          value = dbus.Boolean(self.obj_driver_hwmon.isChecked())
          setDriverParameter("hwmon", value, True)

        if (self.obj_driver_hwmeter.isEnabled()):
          value = dbus.Boolean(self.obj_driver_hwmeter.isChecked())
          setDriverParameter("hwmeter", value, True)

        if (self.obj_driver_duplex.isEnabled()):
          value = dbus.Boolean(self.obj_driver_duplex.isChecked())
          setDriverParameter("duplex", value, True)

        if (self.obj_driver_softmode.isEnabled()):
          value = dbus.Boolean(self.obj_driver_softmode.isChecked())
          setDriverParameter("softmode", value, True)

        if (self.obj_driver_monitor.isEnabled()):
          value = dbus.Boolean(self.obj_driver_monitor.isChecked())
          setDriverParameter("monitor", value, True)

        if (self.obj_driver_dither.isEnabled()):
          if (self.obj_driver_dither.currentIndex() == 0):
            value = dbus.Byte("n")
          elif (self.obj_driver_dither.currentIndex() == 1):
            value = dbus.Byte("r")
          elif (self.obj_driver_dither.currentIndex() == 2):
            value = dbus.Byte("s")
          elif (self.obj_driver_dither.currentIndex() == 3):
            value = dbus.Byte("t")
          else:
            print "saveDriverSettings: Cannot save dither value"
            return
          setDriverParameter("dither", value, True)

        if (self.obj_driver_inchannels.isEnabled()):
          value = dbus.UInt32(self.obj_driver_inchannels.value())
          setDriverParameter("inchannels", value, True)

        if (self.obj_driver_outchannels.isEnabled()):
          value = dbus.UInt32(self.obj_driver_outchannels.value())
          setDriverParameter("outchannels", value, True)

        if (self.obj_driver_shorts.isEnabled()):
          value = dbus.Boolean(self.obj_driver_shorts.isChecked())
          setDriverParameter("shorts", value, True)

        if (self.obj_driver_input_latency.isEnabled()):
          value = dbus.UInt32(self.obj_driver_input_latency.value())
          setDriverParameter("input-latency", value, True)

        if (self.obj_driver_output_latency.isEnabled()):
          value = dbus.UInt32(self.obj_driver_output_latency.value())
          setDriverParameter("output-latency", value, True)

        if (self.obj_driver_midi_driver.isEnabled()):
          if (self.obj_driver_midi_driver.currentIndex() == 0):
            value = dbus.String("none")
          elif (self.obj_driver_midi_driver.currentIndex() == 1):
            value = dbus.String("seq")
          elif (self.obj_driver_midi_driver.currentIndex() == 2):
            value = dbus.String("raw")
          else:
            print "saveDriverSettings: Cannot save midi-driver value"
            return
          setDriverParameter("midi-driver", value, True)

        if (self.obj_driver_wait.isEnabled()):
          value = dbus.UInt32(self.obj_driver_wait.value())
          setDriverParameter("wait", value, True)

        if (self.obj_driver_verbose.isEnabled()):
          value = dbus.UInt32(self.obj_driver_verbose.value())
          setDriverParameter("verbose", value, True)

        if (self.obj_driver_snoop.isEnabled()):
          value = dbus.Boolean(self.obj_driver_snoop.isChecked())
          setDriverParameter("snoop", value, True)

        if (self.obj_driver_channels.isEnabled()):
          value = dbus.UInt32(self.obj_driver_channels.value())
          setDriverParameter("channels", value, True)

    def loadDriverSettings(self, reset=False, force_reset=False):
        settings = jackctl.ReadContainer(["driver"])
        for i in range(len(settings[1])):
          attribute = settings[1][i]
          if (reset):
            value = jackctl.GetParameterValue(["driver",attribute])[1]
            if (force_reset):
              jackctl.ResetParameterValue(["driver",attribute])
          else:
            value = jackctl.GetParameterValue(["driver",attribute])[2]

          if (attribute == "device"):
            self.setComboBoxValue(self.obj_driver_device, QStringStr(value), True)
          elif (attribute == "capture"):
            if (self.driver == "firewire"):
              self.obj_driver_capture.setCurrentIndex(1 if bool(value) else 0)
            else:
              self.setComboBoxValue(self.obj_driver_capture, QStringStr(value), True)
          elif (attribute == "playback"):
            if (self.driver == "firewire"):
              self.obj_driver_playback.setCurrentIndex(1 if bool(value) else 0)
            else:
              self.setComboBoxValue(self.obj_driver_playback, QStringStr(value), True)
          elif (attribute == "rate"):
            self.setComboBoxValue(self.obj_driver_rate, str(value))
          elif (attribute == "period"):
            self.setComboBoxValue(self.obj_driver_period, str(value))
          elif (attribute == "nperiods"):
            self.obj_driver_nperiods.setValue(int(value))
          elif (attribute == "hwmon"):
            self.obj_driver_hwmon.setChecked(bool(value))
          elif (attribute == "hwmeter"):
            self.obj_driver_hwmeter.setChecked(bool(value))
          elif (attribute == "duplex"):
            self.obj_driver_duplex.setChecked(bool(value))
          elif (attribute == "softmode"):
            self.obj_driver_softmode.setChecked(bool(value))
          elif (attribute == "monitor"):
            self.obj_driver_monitor.setChecked(bool(value))
          elif (attribute == "dither"):
            value = str(value)
            if (value == "n"):
              self.obj_driver_dither.setCurrentIndex(0)
            elif (value == "r"):
              self.obj_driver_dither.setCurrentIndex(1)
            elif (value == "s"):
              self.obj_driver_dither.setCurrentIndex(2)
            elif (value == "t"):
              self.obj_driver_dither.setCurrentIndex(3)
            else:
              print "loadDriverSettings: Invalid dither value"
          elif (attribute == "inchannels"):
            self.obj_driver_inchannels.setValue(int(value))
          elif (attribute == "outchannels"):
            self.obj_driver_outchannels.setValue(int(value))
          elif (attribute == "shorts"):
            self.obj_driver_shorts.setChecked(bool(value))
          elif (attribute == "input-latency"):
            self.obj_driver_input_latency.setValue(int(value))
          elif (attribute == "output-latency"):
            self.obj_driver_output_latency.setValue(int(value))
          elif (attribute == "midi-driver"):
            value = str(value)
            if (value == "none"):
              self.obj_driver_midi_driver.setCurrentIndex(0)
            elif (value == "seq"):
              self.obj_driver_midi_driver.setCurrentIndex(1)
            elif (value == "raw"):
              self.obj_driver_midi_driver.setCurrentIndex(2)
            else:
              print "loadDriverSettings: Invalid midi-driver value"
          elif (attribute == "wait"):
            self.obj_driver_wait.setValue(int(value))
          elif (attribute == "verbose"):
            self.obj_driver_verbose.setValue(int(value))
          elif (attribute == "snoop"):
            self.obj_driver_snoop.setChecked(bool(value))
          elif (attribute == "channels"):
            self.obj_driver_channels.setValue(int(value))
          else:
            print "loadDriverSettings: Got a non implemented driver attribute:", attribute

    def checkDriverSelection(self, row, column, prev_row, prev_column):
        # Save previous settings
        self.saveDriverSettings()

        # Set new Jack driver
        self.driver = dbus.String(self.obj_server_driver.item(row, 0).text().toLower())
        jackctl.SetParameterValue(["engine","driver"],self.driver)

        # Add device list
        self.obj_driver_device.clear()
        if (driverHasFeature("device")):
          dev_list = jackctl.GetParameterConstraint(["driver","device"])[3]
          for i in range(len(dev_list)):
            self.obj_driver_device.addItem(dev_list[i][0]+" ["+dev_list[i][1]+"]")

        # Custom 'playback' and 'capture' values
        self.obj_driver_capture.clear()
        self.obj_driver_playback.clear()
        if (self.driver == "alsa"):
          self.obj_driver_capture.addItem("none")
          self.obj_driver_playback.addItem("none")
          dev_list = jackctl.GetParameterConstraint(["driver","device"])[3]
          for i in range(len(dev_list)):
            self.obj_driver_capture.addItem(dev_list[i][0]+" ["+dev_list[i][1]+"]")
            self.obj_driver_playback.addItem(dev_list[i][0]+" ["+dev_list[i][1]+"]")
        elif (self.driver == "dummy"):
          for i in range(16):
            self.obj_driver_capture.addItem("%i" % ((i*2)+2))
            self.obj_driver_playback.addItem("%i" % ((i*2)+2))
        elif (self.driver == "firewire"):
          self.obj_driver_capture.addItem("no")
          self.obj_driver_capture.addItem("yes")
          self.obj_driver_playback.addItem("no")
          self.obj_driver_playback.addItem("yes")
        else:
          print "checkDriverSelection: Custom playback/capture for driver", self.driver, "not implemented yet"

        # Load Driver Settings
        self.loadDriverSettings()

        # Enable widgets according to driver
        self.obj_driver_capture.setEnabled(driverHasFeature("capture"))
        self.obj_driver_capture_label.setEnabled(driverHasFeature("capture"))
        self.obj_driver_playback.setEnabled(driverHasFeature("playback"))
        self.obj_driver_playback_label.setEnabled(driverHasFeature("playback"))
        self.obj_driver_device.setEnabled(driverHasFeature("device"))
        self.obj_driver_device_label.setEnabled(driverHasFeature("device"))
        self.obj_driver_rate.setEnabled(driverHasFeature("rate"))
        self.obj_driver_rate_label.setEnabled(driverHasFeature("rate"))
        self.obj_driver_period.setEnabled(driverHasFeature("period"))
        self.obj_driver_period_label.setEnabled(driverHasFeature("period"))
        self.obj_driver_nperiods.setEnabled(driverHasFeature("nperiods"))
        self.obj_driver_nperiods_label.setEnabled(driverHasFeature("nperiods"))
        self.obj_driver_hwmon.setEnabled(driverHasFeature("hwmon"))
        self.obj_driver_hwmeter.setEnabled(driverHasFeature("hwmeter"))
        self.obj_driver_duplex.setEnabled(driverHasFeature("duplex"))
        self.obj_driver_softmode.setEnabled(driverHasFeature("softmode"))
        self.obj_driver_monitor.setEnabled(driverHasFeature("monitor"))
        self.obj_driver_dither.setEnabled(driverHasFeature("dither"))
        self.obj_driver_dither_label.setEnabled(driverHasFeature("dither"))
        self.obj_driver_inchannels.setEnabled(driverHasFeature("inchannels"))
        self.obj_driver_inchannels_label.setEnabled(driverHasFeature("inchannels"))
        self.obj_driver_outchannels.setEnabled(driverHasFeature("outchannels"))
        self.obj_driver_outchannels_label.setEnabled(driverHasFeature("outchannels"))
        self.obj_driver_shorts.setEnabled(driverHasFeature("shorts"))
        self.obj_driver_input_latency.setEnabled(driverHasFeature("input-latency"))
        self.obj_driver_input_latency_label.setEnabled(driverHasFeature("input-latency"))
        self.obj_driver_output_latency.setEnabled(driverHasFeature("output-latency"))
        self.obj_driver_output_latency_label.setEnabled(driverHasFeature("output-latency"))
        self.obj_driver_midi_driver.setEnabled(driverHasFeature("midi-driver"))
        self.obj_driver_midi_driver_label.setEnabled(driverHasFeature("midi-driver"))
        self.obj_driver_wait.setEnabled(driverHasFeature("wait"))
        self.obj_driver_wait_label.setEnabled(driverHasFeature("wait"))
        self.obj_driver_verbose.setEnabled(driverHasFeature("verbose"))
        self.obj_driver_verbose_label.setEnabled(driverHasFeature("verbose"))
        self.obj_driver_snoop.setEnabled(driverHasFeature("snoop"))
        self.obj_driver_channels.setEnabled(driverHasFeature("channels"))
        self.obj_driver_channels_label.setEnabled(driverHasFeature("channels"))

        # Misc stuff
        if (self.obj_server_driver.item(0, row).text() == "ALSA"):
          self.toolbox_driver_misc.setCurrentIndex(1)
          self.obj_driver_capture_label.setText(self.tr("Input Device:"))
          self.obj_driver_playback_label.setText(self.tr("Output Device:"))

        elif (self.obj_server_driver.item(0, row).text() == "Dummy"):
          self.toolbox_driver_misc.setCurrentIndex(2)
          self.obj_driver_capture_label.setText(self.tr("Input Ports:"))
          self.obj_driver_playback_label.setText(self.tr("Output Ports:"))

        elif (self.obj_server_driver.item(0, row).text() == "FireWire"):
          self.toolbox_driver_misc.setCurrentIndex(3)
          self.obj_driver_capture_label.setText(self.tr("Capture Ports:"))
          self.obj_driver_playback_label.setText(self.tr("Playback Ports:"))

        elif (self.obj_server_driver.item(0, row).text() == "Loopback"):
          self.toolbox_driver_misc.setCurrentIndex(4)

        else:
          self.toolbox_driver_misc.setCurrentIndex(0)

        self.checkDuplexSelection(self.obj_driver_duplex.isChecked())

    def checkDuplexSelection(self, active):
        if (driverHasFeature("duplex")):
          self.obj_driver_capture.setEnabled(active)
          self.obj_driver_capture_label.setEnabled(active)
          self.obj_driver_inchannels.setEnabled(active)
          self.obj_driver_inchannels_label.setEnabled(active)
          self.obj_driver_input_latency.setEnabled(active)
          self.obj_driver_input_latency_label.setEnabled(active)

    def setComboBoxValue(self, box, text, split=False):
        for i in range(box.count()):
          if (box.itemText(i) == text or (box.itemText(i).split(" ")[0] == text and split)):
            box.setCurrentIndex(i)
            break
        else:
          if (text):
            box.addItem(text)
            box.setCurrentIndex(box.count()-1)


# Allow to use this as a standalone app
if __name__ == '__main__':

    # Additional imports
    import sys
    from PyQt4.QtGui import QApplication

    try:
      import dbus
    except:
      dbus = None

    # App initialization
    app = QApplication(sys.argv)

    # Connect to DBus
    if (dbus):
      initBus(dbus.SessionBus())

    # Show GUI
    gui = JackSettingsW()
    gui.show()

    # App-Loop
    sys.exit(app.exec_())

