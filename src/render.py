#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from time import sleep
from PyQt4.QtCore import QProcess, QString, QTime, QTimer, SIGNAL
from PyQt4.QtGui import QDialog, QIcon, QFontMetrics, QMessageBox

# Imports (Custom Stuff)
import icons_rc, jacklib, ui_render

# Properly convert QString to str
def QStringStr(string):
    return str(unicode(string).encode('utf-8'))

# Get Icon from user theme, using our own as backup (Oxygen)
def getIcon(icon, size=16):
    return QIcon.fromTheme(icon, QIcon(":/%ix%i/.png" % (size,size)))

# Render Window
class RenderW(QDialog, ui_render.Ui_RenderW):
    def __init__(self, parent=None, client=None):
        super(RenderW, self).__init__(parent)
        self.setupUi(self)

        global jack_client
        jack_client = client

        self.process = QProcess(self)
        self.timer = QTimer()

        self.b_render.setIcon(getIcon("media-record"))
        self.b_stop.setIcon(getIcon("media-playback-stop"))
        self.b_close.setIcon(getIcon("dialog-close"))
        self.b_stop.setVisible(False)

        self.b_low.setMaximumWidth(QFontMetrics(self.b_low.font()).width(self.tr("Low"))+9)
        self.b_high.setMaximumWidth(QFontMetrics(self.b_high.font()).width(self.tr("High"))+9)

        self.b_now_start.setMaximumWidth(QFontMetrics(self.b_now_start.font()).width(self.tr("now"))+9)
        self.b_now_end.setMaximumWidth(QFontMetrics(self.b_now_end.font()).width(self.tr("now"))+9)

        buffer_size = str(jacklib.get_buffer_size(jack_client))
        for i in range(self.cb_buffer_size.count()):
          if (QStringStr(self.cb_buffer_size.itemText(i)) == buffer_size):
            self.cb_buffer_size.setCurrentIndex(i)

        self.sample_rate = jacklib.get_sample_rate(jack_client)

        # Get List of formats
        self.process.start("jack_capture", ["-pf"])
        self.process.waitForFinished()

        formats = QString(self.process.readAllStandardOutput()).split(" ")
        for i in range(len(formats)-1):
          self.cb_format.addItem(formats[i])
          if (formats[i] == "wav"):
            self.cb_format.setCurrentIndex(i)

        self.cb_depth.setCurrentIndex(4) #Float
        self.rb_stereo.setChecked(True)

        self.max_time = 180
        self.te_end.setTime(QTime(0, 3, 0))
        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.progressBar.setValue(0)

        self.connect(self.b_render, SIGNAL("clicked()"), self.render_start)
        self.connect(self.b_stop, SIGNAL("clicked()"), self.render_stop)
        self.connect(self.b_now_start, SIGNAL("clicked()"), self.setStartNow)
        self.connect(self.b_now_end, SIGNAL("clicked()"), self.setEndNow)
        self.connect(self.te_start, SIGNAL("timeChanged(const QTime)"), self.updateStartTime)
        self.connect(self.te_end, SIGNAL("timeChanged(const QTime)"), self.updateEndTime)
        self.connect(self.timer, SIGNAL("timeout()"), self.updateProgressbar)

    def render_start(self):
        global jack_client

        self.group_render.setEnabled(False)
        self.group_time.setEnabled(False)
        self.group_encoding.setEnabled(False)
        self.b_render.setVisible(False)
        self.b_stop.setVisible(True)
        self.b_close.setEnabled(False)

        freewheel = bool(self.cb_render_mode.currentIndex() == 1)
        buffer_size = int(self.cb_buffer_size.currentText())

        time_start = self.te_start.time()
        time_end   = self.te_end.time()
        MinTime = (time_start.hour()*3600)+(time_start.minute()*60)+(time_start.second())
        MaxTime = (time_end.hour()*3600)+(time_end.minute()*60)+(time_end.second())
        self.max_time = MaxTime

        self.progressBar.setMinimum(MinTime)
        self.progressBar.setMaximum(MaxTime)
        self.progressBar.setValue(MinTime)
        self.progressBar.update()

        if (freewheel):
          self.timer.setInterval(100)
        else:
          self.timer.setInterval(500)

        arguments = []

        # Bit depth
        arguments.append("-b")
        arguments.append(QStringStr(self.cb_depth.currentText().toUpper()))

        # Channels
        arguments.append("-c")
        if (self.rb_mono.isChecked()):
          arguments.append("1")
        elif (self.rb_stereo.isChecked()):
          arguments.append("2")
        elif (self.rb_outro.isChecked()):
          arguments.append(QStringStr(self.sb_channels.value()))
        else:
          arguments.append("2")

        # Format
        arguments.append("-f")
        arguments.append(QStringStr(self.cb_format.currentText()))

        # TODO - Preview
        #if (self.cb_preview.isChecked()):
          #arguments.append("-mb")
          #arguments.append("-mt")
          #arguments.append("dpm")

        # Controlled by transport
        arguments.append("-jt")

        # Silent mode
        arguments.append("-dc")
        arguments.append("-s")

        if (buffer_size != jacklib.get_buffer_size(jack_client)):
          jacklib.set_buffer_size(jack_client, buffer_size)
          print "NOTICE: buffer size changed before render"

        if (jacklib.transport_query(jack_client, None) > 0): #Rolling
          jacklib.transport_stop(jack_client)
        jacklib.transport_locate(jack_client, MinTime*self.sample_rate)

        self.process.start("jack_capture", arguments)
        self.process.waitForStarted()

        if (freewheel):
          sleep(1)

        if (freewheel):
          jacklib.set_freewheel(jack_client, 1)
          print "NOTICE: rendering in freewheel mode"

        self.timer.start()
        jacklib.transport_start(jack_client)

    def render_stop(self):
        global jack_client

        jacklib.transport_stop(jack_client)

        if (self.cb_render_mode.currentIndex() == 1): #Freewheel
          jacklib.set_freewheel(jack_client, 0)

        sleep(1)

        self.process.close()
        self.timer.stop()

        self.group_render.setEnabled(True)
        self.group_time.setEnabled(True)
        self.group_encoding.setEnabled(True)
        self.b_render.setVisible(True)
        self.b_stop.setVisible(False)
        self.b_close.setEnabled(True)

        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.progressBar.setValue(0)
        self.progressBar.update()

    def setStartNow(self):
        global jack_client

        time = jacklib.get_current_transport_frame(jack_client)/self.sample_rate
        secs = time % 60
        mins = (time / 60) % 60
        hrs  = (time / 3600) % 60

        self.te_start.setTime(QTime(hrs, mins, secs))

    def setEndNow(self):
        global jack_client

        time = jacklib.get_current_transport_frame(jack_client)/self.sample_rate
        secs = time % 60
        mins = (time / 60) % 60
        hrs  = (time / 3600) % 60

        self.te_end.setTime(QTime(hrs, mins, secs))

    def updateProgressbar(self):
        global jack_client

        time = jacklib.get_current_transport_frame(jack_client)/self.sample_rate
        self.progressBar.setValue(time)

        if (time > self.max_time):
          self.func_stop()

    def updateStartTime(self, time):
        if (time >= self.te_end.time()):
          self.te_end.setTime(time)
          self.b_render.setEnabled(False)
        else:
          self.b_render.setEnabled(True)

    def updateEndTime(self, time):
        if (time <= self.te_start.time()):
          time = self.te_start.setTime(time)
          self.b_render.setEnabled(False)
        else:
          self.b_render.setEnabled(True)


# Allow to use this as a standalone app
if __name__ == '__main__':

    # Additional imports
    import sys
    from PyQt4.QtGui import QApplication

    # App initialization
    app = QApplication(sys.argv)

    jack_client = jacklib.client_open("Render", jacklib.NullOption, 0)
    #jacklib.activate(jack_client)

    # Show GUI
    gui = RenderW(None, jack_client)
    gui.show()

    # App-Loop
    ret = app.exec_()

    if (jack_client):
      #jacklib.deactivate(jack_client)
      jacklib.client_close(jack_client)

    # Exit properly
    sys.exit(ret)

