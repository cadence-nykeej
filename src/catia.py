#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import os, signal, sys
from PyQt4.QtCore import Qt, QSettings, QString, QTimer, QVariant, SIGNAL, SLOT
from PyQt4.QtGui import QApplication, QDialog, QDialogButtonBox, QFileDialog, QFontMetrics
from PyQt4.QtGui import QGraphicsView, QIcon, QMainWindow, QMessageBox, QPainter

# Imports (Plugins and Resources)
import icons_rc, jacklib, patchcanvas
import ui_catia, ui_settings_app
from shared import *

# Imports (DBus)
try:
  import dbus, jacksettings
  from dbus.mainloop.qt import DBusQtMainLoop
  haveDBus = True
except:
  haveDBus = False

if (haveDBus):
  DBus.loop = DBusQtMainLoop(set_as_default=True)
  DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)

  try:
    DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
    jacksettings.initBus(DBus.bus)
  except:
    DBus.jack = None

  try:
    DBus.a2j = dbus.Interface(DBus.bus.get_object("org.gna.home.a2jmidid", "/"), "org.gna.home.a2jmidid.control")
    a2j_client_name = DBus.a2j.get_jack_client_name()
  except:
    DBus.a2j = None
    a2j_client_name = None

else:
  DBus.a2j = None
  DBus.jack = None
  a2j_client_name = None


# Configure Claudia Dialog
class CatiaSettingsW(QDialog, ui_settings_app.Ui_SettingsW):
    def __init__(self, parent=None):
        super(CatiaSettingsW, self).__init__(parent)
        self.setupUi(self)

        self.loadSettings()

        self.lw_page.hideRow(3)
        self.lw_page.hideRow(4)
        self.lw_page.setCurrentCell(0, 0)
        #self.lw_page.item(0, 0).setIcon(QIcon(":/48x48/catia.png"))

        self.group_main_paths.setVisible(False)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL("clicked()"), self.resetSettings)
        self.connect(self, SIGNAL("accepted()"), self.saveSettings)

        self.connect(self.b_main_def_folder_open, SIGNAL("clicked()"), lambda parent=self, current_path=self.le_main_def_folder.text(),
                                                       lineEdit=self.le_main_def_folder: getAndSetPath(parent, current_path, lineEdit))

    def saveSettings(self):
        settings = QSettings()
        #settings.setValue(QString("Main/DefaultProjectFolder"), self.le_main_def_folder.text())
        settings.setValue(QString("Main/RefreshInterval"), self.sb_gui_refresh.value())
        settings.setValue(QString("Canvas/Theme"), self.cb_canvas_theme.currentText())
        settings.setValue(QString("Canvas/BezierLines"), self.cb_canvas_bezier_lines.isChecked())
        settings.setValue(QString("Canvas/AutoHideGroups"), self.cb_canvas_hide_groups.isChecked())
        settings.setValue(QString("Canvas/FancyEyeCandy"), self.cb_canvas_eyecandy.isChecked())
        settings.setValue(QString("Canvas/Antialiasing"), self.cb_canvas_render_aa.checkState())
        settings.setValue(QString("Canvas/TextAntialiasing"), self.cb_canvas_render_text_aa.isChecked())

        if (haveDBus and DBus.jack):
          settings.setValue(QString("Jack/AutoStartJack"), self.cb_jack_jack.isChecked())

        if (haveDBus and DBus.a2j):
          settings.setValue(QString("Jack/AutoStartA2J"), self.cb_jack_a2j.isChecked())

        #if (havePulseJack):
          #settings.setValue(QString("Jack/AutoStartPulse"), self.cb_jack_pulse.isChecked())
          #settings.setValue(QString("Jack/PulsePlayOnly"), self.cb_jack_pulse_play.isChecked())

    def loadSettings(self):
        settings = QSettings()
        #self.le_main_def_folder.setText(settings.value("Main/DefaultProjectFolder", os.path.join(os.getenv("HOME"),"jack-projects")).toString())
        self.sb_gui_refresh.setValue(settings.value("Main/RefreshInterval", 100).toInt()[0])
        self.cb_canvas_bezier_lines.setChecked(settings.value("Canvas/BezierLines", True).toBool())
        self.cb_canvas_hide_groups.setChecked(settings.value("Canvas/AutoHideGroups", True).toBool())
        self.cb_canvas_eyecandy.setChecked(settings.value("Canvas/FancyEyeCandy", False).toBool())
        self.cb_canvas_render_aa.setCheckState(settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0])
        self.cb_canvas_render_text_aa.setChecked(settings.value("Canvas/TextAntialiasing", True).toBool())

        if (haveDBus and DBus.jack):
          self.cb_jack_jack.setChecked(settings.value("Jack/AutoStartJack", True).toBool())
        else:
          self.cb_jack_jack.setChecked(True)
          self.cb_jack_jack.setEnabled(False)

        if (haveDBus and DBus.a2j):
          self.cb_jack_a2j.setChecked(settings.value("Jack/AutoStartA2J", True).toBool())
        else:
          self.cb_jack_a2j.setEnabled(False)

        #if (havePulseJack):
          #self.cb_jack_pulse.setChecked(settings.value("Jack/AutoStartPulse", False).toBool())
          #self.cb_jack_pulse_play.setChecked(settings.value("Jack/PulsePlayOnly", False).toBool())
        #else:
          #self.cb_jack_pulse.setEnabled(False)
          #self.cb_jack_pulse_play.setEnabled(False)

        theme_name = settings.value("Canvas/Theme", patchcanvas.getDefaultThemeName()).toString()

        for i in range(len(patchcanvas.theme_list)):
          self.cb_canvas_theme.addItem(patchcanvas.theme_list[i]['name'])
          if (patchcanvas.theme_list[i]['name'] == theme_name):
            self.cb_canvas_theme.setCurrentIndex(i)

    def resetSettings(self):
        #self.le_main_def_folder.setText(os.path.join(os.getenv("HOME"),"jack-projects"))
        self.sb_gui_refresh.setValue(100)
        self.cb_canvas_theme.setCurrentIndex(0)
        self.cb_canvas_bezier_lines.setChecked(True)
        self.cb_canvas_hide_groups.setChecked(True)
        self.cb_canvas_eyecandy.setChecked(False)
        self.cb_canvas_render_aa.setCheckState(Qt.PartiallyChecked)
        self.cb_canvas_render_text_aa.setChecked(True)

        if (haveDBus and DBus.jack):
          self.cb_jack_jack.setChecked(True)

        if (haveDBus and DBus.a2j):
          self.cb_jack_a2j.setChecked(True)

        #if (havePulseJack):
          #self.cb_jack_pulse.setChecked(False)
          #self.cb_jack_pulse_play.setChecked(False)

# Main Window
class CatiaMainW(QMainWindow, ui_catia.Ui_CatiaMainW):
    def __init__(self, parent=None):
        super(CatiaMainW, self).__init__(parent)
        self.setupUi(self)

        # Load App Settings
        self.loadSettings()
        Init(self, app)

        # Timers
        self.timer100 = QTimer()
        self.timer100.setInterval(self.saved_settings["Main/RefreshInterval"])
        self.timer500 = QTimer()
        self.timer500.setInterval(self.saved_settings["Main/RefreshInterval"]*5)

        # Icons
        setIcons(self, ["canvas", "jack", "transport"])

        self.act_tools_jack_start.setIcon(getIcon("media-playback-start"))
        self.act_tools_jack_stop.setIcon(getIcon("media-playback-stop"))
        self.act_tools_a2j_start.setIcon(getIcon("media-playback-start"))
        self.act_tools_a2j_stop.setIcon(getIcon("media-playback-stop"))
        self.act_tools_pulse_jack_start.setIcon(getIcon("media-playback-start"))
        self.act_tools_pulse_jack_stop.setIcon(getIcon("media-playback-stop"))

        # Patchbay
        self.group_list = []
        self.group_split_list = []
        self.connection_list = []
        self.last_group_id = 1
        self.last_connection_id = 1

        self.scene = patchcanvas.MyGraphicsScene(self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setRenderHint(QPainter.Antialiasing, True if (self.saved_settings["Canvas/Antialiasing"] == Qt.Checked) else False)
        self.graphicsView.setRenderHint(QPainter.TextAntialiasing, self.saved_settings["Canvas/TextAntialiasing"])
        self.graphicsView.setOptimizationFlag(QGraphicsView.DontSavePainterState, True)
        self.graphicsView.setOptimizationFlag(QGraphicsView.DontAdjustForAntialiasing, not bool(self.saved_settings["Canvas/Antialiasing"]))

        # Create Canvas
        patchcanvas.options['theme_name'] = unicode(self.saved_settings["Canvas/Theme"])
        patchcanvas.options['bezier_lines'] = self.saved_settings["Canvas/BezierLines"]
        patchcanvas.options['antialiasing'] = self.saved_settings["Canvas/Antialiasing"]
        patchcanvas.options['auto_hide_groups'] = self.saved_settings["Canvas/AutoHideGroups"]
        patchcanvas.options['connect_midi2outro'] = True
        patchcanvas.options['fancy_eyecandy'] = self.saved_settings["Canvas/FancyEyeCandy"]

        patchcanvas.features['group_rename'] = False
        patchcanvas.features['port_rename'] = False
        patchcanvas.features['handle_group_pos'] = True

        patchcanvas.init(self.scene, self.canvas_callback)

        # DBus Stuff
        if (haveDBus):
          if (DBus.jack and DBus.jack.IsStarted()):
            self.jackStarted()
          elif (True): #self.saved_settings["Jack/AutoStartJack"]):
            if (self.JackServerStart()):
              self.jackStarted()
            else:
              self.jackStopped()
          else:
            self.jackStopped()

          if (DBus.a2j and DBus.a2j.is_started()):
            self.a2jStarted()
          else:
            self.a2jStopped()

        else: #No DBus
          self.act_tools_jack_start.setEnabled(False)
          self.act_tools_jack_stop.setEnabled(False)
          self.act_tools_configure_jack.setEnabled(False)
          self.b_tools_configure_jack.setEnabled(False)
          self.menu_A2J_Bridge.setEnabled(False)
          self.cb_sample_rate.setEnabled(False)

          self.jackStarted(True)

        #self.menu_PulseJack_Bridge.setEnabled(havePulseJack)
        self.act_jack_render.setEnabled(canRender)
        self.b_tools_render.setEnabled(canRender)

        if (haveDBus and (DBus.jack or DBus.a2j)):
          DBus.bus.add_signal_receiver(self.DBusSignalReceiver, sender_keyword='sender', destination_keyword='dest', interface_keyword='interface',
                        member_keyword='member', path_keyword='path')

        setConnections(self, ["canvas", "jack", "transport"])

        self.connect(self.act_tools_jack_start, SIGNAL("triggered()"), self.JackServerStart)
        self.connect(self.act_tools_jack_stop, SIGNAL("triggered()"), self.JackServerStop)
        self.connect(self.act_tools_a2j_start, SIGNAL("triggered()"), self.A2JBridgeStart)
        self.connect(self.act_tools_a2j_stop, SIGNAL("triggered()"), self.A2JBridgeStop)
        self.connect(self.act_tools_a2j_export_hw, SIGNAL("triggered()"), self.A2JBridgeExportHW)
        self.connect(self.act_tools_pulse_jack_start, SIGNAL("triggered()"), self.PulseJackBridgeStart)
        self.connect(self.act_tools_pulse_jack_stop, SIGNAL("triggered()"), self.PulseJackBridgeStop)
        self.connect(self.act_tools_clear_xruns, SIGNAL("triggered()"), self.func_clear_xruns)
        self.connect(self.b_tools_clear_xruns, SIGNAL("clicked()"), self.func_clear_xruns)
        self.connect(self.b_xruns, SIGNAL("clicked()"), self.func_clear_xruns)

        self.connect(self.timer500, SIGNAL("timeout()"), lambda: refreshDSPLoad(self))
        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshTransport(self))

        self.connect(self.act_settings_configure, SIGNAL("triggered()"), self.configureCatia)

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutCatia)
        self.connect(self.act_help_about_qt, SIGNAL("triggered()"), app, SLOT("aboutQt()"))

        self.connect(self, SIGNAL("XRunCallback"), self._XRunCallback)
        self.connect(self, SIGNAL("BufferSizeCallback"), self._BufferSizeCallback)
        self.connect(self, SIGNAL("SampleRateCallback"), self._SampleRateCallback)
        self.connect(self, SIGNAL("ClientRegistrationCallback"), self._ClientRegistrationCallback)
        self.connect(self, SIGNAL("PortRegistrationCallback"), self._PortRegistrationCallback)
        self.connect(self, SIGNAL("PortConnectCallback"), self._PortConnectCallback)
        self.connect(self, SIGNAL("PortRenameCallback"), self._PortRenameCallback)
        self.connect(self, SIGNAL("ShutdownCallback"), self._ShutdownCallback)

    def DBusSignalReceiver(self, *args, **kwds):
        if (kwds['interface'] == "org.jackaudio.JackControl"):
          if (kwds['member'] == "ServerStarted"):
            self.jackStarted()
          elif (kwds['member'] == "ServerStopped"):
            self.jackStopped()
        elif (kwds['interface'] == "org.gna.home.a2jmidid.control"):
          if (kwds['member'] == "bridge_started"):
            self.a2jStarted()
          elif (kwds['member'] == "bridge_stopped"):
            self.a2jStopped()

    #def DBusReconnect(self):
        #DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)
        #DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
        #DBus.a2j  = dbus.Interface(DBus.bus.get_object("org.gna.home.a2jmidid", "/"), "org.gna.home.a2jmidid.control")

    def jackStarted(self, auto_stop=False):
        #if (haveDBus):
          #self.DBusReconnect()

        if (not jack.client):
          jack.client = jacklib.client_open("catia", jacklib.NullOption, None)
          if (auto_stop and not jack.client):
            self.jackStopped()
            return

        self.timer100.start()
        self.timer500.start()
        self.act_jack_render.setEnabled(canRender)
        self.b_tools_render.setEnabled(canRender)
        self.menu_Transport.setEnabled(True)
        self.group_transport.setEnabled(True)
        self.menuJackServer(True)

        if (haveDBus):
          self.cb_sample_rate.setEnabled(True)

        self.pb_dsp_load.setValue(0)
        self.pb_dsp_load.setMaximum(100)
        self.pb_dsp_load.update()

        self.init_jack()

        if (True and DBus and DBus.a2j and not DBus.a2j.is_started()): #self.saved_settings["Jack/AutoStartA2J"]
          self.A2JBridgeStart()

        # TODO - check for pulse, only run if not
        #if (self.saved_settings["Jack/AutoStartPulse"] and havePulseJack):
          #self.PulseJackBridgeStart()

    def jackStopped(self):
        #if (haveDBus):
          #self.DBusReconnect()

        if (jack.client):
          jacklib.deactivate(jack.client)
          jacklib.client_close(jack.client)
          jack.client = None

        self.timer100.stop()
        self.timer500.stop()
        self.menu_Transport.setEnabled(False)
        self.group_transport.setEnabled(False)
        self.menuJackServer(False)
        self.act_jack_render.setEnabled(False)
        self.b_tools_render.setEnabled(False)

        if (haveDBus):
          setBufferSize(self, jacksettings.getBufferSize())
          setSampleRate(self, jacksettings.getSampleRate())
          setRealTime(self, jacksettings.isRealtime())
          setXruns(self, -1)
        else:
          self.cb_buffer_size.setEnabled(False)
          self.cb_sample_rate.setEnabled(False)
          self.menu_Jack_Buffer_Size.setEnabled(False)

        if (self.selected_transport_view == TRANSPORT_VIEW_HMS):
          self.label_time.setText("00:00:00")
        elif (self.selected_transport_view == TRANSPORT_VIEW_BBT):
          self.label_time.setText("000|0|0000")
        elif (self.selected_transport_view == TRANSPORT_VIEW_FRAMES):
          self.label_time.setText("000'000'000")

        self.pb_dsp_load.setValue(0)
        self.pb_dsp_load.setMaximum(0)
        self.pb_dsp_load.update()

        if (self.next_sample_rate):
          func_set_sample_rate(self, self.next_sample_rate)

        patchcanvas.clear()

    def a2jStarted(self):
        self.menuA2JBridge(True)

    def a2jStopped(self):
        self.menuA2JBridge(False)

    def JackServerStart(self):
        if (haveDBus and DBus.jack):
          return DBus.jack.StartServer()
        else:
          return False #TODO - start jack without dbus

    def JackServerStop(self):
        if (haveDBus and DBus.jack):
          DBus.jack.StopServer()
        else:
          pass #TODO - stop jack without dbus

    def A2JBridgeStart(self):
        DBus.a2j.start()

    def A2JBridgeStop(self):
        DBus.a2j.stop()

    def A2JBridgeExportHW(self):
        ask = QMessageBox.question(self, self.tr("A2J Hardware Export"), self.tr("Enable Hardware Export on the A2J Bridge?"),
            QMessageBox.Yes|QMessageBox.No, QMessageBox.Yes)
        if (ask == QMessageBox.No):
          DBus.a2j.set_hw_export(True)
        elif (ask == QMessageBox.No):
          DBus.a2j.set_hw_export(False)

    def PulseJackBridgeStart(self):
        if (self.saved_settings["Jack/PulsePlayOnly"]):
          os.system("pulse-jack -p &")
        else:
          os.system("pulse-jack &")

    def PulseJackBridgeStop(self):
        os.system("killall -KILL pulseaudio")

    def menuJackServer(self, started):
        if (haveDBus):
          self.act_tools_jack_start.setEnabled(not started)
          self.act_tools_jack_stop.setEnabled(started)
          self.menuA2JBridge(False)

    def menuA2JBridge(self, started):
        if (DBus.jack and DBus.jack.IsStarted()):
          self.act_tools_a2j_start.setEnabled(not started)
          self.act_tools_a2j_stop.setEnabled(started)
          self.act_tools_a2j_export_hw.setEnabled(not started)
        else:
          self.act_tools_a2j_start.setEnabled(False)
          self.act_tools_a2j_stop.setEnabled(False)

    def canvas_callback(self, action, *args):
        if (action == patchcanvas.ACTION_PORT_DISCONNECT_ALL):
          port_id = args[0]
          port_name = jacklib.port_name(args[0])
          port_mode = jacklib.PortIsInput if (jacklib.port_flags(port_id) & jacklib.PortIsInput) else jacklib.PortIsOutput
          port_connections = jacklib.port_get_connections(port_id)

          for i in range(len(port_connections)):
            if (port_mode == jacklib.PortIsInput):
              jacklib.disconnect(jack.client, port_connections[i], port_name)
            else:
              jacklib.disconnect(jack.client, port_name, port_connections[i])

        elif (action == patchcanvas.ACTION_PORT_RENAME):
          port_id = args[0]
          new_name = args[1]
          #jacklib.port_set_alias(port_id, new_name) #FIXME
          jacklib.port_set_name(port_id, new_name)

        elif (action == patchcanvas.ACTION_PORT_INFO):
          port_id = args[0]
          port_name = jacklib.port_name(port_id)
          port_short_name = jacklib.port_short_name(port_id)
          port_flags = jacklib.port_flags(port_id)
          if (JACK2):
            port_type = jacklib.port_type_id(port_id)
          else:
            port_type = jacklib.AUDIO if ("audio" in jacklib.port_type(port_id)) else jacklib.MIDI

          port_latency = jacklib.port_get_latency(port_id)
          port_total_latency = jacklib.port_get_total_latency(jack.client, port_id)

          group_name = port_name.split(":")[0]

          flags = []
          if (port_flags & jacklib.PortIsInput):
            flags.append(self.tr("Input"))
          if (port_flags & jacklib.PortIsOutput):
            flags.append(self.tr("Output"))
          if (port_flags & jacklib.PortIsPhysical):
            flags.append(self.tr("Physical"))
          if (port_flags & jacklib.PortCanMonitor):
            flags.append(self.tr("Can Monitor"))
          if (port_flags & jacklib.PortIsTerminal):
            flags.append(self.tr("Terminal"))
          if (port_flags & jacklib.PortIsActive):
            flags.append(self.tr("Active"))

          flags_text = QString("")
          for i in range(len(flags)):
            if (flags_text): flags_text+= " | "
            flags_text += flags[i]

          if (port_type == jacklib.AUDIO):
            type_text = self.tr("Audio")
          elif (port_type == jacklib.MIDI):
            type_text = self.tr("MIDI")
          else:
            type_text = self.tr("Unknown")

          latency_text = self.tr("%1 ms (%2 frames)").arg(port_latency*1000/self.sample_rate).arg(port_latency)
          latency_total_text = self.tr("%1 ms (%2 frames)").arg(port_total_latency*1000/self.sample_rate).arg(port_total_latency)

          info = self.tr(""
                  "<table>"
                  "<tr><td align='right'><b>Group Name:</b></td><td>&nbsp;%1</td></tr>"
                  "<tr><td align='right'><b>Port Name:</b></td><td>&nbsp;%2</td></tr>"
                  "<tr><td align='right'><b>Port ID:</b></td><td>&nbsp;%3</i></td></tr>"
                  "<tr><td align='right'><b>Full Port Name:</b></td><td>&nbsp;%4</td></tr>"
                  "<tr><td colspan='2'>&nbsp;</td></tr>"
                  "<tr><td align='right'><b>Port Flags:</b></td><td>&nbsp;%5</td></tr>"
                  "<tr><td align='right'><b>Port Type:</b></td><td>&nbsp;%6</td></tr>"
                  "<tr><td colspan='2'>&nbsp;</td></tr>"
                  "<tr><td align='right'><b>Port Latency:</b></td><td>&nbsp;%7</td></tr>"
                  "<tr><td align='right'><b>Port Total Latency:</b></td><td>&nbsp;%8</td></tr>"
                  "</table>"
                  ).arg(group_name).arg(port_short_name).arg(port_id).arg(port_name).arg(flags_text).arg(type_text).arg(latency_text).arg(latency_total_text)

          QMessageBox.information(self, self.tr("Port Information"), info)

        elif (action == patchcanvas.ACTION_PORTS_CONNECT):
          port_a = args[0]
          port_b  = args[2]
          jacklib.connect(jack.client, jacklib.port_name(port_a), jacklib.port_name(port_b))

        elif (action == patchcanvas.ACTION_PORTS_DISCONNECT):
          connection_id = args[0]
          port_a = port_b = None

          for i in range(len(self.connection_list)):
            if (connection_id == self.connection_list[i][0]):
              port_a = self.connection_list[i][1]
              port_b = self.connection_list[i][2]
              break
          else:
            return

          jacklib.disconnect(jack.client, jacklib.port_name(port_a), jacklib.port_name(port_b))

        elif (action == patchcanvas.ACTION_GROUP_DISCONNECT_ALL):
          group_name = args[1]
          port_list = jacklib.get_ports(jack.client, None, None, 0)

          for i in range(len(port_list)):
            if (port_list[i].split(":")[0] == group_name or port_list[i].split(":")[1].split(" [")[0] == group_name):
              port_id = jacklib.port_by_name(jack.client, port_list[i])
              port_mode = jacklib.PortIsInput if (jacklib.port_flags(port_id) & jacklib.PortIsInput) else jacklib.PortIsOutput
              port_connections = jacklib.port_get_connections(port_id)

              for j in range(len(port_connections)):
                if (port_mode == jacklib.PortIsInput):
                  jacklib.disconnect(jack.client, port_connections[j], port_list[i])
                else:
                  jacklib.disconnect(jack.client, port_list[i], port_connections[j])

        elif (action == patchcanvas.ACTION_GROUP_JOIN):
          group_id = args[0]
          patchcanvas.joinGroup(group_id)

        elif (action == patchcanvas.ACTION_GROUP_SPLIT):
          group_id = args[0]
          patchcanvas.splitGroup(group_id)

        elif (action == patchcanvas.ACTION_REQUEST_PORT_CONNECTION_LIST):
          port_id = args[0]
          connections = []

          port_name = jacklib.port_name(port_id)
          port_list = jacklib.get_ports(jack.client, None, None, 0)

          for i in range(len(port_list)):
            if (port_list[i] == port_name):
              port_id = jacklib.port_by_name(jack.client, port_list[i])
              port_mode = jacklib.PortIsInput if (jacklib.port_flags(port_id) & jacklib.PortIsInput) else jacklib.PortIsOutput
              port_connections = jacklib.port_get_connections(port_id)

              for j in range(len(port_connections)):
                connections.append((jacklib.port_by_name(jack.client, port_connections[j]), port_connections[j]))

          return connections

        elif (action == patchcanvas.ACTION_REQUEST_GROUP_CONNECTION_LIST):
          group_name = args[1]
          port_modes = args[2]
          connections = []

          port_list = jacklib.get_ports(jack.client, None, None, 0)

          for i in range(len(port_list)):
            if (port_list[i].split(":")[0] == group_name or port_list[i].split(":")[1].split(" [")[0] == group_name):
              port_id = jacklib.port_by_name(jack.client, port_list[i])
              port_mode = jacklib.PortIsInput if (jacklib.port_flags(port_id) & jacklib.PortIsInput) else jacklib.PortIsOutput
              port_connections = jacklib.port_get_connections(port_id)

              for j in range(len(port_connections)):
                if ( (port_modes & patchcanvas.PORT_MODE_OUTPUT and port_mode == jacklib.PortIsOutput) or
                     (port_modes & patchcanvas.PORT_MODE_INPUT and port_mode == jacklib.PortIsInput) ):
                  connections.append((jacklib.port_by_name(jack.client, port_connections[j]), port_connections[j]))

          return connections

    def canvas_add_group(self, group_name):
        group_id = self.last_group_id
        patchcanvas.addGroup(group_id, group_name)
        self.group_list.append([group_id, group_name])
        self.last_group_id += 1

    def canvas_remove_group(self, group_name):
        group_id = None
        for i in range(len(self.group_list)):
          if (self.group_list[i][1] == group_name):
            group_id = self.group_list[i][0]
            self.group_list.pop(i)
            break
        else:
          return

        patchcanvas.removeGroup(group_id)

    def canvas_add_port(self, port_id, group_name=None):
        group_id = self.last_group_id
        if (not group_name):
          group_name = jacklib.port_name(port_id).split(":")[0]

        if (group_name == a2j_client_name):
          full_name = jacklib.port_short_name(port_id)
          a2j_group_name = full_name.split(" [")[0]
          a2j_port_name  = full_name.split("): ")[1]
          a2j_port_mode  = full_name.split("] (")[1].split("): ")[0]
          if (a2j_group_name): group_name = a2j_group_name
          if (a2j_port_name): port_name = a2j_port_name
          if (a2j_port_mode): port_mode = jacklib.PortIsInput if (a2j_port_mode == "playback") else jacklib.PortIsOutput
          port_type = patchcanvas.PORT_TYPE_OUTRO
          haveA2J = True
          print "Emulating a2j port ->", a2j_group_name+":"+a2j_port_name
        else:
          port_name = jacklib.port_short_name(port_id)
          port_mode = jacklib.PortIsInput if (jacklib.port_flags(port_id) & jacklib.PortIsInput) else jacklib.PortIsOutput
          if (JACK2):
            port_type = jacklib.port_type_id(port_id)
          else:
            port_type = jacklib.AUDIO if ("audio" in jacklib.port_type(port_id)) else jacklib.MIDI
          haveA2J = False

        for i in range(len(self.group_list)):
          if (haveA2J):
            if (" VST" in group_name and group_name[0].isdigit()):
              group_name = group_name.replace(group_name[0], "", 1) # <- TESTING (useful for vsthost/dssi-vst)

            if (group_name.lower() == self.group_list[i][1].lower() or # <- TESTING (useful for LMMS)
                group_name.split(" ")[0].lower() == self.group_list[i][1].split(" ")[0].lower() or # <- TESTING (useful for Renoise and Loomer plugins)
                "vst_"+group_name.rsplit(" ",1)[0].replace(" ","").lower() == self.group_list[i][1] # <- TESTING (useful for vsthost/dssi-vst)
                ):
              group_id = self.group_list[i][0]
              break
          else:
            if (group_name == self.group_list[i][1]):
              group_id = self.group_list[i][0]
              break
        else: #Hack for clients started with no ports
          patchcanvas.addGroup(group_id, group_name)
          self.group_list.append([group_id, group_name])

        patchcanvas.addPort(group_id, port_id, port_name, port_mode, port_type)
        self.last_group_id += 1

        if not group_id in self.group_split_list:
          if (jacklib.port_flags(port_id) & jacklib.PortIsPhysical):
            patchcanvas.splitGroup(group_id)
            patchcanvas.setGroupIcon(group_id, patchcanvas.ICON_HARDWARE)
          self.group_split_list.append(group_id)

    def canvas_remove_port(self, port_id):
        patchcanvas.removePort(port_id)

    def canvas_rename_port(self, port_id, port_name):
        patchcanvas.renamePort(port_id, port_name.split(":")[-1])

    def canvas_connect_ports(self, port_a, port_b):
        connection_id = self.last_connection_id
        patchcanvas.connectPorts(connection_id, port_a, port_b)
        self.connection_list.append([connection_id, port_a, port_b])
        self.last_connection_id += 1

    def canvas_disconnect_ports(self, port_a, port_b):
        for i in range(len(self.connection_list)):
          if (self.connection_list[i][1] == port_a and self.connection_list[i][2] == port_b):
            patchcanvas.disconnectPorts(self.connection_list[i][0])
            self.connection_list.pop(i)
            break

    def init_jack(self):
        if (not jack.client): # Jack Crash/Bug ?
          self.menu_Transport.setEnabled(False)
          self.group_transport.setEnabled(False)
          return

        buffer_size = int(jacklib.get_buffer_size(jack.client))
        realtime = int(jacklib.is_realtime(jack.client))
        self.sample_rate = int(jacklib.get_sample_rate(jack.client))
        self.xruns = 0
        self.last_transport_state = None
        self.last_bpm = 0.0

        setBufferSize(self, buffer_size)
        setRealTime(self, realtime)
        setSampleRate(self, self.sample_rate)
        setXruns(self, self.xruns)

        refreshDSPLoad(self)
        refreshTransport(self)

        self.init_callbacks()
        self.init_ports()

        jacklib.activate(jack.client)

    def init_callbacks(self):
        if (not jack.client): return
        jacklib.set_buffer_size_callback(jack.client, self.JackBufferSizeCallback)
        jacklib.set_sample_rate_callback(jack.client, self.JackSampleRateCallback)
        jacklib.set_xrun_callback(jack.client, self.JackXRunCallback)

        jacklib.set_client_registration_callback(jack.client, self.JackClientRegistrationCallback)
        jacklib.set_port_registration_callback(jack.client, self.JackPortRegistrationCallback)
        jacklib.set_port_connect_callback(jack.client, self.JackPortConnectCallback)
        jacklib.on_shutdown(jack.client, self.JackShutdownCallback)

        if (JACK2):
          jacklib.set_port_rename_callback(jack.client, self.JackPortRenameCallback)

    def init_ports(self):
        if (not jack.client): return

        self.group_list = []
        self.group_split_list = []
        self.connection_list = []
        self.last_group_id = 1
        self.last_connection_id = 1

        a2j_list = []
        port_list = jacklib.get_ports(jack.client, None, None, 0)

        h = 0
        for i in range(len(port_list)):
          if (port_list[i-h].split(":")[0] == a2j_client_name):
            port = port_list.pop(i-h)
            a2j_list.append(port)
            h += 1

        for i in range(len(a2j_list)):
          port_list.append(a2j_list[i])

        for i in range(len(port_list)):
          port_name = port_list[i]
          port_id = jacklib.port_by_name(jack.client, port_name)
          group_name = port_name.split(":")[0]
          self.canvas_add_port(port_id, group_name)

        for i in range(len(port_list)):
          port_id = jacklib.port_by_name(jack.client, port_list[i])

          # Only make connections from an output port
          if (jacklib.port_flags(port_id) & jacklib.PortIsInput):
            continue

          port_connections = jacklib.port_get_connections(port_id)

          for j in range(len(port_connections)):
            port_b = jacklib.port_by_name(jack.client, port_connections[j])
            self.canvas_connect_ports(port_id, port_b)

    def func_clear_xruns(self):
        self.xruns = 0
        setXruns(self, self.xruns)

    def JackBufferSizeCallback(self, buffer_size, arg=None):
        print "JackBufferSizeCallback", buffer_size
        self.emit(SIGNAL("BufferSizeCallback"), buffer_size)
        return 0

    def JackSampleRateCallback(self, sample_rate, arg=None):
        print "JackSampleRateCallback", sample_rate
        self.emit(SIGNAL("SampleRateCallback"), sample_rate)
        return 0

    def JackXRunCallback(self, arg=None):
        print "JackXRunCallback", self.xruns+1
        self.emit(SIGNAL("XRunCallback"))
        return 0

    def JackClientRegistrationCallback(self, client_name, register_yesno, arg=None):
        print "JackClientRegistrationCallback", client_name, register_yesno
        self.emit(SIGNAL("ClientRegistrationCallback"), client_name, register_yesno)
        return 0

    def JackPortRegistrationCallback(self, port_id, register_yesno, arg=None):
        print "JackPortRegistrationCallback", port_id, register_yesno
        self.emit(SIGNAL("PortRegistrationCallback"), port_id, register_yesno)
        return 0

    def JackPortConnectCallback(self, port_a, port_b, connect_yesno, arg=None):
        print "JackPortConnectCallback", port_a, port_b, connect_yesno
        self.emit(SIGNAL("PortConnectCallback"), port_a, port_b, connect_yesno)
        return 0

    def JackPortRenameCallback(self, port_id, old_name, new_name, arg=None):
        print "JackPortRenameCallback", port_id, old_name, new_name
        self.emit(SIGNAL("PortRenameCallback"), port_id, old_name, new_name)
        return 0

    def JackShutdownCallback(self, arg=None):
        print "JackShutdownCallback"
        self.emit(SIGNAL("ShutdownCallback"))
        return 0

    def _BufferSizeCallback(self, buffer_size):
        setBufferSize(self, buffer_size)

    def _SampleRateCallback(self, sample_rate):
        setSampleRate(self, sample_rate)

    def _XRunCallback(self):
        self.xruns += 1
        setXruns(self, self.xruns)

    def _ClientRegistrationCallback(self, client_name, register_yesno):
        if (register_yesno):
          self.canvas_add_group(client_name)
        else:
          self.canvas_remove_group(client_name)

    def _PortRegistrationCallback(self, port_id, register_yesno):
        if (register_yesno):
          self.canvas_add_port(port_id)
        else:
          self.canvas_remove_port(port_id)

    def _PortConnectCallback(self, port_a, port_b, connect_yesno):
        if (connect_yesno):
          self.canvas_connect_ports(port_a, port_b)
        else:
          self.canvas_disconnect_ports(port_a, port_b)

    def _PortRenameCallback(self, port_id, old_name, new_name):
        self.canvas_rename_port(port_id, new_name)

    def _ShutdownCallback(self):
        if (jack.client):
          self.jackStopped()

    def configureCatia(self):
        CatiaSettingsW(self).exec_()

    def aboutCatia(self):
        QMessageBox.about(self, self.tr("About Catia"), self.tr("<h3>Catia</h3>"
            "<br>Version %1"
            "<br>Catia is a nice JACK Audio Patchbay with A2J Bridge integration.<br>"
            "<br>Copyright (C) 2010 falkTX").arg(VERSION))

    def saveSettings(self):
        settings = QSettings()
        settings.setValue("Geometry", QVariant(self.saveGeometry()))
        settings.setValue("ShowToolbar", self.frame_toolbar.isVisible())
        settings.setValue("ShowStatusbar", self.frame_statusbar.isVisible())
        settings.setValue("TransportView", self.selected_transport_view)

    def loadSettings(self, size_too=True):
        settings = QSettings()
        self.restoreGeometry(settings.value("Geometry").toByteArray())

        show_toolbar = settings.value("ShowToolbar", True).toBool()
        self.act_settings_show_toolbar.setChecked(show_toolbar)
        self.frame_toolbar.setVisible(show_toolbar)

        show_statusbar = settings.value("ShowStatusbar", True).toBool()
        self.act_settings_show_statusbar.setChecked(show_statusbar)
        self.frame_statusbar.setVisible(show_statusbar)

        transport_set_view(self, settings.value("TransportView", TRANSPORT_VIEW_HMS).toInt()[0])

        self.saved_settings = {
          #"Main/DefaultFolder": settings.value("Main/DefaultFolder", os.getenv("HOME")+"/jack-projects").toString(),
          "Main/RefreshInterval": settings.value("Main/RefreshInterval", 100).toInt()[0],
          "Canvas/Theme": settings.value("Canvas/Theme", patchcanvas.options['theme_name']).toString(),
          "Canvas/BezierLines": settings.value("Canvas/BezierLines", patchcanvas.options['bezier_lines']).toBool(),
          "Canvas/AutoHideGroups": settings.value("Canvas/AutoHideGroups", True).toBool(),
          "Canvas/FancyEyeCandy": settings.value("Canvas/FancyEyeCandy", patchcanvas.options['fancy_eyecandy']).toBool(),
          "Canvas/Antialiasing": settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0],
          "Canvas/TextAntialiasing": settings.value("Canvas/TextAntialiasing", True).toBool(),
          "Jack/AutoStartJack": settings.value("Jack/AutoStartJack", True).toBool(),
          "Jack/AutoStartA2J": settings.value("Jack/AutoStartA2J", True).toBool(),
          "Jack/AutoStartPulse": settings.value("Jack/AutoStartPulse", False).toBool(),
          "Jack/PulsePlayOnly": settings.value("Jack/PulsePlayOnly", False).toBool()
        }

    def closeEvent(self, event):
        self.saveSettings()
        patchcanvas.clear()
        return QMainWindow.closeEvent(self, event)

#--------------- main ------------------
if __name__ == '__main__':

    # Raster Graphics engine
    QRasterApplication = QApplication
    QRasterApplication.setGraphicsSystem("raster")

    # App initialization
    app = QRasterApplication(sys.argv)
    app.setApplicationName("Catia")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    #app.setWindowIcon(QIcon(":/48x48/catia.png"))

    # Do not close on SIGUSR1
    if (LINUX):
      signal.signal(signal.SIGUSR1, signal.SIG_IGN)

    # Show GUI
    gui = CatiaMainW()
    gui.show()

    # App-Loop
    ret = app.exec_()

    # Close Jack
    if (jack.client):
      jacklib.deactivate(jack.client)
      jacklib.client_close(jack.client)

    # Exit properly
    sys.exit(ret)
