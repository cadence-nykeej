#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QPointF, QRectF, QTimer, SIGNAL
from PyQt4.QtGui import QPainter, QPixmap, QWidget

# Imports (Custom Stuff)
import icons_rc

midi_key2rect_map_h = {
  '0':  QRectF(  0, 0, 18, 64), # C
  '1':  QRectF( 13, 0, 11, 42), # C#
  '2':  QRectF( 18, 0, 25, 64), # D
  '3':  QRectF( 37, 0, 11, 42), # D#
  '4':  QRectF( 42, 0, 18, 64), # E
  '5':  QRectF( 60, 0, 18, 64), # F
  '6':  QRectF( 73, 0, 11, 42), # F#
  '7':  QRectF( 78, 0, 25, 64), # G
  '8':  QRectF( 97, 0, 11, 42), # G#
  '9':  QRectF(102, 0, 25, 64), # A
  '10': QRectF(121, 0, 11, 42), # A#
  '11': QRectF(126, 0, 18, 64)  # B
}

midi_key2rect_map_hs = {
  '0':  QRectF( 0, 0, 10, 64), # C
  '1':  QRectF( 6, 0,  6, 42), # C#
  '2':  QRectF(10, 0, 12, 64), # D
  '3':  QRectF(18, 0,  6, 42), # D#
  '4':  QRectF(22, 0, 10, 64), # E
  '5':  QRectF(30, 0, 10, 64), # F
  '6':  QRectF(36, 0,  6, 42), # F#
  '7':  QRectF(40, 0, 12, 64), # G
  '8':  QRectF(48, 0,  6, 42), # G#
  '9':  QRectF(52, 0, 12, 64), # A
  '10': QRectF(60, 0,  6, 42), # A#
  '11': QRectF(64, 0, 10, 64)  # B
}

midi_key2rect_map_v = {
  '11': QRectF(0,   0, 64, 18), # B
  '10': QRectF(0,  14, 42,  7), # A#
  '9':  QRectF(0,  18, 64, 24), # A
  '8':  QRectF(0,  38, 42,  7), # G#
  '7':  QRectF(0,  42, 64, 24), # G
  '6':  QRectF(0,  62, 42,  7), # F#
  '5':  QRectF(0,  66, 64, 18), # F
  '4':  QRectF(0,  84, 64, 18), # E
  '3':  QRectF(0,  98, 42,  7), # D#
  '2':  QRectF(0, 102, 64, 24), # D
  '1':  QRectF(0, 122, 42,  7), # C#
  '0':  QRectF(0, 126, 64, 18)  # C
}

midi_key2rect_map_vs = {
  '11': QRectF(0,  0, 64, 10), # B
  '10': QRectF(0,  6, 42,  7), # A#
  '9':  QRectF(0,  8, 64, 10), # A
  '8':  QRectF(0, 18, 42,  7), # G#
  '7':  QRectF(0, 20, 64, 10), # G
  '6':  QRectF(0, 30, 42,  7), # F#
  '5':  QRectF(0, 32, 64, 10), # F
  '4':  QRectF(0, 42, 64, 10), # E
  '3':  QRectF(0, 48, 42,  7), # D#
  '2':  QRectF(0, 50, 64, 10), # D
  '1':  QRectF(0, 60, 42,  7), # C#
  '0':  QRectF(0, 62, 64, 10)  # C
}

midi_keyboard2key_map = {
  # 4th octave
  '%i' % (Qt.Key_Z): 48,
  '%i' % (Qt.Key_S): 49,
  '%i' % (Qt.Key_X): 50,
  '%i' % (Qt.Key_D): 51,
  '%i' % (Qt.Key_C): 52,
  '%i' % (Qt.Key_V): 53,
  '%i' % (Qt.Key_G): 54,
  '%i' % (Qt.Key_B): 55,
  '%i' % (Qt.Key_H): 56,
  '%i' % (Qt.Key_N): 57,
  '%i' % (Qt.Key_J): 58,
  '%i' % (Qt.Key_M): 59,
  # 5th octave
  '%i' % (Qt.Key_Q): 60,
  '%i' % (Qt.Key_2): 61,
  '%i' % (Qt.Key_W): 62,
  '%i' % (Qt.Key_3): 63,
  '%i' % (Qt.Key_E): 64,
  '%i' % (Qt.Key_R): 65,
  '%i' % (Qt.Key_5): 66,
  '%i' % (Qt.Key_T): 67,
  '%i' % (Qt.Key_6): 68,
  '%i' % (Qt.Key_Y): 69,
  '%i' % (Qt.Key_7): 70,
  '%i' % (Qt.Key_U): 71,
}

def note_is_black(note):
  base_key = note % 12
  if (base_key == 1 or base_key == 3 or base_key == 6 or base_key == 8 or base_key == 10):
    return True
  else:
    return False

# MIDI Keyboard, using a pixmap for painting
class PixmapKeyboard(QWidget):
    def __init__(self, parent=None):
        super(PixmapKeyboard, self).__init__(parent)

        self.HORIZONTAL = 0
        self.VERTICAL = 1

        self.keys_to_enable = []
        self.rect_to_enable = []
        self.last_mouse_note = -1
        self.needs_update = False

        self.send_midi_data = False
        self.midi_output_data = []

        self.timer = QTimer()
        self.timer.start(50)

        self.setMidiMap('h', True)
        self.setOctaves(6)

        self.connect(self.timer, SIGNAL("timeout()"), self.checkNeedsUpdate)

    def checkNeedsUpdate(self):
        if (self.needs_update):
          self.update()
          self.needs_update = False

    def enableOut(self, data_out):
        self.send_midi_data = True
        self.midi_output_data = data_out

    def get_rect_from_key(self, midi_key):
        return self.midi_map.get(str(midi_key % 12))

    def setMidiMap(self, map_t, orange=False):
        if (map_t == 'h'):
          self.midi_map = midi_key2rect_map_h
          self.pixmap = QPixmap(":/bitmaps/kbd_h%i.png" % (1 if (orange) else 2))
          self.pixmap_mode = self.HORIZONTAL

        elif (map_t == 'hs'):
          self.midi_map = midi_key2rect_map_hs
          self.pixmap = QPixmap(":/bitmaps/kbd_hs%i.png" % (1 if (orange) else 2))
          self.pixmap_mode = self.HORIZONTAL

        elif (map_t == 'v'):
          self.midi_map = midi_key2rect_map_v
          self.pixmap = QPixmap(":/bitmaps/kbd_v%i.png" % (1 if (orange) else 2))
          self.pixmap_mode = self.VERTICAL

        elif (map_t == 'vs'):
          self.midi_map = midi_key2rect_map_vs
          self.pixmap = QPixmap(":/bitmaps/kbd_vs%i.png" % (1 if (orange) else 2))
          self.pixmap_mode = self.VERTICAL

        else:
          print "Wrong Map type!"
          self.setMidiMap('h')
          return

        if (self.pixmap_mode == self.HORIZONTAL):
          self.p_width = self.pixmap.width()
          self.p_height = self.pixmap.height()/2
        elif (self.pixmap_mode == self.VERTICAL):
          self.p_width = self.pixmap.width()/2
          self.p_height = self.pixmap.height()

    def setOctaves(self, n):
        if (n < 1):
          n = 1
        elif (n > 6):
          n = 6

        self.octaves = n

        if (self.pixmap_mode == self.HORIZONTAL):
          self.setMinimumSize(self.p_width*self.octaves, self.p_height)
          self.setMaximumSize(self.p_width*self.octaves, self.p_height)
        elif (self.pixmap_mode == self.VERTICAL):
          self.setMinimumSize(self.p_width, self.p_height*self.octaves)
          self.setMaximumSize(self.p_width, self.p_height*self.octaves)

        self.update()

    def noteOn(self, note):
        #if (23 < note < 96 and note not in self.keys_to_enable):
        if (note not in self.keys_to_enable):
          self.keys_to_enable.append(note)
          self.rect_to_enable.append(self.get_rect_from_key(note))
          self.needs_update = True

          if (self.send_midi_data):
            self.midi_output_data.append((0x90, note, 100)) #TODO

    def noteOff(self, note):
        #if (23 < note < 96 and note in self.keys_to_enable):
        if (note in self.keys_to_enable):
          self.keys_to_enable.remove(note)
          self.rect_to_enable.remove(self.get_rect_from_key(note))
          self.needs_update = True

          if (self.send_midi_data):
            self.midi_output_data.append((0x80, note, 100)) #TODO

    def keyPressEvent(self, event):
        qt_key = str(event.key())

        if (qt_key in midi_keyboard2key_map.keys()):
          midi_key = midi_keyboard2key_map.get(qt_key)
          self.noteOn(midi_key)

        return QWidget.keyPressEvent(self, event)

    def keyReleaseEvent(self, event):
        qt_key = str(event.key())

        if (qt_key in midi_keyboard2key_map.keys()):
          midi_key = midi_keyboard2key_map.get(qt_key)
          self.noteOff(midi_key)

        return QWidget.keyReleaseEvent(self, event)

    def mousePressEvent(self, event):
        self.last_mouse_note = -1
        self.handleMousePos(event.pos())
        return QWidget.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        self.handleMousePos(event.pos())
        return QWidget.mousePressEvent(self, event)

    def handleMousePos(self, pos):
        if (self.pixmap_mode == self.HORIZONTAL):
          octave = pos.x()/self.p_width
          n_pos = QPointF(pos.x()%self.p_width, pos.y())
        elif (self.pixmap_mode == self.VERTICAL):
          octave = (self.octaves-pos.y()/self.p_height)-1
          n_pos = QPointF(pos.x(), pos.y()%self.p_height)

        note = -1
        octave += 3

        if (self.midi_map['1'].contains(n_pos)):   # C#
          note = 1+(octave*12)
        elif (self.midi_map['3'].contains(n_pos)): # D#
          note = 3+(octave*12)
        elif (self.midi_map['6'].contains(n_pos)): # F#
          note = 6+(octave*12)
        elif (self.midi_map['8'].contains(n_pos)): # G#
          note = 8+(octave*12)
        elif (self.midi_map['10'].contains(n_pos)):# A#
          note = 10+(octave*12)
        elif (self.midi_map['0'].contains(n_pos)): # C
          note = 0+(octave*12)
        elif (self.midi_map['2'].contains(n_pos)): # D
          note = 2+(octave*12)
        elif (self.midi_map['4'].contains(n_pos)): # E
          note = 4+(octave*12)
        elif (self.midi_map['5'].contains(n_pos)): # F
          note = 5+(octave*12)
        elif (self.midi_map['7'].contains(n_pos)): # G
          note = 7+(octave*12)
        elif (self.midi_map['9'].contains(n_pos)): # A
          note = 9+(octave*12)
        elif (self.midi_map['11'].contains(n_pos)):# B
          note = 11+(octave*12)

        if (note == -1):
          self.noteOff(self.last_mouse_note)
        elif (self.last_mouse_note != note):
          self.noteOff(self.last_mouse_note)
          self.noteOn(note)

        self.last_mouse_note = note

    def mouseReleaseEvent(self, event):
        if (self.last_mouse_note != -1):
          self.noteOff(self.last_mouse_note)
        return QWidget.mouseReleaseEvent(self, event)

    def paintEvent(self, event):
        painter = QPainter(self)

        for i in range(self.octaves):
          if (self.pixmap_mode == self.HORIZONTAL):
            target = QRectF((self.p_width*i), 0, self.p_width, self.p_height)
          elif (self.pixmap_mode == self.VERTICAL):
            target = QRectF(0, self.p_height*i, self.p_width, self.p_height)
          else:
            print "Error here, cod.003"
            return

          source = QRectF(0, 0, self.p_width, self.p_height)
          painter.drawPixmap(target, self.pixmap, source)

        for i in range(len(self.rect_to_enable)):
          pos = self.rect_to_enable[i]
          note = self.keys_to_enable[i]

          if (note < 36 or note/12 > self.octaves+2):
            continue

          if (35 < note < 48):
            octx = 0
          elif (note < 60):
            octx = 1
          elif (note < 72):
            octx = 2
          elif (note < 84):
            octx = 3
          elif (note < 96):
            octx = 4
          elif (note < 108):
            octx = 5
          else:
            # cannot paint this note
            continue

          if (self.pixmap_mode == self.VERTICAL):
            octx = self.octaves - octx - 1

          if (self.pixmap_mode == self.HORIZONTAL):
            target = QRectF(pos.x()+(self.p_width*octx), 0, pos.width(), pos.height())
            source = QRectF(pos.x(), self.p_height, pos.width(), pos.height())
          elif (self.pixmap_mode == self.VERTICAL):
            target = QRectF(pos.x(), pos.y()+(self.p_height*octx), pos.width(), pos.height())
            source = QRectF(self.p_width, pos.y(), pos.width(), pos.height())

          painter.drawPixmap(target, self.pixmap, source)

          if (note_is_black(note-1) and not note-1 in self.keys_to_enable):
            pos = self.get_rect_from_key(note-1)

            if (self.pixmap_mode == self.HORIZONTAL):
              target = QRectF(pos.x()+(self.p_width*octx), 0, pos.width(), pos.height())
              source = QRectF(pos.x(), 0, pos.width(), pos.height())
            elif (self.pixmap_mode == self.VERTICAL):
              target = QRectF(pos.x(), pos.y()+(self.p_height*octx), pos.width(), pos.height())
              source = QRectF(0, pos.y(), pos.width(), pos.height())

            painter.drawPixmap(target, self.pixmap, source)

          if (note_is_black(note+1) and not note+1 in self.keys_to_enable):
            pos = self.get_rect_from_key(note+1)

            if (self.pixmap_mode == self.HORIZONTAL):
              target = QRectF(pos.x()+(self.p_width*octx), 0, pos.width(), pos.height())
              source = QRectF(pos.x(), 0, pos.width(), pos.height())
            elif (self.pixmap_mode == self.VERTICAL):
              target = QRectF(pos.x(), pos.y()+(self.p_height*octx), pos.width(), pos.height())
              source = QRectF(0, pos.y(), pos.width(), pos.height())

            painter.drawPixmap(target, self.pixmap, source)

