#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import QRectF
from PyQt4.QtGui import QDial, QPainter, QPixmap

# Imports (Custom Stuff)
import icons_rc

# Custom Dial, using a pixmap for paiting
class PixmapDial(QDial):
    def __init__(self, parent=None):
        super(PixmapDial, self).__init__(parent)

        self.p_width = 1
        self.p_height = 1
        self.p_count = 1
        self.use_pixmap = True
        self.setPixmap(1)

    def setPixmap(self, n):
        if (n < 1):
          self.use_pixmap = False
        else:
          self.use_pixmap = True

          if (n > 6):
            n = 6

          self.pixmap = QPixmap(":/bitmaps/dial_%i.png" % (n))
          self.updateSizes()

    def getSize(self):
        return p_width

    def updateSizes(self):
        self.p_width = self.pixmap.width()
        self.p_height = self.pixmap.height()

        if (self.p_width < 1):
          self.p_width = 1

        if (self.p_height < 1):
          self.p_height = 1

        self.p_count = self.p_height/self.p_width

        self.setMinimumSize(self.p_width, self.p_width)
        self.setMaximumSize(self.p_width, self.p_width)

    def paintEvent(self, event):
        if (self.use_pixmap):
          current = float(self.value()-self.minimum())
          divider = float(self.maximum()-self.minimum())

          if (divider == 0.0):
            return

          yper = int((self.p_count-1)*(current/divider))
          ypos = self.p_width*yper

          target = QRectF(0.0, 0.0, self.p_width, self.p_width)
          source = QRectF(0.0, ypos, self.p_width, self.p_width)

          painter = QPainter(self)
          painter.drawPixmap(target, self.pixmap, source)

        else:
          return QDial.paintEvent(self, event)

    def resizeEvent(self, event):
        if (self.use_pixmap):
          self.updateSizes()

        return QDial.resizeEvent(self, event)

