#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import dbus
from dbus.mainloop.qt import DBusQtMainLoop
from commands import getoutput
from random import randint
from time import ctime
from PyQt4.QtCore import Qt, QFile, QRectF, QSettings, QString, QTimer, QVariant, SIGNAL, SLOT
from PyQt4.QtGui import QAction, QApplication, QCursor, QDialog, QDialogButtonBox, QFileDialog, QFontMetrics, QFrame, QGraphicsView
from PyQt4.QtGui import QIcon, QMainWindow, QMenu, QMessageBox, QPainter, QPen, QSystemTrayIcon, QTableWidgetItem, QTreeWidgetItem, QWizard

# Imports (Plugins and Resources)
import database, jacklib, jacksettings, patchcanvas
import icons_rc, ui_settings_app
import ui_claudia, ui_claudia_createroom, ui_claudia_addnew, ui_claudia_addnew_kxstudio, ui_claudia_runcustom
import ui_claudia_saveproject, ui_claudia_projectproperties, ui_claudia_studiolist, ui_claudia_studioname
from shared import *
from xicon import XIcon

# Defines
ITEM_TYPE_STUDIO = 0
ITEM_TYPE_STUDIO_APP = 1
ITEM_TYPE_ROOM = 2
ITEM_TYPE_ROOM_APP = 3

GRAPH_DICT_OBJECT_TYPE_GRAPH = 0
GRAPH_DICT_OBJECT_TYPE_CLIENT = 1
GRAPH_DICT_OBJECT_TYPE_PORT = 2
GRAPH_DICT_OBJECT_TYPE_CONNECTION = 3

URI_CANVAS_WIDTH = "http://ladish.org/ns/canvas/width"
URI_CANVAS_HEIGHT = "http://ladish.org/ns/canvas/height"
URI_CANVAS_X = "http://ladish.org/ns/canvas/x"
URI_CANVAS_Y = "http://ladish.org/ns/canvas/y"
URI_A2J_PORT = "http://ladish.org/ns/a2j"

RECENT_PROJECTS_STORE_MAX_ITEMS = 50

DEFAULT_CANVAS_WIDTH = 3100
DEFAULT_CANVAS_HEIGHT = 2400

DBus.loop = DBusQtMainLoop(set_as_default=True)
DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)
DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
DBus.patchbay = None
DBus.ladish_control = DBus.bus.get_object("org.ladish", "/org/ladish/Control")
DBus.ladish_studio = None
DBus.ladish_room = None
DBus.ladish_graph = None
DBus.ladish_app_iface = None

try:
  DBus.a2j = dbus.Interface(DBus.bus.get_object("org.gna.home.a2jmidid", "/"), "org.gna.home.a2jmidid.control")
except:
  DBus.a2j = None

try:
  DBus.ladish_app_daemon = DBus.bus.get_object("org.ladish.appdb", "/")
except:
  DBus.ladish_app_daemon = None

jacksettings.initBus(DBus.bus)

# Configure Claudia Dialog
class ClaudiaSettingsW(QDialog, ui_settings_app.Ui_SettingsW):
    def __init__(self, parent=None):
        super(ClaudiaSettingsW, self).__init__(parent)
        self.setupUi(self)

        self.loadSettings()

        self.lw_page.setCurrentCell(0, 0)
        self.lw_page.item(0, 0).setIcon(QIcon(":/48x48/claudia.png"))
        self.lw_page.item(4, 0).setIcon(QIcon.fromTheme("application-x-executable", QIcon(":/48x48/exec.png")))

        self.tw_apps.removeTab(1)
        self.tw_apps.removeTab(1)
        self.cb_jack_jack.setVisible(False)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL("clicked()"), self.resetSettings)
        self.connect(self, SIGNAL("accepted()"), self.saveSettings)

        self.connect(self.b_main_def_folder_open, SIGNAL("clicked()"), lambda parent=self, current_path=self.le_main_def_folder.text(),
                                                       lineEdit=self.le_main_def_folder: getAndSetPath(parent, current_path, lineEdit))
        self.connect(self.b_apps_wineprefix_open, SIGNAL("clicked()"), lambda parent=self, current_path=self.le_apps_wineprefix.text(),
                                                       lineEdit=self.le_apps_wineprefix: getAndSetPath(parent, current_path, lineEdit))

    def saveSettings(self):
        settings = QSettings()
        settings.setValue(QString("Main/DefaultProjectFolder"), self.le_main_def_folder.text())
        settings.setValue(QString("Main/RefreshInterval"), self.sb_gui_refresh.value())
        settings.setValue(QString("Canvas/Theme"), self.cb_canvas_theme.currentText())
        settings.setValue(QString("Canvas/BezierLines"), self.cb_canvas_bezier_lines.isChecked())
        settings.setValue(QString("Canvas/AutoHideGroups"), self.cb_canvas_hide_groups.isChecked())
        settings.setValue(QString("Canvas/FancyEyeCandy"), self.cb_canvas_eyecandy.isChecked())
        settings.setValue(QString("Canvas/Antialiasing"), self.cb_canvas_render_aa.checkState())
        settings.setValue(QString("Canvas/TextAntialiasing"), self.cb_canvas_render_text_aa.isChecked())
        settings.setValue(QString("Apps/Database"), "LADISH" if self.rb_database_ladish.isChecked() else "KXStudio")
        #settings.setValue(QString("Apps/Wine/UseWinePrefix"), self.cb_apps_wineprefix.isChecked())
        #settings.setValue(QString("Apps/Wine/WINEPREFIX"), self.le_apps_wineprefix.text())
        #settings.setValue(QString("Apps/Wine/WINE_RT"), self.cb_apps_wine_rt.isChecked())
        #settings.setValue(QString("Apps/Wine/WINE_RT_VALUE"), self.sb_apps_wine_rt.value())
        #settings.setValue(QString("Apps/Wine/WINE_SVR_RT"), self.cb_apps_wine_svr_rt.isChecked())
        #settings.setValue(QString("Apps/Wine/WINE_SVR_RT_VALUE"), self.sb_apps_wine_svr_rt.value())

        if (DBus.a2j):
          settings.setValue(QString("Jack/AutoStartA2J"), self.cb_jack_a2j.isChecked())

        if (havePulseJack):
          settings.setValue(QString("Jack/AutoStartPulse"), self.cb_jack_pulse.isChecked())
          settings.setValue(QString("Jack/PulsePlayOnly"), self.cb_jack_pulse_play.isChecked())

        try:
          ladish_config = DBus.bus.get_object("org.ladish.conf", "/org/ladish/conf")
        except:
          ladish_config = None

        if (ladish_config):
          ladish_config.set('/org/ladish/daemon/notify', "true" if (self.cb_ladish_notify.isChecked()) else "false")
          ladish_config.set('/org/ladish/daemon/studio_autostart', "true" if (self.cb_ladish_studio_autostart.isChecked()) else "false")
          ladish_config.set('/org/ladish/daemon/shell', unicode(self.le_ladish_shell.text()))
          ladish_config.set('/org/ladish/daemon/terminal', unicode(self.le_ladish_terminal.text()))

    def loadSettings(self):
        settings = QSettings()
        self.le_main_def_folder.setText(settings.value("Main/DefaultProjectFolder", os.path.join(os.getenv("HOME"),"ladish-projects")).toString())
        self.sb_gui_refresh.setValue(settings.value("Main/RefreshInterval", 100).toInt()[0])
        self.cb_canvas_bezier_lines.setChecked(settings.value("Canvas/BezierLines", True).toBool())
        self.cb_canvas_hide_groups.setChecked(settings.value("Canvas/AutoHideGroups", True).toBool())
        self.cb_canvas_eyecandy.setChecked(settings.value("Canvas/FancyEyeCandy", False).toBool())
        self.cb_canvas_render_aa.setCheckState(settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0])
        self.cb_canvas_render_text_aa.setChecked(settings.value("Canvas/TextAntialiasing", True).toBool())
        #self.cb_apps_wineprefix.setChecked(settings.value("Apps/Wine/UseWinePrefix", True).toBool())
        #self.le_apps_wineprefix.setText(settings.value("Apps/Wine/WINEPREFIX", os.getenv("HOME")+"/.wine").toString())
        #self.cb_apps_wine_rt.setChecked(settings.value("Apps/Wine/WINE_RT", True).toBool())
        #self.sb_apps_wine_rt.setValue(settings.value("Apps/Wine/WINE_RT_VALUE", 15).toInt()[0])
        #self.cb_apps_wine_svr_rt.setChecked(settings.value("Apps/Wine/WINE_SVR_RT", True).toBool())
        #self.sb_apps_wine_svr_rt.setValue(settings.value("Apps/Wine/WINE_SVR_RT_VALUE", 10).toInt()[0])

        if (DBus.a2j):
          self.cb_jack_a2j.setChecked(settings.value("Jack/AutoStartA2J", True).toBool())
        else:
          self.cb_jack_a2j.setEnabled(False)

        if (havePulseJack):
          self.cb_jack_pulse.setChecked(settings.value("Jack/AutoStartPulse", False).toBool())
          self.cb_jack_pulse_play.setChecked(settings.value("Jack/PulsePlayOnly", False).toBool())
        else:
          self.cb_jack_pulse.setEnabled(False)
          self.cb_jack_pulse_play.setEnabled(False)

        theme_name = settings.value("Canvas/Theme", patchcanvas.getDefaultThemeName()).toString()

        for i in range(len(patchcanvas.theme_list)):
          self.cb_canvas_theme.addItem(patchcanvas.theme_list[i]['name'])
          if (patchcanvas.theme_list[i]['name'] == theme_name):
            self.cb_canvas_theme.setCurrentIndex(i)

        try:
          ladish_config = DBus.bus.get_object("org.ladish.conf", "/org/ladish/conf")
        except:
          ladish_config = None

        if (ladish_config):
          try:
            self.cb_ladish_notify.setChecked(True if (ladish_config.get('/org/ladish/daemon/notify')[0] == "true") else False)
          except:
            self.cb_ladish_notify.setChecked(True)

          try:
            self.cb_ladish_studio_autostart.setChecked(True if (ladish_config.get('/org/ladish/daemon/studio_autostart')[0] == "true") else False)
          except:
            self.cb_ladish_studio_autostart.setChecked(True)

          try:
            self.le_ladish_shell.setText(ladish_config.get('/org/ladish/daemon/shell')[0])
          except:
            self.le_ladish_shell.setText("sh")

          try:
            self.le_ladish_terminal.setText(ladish_config.get('/org/ladish/daemon/terminal')[0])
          except:
            self.le_ladish_terminal.setText("xterm")

          database = settings.value("Apps/Database", "LADISH").toString()
          if (database == "LADISH"):
            self.rb_database_ladish.setChecked(True)
          elif (database == "KXStudio"):
            self.rb_database_kxstudio.setChecked(True)

    def resetSettings(self):
        self.le_main_def_folder.setText(os.path.join(os.getenv("HOME"),"ladish-projects"))
        self.sb_gui_refresh.setValue(100)
        self.cb_ladish_notify.setChecked(True)
        self.cb_ladish_studio_autostart.setChecked(True)
        self.le_ladish_shell.setText("sh")
        self.le_ladish_terminal.setText("xterm")
        self.cb_canvas_theme.setCurrentIndex(0)
        self.cb_canvas_bezier_lines.setChecked(True)
        self.cb_canvas_hide_groups.setChecked(True)
        self.cb_canvas_eyecandy.setChecked(False)
        self.cb_canvas_render_aa.setCheckState(Qt.PartiallyChecked)
        self.cb_canvas_render_text_aa.setChecked(True)
        self.rb_database_ladish.setChecked(True)
        self.rb_database_kxstudio.setChecked(False)
        #self.cb_apps_wineprefix.setChecked(True)
        #self.le_apps_wineprefix.setText(os.getenv("HOME")+"/.wine")
        #self.cb_apps_wine_rt.setChecked(True)
        #self.sb_apps_wine_rt.setValue(15)
        #self.cb_apps_wine_svr_rt.setChecked(True)
        #self.sb_apps_wine_svr_rt.setValue(10)

        if (DBus.a2j):
          self.cb_jack_a2j.setChecked(True)

        if (havePulseJack):
          self.cb_jack_pulse.setChecked(False)
          self.cb_jack_pulse_play.setChecked(False)

# Studio List Dialog
class StudioListW(QDialog, ui_claudia_studiolist.Ui_StudioListW):
    def __init__(self, parent=None):
        super(StudioListW, self).__init__(parent)
        self.setupUi(self)

        self.studio_to_return = QString("")
        self.tableWidget.setColumnWidth(0, 125)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        studio_list_dump = DBus.ladish_control.GetStudioList()
        for i in range(len(studio_list_dump)):
          name = QString(studio_list_dump[i][0])
          date = QString(ctime(float(studio_list_dump[i][1][u'Modification Time'])))

          w_name = QTableWidgetItem(name)
          w_date = QTableWidgetItem(date)
          self.tableWidget.insertRow(0)
          self.tableWidget.setItem(0, 0, w_name)
          self.tableWidget.setItem(0, 1, w_date)

        self.connect(self, SIGNAL("accepted()"), self.setName)
        self.connect(self.tableWidget, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection)
        self.connect(self.tableWidget, SIGNAL("cellDoubleClicked(int, int)"), self.accept)

        if (self.tableWidget.rowCount() > 0):
          self.tableWidget.setCurrentCell(0, 0)

    def setName(self):
        self.studio_to_return = self.tableWidget.item(self.tableWidget.currentRow(), 0).text()

    def checkSelection(self, row, column, prev_row, prev_column):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (row >= 0) else False)

# Studio Name Dialog
class StudioNameW(QDialog, ui_claudia_studioname.Ui_StudioNameW):
    def __init__(self, parent=None, setName=True, block=True):
        super(StudioNameW, self).__init__(parent)
        self.setupUi(self)

        self.studio_list = []
        self.studio_to_return = QString("")
        self.block = block
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(not block)

        if (bool(DBus.ladish_control.IsStudioLoaded()) and setName):
          current_name = QString(DBus.ladish_studio.GetName())
          self.studio_list.append(current_name)
          self.le_name.setText(current_name)

        studio_list_dump = DBus.ladish_control.GetStudioList()
        for i in range(len(studio_list_dump)):
          self.studio_list.append(QString(studio_list_dump[i][0]))

        self.connect(self, SIGNAL("accepted()"), self.setName)
        self.connect(self.le_name, SIGNAL("textChanged(QString)"), self.checkText)

    def setName(self):
        self.studio_to_return = self.le_name.text()

    def checkText(self, text):
        if (self.block): false = (text.isEmpty() or text in self.studio_list)
        else: false = text.isEmpty()
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(not false)

# Create Room Dialog
class CreateRoomW(QDialog, ui_claudia_createroom.Ui_CreateRoomW):
    def __init__(self, parent=None):
        super(CreateRoomW, self).__init__(parent)
        self.setupUi(self)

        self.room_to_return = []
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        room_templates_list_dump = DBus.ladish_control.GetRoomTemplateList()
        for i in range(len(room_templates_list_dump)):
          self.lw_templates.addItem(QString(room_templates_list_dump[i][0]))

        self.connect(self, SIGNAL("accepted()"), self.setRoom)
        self.connect(self.le_name, SIGNAL("textChanged(QString)"), self.checkText)

        if (self.lw_templates.count() > 0):
          self.lw_templates.setCurrentRow(0)

    def setRoom(self):
        self.room_to_return = [unicode(self.le_name.text()), unicode(self.lw_templates.currentItem().text())]

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or self.lw_templates.currentRow() < 0) else True)

# Save Project Dialog
class SaveProjectW(QDialog, ui_claudia_saveproject.Ui_SaveProjectW):
    def __init__(self, parent=None, path="", name="", setName=True):
        super(SaveProjectW, self).__init__(parent)
        self.setupUi(self)

        self.project_to_return = []
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (path and name) else False)

        if (setName):
          self.le_path.setText(QString(path))
          self.le_name.setText(QString(name))

        self.connect(self, SIGNAL("accepted()"), self.setProject)
        self.connect(self.le_path, SIGNAL("textChanged(QString)"), self.checkText_path)
        self.connect(self.le_name, SIGNAL("textChanged(QString)"), self.checkText_name)
        self.connect(self.b_open, SIGNAL("clicked()"), self.checkFolder)

    def setProject(self):
        self.project_to_return = [self.le_path.text(), self.le_name.text()]

    def checkFolder(self):
        new_path = getAndSetPath(self, self.parent().saved_settings["Main/DefaultProjectFolder"] if (self.le_path.text().isEmpty()) else self.le_path.text(), self.le_path)
        #if (new_path.isEmpty()):
          #pass
        #elif (not QFile.exists(new_path+"/ladish-project.xml")):
          #QMessageBox.warning(self, self.tr("Error"), self.tr("The selected folder does not contain a ladish project!"))

    def checkText_name(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or self.le_path.text().isEmpty()) else True)

    def checkText_path(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or self.le_name.text().isEmpty() or not QFile.exists(text+"/ladish-project.xml")) else True)

# Project Properties Dialog
class ProjectPropertiesW(QDialog, ui_claudia_projectproperties.Ui_ProjectPropertiesW):
    def __init__(self, parent=None, name="", description="", notes=""):
        super(ProjectPropertiesW, self).__init__(parent)
        self.setupUi(self)

        self.default_name = name
        self.last_name = name
        self.data_to_return = [False, name, description, notes]
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (path and name) else False)

        self.connect(self, SIGNAL("accepted()"), self.setData)
        self.connect(self.le_name, SIGNAL("textChanged(QString)"), self.checkText_name)
        self.connect(self.cb_save_now, SIGNAL("clicked(bool)"), self.checkSaveNow)

        self.le_name.setText(QString(name))
        self.le_description.setText(QString(description))
        self.le_notes.setPlainText(QString(notes))

    def setData(self):
        if ("toPlainText" in dir(self.le_notes)): #PyQt Bug
          notes = self.le_notes.toPlainText()
        else:
          notes = self.le_notes.plainText()

        self.data_to_return = [self.cb_save_now.isChecked(), self.le_name.text(), self.le_description.text(), notes]

    def checkSaveNow(self, save):
        if (save):
          self.le_name.setText(self.last_name)
        else:
          self.last_name = self.le_name.text()
          self.le_name.setText(self.default_name)

    def checkText_name(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(not text.isEmpty())

# Add New App Dialog
class AddNewW(QWizard, ui_claudia_addnew.Ui_AddNewW):
    def __init__(self, parent=None):
        super(AddNewW, self).__init__(parent)
        self.setupUi(self)

        self.app_to_return = []
        self.tw_list_normal.setColumnWidth(0, 22)
        self.tw_list_normal.setColumnWidth(1, self.tw_list_normal.width()/4)
        self.tw_list_normal.setColumnWidth(2, self.tw_list_normal.width()/2)
        self.tw_list_normal.setColumnWidth(3, 40)
        self.rb_level_2.setEnabled(False)
        self.rb_level_3.setEnabled(False)

        i_normal = i_bristol = i_wine = i_vst = 0
        app_list = DBus.ladish_app_daemon.get_app_list()

        for i in range(len(app_list)):
          name = app_list[i]['name']
          generic_name = app_list[i]['generic_name']
          comment = app_list[i]['comment']
          icon  = app_list[i]['icon']
          exec_ = app_list[i]['exec']
          terminal = app_list[i]['terminal']
          type_ = app_list[i]['type']

          if (type_ == "normal"):
            w_name = QTableWidgetItem(name)
            w_comment = QTableWidgetItem(comment)
            w_terminal = QTableWidgetItem(self.tr("Yes") if (terminal == "true") else self.tr("No"))

            self.tw_list_normal.insertRow(i_normal)
            self.tw_list_normal.setItem(i_normal, 1, w_name)
            self.tw_list_normal.setItem(i_normal, 2, w_comment)
            self.tw_list_normal.setItem(i_normal, 3, w_terminal)

            i_normal += 1

        self.tw_list_normal.sortItems(1)

        self.connect(self, SIGNAL("currentIdChanged(int)"), self.changedPage)
        self.connect(self, SIGNAL("accepted()"), self.setApp)
        self.connect(self.rb_app_normal, SIGNAL("clicked()"), lambda index=0: self.stackedWidget.setCurrentIndex(index))
        self.connect(self.rb_app_bristol, SIGNAL("clicked()"), lambda index=1: self.stackedWidget.setCurrentIndex(index))
        self.connect(self.rb_app_wine, SIGNAL("clicked()"), lambda index=2: self.stackedWidget.setCurrentIndex(index))
        self.connect(self.rb_app_vst, SIGNAL("clicked()"), lambda index=3: self.stackedWidget.setCurrentIndex(index))
        self.connect(self.tw_list_normal, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection_normal)
        self.connect(self.tw_list_bristol, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection_bristol)
        self.connect(self.tw_list_wine, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection_wine)
        self.connect(self.tw_list_vst, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection_vst)

    def changedPage(self, index):
        if (index == 1):
          self.checkSelection()
        elif (index == 2):
          self.checkApp()

    def setApp(self):
        if (self.rb_level_0.isChecked()):
          level = 0
        elif (self.rb_level_1.isChecked()):
          level = 1
        elif (self.rb_level_2.isChecked()):
          level = 2
        elif (self.rb_level_3.isChecked()):
          level = 3
        else:
          level = 0

        self.app_to_return = [unicode(self.le_command.text()), unicode(self.le_name.text()), True if (self.cb_terminal.isChecked()) else False, level]

    def checkApp(self):
        if (self.stackedWidget.currentIndex() == 0): #normal
          item = self.tw_list_normal.item(self.tw_list_normal.currentRow(), 1)
        elif (self.stackedWidget.currentIndex() == 1): #bristol
          item = self.tw_list_bristol.item(self.tw_list_bristol.currentRow(), 1)
        elif (self.stackedWidget.currentIndex() == 2): #wine
          item = self.tw_list_wine.item(self.tw_list_wine.currentRow(), 1)
        elif (self.stackedWidget.currentIndex() == 3): #vst
          item = self.tw_list_vst.item(self.tw_list_vst.currentRow(), 1)
        else:
          print "invalid index of stackedWidget"
          return

        if not item:
          print "invalid item"
          return

        app_list = DBus.ladish_app_daemon.get_app_list()
        for i in range(len(app_list)):
          name = app_list[i]['name']
          generic_name = app_list[i]['generic_name']
          comment = app_list[i]['comment']
          icon  = app_list[i]['icon']
          exec_ = app_list[i]['exec']
          terminal = app_list[i]['terminal']
          type_ = app_list[i]['type']

          if (name == item.text()):
            break

        else:
          print "could not find app"
          return

        if (type_ == "normal"):
          self.le_command.setText(exec_)
          self.le_name.setText(name)
          self.cb_terminal.setChecked(True if (terminal == "true") else False)
          self.rb_level_0.setChecked(True)

    def checkSelection(self):
        if (self.rb_app_normal.isChecked()):
          self.button(QWizard.NextButton).setEnabled(True if (self.tw_list_normal.currentItem()) else False)
        elif (self.rb_app_bristol.isChecked()):
          self.button(QWizard.NextButton).setEnabled(True if (self.tw_list_bristol.currentItem()) else False)
        elif (self.rb_app_wine.isChecked()):
          self.button(QWizard.NextButton).setEnabled(True if (self.tw_list_wine.currentItem()) else False)
        elif (self.rb_app_vst.isChecked()):
          self.button(QWizard.NextButton).setEnabled(True if (self.tw_list_vst.currentItem()) else False)

    def checkSelection_normal(self, row, column, prev_row, prev_column):
        self.button(QWizard.NextButton).setEnabled(True if (row >= 0) else False)

    def checkSelection_bristol(self, row, column, prev_row, prev_column):
        self.button(QWizard.NextButton).setEnabled(True if (row >= 0) else False)

    def checkSelection_wine(self, row, column, prev_row, prev_column):
        self.button(QWizard.NextButton).setEnabled(True if (row >= 0) else False)

    def checkSelection_vst(self, row, column, prev_row, prev_column):
        self.button(QWizard.NextButton).setEnabled(True if (row >= 0) else False)

# Add New App Dialog (KXStudio)
class AddNewKXStudioW(QDialog, ui_claudia_addnew_kxstudio.Ui_AddNewKXStudioW):
    def __init__(self, parent=None, appBus=None, url_folder="", fake=True):
        super(AddNewKXStudioW, self).__init__(parent)
        self.setupUi(self)

        self.appBus = appBus
        self.url_folder = url_folder
        self.url_folder_is_fake = fake

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        # Load Settings (GUI state)
        self.settings = QSettings()
        if (self.settings.contains("KXStudioSplitterDAW")):
          self.loadSettings()
        else: # First-Run
          self.splitter_DAW.setSizes([500,200])
          self.splitter_Host.setSizes([500,200])
          self.splitter_Instrument.setSizes([500,200])
          self.splitter_Bristol.setSizes([500,200])
          self.splitter_Effect.setSizes([500,200])
          self.splitter_Tool.setSizes([500,200])

        # For the custom icons
        self.MyIcons = XIcon()
        self.MyIcons.addIconPath("/usr/share/cadence/icons/")
        self.MyIcons.addThemeName(str(unicode(QIcon.themeName()).encode('utf-8')))
        self.MyIcons.addThemeName("oxygen") #Just in case...

        # Set-up GUI
        self.listDAW.setColumnWidth(0, 22)
        self.listDAW.setColumnWidth(1, 150)
        self.listDAW.setColumnWidth(2, 125)

        self.listHost.setColumnWidth(0, 22)
        self.listHost.setColumnWidth(1, 150)

        self.listInstrument.setColumnWidth(0, 22)
        self.listInstrument.setColumnWidth(1, 150)
        self.listInstrument.setColumnWidth(2, 125)

        self.listBristol.setColumnWidth(0, 22)
        self.listBristol.setColumnWidth(1, 100)

        self.listEffect.setColumnWidth(0, 22)
        self.listEffect.setColumnWidth(1, 225)
        self.listEffect.setColumnWidth(2, 125)

        self.listTool.setColumnWidth(0, 22)
        self.listTool.setColumnWidth(1, 225)
        self.listTool.setColumnWidth(2, 125)

        self.listDAW.setContextMenuPolicy(Qt.CustomContextMenu)
        self.listHost.setContextMenuPolicy(Qt.CustomContextMenu)
        self.listInstrument.setContextMenuPolicy(Qt.CustomContextMenu)
        self.listBristol.setContextMenuPolicy(Qt.CustomContextMenu)
        self.listEffect.setContextMenuPolicy(Qt.CustomContextMenu)
        self.listTool.setContextMenuPolicy(Qt.CustomContextMenu)

        self.test_url = True
        self.test_selected = False

        self.clearInfo_DAW()
        self.clearInfo_Host()
        self.clearInfo_Intrument()
        self.clearInfo_Bristol()
        self.clearInfo_Effect()
        self.clearInfo_Tool()

        self.refreshAll()

        self.connect(self.tabWidget, SIGNAL("currentChanged(int)"), self.checkSelectedTab)

        self.connect(self.listDAW, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedDAW)
        self.connect(self.listHost, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedHost)
        self.connect(self.listInstrument, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedInstrument)
        self.connect(self.listBristol, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedBristol)
        self.connect(self.listEffect, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedEffect)
        self.connect(self.listTool, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelectedTool)
        self.connect(self.listDAW, SIGNAL("cellDoubleClicked(int, int)"), self.addApp)
        self.connect(self.listHost, SIGNAL("cellDoubleClicked(int, int)"), self.addApp)
        self.connect(self.listInstrument, SIGNAL("cellDoubleClicked(int, int)"), self.addApp)
        self.connect(self.listBristol, SIGNAL("cellDoubleClicked(int, int)"), self.addApp)
        self.connect(self.listEffect, SIGNAL("cellDoubleClicked(int, int)"), self.addApp)
        self.connect(self.listTool, SIGNAL("cellDoubleClicked(int, int)"), self.addApp)

        self.connect(self.url_documentation_daw, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_website_daw, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_documentation_host, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_website_host, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_documentation_ins, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_website_ins, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_documentation_bristol, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_website_bristol, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_documentation_effect, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_website_effect, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_documentation_tool, SIGNAL("leftClickedUrl(QString)"), self.openUrl)
        self.connect(self.url_website_tool, SIGNAL("leftClickedUrl(QString)"), self.openUrl)

        self.connect(self, SIGNAL("accepted()"), self.addApp)

    def getSelectedApp(self):
        tab = self.tabWidget.currentIndex()
        if (tab == 0):
          listSel = self.listDAW
        elif (tab == 1):
          listSel = self.listHost
        elif (tab == 2):
          listSel = self.listInstrument
        elif (tab == 3):
          listSel = self.listBristol
        elif (tab == 4):
          listSel = self.listEffect
        elif (tab == 5):
          listSel = self.listTool
        else:
          return ""

        return listSel.item(listSel.currentRow(), 1 if (tab != 3) else 2).text()

    def getBinaryFromAppName(self, appname):
        list_DAW = database.list_DAW
        for i in range(len(list_DAW)):
          if (appname == list_DAW[i][1]):
              binary = list_DAW[i][3]
              break

        list_Host = database.list_Host
        for i in range(len(list_Host)):
          if (appname == list_Host[i][1]):
              binary = list_Host[i][4]
              break

        list_Instrument = database.list_Instrument
        for i in range(len(list_Instrument)):
          if (appname == list_Instrument[i][1]):
              binary = list_Instrument[i][3]
              break

        list_Bristol = database.list_Bristol
        for i in range(len(list_Bristol)):
          if (appname == list_Bristol[i][1]):
              binary = "startBristol -audio jack -midi jack -"+list_Bristol[i][3]
              break

        list_Effect = database.list_Effect
        for i in range(len(list_Effect)):
          if (appname == list_Effect[i][1]):
              binary = list_Effect[i][3]
              break

        list_Tool = database.list_Tool
        for i in range(len(list_Tool)):
          if (appname == list_Tool[i][1]):
              binary = list_Tool[i][3]
              break

        if (binary):
          return binary
        else:
          print "Error here, cod.002"
          return ""

    def addApp(self):
        app = self.getSelectedApp()
        binary = self.getBinaryFromAppName(app)

        ran_check = str(randint(1, 99999))
        sample_rate = str(int(DBus.jack.GetSampleRate()))
        url_folder = str(unicode(self.url_folder).encode('utf-8'))
        proj_bpm = str("120.0") #TODO - get proper BPM when code implemented (BPM/BBT)

        if (url_folder[-1] != "/"):
          url_folder += "/"

        if not os.path.exists(url_folder):
          os.mkdir(url_folder)

        if (binary.split(" ")[0] == "startBristol"):
          synth = binary.split("-audio jack -midi jack -")[1]
          proj_folder = url_folder+"bristol_"+synth+"_"+ran_check
          os.mkdir(proj_folder)
          if (not self.url_folder_is_fake): proj_folder = proj_folder.replace(url_folder,"")
          cmd = binary+" -emulate "+synth+" -cache "+proj_folder+" -memdump "+proj_folder+" -import "+proj_folder+"/memory -exec"
          self.appBus.RunCustom(False, cmd, str(app), 1)

        elif (app == "Ardour" or app == "ArdourVST" or app == "ArdourVST (32bit)"):
          proj_folder = url_folder+"Ardour2_"+ran_check
          os.mkdir(proj_folder)
          os.system("cp "+sys.path[0]+"/../templates/Ardour2/* '"+proj_folder+"'")
          os.system('sed -i "s/X_BPM_X-KLAUDIA-X_BPM_X/'+proj_bpm+'/" "'+proj_folder+'/Ardour2.ardour"')
          os.system('sed -i "s/X_SR_X-KLAUDIA-X_SR_X/'+sample_rate+'/" "'+proj_folder+'/Ardour2.ardour"')
          os.system("cd '"+proj_folder+"' && mv Ardour2.ardour Ardour2_"+ran_check+".ardour")
          os.mkdir(proj_folder+"/analysis")
          os.mkdir(proj_folder+"/dead_sounds")
          os.mkdir(proj_folder+"/export")
          os.mkdir(proj_folder+"/interchange")
          os.mkdir(proj_folder+"/interchange/Ardour")
          os.mkdir(proj_folder+"/interchange/Ardour/audiofiles")
          os.mkdir(proj_folder+"/peaks")
          if (not self.url_folder_is_fake): proj_folder = proj_folder.replace(url_folder,"")
          self.appBus.RunCustom(False, binary+" '"+proj_folder+"'", str(app), 1)
        elif (app == "Hydrogen" or app == "Hydrogen (SVN)"):
          if (app == "Hydrogen (SVN)"):
            level = 1
          else:
            level = 0
          proj_file = url_folder+"Hydrogen_"+ran_check+".h2song"
          os.system("cp "+sys.path[0]+"/../templates/Hydrogen.h2song '"+proj_file+"'")
          os.system('sed -i "s/X_BPM_X-KLAUDIA-X_BPM_X/'+proj_bpm+'/" "'+proj_file+'"')
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "hydrogen -s '"+proj_file+"'", str(app), level)
        elif (app == "MusE" or app == "MusE (SVN)"):
          proj_file = url_folder+"MusE_"+ran_check+".med"
          os.system("cp "+sys.path[0]+"/../templates/MusE.med '"+proj_file+"'")
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "muse '"+proj_file+"'", str(app), 0)
        elif (app == "Non-DAW"):
          proj_file = url_folder+"Non-DAW_"+ran_check
          os.system("cp -r "+sys.path[0]+"/../templates/Non-DAW '"+proj_file+"'")
          os.system('sed -i "s/X_BPM_X-KLAUDIA-X_BPM_X/'+proj_bpm+'/" "'+proj_file+'/history"')
          os.system('sed -i "s/X_SR_X-KLAUDIA-X_SR_X/'+sample_rate+'/" "'+proj_file+'/info"')
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "non-daw '"+proj_file+"'", str(app), 0)
        elif (app == "Non-Sequencer"):
          proj_file = url_folder+"Non-Sequencer_"+ran_check+".non"
          os.system("cp "+sys.path[0]+"/../templates/Non-Sequencer.non '"+proj_file+"'")
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "non-sequencer '"+proj_file+"'", str(app), 0)
        elif (app == "Qtractor" or app == "QtractorVST" or app == "QtractorVST (32bit)" or app == "Qtractor (SVN)"):
          proj_file = url_folder+"Qtractor_"+ran_check+".qtr"
          os.system("cp "+sys.path[0]+"/../templates/Qtractor.qtr '"+proj_file+"'")
          os.system('sed -i "s/X_BPM_X-KLAUDIA-X_BPM_X/'+proj_bpm+'/" "'+proj_file+'"')
          os.system('sed -i "s/X_SR_X-KLAUDIA-X_SR_X/'+sample_rate+'/" "'+proj_file+'"')
          os.system('sed -i "s/X_FOLDER_X-KLAUDIA-X_FOLDER_X/'+url_folder.replace("/","\/")+'/" "'+proj_file+'"')
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, binary+" '"+proj_file+"'", str(app), 1)
        elif (app == "Renoise" or app == "Renoise (64bit)"):
          os.mkdir(url_folder+"tmp_bu")
          os.system("cp "+sys.path[0]+"/../templates/Renoise.xml "+url_folder+"/tmp_bu")
          os.system('sed -i "s/X_BPM_X-KLAUDIA-X_BPM_X/'+proj_bpm+'/" "'+url_folder+'/tmp_bu/Renoise.xml"')
          os.system("cd '"+url_folder+"tmp_bu' && mv Renoise.xml Song.xml && zip ../Renoise_"+ran_check+".xrns Song.xml")
          os.system("rm -rf '"+url_folder+"tmp_bu'")
          url_folder = ""
          self.appBus.RunCustom(False, binary+" '"+url_folder+"Renoise_"+ran_check+".xrns'", str(app), 0)
        elif (app == "Rosegarden"):
          proj_file = url_folder+"Rosegarden_"+ran_check+".rg"
          os.system("cp "+sys.path[0]+"/../templates/Rosegarden.rg '"+proj_file+"'")
          os.system('sed -i "s/X_BPM_X-KLAUDIA-X_BPM_X/'+proj_bpm+'/" "'+proj_file+'"')
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "rosegarden '"+proj_file+"'", str(app), 1)
        elif (app == "Seq24"):
          proj_file = url_folder+"Seq24_"+ran_check+".midi"
          os.system("cp "+sys.path[0]+"/../templates/Seq24.midi '"+proj_file+"'")
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "seq24 '"+proj_file+"'", str(app), 1)

        elif (app == "Calf Jack Host (GIT)"):
          proj_file = url_folder+"CalfJackHost_"+ran_check
          os.system("cp "+sys.path[0]+"/../templates/CalfJackHost '"+proj_file+"'")
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          DBus.appBus.RunCustom(False, "calfjackhost --load '"+proj_file+"'", str(app), 1)
        elif (app == "FeSTige"):
          room_name = ""
          if (DBus.ladish_room):
            room_name = DBus.ladish_room.GetName()
          self.appBus.RunCustom(False, "festige "+url_folder+" "+room_name, str(app), 0)
        elif (app == "Jack Rack"):
          proj_file = url_folder+"Jack-Rack_"+ran_check+".xml"
          os.system("cp "+sys.path[0]+"/../templates/Jack-Rack.xml '"+proj_file+"'")
          os.system('sed -i "s/X_SR_X-KLAUDIA-X_SR_X/'+sample_rate+'/" "'+proj_file+'"')
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "jack-rack '"+proj_file+"'", str(app), 0)

        elif (app == "Qsampler" or app == "Qsampler (SVN)"):
          if (app == "Qsampler (SVN)"):
            level = 1
          else:
            level = 0
          proj_file = url_folder+"Qsampler_"+ran_check+".lscp"
          os.system("cp "+sys.path[0]+"/../templates/Qsampler.lscp '"+proj_file+"'")
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "qsampler '"+proj_file+"'", str(app), level)
        elif (app == "Yoshimi"):
          proj_file = url_folder+"Yoshimi_"+ran_check+".state"
          os.system("cp "+sys.path[0]+"/../templates/Yoshimi.state '"+proj_file+"'")
          os.system('sed -i "s/X_SR_X-KLAUDIA-X_SR_X/'+sample_rate+'/" "'+proj_file+'"')
          os.system('sed -i "s/X_FILE_X-KLAUDIA-X_FILE_X/'+proj_file.replace("/","\/")+'/" "'+proj_file+'"')
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "yoshimi -j -J --state='"+proj_file+"'", str(app), 1)

        elif (app == "Jamin"):
          proj_file = url_folder+"Jamin_"+ran_check+".jam"
          os.system("cp "+sys.path[0]+"/../templates/Jamin.jam '"+proj_file+"'")
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "jamin -f '"+proj_file+"'", str(app), 0)

        elif (app == "Jack Mixer"):
          proj_file = url_folder+"Jack-Mixer_"+ran_check+".xml"
          os.system("cp "+sys.path[0]+"/../templates/Jack-Mixer.xml '"+proj_file+"'")
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "jack_mixer -c '"+proj_file+"'", str(app), 1)
        elif (app == "Non-Mixer"):
          proj_file = url_folder+"Non-Mixer_"+ran_check
          os.system("cp -r "+sys.path[0]+"/../templates/Non-Mixer '"+proj_file+"'")
          if (not self.url_folder_is_fake): proj_file = proj_file.replace(url_folder,"")
          self.appBus.RunCustom(False, "non-mixer '"+proj_file+"'", str(app), 0)

        else:
          self.appBus.RunCustom(False, binary, str(app), 0)

        self.close()

    def clearInfo_DAW(self):
        self.ico_app_daw.setPixmap(QIcon.fromTheme("start-here").pixmap(48, 48))
        self.label_name_daw.setText("App Name")
        self.ico_ladspa_daw.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.ico_dssi_daw.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.ico_lv2_daw.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.ico_vst_daw.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.label_vst_mode_daw.setText("")
        self.ico_jack_transport_daw.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.label_midi_mode_daw.setText("---")
        self.label_ladish_level_daw.setText("0")
        self.showDoc_DAW(0, 0)
        self.frame_DAW.setEnabled(False)

    def clearInfo_Host(self):
        self.ico_app_host.setPixmap(QIcon.fromTheme("start-here").pixmap(48, 48))
        self.label_name_host.setText("App Name")
        self.ico_ladspa_host.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.ico_dssi_host.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.ico_lv2_host.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.ico_vst_host.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.label_vst_mode_host.setText("")
        self.label_midi_mode_host.setText("---")
        self.label_ladish_level_host.setText("0")
        self.showDoc_Host(0, 0)
        self.frame_Host.setEnabled(False)

    def clearInfo_Intrument(self):
        self.ico_app_ins.setPixmap(QIcon.fromTheme("start-here").pixmap(48, 48))
        self.label_name_ins.setText("App Name")
        self.ico_builtin_fx_ins.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.ico_audio_input_ins.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.label_midi_mode_ins.setText("---")
        self.label_ladish_level_ins.setText("0")
        self.showDoc_Instrument(0, 0)
        self.frame_Instrument.setEnabled(False)

    def clearInfo_Bristol(self):
        self.ico_app_bristol.setPixmap(QIcon.fromTheme("start-here").pixmap(48, 48))
        self.label_name_bristol.setText("App Name")
        self.ico_builtin_fx_bristol.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.ico_audio_input_bristol.setPixmap(QIcon("dialog-cancel").pixmap(16, 16))
        self.label_midi_mode_bristol.setText("---")
        self.label_ladish_level_bristol.setText("0")
        self.showDoc_Bristol(0, 0)
        self.frame_Bristol.setEnabled(False)

    def clearInfo_Effect(self):
        self.ico_app_effect.setPixmap(QIcon.fromTheme("start-here").pixmap(48, 48))
        self.label_name_effect.setText("App Name")
        self.ico_stereo_effect.setPixmap(QIcon.fromTheme("dialog-cancel").pixmap(16, 16))
        self.label_midi_mode_effect.setText("---")
        self.label_ladish_level_effect.setText("0")
        self.showDoc_Effect(0, 0)
        self.frame_Effect.setEnabled(False)

    def clearInfo_Tool(self):
        self.ico_app_tool.setPixmap(QIcon.fromTheme("start-here").pixmap(48, 48))
        self.label_name_tool.setText("App Name")
        self.label_midi_mode_tool.setText("---")
        self.label_ladish_level_tool.setText("0")
        self.showDoc_Tool(0, 0)
        self.frame_Tool.setEnabled(False)

    def checkSelectedTab(self, tab):
        self.test_selected = False
        if (tab == 0):  # DAW
          if (self.listDAW.selectedItems()):
            self.test_selected = True
        elif (tab == 1):  # Host
          if (self.listHost.selectedItems()):
            self.test_selected = True
        elif (tab == 2):  # Instrument
          if (self.listInstrument.selectedItems()):
            self.test_selected = True
        elif (tab == 3):  # Bristol
          if (self.listBristol.selectedItems()):
            self.test_selected = True
        elif (tab == 4):  # Effect
          if (self.listEffect.selectedItems()):
            self.test_selected = True
        elif (tab == 5):  # Tool
          if (self.listTool.selectedItems()):
            self.test_selected = True

        self.checkButtons()

    def checkSelectedDAW(self, row, column, p_row, p_column):
        if (row >= 0):
          self.test_selected = True

          app_name = self.listDAW.item(row, 1).text()
          app_info = []
          list_DAW = database.list_DAW

          for i in range(len(list_DAW)):
            if (app_name == list_DAW[i][1]):
              app_info = list_DAW[i]
              break

          if (not app_info):
            print "Error here, cod.001"
            return

          self.frame_DAW.setEnabled(True)
          self.ico_app_daw.setPixmap(QIcon(self.MyIcons.getIconPath(app_info[4], 48)).pixmap(48, 48))
          self.ico_ladspa_daw.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][0]), 16)).pixmap(16, 16))
          self.ico_dssi_daw.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][1]), 16)).pixmap(16, 16))
          self.ico_lv2_daw.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][2]), 16)).pixmap(16, 16))
          self.ico_vst_daw.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][3]), 16)).pixmap(16, 16))
          self.ico_jack_transport_daw.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][5]), 16)).pixmap(16, 16))
          self.label_name_daw.setText(app_info[1])
          self.label_vst_mode_daw.setText(app_info[8][4])
          self.ico_midi_mode_daw.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][6]), 16)).pixmap(16, 16))
          self.label_midi_mode_daw.setText(app_info[8][7])
          self.label_ladish_level_daw.setText(str(app_info[6]))

          doc = app_info[9][0] if (os.path.exists(app_info[9][0].replace("file://",""))) else ""
          self.showDoc_DAW(doc, app_info[9][1])
        else:
          self.test_selected = False
          self.clearInfo_DAW()
        self.checkButtons()

    def checkSelectedHost(self, row, column, p_row, p_column):
        if (row >= 0):
          self.test_selected = True

          app_name = self.listHost.item(row, 1).text()
          app_info = []
          list_Host = database.list_Host

          for i in range(len(list_Host)):
            if (app_name == list_Host[i][1]):
              app_info = list_Host[i]
              break

          if (not app_info):
            print "Error here, cod.003"
            return

          self.frame_Host.setEnabled(True)
          self.ico_app_host.setPixmap(QIcon(self.MyIcons.getIconPath(app_info[5], 48)).pixmap(48, 48))
          self.ico_internal_host.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[9][0]), 16)).pixmap(16, 16))
          self.ico_ladspa_host.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[9][1]), 16)).pixmap(16, 16))
          self.ico_dssi_host.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[9][2]), 16)).pixmap(16, 16))
          self.ico_lv2_host.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[9][3]), 16)).pixmap(16, 16))
          self.ico_vst_host.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[9][4]), 16)).pixmap(16, 16))
          self.label_name_host.setText(app_info[1])
          self.label_vst_mode_host.setText(app_info[9][5])
          self.label_midi_mode_host.setText(app_info[9][6])
          self.label_ladish_level_host.setText(str(app_info[7]))

          doc = app_info[10][0] if (os.path.exists(app_info[10][0].replace("file://",""))) else ""
          self.showDoc_Host(doc, app_info[10][1])
        else:
          self.test_selected = False
          self.clearInfo_DAW()
        self.checkButtons()

    def checkSelectedInstrument(self, row, column, p_row, p_column):
        if (row >= 0):
          self.test_selected = True

          app_name = self.listInstrument.item(row, 1).text()
          app_info = []
          list_Instrument = database.list_Instrument

          for i in range(len(list_Instrument)):
            if (app_name == list_Instrument[i][1]):
              app_info = list_Instrument[i]
              break

          if (not app_info):
            print "Error here, cod.004"
            return

          self.frame_Instrument.setEnabled(True)
          self.ico_app_ins.setPixmap(QIcon(self.MyIcons.getIconPath(app_info[4], 48)).pixmap(48, 48))
          self.ico_builtin_fx_ins.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][0]), 16)).pixmap(16, 16))
          self.ico_audio_input_ins.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][1]), 16)).pixmap(16, 16))
          self.label_name_ins.setText(app_info[1])
          self.label_midi_mode_ins.setText(app_info[8][2])
          self.label_ladish_level_ins.setText(str(app_info[6]))

          doc = app_info[9][0] if (os.path.exists(app_info[9][0].replace("file://",""))) else ""
          self.showDoc_Instrument(doc, app_info[9][1])
        else:
          self.test_selected = False
          self.clearInfo_Intrument()
        self.checkButtons()

    def checkSelectedBristol(self, row, column, p_row, p_column):
        if (row >= 0):
          self.test_selected = True

          app_name = self.listBristol.item(row, 2).text()
          app_info = []
          list_Bristol = database.list_Bristol

          for i in range(len(list_Bristol)):
            if (app_name == list_Bristol[i][1]):
              app_info = list_Bristol[i]
              break

          if (not app_info):
            print "Error here, cod.007"
            return

          self.frame_Bristol.setEnabled(True)
          self.ico_app_bristol.setPixmap(QIcon(self.MyIcons.getIconPath(app_info[4], 48)).pixmap(48, 48))
          self.ico_builtin_fx_bristol.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][0]), 16)).pixmap(16, 16))
          self.ico_audio_input_bristol.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][1]), 16)).pixmap(16, 16))
          self.label_name_bristol.setText(app_info[1])
          self.label_midi_mode_bristol.setText(app_info[8][2])
          self.label_ladish_level_bristol.setText(str(app_info[6]))

          doc = app_info[9][0] if (os.path.exists(app_info[9][0].replace("file://",""))) else ""
          self.showDoc_Bristol(doc, app_info[9][1])
        else:
          self.test_selected = False
          self.clearInfo_Bristol()
        self.checkButtons()

    def checkSelectedEffect(self, row, column, p_row, p_column):
        if (row >= 0):
          self.test_selected = True

          app_name = self.listEffect.item(row, 1).text()
          app_info = []
          list_Effect = database.list_Effect

          for i in range(len(list_Effect)):
            if (app_name == list_Effect[i][1]):
              app_info = list_Effect[i]
              break

          if (not app_info):
            print "Error here, cod.005"
            return

          self.frame_Effect.setEnabled(True)
          self.ico_app_effect.setPixmap(QIcon(self.MyIcons.getIconPath(app_info[4], 48)).pixmap(48, 48))
          self.ico_stereo_effect.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][0]), 16)).pixmap(16, 16))
          self.label_name_effect.setText(app_info[1])
          self.label_midi_mode_effect.setText(app_info[8][1])
          self.label_ladish_level_effect.setText(str(app_info[6]))

          doc = app_info[9][0] if (os.path.exists(app_info[9][0].replace("file://",""))) else ""
          self.showDoc_Effect(doc, app_info[9][1])
        else:
          self.test_selected = False
          self.clearInfo_Effect()
        self.checkButtons()

    def checkSelectedTool(self, row, column, p_row, p_column):
        if (row >= 0):
          self.test_selected = True

          app_name = self.listTool.item(row, 1).text()
          app_info = []
          list_Tool = database.list_Tool

          for i in range(len(list_Tool)):
            if (app_name == list_Tool[i][1]):
              app_info = list_Tool[i]
              break

          if (not app_info):
            print "Error here, cod.006"
            return

          self.frame_Tool.setEnabled(True)
          self.ico_app_tool.setPixmap(QIcon(self.MyIcons.getIconPath(app_info[4], 48)).pixmap(48, 48))
          self.label_name_tool.setText(app_info[1])
          self.label_midi_mode_tool.setText(app_info[8][0])
          self.ico_jack_transport_tool.setPixmap(QIcon(self.MyIcons.getIconPath(self.getIconForYesNo(app_info[8][1]), 16)).pixmap(16, 16))
          self.label_ladish_level_tool.setText(str(app_info[6]))

          doc = app_info[9][0] if (os.path.exists(app_info[9][0].replace("file://",""))) else ""
          self.showDoc_Tool(doc, app_info[9][1])
        else:
          self.test_selected = False
          self.clearInfo_Tool()
        self.checkButtons()

    def setDocUrl(self, item, link):
        item.setText(self.tr("<a href='%1'>Documentation</a>").arg(link))

    def setWebUrl(self, item, link):
        item.setText(self.tr("<a href='%1'>WebSite</a>").arg(link))

    def showDoc_DAW(self, doc, web):
        if (doc):
          self.url_documentation_daw.setVisible(True)
          self.setDocUrl(self.url_documentation_daw, doc)
        else: self.url_documentation_daw.setVisible(False)

        if (web):
          self.url_website_daw.setVisible(True)
          self.setWebUrl(self.url_website_daw, web)
        else: self.url_website_daw.setVisible(False)

        if (not doc and not web):
          self.label_no_help_daw.setVisible(True)
        else:
          self.label_no_help_daw.setVisible(False)

    def showDoc_Host(self, doc, web):
        if (doc):
          self.url_documentation_host.setVisible(True)
          self.setDocUrl(self.url_documentation_host, doc)
        else: self.url_documentation_host.setVisible(False)

        if (web):
          self.url_website_host.setVisible(True)
          self.setWebUrl(self.url_website_host, web)
        else: self.url_website_host.setVisible(False)

        if (not doc and not web):
          self.label_no_help_host.setVisible(True)
        else:
          self.label_no_help_host.setVisible(False)

    def showDoc_Instrument(self, doc, web):
        if (doc):
          self.url_documentation_ins.setVisible(True)
          self.setDocUrl(self.url_documentation_ins, doc)
        else: self.url_documentation_ins.setVisible(False)

        if (web):
          self.url_website_ins.setVisible(True)
          self.setWebUrl(self.url_website_ins, web)
        else: self.url_website_ins.setVisible(False)

        if (not doc and not web):
          self.label_no_help_ins.setVisible(True)
        else:
          self.label_no_help_ins.setVisible(False)

    def showDoc_Bristol(self, doc, web):
        if (doc):
          self.url_documentation_bristol.setVisible(True)
          self.setDocUrl(self.url_documentation_bristol, doc)
        else: self.url_documentation_bristol.setVisible(False)

        if (web):
          self.url_website_bristol.setVisible(True)
          self.setWebUrl(self.url_website_bristol, web)
        else: self.url_website_bristol.setVisible(False)

        if (not doc and not web):
          self.label_no_help_bristol.setVisible(True)
        else:
          self.label_no_help_bristol.setVisible(False)

    def showDoc_Effect(self, doc, web):
        if (doc):
          self.url_documentation_effect.setVisible(True)
          self.setDocUrl(self.url_documentation_effect, doc)
        else: self.url_documentation_effect.setVisible(False)

        if (web):
          self.url_website_effect.setVisible(True)
          self.setWebUrl(self.url_website_effect, web)
        else: self.url_website_effect.setVisible(False)

        if (not doc and not web):
          self.label_no_help_effect.setVisible(True)
        else:
          self.label_no_help_effect.setVisible(False)

    def showDoc_Tool(self, doc, web):
        if (doc):
          self.url_documentation_tool.setVisible(True)
          self.setDocUrl(self.url_documentation_tool, doc)
        else: self.url_documentation_tool.setVisible(False)

        if (web):
          self.url_website_tool.setVisible(True)
          self.setWebUrl(self.url_website_tool, web)
        else: self.url_website_tool.setVisible(False)

        if (not doc and not web):
          self.label_no_help_tool.setVisible(True)
        else:
          self.label_no_help_tool.setVisible(False)

    def openUrl(self, url):
        os.system("xdg-open '"+str(url)+"' &")

    def checkButtons(self):
        if (self.test_url and self.test_selected):
          self.setOk(True)
        else:
          self.setOk(False)

    def clearAll(self):
        self.listDAW.clearContents()
        self.listHost.clearContents()
        self.listInstrument.clearContents()
        self.listBristol.clearContents()
        self.listEffect.clearContents()
        self.listTool.clearContents()
        for i in range(self.listDAW.rowCount()):
          self.listDAW.removeRow(0)
        for i in range(self.listHost.rowCount()):
          self.listHost.removeRow(0)
        for i in range(self.listInstrument.rowCount()):
          self.listInstrument.removeRow(0)
        for i in range(self.listBristol.rowCount()):
          self.listBristol.removeRow(0)
        for i in range(self.listEffect.rowCount()):
          self.listEffect.removeRow(0)
        for i in range(self.listTool.rowCount()):
          self.listTool.removeRow(0)

    def getIconForYesNo(self, yesno):
        if (yesno):
          return "dialog-ok-apply"
        else:
          return "dialog-cancel"

    def refreshAll(self):
        self.clearAll()

        SHOW_ALL = False

        if (not SHOW_ALL):
          pkg_out = getoutput("dpkg -l").split("\n")
          pkglist = []
          for i in range(len(pkg_out)):
            if (pkg_out[i][0] == "i" and pkg_out[i][1] == "i"):
              package = pkg_out[i].split()[1]
              pkglist.append(package)

        last_pos = 0
        list_DAW = database.list_DAW
        for i in range(len(list_DAW)):
          if (SHOW_ALL or list_DAW[i][0] in pkglist):
            AppName = list_DAW[i][1]
            Type    = list_DAW[i][2]
            Binary  = list_DAW[i][3]
            Icon    = list_DAW[i][4]
            Save    = list_DAW[i][5]
            Level   = list_DAW[i][6]
            Licence = list_DAW[i][7]
            Features = list_DAW[i][8]
            Docs    = list_DAW[i][9]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.MyIcons.getIconPath(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(Licence)

            self.listDAW.insertRow(last_pos)
            self.listDAW.setItem(last_pos, 0, w_icon)
            self.listDAW.setItem(last_pos, 1, w_name)
            self.listDAW.setItem(last_pos, 2, w_type)
            self.listDAW.setItem(last_pos, 3, w_save)
            self.listDAW.setItem(last_pos, 4, w_licc)

            last_pos += 1

        last_pos = 0
        list_Host = database.list_Host
        for i in range(len(list_Host)):
          if (SHOW_ALL or list_Host[i][0] in pkglist):
            AppName = list_Host[i][1]
            Has_Ins = list_Host[i][2]
            Has_Eff = list_Host[i][3]
            Binary  = list_Host[i][4]
            Icon    = list_Host[i][5]
            Save    = list_Host[i][6]
            Level   = list_Host[i][7]
            Licence = list_Host[i][8]
            Features = list_Host[i][9]
            Docs    = list_Host[i][10]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.MyIcons.getIconPath(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_h_in = QTableWidgetItem(Has_Ins)
            w_h_ef = QTableWidgetItem(Has_Eff)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(Licence)

            self.listHost.insertRow(last_pos)
            self.listHost.setItem(last_pos, 0, w_icon)
            self.listHost.setItem(last_pos, 1, w_name)
            self.listHost.setItem(last_pos, 2, w_h_in)
            self.listHost.setItem(last_pos, 3, w_h_ef)
            self.listHost.setItem(last_pos, 4, w_save)
            self.listHost.setItem(last_pos, 5, w_licc)

            last_pos += 1

        last_pos = 0
        list_Instrument = database.list_Instrument
        for i in range(len(list_Instrument)):
          if (SHOW_ALL or list_Instrument[i][0] in pkglist):
            AppName = list_Instrument[i][1]
            Type    = list_Instrument[i][2]
            Binary  = list_Instrument[i][3]
            Icon    = list_Instrument[i][4]
            Save    = list_Instrument[i][5]
            Level   = list_Instrument[i][6]
            Licence = list_Instrument[i][7]
            Features = list_Instrument[i][8]
            Docs    = list_Instrument[i][9]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.MyIcons.getIconPath(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(Licence)

            self.listInstrument.insertRow(last_pos)
            self.listInstrument.setItem(last_pos, 0, w_icon)
            self.listInstrument.setItem(last_pos, 1, w_name)
            self.listInstrument.setItem(last_pos, 2, w_type)
            self.listInstrument.setItem(last_pos, 3, w_save)
            self.listInstrument.setItem(last_pos, 4, w_licc)

            last_pos += 1

        last_pos = 0
        list_Bristol = database.list_Bristol
        for i in range(len(list_Bristol)):
          if (SHOW_ALL or list_Bristol[i][0] in pkglist):
            FullName  = list_Bristol[i][1]
            Type      = list_Bristol[i][2]
            ShortName = list_Bristol[i][3]
            Icon      = list_Bristol[i][4]
            Save      = list_Bristol[i][5]
            Level     = list_Bristol[i][6]
            Licence   = list_Bristol[i][7]
            Features = list_Bristol[i][8]
            Docs    = list_Bristol[i][9]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.MyIcons.getIconPath(Icon, 16)))
            w_fullname  = QTableWidgetItem(FullName)
            w_shortname = QTableWidgetItem(ShortName)

            self.listBristol.insertRow(last_pos)
            self.listBristol.setItem(last_pos, 0, w_icon)
            self.listBristol.setItem(last_pos, 1, w_shortname)
            self.listBristol.setItem(last_pos, 2, w_fullname)

            last_pos += 1

        last_pos = 0
        list_Effect = database.list_Effect
        for i in range(len(list_Effect)):
          if (SHOW_ALL or list_Effect[i][0] in pkglist):
            AppName = list_Effect[i][1]
            Type    = list_Effect[i][2]
            Binary  = list_Effect[i][3]
            Icon    = list_Effect[i][4]
            Save    = list_Effect[i][5]
            Level   = list_Effect[i][6]
            Licence = list_Effect[i][7]
            Features = list_Effect[i][8]
            Docs    = list_Effect[i][9]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.MyIcons.getIconPath(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(Licence)

            self.listEffect.insertRow(last_pos)
            self.listEffect.setItem(last_pos, 0, w_icon)
            self.listEffect.setItem(last_pos, 1, w_name)
            self.listEffect.setItem(last_pos, 2, w_type)
            self.listEffect.setItem(last_pos, 3, w_save)
            self.listEffect.setItem(last_pos, 4, w_licc)

            last_pos += 1

        last_pos = 0
        list_Tool = database.list_Tool
        for i in range(len(list_Tool)):
          if (SHOW_ALL or list_Tool[i][0] in pkglist):
            AppName = list_Tool[i][1]
            Type    = list_Tool[i][2]
            Binary  = list_Tool[i][3]
            Icon    = list_Tool[i][4]
            Save    = list_Tool[i][5]
            Level   = list_Tool[i][6]
            Licence = list_Tool[i][7]
            Features = list_Tool[i][8]
            Docs    = list_Tool[i][9]

            w_icon = QTableWidgetItem("")
            w_icon.setIcon(QIcon(self.MyIcons.getIconPath(Icon, 16)))
            w_name = QTableWidgetItem(AppName)
            w_type = QTableWidgetItem(Type)
            w_save = QTableWidgetItem(Save)
            w_licc = QTableWidgetItem(Licence)

            self.listTool.insertRow(last_pos)
            self.listTool.setItem(last_pos, 0, w_icon)
            self.listTool.setItem(last_pos, 1, w_name)
            self.listTool.setItem(last_pos, 2, w_type)
            self.listTool.setItem(last_pos, 3, w_save)
            self.listTool.setItem(last_pos, 4, w_licc)

            last_pos += 1

        self.listDAW.setCurrentCell(-1, -1)
        self.listHost.setCurrentCell(-1, -1)
        self.listInstrument.setCurrentCell(-1, -1)
        self.listBristol.setCurrentCell(-1, -1)
        self.listEffect.setCurrentCell(-1, -1)
        self.listTool.setCurrentCell(-1, -1)

        self.listDAW.sortByColumn(1, Qt.AscendingOrder)
        self.listHost.sortByColumn(1, Qt.AscendingOrder)
        self.listInstrument.sortByColumn(1, Qt.AscendingOrder)
        self.listBristol.sortByColumn(2, Qt.AscendingOrder)
        self.listEffect.sortByColumn(1, Qt.AscendingOrder)
        self.listTool.sortByColumn(1, Qt.AscendingOrder)

    def saveSettings(self):
        self.settings.setValue("KXStudioSplitterDAW", self.splitter_DAW.saveState())
        self.settings.setValue("KXStudioSplitterHost", self.splitter_Host.saveState())
        self.settings.setValue("KXStudioSplitterInstrument", self.splitter_Instrument.saveState())
        self.settings.setValue("KXStudioSplitterBristol", self.splitter_Bristol.saveState())
        self.settings.setValue("KXStudioSplitterEffect", self.splitter_Effect.saveState())
        self.settings.setValue("KXStudioSplitterTool", self.splitter_Tool.saveState())

    def loadSettings(self):
        self.splitter_DAW.restoreState(self.settings.value("KXStudioSplitterDAW").toByteArray())
        self.splitter_Host.restoreState(self.settings.value("KXStudioSplitterHost").toByteArray())
        self.splitter_Instrument.restoreState(self.settings.value("KXStudioSplitterInstrument").toByteArray())
        self.splitter_Bristol.restoreState(self.settings.value("KXStudioSplitterBristol").toByteArray())
        self.splitter_Effect.restoreState(self.settings.value("KXStudioSplitterEffect").toByteArray())
        self.splitter_Tool.restoreState(self.settings.value("KXStudioSplitterTool").toByteArray())

    def closeEvent(self, event):
        self.saveSettings()
        event.accept()

    def setOk(self, yesno):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(yesno)

# Run Custom App Dialog
class RunCustomW(QDialog, ui_claudia_runcustom.Ui_RunCustomW):
    def __init__(self, parent=None, command="", name="", terminal=False, level=0, active=False):
        super(RunCustomW, self).__init__(parent)
        self.setupUi(self)

        self.app_to_return = []
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if command else False)

        self.le_command.setText(command)
        self.le_name.setText(name)
        self.cb_terminal.setChecked(terminal)

        if (level == 0):
          self.rb_level_0.setChecked(True)
        elif (level == 1):
          self.rb_level_1.setChecked(True)
        elif (level == 2):
          self.rb_level_2.setChecked(True)
        elif (level == 3):
          self.rb_level_3.setChecked(True)
        else:
          self.rb_level_0.setChecked(True)

        if (active):
          self.le_command.setEnabled(False)
          self.cb_terminal.setEnabled(False)
          self.rb_level_0.setEnabled(False)
          self.rb_level_1.setEnabled(False)
          self.rb_level_2.setEnabled(False)
          self.rb_level_3.setEnabled(False)

        ### Not yet supported
        self.rb_level_2.setEnabled(False)
        self.rb_level_3.setEnabled(False)
        ### Not yet supported

        self.connect(self, SIGNAL("accepted()"), self.setApp)
        self.connect(self.le_command, SIGNAL("textChanged(QString)"), self.checkText)

    def setApp(self):
        if (self.rb_level_0.isChecked()):
          level = 0
        elif (self.rb_level_1.isChecked()):
          level = 1
        elif (self.rb_level_2.isChecked()):
          level = 2
        elif (self.rb_level_3.isChecked()):
          level = 3
        else:
          level = 0

        self.app_to_return = [unicode(self.le_command.text()), unicode(self.le_name.text()), True if (self.cb_terminal.isChecked()) else False, level]

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if text.isEmpty() else True)

# Custom Mini Canvas Preview, using a QFrame
class CanvasPreviewFrame(QFrame):
    def __init__(self, parent=None, scene=None):
        super(CanvasPreviewFrame, self).__init__(parent)

        # MiniCanvas Width -> 207 = 3100/15
        # MiniCanvas Height -> 160 = 2400/15

        self._width = float(DEFAULT_CANVAS_WIDTH)/15
        self._height = float(DEFAULT_CANVAS_HEIGHT)/15

        self.setMinimumSize(0, self._height)
        self.setMaximumSize(16777215, self._height) # 16777215 - value taken from QtDesigner
        self.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)

        self.is_initiated = False
        self.scene = scene

        self.rect_wpad = 0.0
        self.rect_hpad = 0.0

        self.rect_size = QRectF(0.0, 0.0, 1.0, 1.0)
        self.target = QRectF(0, 0, DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT)

        self.wsize = float(self.width())
        self.source = self.getSourceRet()

    def firstInit(self):
        x = (self._width/2)-(self.rect_size.width()/2)
        y = (self._height/2)-(self.rect_size.height()/2)

        xp = x*100/self._width
        yp = y*100/self._height

        self.setRectPos(x+self.source.x(), y)
        self.emit(SIGNAL("miniCanvasMoved(int, int)"), xp, yp)
        #self.nativeParentWidget().minicanvas_moved(xp, yp)

        self.is_initiated = True

    def getSourceRet(self):
        width = (self.width()-self._width)/2
        height = (self.height()-self._height)/2
        return QRectF(width, height, self._width, self._height)

    def setRectPos(self, x, y):
        self.rect_size = QRectF(x, y, self.rect_size.width(), self.rect_size.height())
        self.update()

    def setRectPosX(self, xp):
        x = xp*self._width/100
        self.rect_size = QRectF(x+self.source.x(), self.rect_size.y(), self.rect_size.width(), self.rect_size.height())
        self.update()

    def setRectPosY(self, yp):
        y = yp*self._height/100
        self.rect_size = QRectF(self.rect_size.x(), y, self.rect_size.width(), self.rect_size.height())
        self.update()

    def setRectPosXY(self, xp, yp):
        x = xp*self._width/100
        y = yp*self._height/100
        self.rect_size = QRectF(x+self.source.x(), y, self.rect_size.width(), self.rect_size.height())
        self.update()

    def setRectSize(self, width_p, height_p):
        width = width_p*self._width/100
        height = height_p*self._height/100
        self.rect_size = QRectF(self.rect_size.x(), self.rect_size.y(), width, height)
        self.rect_wpad = width*1/3
        self.rect_hpad = height*1/3
        self.update()

    def mouseMoveEvent(self, event):
        x = float(event.x())-self.source.x()-self.rect_wpad
        y = float(event.y())-self.source.y()-self.rect_hpad

        if (x < 0.0): x = 0.0
        elif (x > self._width-self.rect_size.width()): x = self._width-self.rect_size.width()

        if (y < 0.0): y = 0.0
        elif (y > self._height-self.rect_size.height()): y = self._height-self.rect_size.height()

        xp = x*100/self._width
        yp = y*100/self._height

        #self.setRectPos(x+self.source.x(), y)

        self.emit(SIGNAL("miniCanvasMoved(int, int)"), xp, yp)
        return event.accept()

    def paintEvent(self, event):
        painter = QPainter(self)

        painter.setBrush(self.scene.backgroundBrush())
        painter.drawRect(0, 0, self.width(), self.height())

        self.scene.render(painter, self.source, self.target, Qt.KeepAspectRatio)

        painter.setBrush(Qt.NoBrush)
        painter.setPen(QPen(Qt.blue, 2))
        painter.drawRect(self.rect_size)

        return QFrame.paintEvent(self, event)

    def resizeEvent(self, event):
        self.wsize = self.width()
        self.source = self.getSourceRet()
        QTimer.singleShot(0, self.nativeParentWidget().minicanvas_hvcheck)
        return QFrame.resizeEvent(self, event)

# Main Window
class ClaudiaMainW(QMainWindow, ui_claudia.Ui_ClaudiaMainW):
    def __init__(self, parent=None):
        super(ClaudiaMainW, self).__init__(parent)
        self.setupUi(self)

        # Load App Settings
        self.loadSettings()

        # Internal timer
        self.timer100 = QTimer()
        self.timer100.setInterval(self.saved_settings["Main/RefreshInterval"])
        self.timer1000 = QTimer()
        self.timer1000.setInterval(1000)
        self.timer1000.start()

        # Icons
        setIcons(self, ["canvas", "jack", "transport"])

        self.act_studio_new.setIcon(getIcon("document-new"))
        self.menu_studio_load.setIcon(getIcon("document-open"))
        self.act_studio_start.setIcon(getIcon("media-playback-start"))
        self.act_studio_stop.setIcon(getIcon("media-playback-stop"))
        self.act_studio_rename.setIcon(getIcon("edit-rename"))
        self.act_studio_save.setIcon(getIcon("document-save"))
        self.act_studio_save_as.setIcon(getIcon("document-save-as"))
        self.act_studio_unload.setIcon(getIcon("dialog-close"))
        self.menu_studio_delete.setIcon(getIcon("edit-delete"))
        self.b_studio_new.setIcon(getIcon("document-new"))
        self.b_studio_load.setIcon(getIcon("document-open"))
        self.b_studio_save.setIcon(getIcon("document-save"))
        self.b_studio_save_as.setIcon(getIcon("document-save-as"))

        self.act_room_create.setIcon(getIcon("list-add"))
        self.menu_room_delete.setIcon(getIcon("edit-delete"))

        self.act_project_new.setIcon(getIcon("document-new"))
        self.menu_project_load.setIcon(getIcon("document-open"))
        self.act_project_save.setIcon(getIcon("document-save"))
        self.act_project_save_as.setIcon(getIcon("document-save-as"))
        self.act_project_unload.setIcon(getIcon("dialog-close"))
        self.act_project_properties.setIcon(getIcon("edit-rename"))
        self.b_project_new.setIcon(getIcon("document-new"))
        self.b_project_load.setIcon(getIcon("document-open"))
        self.b_project_save.setIcon(getIcon("document-save"))
        self.b_project_save_as.setIcon(getIcon("document-save-as"))

        self.act_app_add_new.setIcon(getIcon("list-add"))
        self.act_app_run_custom.setIcon(getIcon("system-run"))

        self.act_tools_reactivate_ladishd.setIcon(getIcon("view-refresh"))
        self.act_quit.setIcon(getIcon("application-exit"))
        self.act_settings_configure.setIcon(getIcon("configure"))

        # Patchbay
        self.last_connection_id = 1
        self.group_list = []
        self.group_split_list = []
        self.connection_list = []

        self.scene = patchcanvas.MyGraphicsScene(self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setSceneRect(QRectF(0, 0, DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT))
        self.graphicsView.setRenderHint(QPainter.Antialiasing, True if (self.saved_settings["Canvas/Antialiasing"] == Qt.Checked) else False)
        self.graphicsView.setRenderHint(QPainter.TextAntialiasing, self.saved_settings["Canvas/TextAntialiasing"])
        self.graphicsView.setOptimizationFlag(QGraphicsView.DontSavePainterState, True)
        self.graphicsView.setOptimizationFlag(QGraphicsView.DontAdjustForAntialiasing, not bool(self.saved_settings["Canvas/Antialiasing"]))

        self.miniCanvasPreview = CanvasPreviewFrame(self, self.scene)
        self.AppListLayout.addWidget(self.miniCanvasPreview)

        # Create Canvas
        patchcanvas.options['theme_name'] = unicode(self.saved_settings["Canvas/Theme"])
        patchcanvas.options['bezier_lines'] = self.saved_settings["Canvas/BezierLines"]
        patchcanvas.options['antialiasing'] = self.saved_settings["Canvas/Antialiasing"]
        patchcanvas.options['auto_hide_groups'] = self.saved_settings["Canvas/AutoHideGroups"]
        patchcanvas.options['connect_midi2outro'] = True
        patchcanvas.options['fancy_eyecandy'] = self.saved_settings["Canvas/FancyEyeCandy"]

        patchcanvas.features['group_rename'] = False
        patchcanvas.features['port_rename'] = False
        patchcanvas.features['handle_group_pos'] = False

        patchcanvas.init(self.scene, self.canvas_callback)
        patchcanvas.setInitialPos(DEFAULT_CANVAS_WIDTH/2, DEFAULT_CANVAS_HEIGHT/2)
        patchcanvas.setCanvasSize(0, 0, DEFAULT_CANVAS_WIDTH, DEFAULT_CANVAS_HEIGHT)

        # Systray
        self.act_show_gui = QAction(self.tr("Hide Main &Window"), self)

        self.systray_menu = QMenu(self)
        self.systray_menu.addAction(self.act_show_gui)
        self.systray_menu.addSeparator()
        self.systray_menu.addAction(self.act_studio_new)
        self.systray_menu.addMenu(self.menu_studio_load)
        self.systray_menu.addSeparator()
        self.systray_menu.addAction(self.act_studio_start)
        self.systray_menu.addAction(self.act_studio_stop)
        self.systray_menu.addSeparator()
        self.systray_menu.addAction(self.act_studio_save)
        self.systray_menu.addAction(self.act_studio_save_as)
        self.systray_menu.addAction(self.act_studio_rename)
        self.systray_menu.addAction(self.act_studio_unload)
        self.systray_menu.addMenu(self.menu_studio_delete)
        self.systray_menu.addSeparator()
        self.systray_menu.addMenu(self.menu_Tools)
        self.systray_menu.addAction(self.act_settings_configure)
        self.systray_menu.addSeparator()
        self.systray_menu.addAction(self.act_quit)

        self.systray = QSystemTrayIcon(self)
        self.systray.setContextMenu(self.systray_menu)
        self.systray.setIcon(QIcon(':/48x48/claudia.png'))
        self.systray.show()

        if (self.saved_settings['Apps/Database'] == "LADISH" and not DBus.ladish_app_daemon):
          self.act_app_add_new.setEnabled(False)

        self.next_sample_rate = 0

        if (bool(DBus.jack.IsStarted())):
          self.jackStarted()
        else:
          self.jackStopped()

        if (bool(DBus.ladish_control.IsStudioLoaded())):
          self.studioLoaded()
          if (bool(DBus.ladish_studio.IsStarted())):
            self.studioStarted()
            self.init_ports()
          else:
            self.studioStopped()
        else:
          self.studioUnloaded()

        # DBus Stuff
        DBus.bus.add_signal_receiver(self.DBusSignalReceiver, sender_keyword='sender', destination_keyword='dest', interface_keyword='interface',
                        member_keyword='member', path_keyword='path')

        setConnections(self, ["canvas", "jack", "transport"])

        self.connect(self.act_studio_new, SIGNAL("triggered()"), self.func_studio_new)
        self.connect(self.act_studio_start, SIGNAL("triggered()"), self.func_studio_start)
        self.connect(self.act_studio_stop, SIGNAL("triggered()"), self.func_studio_stop)
        self.connect(self.act_studio_save, SIGNAL("triggered()"), self.func_studio_save)
        self.connect(self.act_studio_save_as, SIGNAL("triggered()"), self.func_studio_save_as)
        self.connect(self.act_studio_rename, SIGNAL("triggered()"), self.func_studio_rename)
        self.connect(self.act_studio_unload, SIGNAL("triggered()"), self.func_studio_unload)
        self.connect(self.b_studio_new, SIGNAL("clicked()"), self.func_studio_new)
        self.connect(self.b_studio_load, SIGNAL("clicked()"), self.func_studio_load_b)
        self.connect(self.b_studio_save, SIGNAL("clicked()"), self.func_studio_save)
        self.connect(self.b_studio_save_as, SIGNAL("clicked()"), self.func_studio_save_as)

        self.connect(self.act_room_create, SIGNAL("triggered()"), self.func_room_create)

        self.connect(self.act_project_new, SIGNAL("triggered()"), self.func_project_new)
        self.connect(self.act_project_save, SIGNAL("triggered()"), self.func_project_save)
        self.connect(self.act_project_save_as, SIGNAL("triggered()"), self.func_project_save_as)
        self.connect(self.act_project_unload, SIGNAL("triggered()"), self.func_project_unload)
        self.connect(self.act_project_properties, SIGNAL("triggered()"), self.func_project_properties)
        self.connect(self.b_project_new, SIGNAL("clicked()"), self.func_project_new)
        self.connect(self.b_project_load, SIGNAL("clicked()"), self.func_project_load)
        self.connect(self.b_project_save, SIGNAL("clicked()"), self.func_project_save)
        self.connect(self.b_project_save_as, SIGNAL("clicked()"), self.func_project_save_as)

        self.connect(self.act_app_add_new, SIGNAL("triggered()"), self.func_app_add_new)
        self.connect(self.act_app_run_custom, SIGNAL("triggered()"), self.func_app_run_custom)

        self.connect(self.act_tools_clear_xruns, SIGNAL("triggered()"), self.func_clear_xruns)
        self.connect(self.b_tools_clear_xruns, SIGNAL("clicked()"), self.func_clear_xruns)
        self.connect(self.b_xruns, SIGNAL("clicked()"), self.func_clear_xruns)

        self.connect(self.act_show_gui, SIGNAL("triggered()"), self.func_show_gui)
        self.connect(self.systray, SIGNAL("activated(QSystemTrayIcon::ActivationReason)"), self.func_clicked_systray)

        self.connect(self.graphicsView.horizontalScrollBar(), SIGNAL("valueChanged(int)"), self.minicanvas_hcheck)
        self.connect(self.graphicsView.verticalScrollBar(), SIGNAL("valueChanged(int)"), self.minicanvas_vcheck)

        self.connect(self.scene, SIGNAL("sceneItemsMoved"), self.canvas_items_moved)
        self.connect(self.miniCanvasPreview, SIGNAL("miniCanvasMoved(int, int)"), self.minicanvas_moved)

        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshBufferSize(self))
        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshSampleRate(self))
        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshDSPLoad(self))
        self.connect(self.timer100, SIGNAL("timeout()"), lambda: refreshTransport(self))
        self.connect(self.timer100, SIGNAL("timeout()"), self.refreshXruns)
        self.connect(self.timer1000, SIGNAL("timeout()"), self.refreshDBusFix)

        self.connect(self.menu_studio_load, SIGNAL("aboutToShow()"), lambda menu=self.menu_studio_load,
                     function=self.func_studio_load_m: self.updateMenuStudioList(menu, function))
        self.connect(self.menu_studio_delete, SIGNAL("aboutToShow()"), lambda menu=self.menu_studio_delete,
                     function=self.func_studio_delete_m: self.updateMenuStudioList(menu, function))
        self.connect(self.menu_room_delete, SIGNAL("aboutToShow()"), self.updateMenuRoomList)
        self.connect(self.menu_project_load, SIGNAL("aboutToShow()"), self.updateMenuProjectList)

        self.connect(self.treeWidget, SIGNAL("itemSelectionChanged()"), self.checkCurrentRoom)
        #self.connect(self.treeWidget, SIGNAL("itemPressed(QTreeWidgetItem*, int)"), self.checkCurrentRoom)
        self.connect(self.treeWidget, SIGNAL("itemDoubleClicked(QTreeWidgetItem*, int)"), self.doubleClickedAppList)
        self.connect(self.treeWidget, SIGNAL("customContextMenuRequested(QPoint)"), self.showAppListCustomMenu)

        self.connect(self.act_settings_configure, SIGNAL("triggered()"), self.configureClaudia)

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutClaudia)
        self.connect(self.act_help_about_qt, SIGNAL("triggered()"), app, SLOT("aboutQt()"))

        self.connect(self, SIGNAL("ShutdownCallback"), self._ShutdownCallback)
        self.connect(self, SIGNAL("QueueExecutionHalted"), self.signal_QueueExecutionHalted_exec)

    def resizeEvent(self, event):
        QTimer.singleShot(0, self.minicanvas_recheck_size)
        return QMainWindow.resizeEvent(self, event)

    def minicanvas_recheck_size(self):
        self.miniCanvasPreview.setRectSize(self.graphicsView.width()*100/DEFAULT_CANVAS_WIDTH, self.graphicsView.height()*100/DEFAULT_CANVAS_HEIGHT)
        if (not self.miniCanvasPreview.is_initiated):
          self.miniCanvasPreview.firstInit()

        self.minicanvas_update()

    def minicanvas_moved(self, xp, yp):
        x = DEFAULT_CANVAS_WIDTH*xp/100
        y = DEFAULT_CANVAS_HEIGHT*yp/100

        self.graphicsView.horizontalScrollBar().setValue(x)
        self.graphicsView.verticalScrollBar().setValue(y)

    def minicanvas_hcheck(self, x):
        self.miniCanvasPreview.setRectPosX(float(x)*100/DEFAULT_CANVAS_WIDTH)

    def minicanvas_vcheck(self, y):
        self.miniCanvasPreview.setRectPosY(float(y)*100/DEFAULT_CANVAS_HEIGHT)

    def minicanvas_hvcheck(self):
        self.minicanvas_recheck_size()

        x = self.graphicsView.horizontalScrollBar().value()
        y = self.graphicsView.verticalScrollBar().value()
        self.miniCanvasPreview.setRectPosXY(float(x)*100/DEFAULT_CANVAS_WIDTH, float(y)*100/DEFAULT_CANVAS_HEIGHT)

    def canvas_items_moved(self, items):
        for i in range(len(items)):
          #if ("org.ladish" in DBus.bus.list_names()):
          DBus.ladish_graph.Set(GRAPH_DICT_OBJECT_TYPE_CLIENT, items[i][0], URI_CANVAS_X, str(items[i][1].x()))
          DBus.ladish_graph.Set(GRAPH_DICT_OBJECT_TYPE_CLIENT, items[i][0], URI_CANVAS_Y, str(items[i][1].y()))

        self.minicanvas_update()

    def minicanvas_update(self):
        self.miniCanvasPreview.update()

    def DBusSignalReceiver(self, *args, **kwds):
        if (kwds['interface'] == "org.jackaudio.JackControl"):
          print "jack signal", kwds['member']
          if (kwds['member'] == "ServerStarted"):
            self.jackStarted()
          elif (kwds['member'] == "ServerStopped"):
            self.jackStopped()

        elif (kwds['interface'] == "org.jackaudo.JackPatchbay"):
          if (kwds['path'] == DBus.patchbay.object_path):
            print "patchbay signal", kwds['member']
            if (kwds['member'] == "ClientAppeared"):
              self.signal_ClientAppeared(args)
            elif (kwds['member'] == "ClientDisappeared"):
              self.signal_ClientDisappeared(args)
            elif (kwds['member'] == "PortAppeared"):
              self.signal_PortAppeared(args)
            elif (kwds['member'] == "PortDisappeared"):
              self.signal_PortDisappeared(args)
            elif (kwds['member'] == "PortRenamed"):
              self.signal_PortRenamed(args)
            elif (kwds['member'] == "PortsConnected"):
              self.signal_PortsConnected(args)
            elif (kwds['member'] == "PortsDisconnected"):
              self.signal_PortsDisconnected(args)

        elif (kwds['interface'] == "org.ladish.Control"):
          print "ladish_control signal", kwds['member']
          if (kwds['member'] == "StudioAppeared"):
            self.studioLoaded()
            if (bool(DBus.ladish_studio.IsStarted())):
              self.studioStarted()
            else:
              self.studioStopped()
          elif (kwds['member'] == "StudioDisappeared"):
            self.studioUnloaded()
          elif (kwds['member'] == "QueueExecutionHalted"):
            self.signal_QueueExecutionHalted()
          elif (kwds['member'] == "CleanExit"):
            self.signal_CleanExit(args)

        elif (kwds['interface'] == "org.ladish.Studio"):
          print "ladish_studio signal", kwds['member']
          if (kwds['member'] == "StudioStarted"):
            self.studioStarted()
          elif (kwds['member'] == "StudioStopped"):
            self.studioStopped()
          elif (kwds['member'] == "RoomAppeared"):
            self.signal_RoomAppeared(args)
          elif (kwds['member'] == "RoomDisappeared"):
            self.signal_RoomDisappeared(args)
          elif (kwds['member'] == "RoomChanged"):
            self.signal_RoomChanged(args)

        elif (kwds['interface'] == "org.ladish.Room"):
          print "ladish_room signal", kwds['member']
          if (kwds['member'] == "ProjectPropertiesChanged"):
            self.signal_ProjectPropertiesChanged(kwds['path'], args)

        elif (kwds['interface'] == "org.ladish.AppSupervisor"):
          print "ladish_app_iface signal", kwds['member']
          if (kwds['member'] == "AppStateChanged"):
            self.signal_AppStateChanged(kwds['path'], args)
          elif (kwds['member'] == "AppAdded"):
            self.signal_AppAdded(kwds['path'], args)
          elif (kwds['member'] == "AppRemoved"):
            self.signal_AppRemoved(kwds['path'], args)

    def DBusReconnect(self):
        DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)
        DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")
        DBus.ladish_control = DBus.bus.get_object("org.ladish", "/org/ladish/Control")

    def jackStarted(self):
        #self.DBusReconnect()

        if (not jack.client):
          jack.client = jacklib.client_open("claudia", jacklib.NullOption, None)
          jacklib.on_shutdown(jack.client, self.JackShutdownCallback)
          jacklib.activate(jack.client)

        self.timer100.start()
        self.act_jack_render.setEnabled(canRender)
        self.b_tools_render.setEnabled(canRender)
        self.menu_Transport.setEnabled(True)
        self.group_transport.setEnabled(True)

        self.pb_dsp_load.setValue(0)
        self.pb_dsp_load.setMaximum(100)
        self.pb_dsp_load.update()

        self.init_jack()

        if (self.saved_settings["Jack/AutoStartA2J"] and DBus.a2j and not DBus.a2j.is_started()):
          DBus.a2j.start()

        # TODO - check for pulse, only run if not
        #if (self.saved_settings["Jack/AutoStartPulse"] and havePulseJack):
          #if (self.saved_settings["Jack/PulsePlayOnly"]):
            #os.system("pulse-jack -p &")
          #else:
            #os.system("pulse-jack &")

    def jackStopped(self):
        #self.DBusReconnect()

        if (jack.client):
          jacklib.deactivate(jack.client)
          jacklib.client_close(jack.client)
          jack.client = None

        self.timer100.stop()
        self.act_jack_render.setEnabled(False)
        self.b_tools_render.setEnabled(False)
        self.menu_Transport.setEnabled(False)
        self.group_transport.setEnabled(False)

        setBufferSize(self, jacksettings.getBufferSize())
        setSampleRate(self, jacksettings.getSampleRate())
        setRealTime(self, jacksettings.isRealtime())
        setXruns(self, -1)

        if (self.selected_transport_view == TRANSPORT_VIEW_HMS):
          self.label_time.setText("00:00:00")
        elif (self.selected_transport_view == TRANSPORT_VIEW_BBT):
          self.label_time.setText("000|0|0000")
        elif (self.selected_transport_view == TRANSPORT_VIEW_FRAMES):
          self.label_time.setText("000'000'000")

        self.pb_dsp_load.setValue(0)
        self.pb_dsp_load.setMaximum(0)
        self.pb_dsp_load.update()

        if (self.next_sample_rate):
          func_set_sample_rate(self, self.next_sample_rate)

    def studioLoaded(self):
        DBus.patchbay = dbus.Interface(DBus.ladish_studio, 'org.jackaudio.JackPatchbay')
        DBus.ladish_studio = DBus.bus.get_object("org.ladish", "/org/ladish/Studio")
        DBus.ladish_graph = dbus.Interface(DBus.ladish_studio, 'org.ladish.GraphDict')
        DBus.ladish_app_iface = dbus.Interface(DBus.ladish_studio, 'org.ladish.AppSupervisor')

        self.label_first_time.setVisible(False)
        self.graphicsView.setVisible(True)
        self.miniCanvasPreview.setVisible(True)
        if (self.miniCanvasPreview.is_initiated):
          self.minicanvas_recheck_size()

        self.menu_Room.setEnabled(True)
        self.menu_Project.setEnabled(False)
        self.menu_Application.setEnabled(True)
        self.group_project.setEnabled(False)

        self.act_studio_rename.setEnabled(True)
        self.act_studio_unload.setEnabled(True)

        self.init_studio()

    def studioUnloaded(self):
        DBus.patchbay = None
        DBus.ladish_studio = None
        DBus.ladish_graph = None
        DBus.ladish_app_iface = None

        self.last_item_type = None
        self.last_room_path = None

        self.label_first_time.setVisible(True)
        self.graphicsView.setVisible(False)
        self.miniCanvasPreview.setVisible(False)

        self.menu_Room.setEnabled(False)
        self.menu_Project.setEnabled(False)
        self.menu_Application.setEnabled(False)
        self.group_project.setEnabled(False)

        self.act_studio_start.setEnabled(False)
        self.act_studio_stop.setEnabled(False)
        self.act_studio_rename.setEnabled(False)
        self.act_studio_save.setEnabled(False)
        self.act_studio_save_as.setEnabled(False)
        self.act_studio_unload.setEnabled(False)

        self.b_studio_save.setEnabled(False)
        self.b_studio_save_as.setEnabled(False)

        self.treeWidget.clear()

        patchcanvas.clear()

    def studioStarted(self):
        self.act_studio_start.setEnabled(False)
        self.act_studio_stop.setEnabled(True)
        self.act_studio_save.setEnabled(True)
        self.act_studio_save_as.setEnabled(True)

        self.b_studio_save.setEnabled(True)
        self.b_studio_save_as.setEnabled(True)

    def studioStopped(self):
        self.act_studio_start.setEnabled(True)
        self.act_studio_stop.setEnabled(False)
        self.act_studio_save.setEnabled(False)
        self.act_studio_save_as.setEnabled(False)

        self.b_studio_save.setEnabled(False)
        self.b_studio_save_as.setEnabled(False)

    def canvas_callback(self, action, *args):
        if (action == patchcanvas.ACTION_PORT_DISCONNECT_ALL):
          port_id = args[0]
          port_name = args[1]

          graph_dump = DBus.patchbay.GetGraph(0)
          graph_version = graph_dump[0]
          graph_ports = graph_dump[1]
          graph_conns = graph_dump[2]

          # Graph Connections
          for i in range(len(graph_conns)):
            conn_source_id   = int(graph_conns[i][0])
            conn_source_name = str(graph_conns[i][1])
            conn_source_port_id   = int(graph_conns[i][2])
            conn_source_port_name = str(graph_conns[i][3])
            conn_target_id   = int(graph_conns[i][4])
            conn_target_name = str(graph_conns[i][5])
            conn_target_port_id   = int(graph_conns[i][6])
            conn_target_port_name = str(graph_conns[i][7])

            if (conn_source_port_id == port_id):
              DBus.patchbay.DisconnectPortsByID(port_id, conn_target_port_id)
            elif (conn_target_port_id == port_id):
              DBus.patchbay.DisconnectPortsByID(conn_source_port_id, port_id)

        elif (action == patchcanvas.ACTION_PORT_INFO):
          this_port_id = args[0]

          graph_dump = DBus.patchbay.GetGraph(0)
          graph_version = graph_dump[0]
          graph_ports = graph_dump[1]
          graph_conns = graph_dump[2]

          # Graph Ports
          for i in range(len(graph_ports)):
            group_id    = graph_ports[i][0]
            group_name  = graph_ports[i][1]
            group_ports = graph_ports[i][2]

            for j in range(len(group_ports)):
              port_id    = group_ports[j][0]
              port_name  = group_ports[j][1]
              port_flags = group_ports[j][2]
              port_type  = group_ports[j][3]
              if (this_port_id == port_id):
                break

            if (this_port_id == port_id):
              break

          if (DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_PORT, this_port_id, URI_A2J_PORT) == "yes"):
            port_flags_ = self.get_real_a2j_port_flags(group_name, port_name, jacklib.PortIsInput if (port_flags & jacklib.PortIsInput) else jacklib.PortIsOutput)
            if (port_flags_ != 0):
              port_flags = port_flags_

          flags = []
          if (port_flags & jacklib.PortIsInput):
            flags.append(self.tr("Input"))
          if (port_flags & jacklib.PortIsOutput):
            flags.append(self.tr("Output"))
          if (port_flags & jacklib.PortIsPhysical):
            flags.append(self.tr("Physical"))
          if (port_flags & jacklib.PortCanMonitor):
            flags.append(self.tr("Can Monitor"))
          if (port_flags & jacklib.PortIsTerminal):
            flags.append(self.tr("Terminal"))
          if (port_flags & jacklib.PortIsActive):
            flags.append(self.tr("Active"))

          flags_text = self.tr("")
          for i in range(len(flags)):
            if (flags_text): flags_text+= " | "
            flags_text += flags[i]

          if (port_type == jacklib.AUDIO):
            type_text = self.tr("Audio")
          elif (port_type == jacklib.MIDI):
            type_text = self.tr("MIDI")
          else:
            type_text = self.tr("Unknown")

          port_full_name = group_name+":"+port_name

          info = self.tr(""
                  "<table>"
                  "<tr><td align='right'><b>Group Name:</b></td><td>&nbsp;%1</td></tr>"
                  "<tr><td align='right'><b>Group ID:</b></td><td>&nbsp;%2</td></tr>"
                  "<tr><td align='right'><b>Port Name:</b></td><td>&nbsp;%3</td></tr>"
                  "<tr><td align='right'><b>Port ID:</b></td><td>&nbsp;%4</i></td></tr>"
                  "<tr><td align='right'><b>Full Port Name:</b></td><td>&nbsp;%5</td></tr>"
                  "<tr><td colspan='2'>&nbsp;</td></tr>"
                  "<tr><td align='right'><b>Port Flags:</b></td><td>&nbsp;%6</td></tr>"
                  "<tr><td align='right'><b>Port Type:</b></td><td>&nbsp;%7</td></tr>"
                  "</table>"
                  ).arg(group_name).arg(group_id).arg(port_name).arg(port_id).arg(port_full_name).arg(flags_text).arg(type_text)

          QMessageBox.information(self, self.tr("Port Information"), info)

        elif (action == patchcanvas.ACTION_PORTS_CONNECT):
          port_a = args[0]
          port_b = args[2]
          DBus.patchbay.ConnectPortsByID(port_a, port_b)

        elif (action == patchcanvas.ACTION_PORTS_DISCONNECT):
          connection_id = args[0]
          port_a = port_b = None

          for i in range(len(self.connection_list)):
            if (connection_id == self.connection_list[i][0]):
              port_a = self.connection_list[i][1]
              port_b = self.connection_list[i][2]
              break
          else:
            return

          DBus.patchbay.DisconnectPortsByID(port_a, port_b)

        elif (action == patchcanvas.ACTION_GROUP_DISCONNECT_ALL):
          group_id = args[0]
          group_name = args[1]

          graph_dump = DBus.patchbay.GetGraph(0)
          graph_version = graph_dump[0]
          graph_ports = graph_dump[1]
          graph_conns = graph_dump[2]

          # Graph Connections
          for i in range(len(graph_conns)):
            conn_source_id   = int(graph_conns[i][0])
            conn_source_name = str(graph_conns[i][1])
            conn_source_port_id   = int(graph_conns[i][2])
            conn_source_port_name = str(graph_conns[i][3])
            conn_target_id   = int(graph_conns[i][4])
            conn_target_name = str(graph_conns[i][5])
            conn_target_port_id   = int(graph_conns[i][6])
            conn_target_port_name = str(graph_conns[i][7])

            if (conn_source_name == group_name or conn_target_name == group_name):
              DBus.patchbay.DisconnectPortsByID(conn_source_port_id, conn_target_port_id)

        elif (action == patchcanvas.ACTION_GROUP_JOIN):
          group_id = args[0]
          patchcanvas.joinGroup(group_id)

        elif (action == patchcanvas.ACTION_GROUP_SPLIT):
          group_id = args[0]
          patchcanvas.splitGroup(group_id)

        elif (action == patchcanvas.ACTION_REQUEST_PORT_CONNECTION_LIST):
          port_id = args[0]
          connections = []

          graph_dump = DBus.patchbay.GetGraph(0)
          graph_version = graph_dump[0]
          graph_ports = graph_dump[1]
          graph_conns = graph_dump[2]

          for i in range(len(graph_conns)):
            conn_source_id   = int(graph_conns[i][0])
            conn_source_name = str(graph_conns[i][1])
            conn_source_port_id   = int(graph_conns[i][2])
            conn_source_port_name = str(graph_conns[i][3])
            conn_target_id   = int(graph_conns[i][4])
            conn_target_name = str(graph_conns[i][5])
            conn_target_port_id   = int(graph_conns[i][6])
            conn_target_port_name = str(graph_conns[i][7])

            if (conn_source_port_id == port_id):
              connections.append((conn_target_port_id, conn_target_name+":"+conn_target_port_name))

            if (conn_target_port_id == port_id):
              connections.append((conn_source_port_id, conn_source_name+":"+conn_source_port_name))

          return connections

        elif (action == patchcanvas.ACTION_REQUEST_GROUP_CONNECTION_LIST):
          group_id = args[0]
          port_modes = args[2]
          connections = []

          graph_dump = DBus.patchbay.GetGraph(0)
          graph_version = graph_dump[0]
          graph_ports = graph_dump[1]
          graph_conns = graph_dump[2]

          for i in range(len(graph_conns)):
            conn_source_id   = int(graph_conns[i][0])
            conn_source_name = str(graph_conns[i][1])
            conn_source_port_id   = int(graph_conns[i][2])
            conn_source_port_name = str(graph_conns[i][3])
            conn_target_id   = int(graph_conns[i][4])
            conn_target_name = str(graph_conns[i][5])
            conn_target_port_id   = int(graph_conns[i][6])
            conn_target_port_name = str(graph_conns[i][7])

            if (conn_source_id == group_id and port_modes & patchcanvas.PORT_MODE_OUTPUT):
              connections.append((conn_target_port_id, conn_target_name+":"+conn_target_port_name))

            if (conn_target_id == group_id and port_modes & patchcanvas.PORT_MODE_INPUT):
              connections.append((conn_source_port_id, conn_source_name+":"+conn_source_port_name))

          return connections

    def canvas_add_group(self, group_id, group_name):
        room_list_names = self.get_room_list_names()

        if (group_name in room_list_names):
          icon = patchcanvas.ICON_LADISH_ROOM
        else:
          icon = patchcanvas.ICON_APPLICATION

        patchcanvas.addGroup(group_id, group_name, icon=icon)
        self.group_list.append([group_id, group_name])

        x = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_X)
        y = DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_CLIENT, group_id, URI_CANVAS_Y)

        if (x != None and y != None):
          patchcanvas.setGroupPos(group_id, float(x), float(y), float(x), float(y)) #TODO - alternate splitted group pos

        QTimer.singleShot(0, self.minicanvas_update)

    def canvas_remove_group(self, group_id, group_name):
        for i in range(len(self.group_list)):
          if (self.group_list[i][0] == group_id):
            self.group_list.pop(i)
            break
        else:
          return

        patchcanvas.removeGroup(group_id)
        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def canvas_add_port(self, port_id, port_name, port_mode, port_type, group_id, group_name, split):
        for i in range(len(self.group_list)):
          if (group_id == self.group_list[i][0]):
            break
        else: #Hack for clients started with no ports
          self.canvas_add_group(group_id, group_name)

        if (DBus.ladish_graph.Get(GRAPH_DICT_OBJECT_TYPE_PORT, port_id, URI_A2J_PORT) == "yes"):
          port_type = patchcanvas.PORT_TYPE_OUTRO

          flags = self.get_real_a2j_port_flags(group_name, port_name, port_mode)
          if (flags & jacklib.PortIsPhysical):
            split = True

        patchcanvas.addPort(group_id, port_id, port_name, port_mode, port_type)
        QTimer.singleShot(0, self.miniCanvasPreview.update)

        if not group_id in self.group_split_list:
          if (split):
            patchcanvas.splitGroup(group_id)
            patchcanvas.setGroupIcon(group_id, patchcanvas.ICON_HARDWARE)
          elif ( group_name == "Hardware Capture" or
                 group_name == "Hardware Playback" or
                 group_name == "Capture" or
                 group_name == "Playback"
               ):
            patchcanvas.setGroupIcon(group_id, patchcanvas.ICON_HARDWARE)
          self.group_split_list.append(group_id)

    def canvas_remove_port(self, port_id):
        patchcanvas.removePort(port_id)
        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def canvas_rename_port(self, port_id, port_name):
        patchcanvas.renamePort(port_id, port_name.split(":")[-1])
        QTimer.singleShot(0, self.miniCanvasPreview.update)

    def canvas_connect_ports(self, port_a, port_b):
        connection_id = self.last_connection_id
        patchcanvas.connectPorts(connection_id, port_a, port_b)
        self.connection_list.append([connection_id, port_a, port_b])
        self.last_connection_id += 1

    def canvas_disconnect_ports(self, port_a, port_b):
        for i in range(len(self.connection_list)):
          if ( (self.connection_list[i][1] == port_a and self.connection_list[i][2] == port_b) or
               (self.connection_list[i][2] == port_a and self.connection_list[i][1] == port_b) ):
            patchcanvas.disconnectPorts(self.connection_list[i][0])
            self.connection_list.pop(i)
            break

    def canvas_check_mini_view(self):
        rect = self.graphicsView.sceneRect()
        if (rect != self.last_view_rect):
          self.miniCanvasPreview.fitInView(rect, Qt.KeepAspectRatio)
        self.last_view_rect = rect

    def get_room_list_names(self):
        room_list_names = []
        room_list_dump = DBus.ladish_studio.GetRoomList()
        for i in range(len(room_list_dump)):
          room_path = room_list_dump[i][0]
          ladish_room = DBus.bus.get_object("org.ladish", room_path)
          room_name = QString(ladish_room.GetName())
          room_list_names.append(room_name)

        return room_list_names

    def get_real_a2j_port_flags(self, group_name, port_name, port_mode):
        str1 = "a2j:"+group_name+" "
        str2 = " (%s): " % ("capture" if (port_mode == patchcanvas.PORT_MODE_INPUT) else "playback") + port_name

        port_list = jacklib.get_ports(jack.client, None, None, 0)
        for i in range(len(port_list)):
            if (port_list[i].split("[")[0] == str1 and port_list[i].split("]")[-1] == str2):
              port_name = port_list[i]
              break
        else:
          return 0

        port_id = jacklib.port_by_name(jack.client, port_name)
        return jacklib.port_flags(port_id)

    def func_clear_xruns(self):
        DBus.jack.ResetXruns()

    def init_jack(self):
        if (not jack.client): # Jack Crash/Bug ?
          self.menu_Transport.setEnabled(False)
          self.group_transport.setEnabled(False)

        self.buffer_size = int(DBus.jack.GetBufferSize())
        realtime = bool(DBus.jack.IsRealtime())
        self.sample_rate = int(DBus.jack.GetSampleRate())
        self.last_transport_state = None
        self.last_bpm = 0.0
        self.last_buffer_size = self.buffer_size
        self.last_sample_rate = self.sample_rate

        setBufferSize(self, self.buffer_size)
        setRealTime(self, realtime)
        setSampleRate(self, self.sample_rate)

        refreshDSPLoad(self)
        refreshTransport(self)
        self.refreshXruns()

    def init_studio(self):
        studio_item = QTreeWidgetItem(ITEM_TYPE_STUDIO)
        studio_item.setText(0, QString(DBus.ladish_studio.GetName()))
        self.treeWidget.insertTopLevelItem(0, studio_item)

        self.treeWidget.setCurrentItem(studio_item)
        self.last_item_type = ITEM_TYPE_STUDIO
        self.last_room_path = None

        self.init_apps()

    def init_ports(self):
        self.last_connection_id = 1
        self.group_list = []
        self.group_split_list = []
        self.connection_list = []

        graph_dump = DBus.patchbay.GetGraph(0)
        graph_version = graph_dump[0]
        graph_ports = graph_dump[1]
        graph_conns = graph_dump[2]

        # Graph Ports
        for i in range(len(graph_ports)):
          idx   = graph_ports[i][0]
          name  = graph_ports[i][1]
          ports = graph_ports[i][2]

          for j in range(len(ports)):
            port_id    = int(ports[j][0])
            port_name  = str(ports[j][1])
            port_flags = int(ports[j][2])
            port_type  = int(ports[j][3])

            if (port_flags & jacklib.PortIsInput):
              port_mode = jacklib.PortIsInput
            elif (port_flags & jacklib.PortIsOutput):
              port_mode = jacklib.PortIsOutput
            else:
              print "Invalid port mode for", port_name
              port_mode = None

            self.canvas_add_port(port_id, port_name, port_mode, port_type, idx, name, (port_flags & jacklib.PortIsPhysical))

        # Graph Connections
        for i in range(len(graph_conns)):
          conn_source_id   = int(graph_conns[i][0])
          conn_source_name = str(graph_conns[i][1])
          conn_source_port_id   = int(graph_conns[i][2])
          conn_source_port_name = str(graph_conns[i][3])
          conn_target_id   = int(graph_conns[i][4])
          conn_target_name = str(graph_conns[i][5])
          conn_target_port_id   = int(graph_conns[i][6])
          conn_target_port_name = str(graph_conns[i][7])

          self.canvas_connect_ports(conn_source_port_id, conn_target_port_id)

    def init_apps(self):
        studio_iface = dbus.Interface(DBus.ladish_studio, 'org.ladish.AppSupervisor')
        studio_app_list_dump = DBus.ladish_app_iface.GetAll()
        studio_item = self.treeWidget.topLevelItem(0)

        studio_graph_version = studio_app_list_dump[0]
        for i in range(len(studio_app_list_dump[1])):
          number   = studio_app_list_dump[1][i][0]
          name     = studio_app_list_dump[1][i][1]
          active   = studio_app_list_dump[1][i][2]
          terminal = studio_app_list_dump[1][i][3]
          level    = studio_app_list_dump[1][i][4]
          text = QString("[L%1]%2 %3").arg(level).arg("" if (active) else " (inactive)").arg(name)
          item = QTreeWidgetItem(ITEM_TYPE_STUDIO_APP)
          item.setText(0, text)
          item.properties = [number, name, active, terminal, level]
          studio_item.addChild(item)

        room_list_dump = DBus.ladish_studio.GetRoomList()
        for i in range(len(room_list_dump)):
          room_path = room_list_dump[i][0]
          ladish_room = DBus.bus.get_object("org.ladish", room_path)
          room_name = ladish_room.GetName()

          room_iface = dbus.Interface(ladish_room, 'org.ladish.AppSupervisor')
          room_app_list_dump = room_iface.GetAll()
          room_item = self.addNewRoom(room_path, room_name)

          room_graph_version = studio_app_list_dump[0]
          for j in range(len(room_app_list_dump[1])):
            number   = room_app_list_dump[1][j][0]
            name     = room_app_list_dump[1][j][1]
            active   = room_app_list_dump[1][j][2]
            terminal = room_app_list_dump[1][j][3]
            level    = room_app_list_dump[1][j][4]
            text = QString("[L%1]%2 %3").arg(level).arg("" if (active) else " (inactive)").arg(name)
            item = QTreeWidgetItem(ITEM_TYPE_ROOM_APP)
            item.setText(0, text)
            item.properties = [number, name, active, terminal, level]
            room_item.addChild(item)

        self.treeWidget.expandAll()

    def func_show_gui(self):
        if (self.isVisible()):
          self.act_show_gui.setText(self.tr("Show Main &Window"))
          self.hide()
        else:
          self.act_show_gui.setText(self.tr("Hide Main &Window"))
          self.show()

    def func_clicked_systray(self, reason):
        if (reason == QSystemTrayIcon.DoubleClick or reason == QSystemTrayIcon.Trigger):
          self.func_show_gui()

    def func_studio_new(self):
        dialog = StudioNameW(self, False)
        if (dialog.exec_()):
          DBus.ladish_control.NewStudio(dbus.String(dialog.studio_to_return))

    def func_studio_load_m(self, studio_name):
        DBus.ladish_control.LoadStudio(studio_name)

    def func_studio_load_b(self):
        dialog = StudioListW(self)
        if (dialog.exec_()):
          DBus.ladish_control.LoadStudio(dbus.String(dialog.studio_to_return))

    def func_studio_start(self):
        DBus.ladish_studio.Start()

    def func_studio_stop(self):
        DBus.ladish_studio.Stop()

    def func_studio_rename(self):
        dialog = StudioNameW(self, True)
        if (dialog.exec_()):
          DBus.ladish_studio.Rename(dbus.String(dialog.studio_to_return))

    def func_studio_save(self):
        DBus.ladish_studio.Save()

    def func_studio_save_as(self):
        dialog = StudioNameW(self, True, False)
        if (dialog.exec_()):
          DBus.ladish_studio.SaveAs(dbus.String(dialog.studio_to_return))

    def func_studio_unload(self):
        DBus.ladish_studio.Unload()

    def func_studio_delete_m(self, studio_name):
        DBus.ladish_control.DeleteStudio(dbus.String(studio_name))

    def func_room_create(self):
        dialog = CreateRoomW(self)
        if (dialog.exec_()):
          new_room = dialog.room_to_return
          name     = new_room[0]
          template = new_room[1]
          DBus.ladish_studio.CreateRoom(name, template)

    def func_room_delete_m(self, room_name):
        DBus.ladish_studio.DeleteRoom(dbus.String(room_name))

    def func_project_new(self):
        dialog = SaveProjectW(self)
        dialog.setWindowTitle(self.tr("New Project"))

        if (dialog.exec_()):
          path = dbus.String(dialog.project_to_return[0])
          name = dbus.String(dialog.project_to_return[1])
          DBus.ladish_room.SaveProject(path, name)

    def func_project_load(self):
        project_path = QFileDialog.getExistingDirectory(self, self.tr("Open Project"), self.saved_settings["Main/DefaultProjectFolder"])
        if (project_path.isEmpty()):
          return
        else:
          if (QFile.exists(project_path+"/ladish-project.xml")):
            DBus.ladish_room.LoadProject(unicode(project_path))
          else:
            QMessageBox.warning(self, self.tr("Warning"), self.tr("The selected folder does not contain a ladish project!"))

    def func_project_save(self):
        project_properties_dump = DBus.ladish_room.GetProjectProperties()
        graph_version = project_properties_dump[0]
        project_properties = project_properties_dump[1]

        if len(project_properties):
          path = dbus.String(project_properties[u'dir'])
          name = dbus.String(project_properties[u'name'])
          DBus.ladish_room.SaveProject(path, name)
        else:
          dialog = SaveProjectW(self)
          if (dialog.exec_()):
            path = dbus.String(dialog.project_to_return[0])
            name = dbus.String(dialog.project_to_return[1])
            DBus.ladish_room.SaveProject(path, name)

    def func_project_save_as(self):
        project_properties_dump = DBus.ladish_room.GetProjectProperties()
        graph_version = project_properties_dump[0]
        project_properties = project_properties_dump[1]

        if len(project_properties):
          path = dbus.String(project_properties[u'dir'])
          name = dbus.String(project_properties[u'name'])
          dialog = SaveProjectW(self, path, name, True)
        else:
          dialog = SaveProjectW(self)

        if (dialog.exec_()):
          path = dbus.String(dialog.project_to_return[0])
          name = dbus.String(dialog.project_to_return[1])
          DBus.ladish_room.SaveProject(path, name)

    def func_project_unload(self):
        DBus.ladish_room.UnloadProject()

    def func_project_properties(self):
        project_properties_dump = DBus.ladish_room.GetProjectProperties()
        graph_version = project_properties_dump[0]
        project_properties = project_properties_dump[1]

        name = project_properties[u'name']
        dir_ = project_properties[u'dir']

        if ("description" in project_properties.keys()):
          description = project_properties[u'description']
        else:
          description = ""

        if ("notes" in project_properties.keys()):
          notes = project_properties[u'notes']
        else:
          notes = ""

        dialog = ProjectPropertiesW(self, name, description, notes)

        if (dialog.exec_()):
          save_now = dialog.data_to_return[0]
          new_name = dbus.String(dialog.data_to_return[1])
          description = dbus.String(dialog.data_to_return[2])
          notes = dbus.String(dialog.data_to_return[3])

          DBus.ladish_room.SetProjectDescription(description)
          DBus.ladish_room.SetProjectNotes(notes)

          if (save_now):
            DBus.ladish_room.SaveProject(dbus.String(dir_), new_name)

    def func_app_add_new(self):
        if (self.saved_settings['Apps/Database'] == "LADISH"):
          dialog = AddNewW(self)
          if (dialog.exec_()):
            new_app = dialog.app_to_return
            command  = new_app[0]
            name     = new_app[1]
            terminal = new_app[2]
            level    = new_app[3]
            DBus.ladish_app_iface.RunCustom(terminal, command, name, level)

        elif (self.saved_settings['Apps/Database'] == "KXStudio"):
          url_folder = QString("")
          if (self.last_item_type == ITEM_TYPE_STUDIO or self.last_item_type == ITEM_TYPE_STUDIO_APP):
            url_folder = self.saved_settings['Main/DefaultProjectFolder']
            fake = True

          elif (self.last_item_type == ITEM_TYPE_ROOM or self.last_item_type == ITEM_TYPE_ROOM_APP):
            project_properties_dump = DBus.ladish_room.GetProjectProperties()
            graph_version = project_properties_dump[0]
            project_properties = project_properties_dump[1]

            if len(project_properties):
              url_folder = QString(project_properties[u'dir'])
              fake = False
            else:
              url_folder = self.saved_settings['Main/DefaultProjectFolder']
              fake = True

          else:
            print "Invalid last_item_type"
            return

          dialog = AddNewKXStudioW(self, DBus.ladish_app_iface, url_folder, fake)
          dialog.exec_()
        else:
          print "Unsupported database"
          return

    def func_app_run_custom(self):
        dialog = RunCustomW(self)
        if (dialog.exec_()):
          new_app = dialog.app_to_return
          command  = new_app[0]
          name     = new_app[1]
          terminal = new_app[2]
          level    = new_app[3]
          DBus.ladish_app_iface.RunCustom(terminal, command, name, level)

    def addNewRoom(self, room_path, room_name):
        room_object = DBus.bus.get_object("org.ladish", room_path)
        project_properties_dump = room_object.GetProjectProperties()

        graph_version = project_properties_dump[0]
        project_properties = project_properties_dump[1]

        if len(project_properties):
          project_path = project_properties[u'dir']
          project_name = project_properties[u'name']
          item_string = "("+project_name+")"
        else:
          project_path = None
          project_name = None
          item_string = ""

        item = QTreeWidgetItem(ITEM_TYPE_ROOM)
        item.properties = [room_path, room_name]
        item.setText(0, QString("%1 %2").arg(room_name).arg(item_string))
        index = int(room_path.replace("/org/ladish/Room",""))

        for i in range(index):
          if (not self.treeWidget.topLevelItem(i)):
            fake_item = QTreeWidgetItem(-1)
            self.treeWidget.insertTopLevelItem(i, fake_item)
            fake_item.setHidden(True)

        self.treeWidget.insertTopLevelItem(index, item)
        return item

    def updateMenuStudioList(self, menu, function):
        menu.clear()
        studio_list_dump = DBus.ladish_control.GetStudioList()
        if (len(studio_list_dump) == 0):
            act_no_studio = QAction(self.tr("Empty studio list"), menu)
            act_no_studio.setEnabled(False)
            menu.addAction(act_no_studio)
        else:
          for i in range(len(studio_list_dump)):
            studio_name = studio_list_dump[i][0]
            act_x_studio = QAction(studio_name, menu)
            menu.addAction(act_x_studio)
            self.connect(act_x_studio, SIGNAL("triggered()"), lambda name=studio_name: function(name))

    def updateMenuRoomList(self):
        self.menu_room_delete.clear()
        if (bool(DBus.ladish_control.IsStudioLoaded())):
          room_list_dump = DBus.ladish_studio.GetRoomList()
          if (len(room_list_dump) == 0):
            self.createEmptyMenuRoomActon()
          else:
            for i in range(len(room_list_dump)):
              ladish_room = DBus.bus.get_object("org.ladish", room_list_dump[i][0])
              room_name = ladish_room.GetName()
              act_x_room = QAction(room_name, self.menu_room_delete)
              self.menu_room_delete.addAction(act_x_room)
              self.connect(act_x_room, SIGNAL("triggered()"), lambda name=room_name: self.func_room_delete_m(name))
        else:
          self.createEmptyMenuRoomActon()

    def createEmptyMenuRoomActon(self):
        act_no_room = QAction(self.tr("Empty room list"), self.menu_room_delete)
        act_no_room.setEnabled(False)
        self.menu_room_delete.addAction(act_no_room)

    def updateMenuProjectList(self):
        self.menu_project_load.clear()
        act_project_load = QAction(self.tr("Load from folder..."), self.menu_project_load)
        self.menu_project_load.addAction(act_project_load)
        self.connect(act_project_load, SIGNAL("triggered()"), self.func_project_load)

        ladish_recent_iface = dbus.Interface(DBus.ladish_room, "org.ladish.RecentItems")
        proj_list_dump = ladish_recent_iface.get(RECENT_PROJECTS_STORE_MAX_ITEMS)

        if (len(proj_list_dump) > 0):
          self.menu_project_load.addSeparator()
          for i in range(len(proj_list_dump)):
            proj_path = proj_list_dump[i][0]
            proj_name = proj_list_dump[i][1]['name']

            act_x_proj = QAction(proj_name, self.menu_project_load)
            self.menu_project_load.addAction(act_x_proj)
            self.connect(act_x_proj, SIGNAL("triggered()"), lambda path=proj_path: self.func_x_load(path))

    def func_x_load(self, path):
        DBus.ladish_room.LoadProject(unicode(path))

    def checkCurrentRoom(self):
        item = self.treeWidget.currentItem()
        room_path = None

        if not item:
          return

        if (item.type() == ITEM_TYPE_STUDIO or item.type() == ITEM_TYPE_STUDIO_APP):
          self.menu_Project.setEnabled(False)
          self.group_project.setEnabled(False)
          self.menu_Application.setEnabled(True)
          DBus.ladish_room = None
          DBus.ladish_app_iface = dbus.Interface(DBus.ladish_studio, "org.ladish.AppSupervisor")
          ITEM_TYPE = ITEM_TYPE_STUDIO

        elif (item.type() == ITEM_TYPE_ROOM or item.type() == ITEM_TYPE_ROOM_APP):
          self.menu_Project.setEnabled(True)
          self.group_project.setEnabled(True)
          if (item.type() == ITEM_TYPE_ROOM):
            room_path = item.properties[0]
          elif (item.type() == ITEM_TYPE_ROOM_APP):
            room_path = item.parent().properties[0]
          else:
            print "Couldn't find room_path"
            return

          DBus.ladish_room = DBus.bus.get_object("org.ladish", room_path)
          DBus.ladish_app_iface = dbus.Interface(DBus.ladish_room, "org.ladish.AppSupervisor")
          ITEM_TYPE = ITEM_TYPE_ROOM

          project_properties_dump = DBus.ladish_room.GetProjectProperties()
          has_project = (len(project_properties_dump[1]) > 0) #project_properties

          self.act_project_save.setEnabled(has_project)
          self.act_project_save_as.setEnabled(has_project)
          self.act_project_unload.setEnabled(has_project)
          self.act_project_properties.setEnabled(has_project)
          self.b_project_save.setEnabled(has_project)
          self.b_project_save_as.setEnabled(has_project)
          self.menu_Application.setEnabled(has_project)

        else:
          print "Invalid ITEM_TYPE"
          return

        if (ITEM_TYPE != self.last_item_type and room_path != self.last_room_path):
          if (ITEM_TYPE == ITEM_TYPE_STUDIO):
            object_path = DBus.ladish_studio
          elif (ITEM_TYPE == ITEM_TYPE_ROOM):
            object_path = DBus.ladish_room
          else:
            print "Invalid ITEM_TYPE (2)"
            return

          patchcanvas.clear()
          DBus.patchbay = dbus.Interface(object_path, 'org.jackaudio.JackPatchbay')
          DBus.ladish_graph = dbus.Interface(object_path, 'org.ladish.GraphDict')
          self.init_ports()

        self.last_item_type = ITEM_TYPE
        self.last_room_path = room_path

    def doubleClickedAppList(self, item, row):
        if (item.type() == ITEM_TYPE_STUDIO_APP or item.type() == ITEM_TYPE_ROOM_APP):
          if (not item.properties[2]):
            DBus.ladish_app_iface.StartApp(item.properties[0])

    def showAppListCustomMenu(self, pos):
        item = self.treeWidget.currentItem()
        if (item):
          cMenu = QMenu()
          if (item.type() == ITEM_TYPE_STUDIO):
            act_x_add_new = cMenu.addAction(self.tr("Add New..."))
            act_x_run_custom = cMenu.addAction(self.tr("Run Custom..."))
            cMenu.addSeparator()
            act_x_create_room = cMenu.addAction(self.tr("Create Room..."))

            act_x_add_new.setIcon(QIcon.fromTheme("list-add", QIcon(":/16x16/list-add.png")))
            act_x_run_custom.setIcon(QIcon.fromTheme("system-run", QIcon(":/16x16/system-run.png")))
            act_x_create_room.setIcon(QIcon.fromTheme("list-add", QIcon(":/16x16/list-add.png")))
            act_x_add_new.setEnabled(self.act_app_add_new.isEnabled())

            self.connect(act_x_add_new, SIGNAL("triggered()"), self.func_app_add_new)
            self.connect(act_x_run_custom, SIGNAL("triggered()"), self.func_app_run_custom)
            self.connect(act_x_create_room, SIGNAL("triggered()"), self.func_room_create)

          elif (item.type() == ITEM_TYPE_ROOM):
            act_x_add_new = cMenu.addAction(self.tr("Add New..."))
            act_x_run_custom = cMenu.addAction(self.tr("Run Custom..."))
            cMenu.addSeparator()
            act_x_new = cMenu.addAction(self.tr("New Project..."))
            cMenu.addMenu(self.menu_project_load)
            act_x_save = cMenu.addAction(self.tr("Save Project"))
            act_x_save_as = cMenu.addAction(self.tr("Save Project As..."))
            act_x_unload = cMenu.addAction(self.tr("Unload Project"))
            cMenu.addSeparator()
            act_x_properties = cMenu.addAction(self.tr("Project Properties..."))
            cMenu.addSeparator()
            act_x_delete_room = cMenu.addAction(self.tr("Delete Room"))

            act_x_add_new.setIcon(QIcon.fromTheme("list-add", QIcon(":/16x16/list-add.png")))
            act_x_run_custom.setIcon(QIcon.fromTheme("system-run", QIcon(":/16x16/system-run.png")))
            act_x_new.setIcon(QIcon.fromTheme("document-new", QIcon(":/16x16/document-new.png")))
            act_x_save.setIcon(QIcon.fromTheme("document-save", QIcon(":/16x16/document-save.png")))
            act_x_save_as.setIcon(QIcon.fromTheme("document-save-as", QIcon(":/16x16/document-save-as.png")))
            act_x_unload.setIcon(QIcon.fromTheme("dialog-close", QIcon(":/16x16/dialog-close.png")))
            act_x_properties.setIcon(QIcon.fromTheme("edit-rename", QIcon(":/16x16/edit-rename.png")))
            act_x_delete_room.setIcon(QIcon.fromTheme("edit-delete", QIcon(":/16x16/edit-delete.png")))

            act_x_add_new.setEnabled(self.menu_Application.isEnabled() and self.act_app_add_new.isEnabled())

            project_properties_dump = DBus.ladish_room.GetProjectProperties()
            if (len(project_properties_dump[1]) <= 0):
              act_x_run_custom.setEnabled(False)
              act_x_save.setEnabled(False)
              act_x_save_as.setEnabled(False)
              act_x_unload.setEnabled(False)
              act_x_properties.setEnabled(False)

            self.connect(act_x_add_new, SIGNAL("triggered()"), self.func_app_add_new)
            self.connect(act_x_run_custom, SIGNAL("triggered()"), self.func_app_run_custom)
            self.connect(act_x_new, SIGNAL("triggered()"), self.func_project_new)
            self.connect(act_x_save, SIGNAL("triggered()"), self.func_project_save)
            self.connect(act_x_save_as, SIGNAL("triggered()"), self.func_project_save_as)
            self.connect(act_x_unload, SIGNAL("triggered()"), self.func_project_unload)
            self.connect(act_x_properties, SIGNAL("triggered()"), self.func_project_properties)
            self.connect(act_x_delete_room, SIGNAL("triggered()"), lambda room_name=DBus.ladish_room.GetName(): self.func_room_delete_m(room_name))

          elif (item.type() == ITEM_TYPE_STUDIO_APP or item.type() == ITEM_TYPE_ROOM_APP):
            number = item.properties[0]
            name   = item.properties[1]
            active = item.properties[2]
            terminal = item.properties[3]
            level  = item.properties[4]

            if (active):
              act_x_stop = cMenu.addAction(self.tr("Stop"))
              act_x_kill = cMenu.addAction(self.tr("Kill"))
              act_x_stop.setIcon(QIcon.fromTheme("media-playback-stop", QIcon(":/16x16/media-playback-stop.png")))
              act_x_kill.setIcon(QIcon.fromTheme("dialog-close", QIcon(":/16x16/dialog-close.png")))
              self.connect(act_x_stop, SIGNAL("triggered()"), lambda: self.func_x_stop(number))
              self.connect(act_x_kill, SIGNAL("triggered()"), lambda: self.func_x_kill(number))
            else:
              act_x_start = cMenu.addAction(self.tr("Start"))
              act_x_start.setIcon(QIcon.fromTheme("media-playback-start", QIcon(":/16x16/media-playback-start.png")))
              self.connect(act_x_start, SIGNAL("triggered()"), lambda: self.func_x_start(number))
            act_x_properties = cMenu.addAction(self.tr("Properties"))
            cMenu.addSeparator()
            act_x_remove = cMenu.addAction(self.tr("Remove"))
            act_x_properties.setIcon(QIcon.fromTheme("edit-rename", QIcon(":/16x16/edit-rename.png")))
            act_x_remove.setIcon(QIcon.fromTheme("edit-delete", QIcon(":/16x16/edit-delete.png")))
            self.connect(act_x_properties, SIGNAL("triggered()"), lambda: self.func_x_properties(number))
            self.connect(act_x_remove, SIGNAL("triggered()"), lambda: self.func_x_remove(number))

        cMenu.exec_(QCursor.pos())

    def func_x_start(self, number):
        DBus.ladish_app_iface.StartApp(number)

    def func_x_stop(self, number):
        DBus.ladish_app_iface.StopApp(number)

    def func_x_kill(self, number):
        DBus.ladish_app_iface.KillApp(number)

    def func_x_properties(self, number):
        info_dump = DBus.ladish_app_iface.GetAppProperties(number)
        name     = info_dump[0]
        command  = info_dump[1]
        active   = info_dump[2]
        terminal = info_dump[3]
        level    = info_dump[4]
        dialog = RunCustomW(self, command, name, terminal, level, active)
        dialog.setWindowTitle(self.tr("App properties"))
        if (dialog.exec_()):
          ret = dialog.app_to_return
          command  = ret[0]
          name     = ret[1]
          terminal = ret[2]
          level    = ret[3]
          DBus.ladish_app_iface.SetAppProperties(number, name, command, terminal, level)

    def func_x_remove(self, number):
        DBus.ladish_app_iface.RemoveApp(number)

    def signal_ClientAppeared(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        self.canvas_add_group(group_id, group_name)

    def signal_ClientDisappeared(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        self.canvas_remove_group(group_id, group_name)

    def signal_PortAppeared(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        port_id    = args[3]
        port_name  = args[4]
        port_flags = args[5]
        port_type  = args[6]

        if (port_flags & jacklib.PortIsInput):
          port_mode = jacklib.PortIsInput
        elif (port_flags & jacklib.PortIsOutput):
          port_mode = jacklib.PortIsOutput
        else:
          print "Invalid port mode for", port_name
          port_mode = None

        self.canvas_add_port(port_id, port_name, port_mode, port_type, group_id, group_name, (port_flags & jacklib.PortIsPhysical))

    def signal_PortDisappeared(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        port_id    = args[3]
        port_name  = args[4]
        self.canvas_remove_port(port_id)

    def signal_PortRenamed(self, args):
        graph_version = args[0]
        group_id   = args[1]
        group_name = args[2]
        port_id    = args[3]
        port_name  = args[4]
        new_name   = args[5]
        self.canvas_rename_port(port_id, new_name)

    def signal_PortsConnected(self, args):
        graph_version = args[0]
        source_group_id   = args[1]
        source_group_name = args[2]
        source_port_id    = args[3]
        source_port_name  = args[4]
        target_group_id   = args[5]
        target_group_name = args[6]
        target_port_id    = args[7]
        target_port_name  = args[8]
        self.canvas_connect_ports(source_port_id, target_port_id)

    def signal_PortsDisconnected(self, args):
        graph_version = args[0]
        source_group_id   = args[1]
        source_group_name = args[2]
        source_port_id    = args[3]
        source_port_name  = args[4]
        target_group_id   = args[5]
        target_group_name = args[6]
        target_port_id    = args[7]
        target_port_name  = args[8]
        self.canvas_disconnect_ports(source_port_id, target_port_id)

    def signal_AppStateChanged(self, path, args):
        graph_version = args[0]
        number   = args[1]
        name     = args[2]
        active   = args[3]
        terminal = args[4]
        level    = args[5]

        if (path == "/org/ladish/Studio"):
          index = 0
        else:
          index = int(path.replace("/org/ladish/Room",""))

        top_level_item = self.treeWidget.topLevelItem(index)
        for i in range(top_level_item.childCount()):
          if (top_level_item.child(i).properties[0] == number):
            if (top_level_item.child(i).properties[1] != name):
              patchcanvas.renameGroup(top_level_item.child(i).properties[1], name)
            top_level_item.child(i).properties = [number, name, active, terminal, level]
            top_level_item.child(i).setText(0, QString("[L%1]%2 %3").arg(level).arg("" if (active) else " (inactive)").arg(name))
            break

    def signal_AppAdded(self, path, args):
        graph_version = args[0]
        number   = args[1]
        name     = args[2]
        active   = args[3]
        terminal = args[4]
        level    = args[5]

        if (path == "/org/ladish/Studio"):
          index = 0
          ITEM_TYPE = ITEM_TYPE_STUDIO_APP
        else:
          index = int(path.replace("/org/ladish/Room",""))
          ITEM_TYPE = ITEM_TYPE_ROOM_APP

        top_level_item = self.treeWidget.topLevelItem(index)
        text = QString("[L%1]%2 %3").arg(level).arg("" if (active) else " (inactive)").arg(name)
        item = QTreeWidgetItem(ITEM_TYPE)
        item.setText(0, text)
        item.properties = [number, name, active, terminal, level]
        top_level_item.addChild(item)

    def signal_AppRemoved(self, path, args):
        graph_version = args[0]
        number   = args[1]

        if (path == "/org/ladish/Studio"):
          index = 0
        else:
          index = int(path.replace("/org/ladish/Room",""))

        top_level_item = self.treeWidget.topLevelItem(index)
        for i in range(top_level_item.childCount()):
          if (top_level_item.child(i).properties[0] == number):
            top_level_item.takeChild(i)
            break

    def signal_RoomAppeared(self, args):
        path = args[0]
        name = args[1][u'name']
        template = args[1][u'template']
        index = int(path.replace("/org/ladish/Room",""))
        room_item = QTreeWidgetItem(ITEM_TYPE_ROOM)
        room_item.properties = [path, name]
        room_item.setText(0, QString(name))
        self.treeWidget.insertTopLevelItem(index, room_item)

    def signal_RoomDisappeared(self, args):
        path = args[0]
        name = args[1][u'name']
        template = args[1][u'template']
        index = int(path.replace("/org/ladish/Room",""))

        top_level_item = self.treeWidget.topLevelItem(index)
        for i in range(top_level_item.childCount()):
          top_level_item.takeChild(i)

        self.treeWidget.takeTopLevelItem(index)

    def signal_RoomChanged(self, args):
        print "TODO", args

    def signal_ProjectPropertiesChanged(self, path, args):
        graph_version = args[0]
        properties = args[1]
        has_project = (len(properties) > 0)

        if (has_project):
          room_path = properties[u'dir']
          room_name = properties[u'name']
          item_string = "("+room_name+")"
        else:
          room_path = None
          room_name = None
          item_string = ""

        self.act_project_save.setEnabled(has_project)
        self.act_project_save_as.setEnabled(has_project)
        self.act_project_unload.setEnabled(has_project)
        self.act_project_properties.setEnabled(has_project)
        self.b_project_save.setEnabled(has_project)
        self.b_project_save_as.setEnabled(has_project)
        self.menu_Application.setEnabled(has_project)

        index = int(path.replace("/org/ladish/Room",""))
        item = self.treeWidget.topLevelItem(index)
        item.setText(0, QString("%1 %2").arg(item.properties[1]).arg(item_string))

    def signal_QueueExecutionHalted(self):
        self.emit(SIGNAL("QueueExecutionHalted"))

    def signal_QueueExecutionHalted_exec(self):
        log_path = os.path.join(os.getenv("HOME"), ".log", "ladish", "ladish.log")
        if (os.path.exists(log_path)):
          log_file = open(log_path)
          log_text = log_file.read().split("ERROR: ")[-1].split("\n")[0].replace("[31m","").replace("[33m","").replace("[0m","")
          log_file.close()
        else:
          log_text = None

        msgbox = QMessageBox(QMessageBox.Critical, self.tr("Execution Halted"), self.tr(
                             "Something went wrong with ladish so the last action was not sucessful.\n"), QMessageBox.Ok, self)
        msgbox.setInformativeText(self.tr("You can check the ladish log file (or click in the 'Show Details' button) to find out what went wrong."))
        if (log_text): msgbox.setDetailedText(log_text)
        msgbox.show()

    def signal_CleanExit(self, args):
        self.timer1000.stop()
        QTimer.singleShot(1000, self.DBusReconnect)
        QTimer.singleShot(1500, self.timer1000.start)

    def JackShutdownCallback(self, arg=None):
        print "JackShutdownCallback"
        self.emit(SIGNAL("ShutdownCallback"))
        return 0

    def _ShutdownCallback(self):
        if (jack.client):
          jacklib.deactivate(jack.client)
          jacklib.client_close(jack.client)
          jack.client = None

    def refreshXruns(self):
        xruns = int(DBus.jack.GetXruns())
        setXruns(self, xruns)

    def refreshDBusFix(self):
        # Dummy function to force refresh of ladish DBus
        DBus.ladish_control.GetStudioList()

    def configureClaudia(self):
        ClaudiaSettingsW(self).exec_()

    def aboutClaudia(self):
        QMessageBox.about(self, self.tr("About Claudia"), self.tr("<h3>Claudia</h3>"
            "<br>Version %1"
            "<br>Claudia is a Graphical User Interface to LADISH.<br>"
            "<br>Copyright (C) 2010 falkTX").arg(VERSION))

    def saveSettings(self):
        settings = QSettings()
        settings.setValue("Geometry", QVariant(self.saveGeometry()))
        settings.setValue("SplitterSizes", self.splitter.saveState())
        settings.setValue("ShowToolbar", self.frame_toolbar.isVisible())
        settings.setValue("ShowStatusbar", self.frame_statusbar.isVisible())
        settings.setValue("TransportView", self.selected_transport_view)

    def loadSettings(self, size_too=True):
        settings = QSettings()
        self.restoreGeometry(settings.value("Geometry").toByteArray())

        splitter_sizes = settings.value("SplitterSizes").toByteArray()
        if (splitter_sizes):
          self.splitter.restoreState(splitter_sizes)
        else:
          self.splitter.setSizes((100, 400))

        show_toolbar = settings.value("ShowToolbar", True).toBool()
        self.act_settings_show_toolbar.setChecked(show_toolbar)
        self.frame_toolbar.setVisible(show_toolbar)

        show_statusbar = settings.value("ShowStatusbar", True).toBool()
        self.act_settings_show_statusbar.setChecked(show_statusbar)
        self.frame_statusbar.setVisible(show_statusbar)

        transport_set_view(self, settings.value("TransportView", TRANSPORT_VIEW_HMS).toInt()[0])

        self.saved_settings = {
          "Main/DefaultProjectFolder": settings.value("Main/DefaultProjectFolder", os.getenv("HOME")+"/ladish-projects").toString(),
          "Main/RefreshInterval": settings.value("Main/RefreshInterval", 100).toInt()[0],
          "Canvas/Theme": settings.value("Canvas/Theme", patchcanvas.options['theme_name']).toString(),
          "Canvas/BezierLines": settings.value("Canvas/BezierLines", patchcanvas.options['bezier_lines']).toBool(),
          "Canvas/AutoHideGroups": settings.value("Canvas/AutoHideGroups", True).toBool(),
          "Canvas/FancyEyeCandy": settings.value("Canvas/FancyEyeCandy", patchcanvas.options['fancy_eyecandy']).toBool(),
          "Canvas/Antialiasing": settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0],
          "Canvas/TextAntialiasing": settings.value("Canvas/TextAntialiasing", True).toBool(),
          "Jack/AutoStartA2J": settings.value("Jack/AutoStartA2J", True).toBool(),
          "Jack/AutoStartPulse": settings.value("Jack/AutoStartPulse", False).toBool(),
          "Jack/PulsePlayOnly": settings.value("Jack/PulsePlayOnly", False).toBool(),
          "Apps/Database": settings.value("Apps/Database", "LADISH").toString(),
          #"Apps/VST/UseWinePrefix": settings.value("VST/UseWinePrefix", True).toBool(),
          #"Apps/VST/WINEPREFIX": settings.value("VST/WINEPREFIX", os.getenv("HOME")+"/.wine").toString(),
          #"Apps/VST/WINE_RT": settings.value("VST/WINE_RT", True).toBool(),
          #"Apps/VST/WINE_SVR_RT": settings.value("VST/WINE_SVR_RT", True).toBool()
        }

    def closeEvent(self, event):
        self.saveSettings()
        patchcanvas.clear()
        return QMainWindow.closeEvent(self, event)

#--------------- main ------------------
if __name__ == '__main__':

    # Raster Graphics engine
    QRasterApplication = QApplication
    QRasterApplication.setGraphicsSystem("raster")

    # App initialization
    app = QRasterApplication(sys.argv)
    app.setApplicationName("Claudia")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    app.setWindowIcon(QIcon(":/48x48/claudia.png"))

    # Do not close on SIGUSR1
    signal.signal(signal.SIGUSR1, signal.SIG_IGN)

    # Show GUI
    gui = ClaudiaMainW()
    gui.show()

    # App-Loop
    ret = app.exec_()

    # Close Jack
    if (jack.client):
      jacklib.deactivate(jack.client)
      jacklib.client_close(jack.client)

    # Exit properly
    sys.exit(ret)
