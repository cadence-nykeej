#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import dbus
from dbus.mainloop.qt import DBusQtMainLoop
from PyQt4.QtCore import QProcess, QSettings, QThread, QVariant, SIGNAL, SLOT
from PyQt4.QtGui import QAction, QApplication, QDialog, QIcon, QMainWindow, QMessageBox, QSystemTrayIcon

# Imports (Plugins and Resources)
import icons_rc, jacksettings, ui_cadence
from xicon import XIcon
from shared import *

DBus.loop = DBusQtMainLoop(set_as_default=True)
DBus.bus  = dbus.SessionBus(mainloop=DBus.loop)

def refreshDBus():
  DBus.jack = DBus.bus.get_object("org.jackaudio.service", "/org/jackaudio/Controller")

  try:
    DBus.a2j = dbus.Interface(DBus.bus.get_object("org.gna.home.a2jmidid", "/"), "org.gna.home.a2jmidid.control")
  except:
    DBus.a2j = None

  if ("org.ladish" in DBus.bus.list_names()):
    DBus.ladish_control = DBus.bus.get_object("org.ladish", "/org/ladish/Control")
    if (bool(DBus.ladish_control.IsStudioLoaded())):
      DBus.ladish_studio = DBus.bus.get_object("org.ladish", "/org/ladish/Studio")
    else:
      DBus.ladish_studio = None

  else:
    DBus.ladish_control = None
    DBus.ladish_studio = None

  jacksettings.initBus(DBus.bus)

# Main Window
class CadenceMainW(QMainWindow, ui_cadence.Ui_CadenceMainW):
    def __init__(self, parent=None):
        super(CadenceMainW, self).__init__(parent)
        self.setupUi(self)

        # Load App Settings
        self.settings = QSettings()
        self.loadSettings()

        # TODO - Not Implemented Yet
        self.b_jack_restart.setEnabled(False)
        self.tb_jack.setEnabled(False)
        self.group_checks.setEnabled(False)
        self.group_a2j.setEnabled(False)
        self.group_alsa.setEnabled(False)
        self.group_pulse.setEnabled(False)
        self.menu_Jack_Buffer_Size.setEnabled(False)
        self.menu_A2J.setEnabled(False)
        self.menu_Pulse.setEnabled(False)
        self.menu_Tools.setEnabled(False)
        self.act_help_doc.setEnabled(False)
        self.tabWidget.setTabEnabled(1, False)
        self.tabWidget.setTabEnabled(2, False)

        # Globals
        self.l_distro.setText(os.uname()[0]) #FIXME
        self.l_arch.setText(os.uname()[4])
        self.l_kernel.setText(os.uname()[2])
        self.l_realtime.setText("")

        # Internal timer
        self.timer = QTimer()
        self.timer.setInterval(250)

        # Set Icons
        self.MyIcons = XIcon()
        self.MyIcons.addThemeName(QStringStr(QIcon.themeName()))
        self.MyIcons.addThemeName("oxygen") #Just in case...

        self.act_quit.setIcon(getIcon("application-exit"))
        self.act_help_about.setIcon(QIcon(":/svg/j2sc.svg"))

        self.icon_apply = self.MyIcons.getIconPath("dialog-ok-apply", 16)
        self.icon_cancel = self.MyIcons.getIconPath("dialog-cancel", 16)

        # Systray
        self.act_show_gui = QAction(self.tr("Hide Main &Window"), self)

        self.systray_menu = QMenu(self)
        self.systray_menu.addAction(self.act_show_gui)
        self.systray_menu.addSeparator()
        self.systray_menu.addMenu(self.menu_Jack)
        self.systray_menu.addMenu(self.menu_A2J)
        self.systray_menu.addMenu(self.menu_Pulse)
        self.systray_menu.addSeparator()
        self.systray_menu.addAction(self.act_quit)

        self.systray = QSystemTrayIcon(self)
        self.systray.setContextMenu(self.systray_menu)
        self.systray.setIcon(QIcon(":/svg/j2sc.svg"))
        self.systray.show()

        if (bool(DBus.jack.IsStarted())):
          self.jackStarted()
        else:
          self.jackStopped()

        # DBus Stuff
        DBus.bus.add_signal_receiver(self.DBusSignalReceiver, sender_keyword='sender', destination_keyword='dest', interface_keyword='interface',
                        member_keyword='member', path_keyword='path')

        # Connect actions to functions
        self.connect(self.act_jack_start, SIGNAL("triggered()"), self.func_Jack_Start)
        self.connect(self.act_jack_stop, SIGNAL("triggered()"), self.func_Jack_Stop)
        #self.connect(self.act_force, SIGNAL("triggered()"), self.func_Jack_Restart)
        self.connect(self.act_jack_configure, SIGNAL("triggered()"), lambda: configureJack(self))
        self.connect(self.b_jack_start, SIGNAL("clicked()"), self.func_Jack_Start)
        self.connect(self.b_jack_stop, SIGNAL("clicked()"), self.func_Jack_Stop)
        #self.connect(self.b_jack_restart, SIGNAL("clicked()"), self.func_Jack_Restart)
        self.connect(self.b_jack_configure, SIGNAL("clicked()"), lambda: configureJack(self))

        self.connect(self.act_bf_32, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 32))
        self.connect(self.act_bf_64, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 64))
        self.connect(self.act_bf_128, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 128))
        self.connect(self.act_bf_256, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 256))
        self.connect(self.act_bf_512, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 512))
        self.connect(self.act_bf_1024, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 1024))
        self.connect(self.act_bf_2048, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 2048))
        self.connect(self.act_bf_4096, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 4096))
        self.connect(self.act_bf_8192, SIGNAL("triggered(bool)"), lambda: func_set_buffer_size_m(self, 8192))

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutCadence)

        self.connect(self.act_show_gui, SIGNAL("triggered()"), self.func_show_gui)
        self.connect(self.systray, SIGNAL("activated(QSystemTrayIcon::ActivationReason)"), self.func_clicked_systray)

        self.connect(self, SIGNAL("ShutdownCallback"), self._ShutdownCallback)
        self.connect(self.timer, SIGNAL("timeout()"), self.refreshJackStatus)

        self.refreshJackStatus()

    def DBusSignalReceiver(self, *args, **kwds):
        if (kwds['interface'] == "org.jackaudio.JackControl"):
          print "jack signal", kwds['member']
          if (kwds['member'] == "ServerStarted"):
            self.jackStarted()
          elif (kwds['member'] == "ServerStopped"):
            self.jackStopped()

    def jackStarted(self):
        if (not jack.client):
          jack.client = jacklib.client_open("claudia", jacklib.NullOption, None)
          jacklib.on_shutdown(jack.client, self.JackShutdownCallback)
          jacklib.activate(jack.client)

        self.timer.start()

        self.b_jack_start.setEnabled(False)
        self.b_jack_stop.setEnabled(True)
        self.b_jack_configure.setEnabled(True)
        self.act_jack_start.setEnabled(False)
        self.act_jack_stop.setEnabled(True)
        self.act_jack_configure.setEnabled(True)

        if (self.settings.value("A2J/AutoStart", False).toBool() and DBus.a2j and not DBus.a2j.is_started()):
          DBus.a2j.start()

    def jackStopped(self):
        if (jack.client):
          jacklib.deactivate(jack.client)
          jacklib.client_close(jack.client)
          jack.client = None

        self.timer.stop()

        self.b_jack_start.setEnabled(True)
        self.b_jack_stop.setEnabled(False)
        self.b_jack_configure.setEnabled(True)
        self.act_jack_start.setEnabled(True)
        self.act_jack_stop.setEnabled(False)
        self.act_jack_configure.setEnabled(True)

    def func_show_gui(self):
        if (self.isVisible()):
          self.act_show_gui.setText(self.tr("Show Main &Window"))
          self.hide()
        else:
          self.act_show_gui.setText(self.tr("Hide Main &Window"))
          self.show()

    def func_clicked_systray(self, reason):
        if (reason == QSystemTrayIcon.DoubleClick or reason == QSystemTrayIcon.Trigger):
          self.func_show_gui()

    def func_Jack_Start(self):
        try:
          if (DBus.ladish_studio and DBus.ladish_control and bool(DBus.ladish_control.IsStudioLoaded())):
            DBus.ladish_studio.Start()
          else:
            DBus.jack.StartServer()
        except:
          QMessageBox.critical(self, self.tr("Error"), self.tr("Could not start Jack!"))

    def func_Jack_Stop(self):
        try:
          if (DBus.ladish_studio and DBus.ladish_control and bool(DBus.ladish_control.IsStudioLoaded())):
            DBus.ladish_studio.Stop()
          else:
            DBus.jack.StopServer()
        except:
          QMessageBox.critical(self, self.tr("Error"), self.tr("Could not stop Jack!"))

    def refreshJackStatus(self):
        if bool(DBus.jack.IsStarted()):
          latency = DBus.jack.GetLatency()
          if (latency < 10):
            latD = 3
          elif (latency < 100):
            latD = 4
          elif (latency < 1000):
            latD = 5
          elif (latency < 10000):
            latD = 6
          str_latency = str(latency)[0:latD]

          self.l_jackserver.setText(self.tr("Running"))
          self.l_ico_jackserver.setPixmap(QIcon(self.MyIcons.getIconPath("dialog-ok-apply", 16)).pixmap(16, 16))
          self.l_samplerate.setText(str(DBus.jack.GetSampleRate())+" Hz")
          self.l_latency.setText(str_latency+" ms")
          self.l_buffersize.setText(str(DBus.jack.GetBufferSize())+" Samples")
          self.l_xruns.setText(str(DBus.jack.GetXruns()))

          dspload = str(DBus.jack.GetLoad())[0:4]
          self.l_dspload.setText(str(dspload)+"%")

          if bool(DBus.jack.IsRealtime()):
            self.l_realtime.setText(self.tr("Yes"))
            self.l_ico_realtime.setPixmap(QIcon(self.MyIcons.getIconPath("dialog-ok-apply", 16)).pixmap(16, 16))
          else:
            self.l_realtime.setText(self.tr("No"))
            self.l_ico_realtime.setPixmap(QIcon(self.MyIcons.getIconPath("dialog-cancel", 16)).pixmap(16, 16))

          #try:
            #size = str(DBus.jackBus.GetBufferSize())
            #self.menuBufferSize(size)
          #except:
            #self.menuBufferSize(0)

          if (jack.client):
            self.l_jack.setText(self.tr("Jack is Running"))
          else:
            self.l_jack.setText(self.tr("Jack is Sick"))

        else:
          self.l_jackserver.setText(self.tr("Stopped"))
          self.l_ico_jackserver.setPixmap(QIcon(self.MyIcons.getIconPath("dialog-cancel", 16)).pixmap(16, 16))
          self.l_samplerate.setText("---")
          self.l_latency.setText("---")
          self.l_buffersize.setText("---")
          self.l_xruns.setText("---")
          self.l_dspload.setText("---")
          #self.menuBufferSize(0)

          self.l_jack.setText(self.tr("Jack is Stopped"))

        systrayText = self.tr("<table>"
                       "<tr><td align='center' colspan='2'><h4>Cadence</h4></td></tr>"
                       "<tr><td align='right'>Jack Status:</td><td>%1</td></tr>"
                       "<tr><td align='right'>Realtime:</td><td>%2</td></tr>"
                       "<tr><td align='right'>DSP Load:</td><td>%3</td></tr>"
                       "<tr><td align='right'>Xruns:</td><td>%4</td></tr>"
                       "<tr><td align='right'>Buffer Size:</td><td>%5</td></tr>"
                       "<tr><td align='right'>Sample Rate:</td><td>%6</td></tr>"
                       "<tr><td align='right'>Latency:</td><td>%7</td></tr>"
                       "</table>").arg(self.l_jackserver.text()).arg(
                       self.l_realtime.text()).arg(self.l_dspload.text()).arg(self.l_xruns.text()).arg(
                       self.l_buffersize.text()).arg(self.l_samplerate.text()).arg(self.l_latency.text())
        self.systray.setToolTip(systrayText)

    def JackShutdownCallback(self, arg=None):
        print "JackShutdownCallback"
        self.emit(SIGNAL("ShutdownCallback"))
        return 0

    def _ShutdownCallback(self):
        if (jack.client):
          jacklib.deactivate(jack.client)
          jacklib.client_close(jack.client)
          jack.client = None

    def customMessageBox(self, icon, title, text, extra_text=""):
        msgBox = QMessageBox(self)
        msgBox.setIcon(icon)
        msgBox.setWindowTitle(title)
        msgBox.setText(text)
        msgBox.setInformativeText(extra_text)
        msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msgBox.setDefaultButton(QMessageBox.No)
        return msgBox.exec_()

    def aboutCadence(self):
        QMessageBox.about(self, self.tr("About Cadence"), self.tr("<h3>Cadence</h3>"
            "<br>Version %1"
            "<br>Cadence is a set of tools useful for Audio-related tasks, using Python and Qt.<br>"
            "<br>Copyright (C) 2010 falkTX").arg(VERSION))

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))

        self.settings.setValue("Jack/AutoStart", self.cb_jack_autostart.isChecked())
        self.settings.setValue("A2J/AutoStart", self.cb_a2j_autostart.isChecked())
        self.settings.setValue("Pulse2Jack/AutoStart", self.cb_pulse_autostart.isChecked())

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())

        self.cb_jack_autostart.setChecked(self.settings.value("Jack/AutoStart", False).toBool())
        self.cb_a2j_autostart.setChecked(self.settings.value("A2J/AutoStart", False).toBool())
        self.cb_pulse_autostart.setChecked(self.settings.value("Pulse2Jack/AutoStart", False).toBool())

    def closeEvent(self, event):
        self.saveSettings()
        return QMainWindow.closeEvent(self, event)


#--------------- main ------------------
if __name__ == '__main__':

    # Raster Graphics engine
    QRasterApplication = QApplication
    QRasterApplication.setGraphicsSystem("raster")

    # App initialization
    app = QRasterApplication(sys.argv)
    app.setApplicationName("Cadence")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    app.setWindowIcon(QIcon(":/svg/j2sc.svg")) #FIXME - Use this icon??

    # Connect to DBus
    refreshDBus()

    # Show GUI
    gui = CadenceMainW()
    gui.show()

    # App-Loop
    ret = app.exec_()

    # Close Jack
    if (jack.client):
      jacklib.deactivate(jack.client)
      jacklib.client_close(jack.client)

    # Exit properly
    sys.exit(ret)
