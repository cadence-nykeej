#!/usr/bin/env python
# -*- coding: utf-8 -*-

# NOTE - This is a test app to see how far jacklib can go

# Imports (Global)
import sys
from PyQt4.QtCore import SIGNAL
from PyQt4.QtGui import QApplication, QDialog

# Imports (Plugins and Resources)
import ctypes, jacklib, icons_rc, pixmapdial, ui_plugin_audio_gain

global x_left, x_right
x_left  = 1.0
x_right = 1.0

print "wmmmm"

def process_callback(nframes, arg):
  global x_left, x_right

  p_in1 = jacklib.port_get_buffer_audio(in_port1, nframes)
  p_in2 = jacklib.port_get_buffer_audio(in_port2, nframes)
  p_out1 = jacklib.port_get_buffer_audio(out_port1, nframes)
  p_out2 = jacklib.port_get_buffer_audio(out_port2, nframes)

  for i in range(nframes):
    p_out1[i] == p_in1[i]
    p_out2[i] == p_in2[i]

  return 0

client = jacklib.client_open("meter2", jacklib.NullOption, 0)
in_port1 = jacklib.port_register(client, "in-left", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsInput, 0)
in_port2 = jacklib.port_register(client, "in-right", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsInput, 0)
out_port1 = jacklib.port_register(client, "out-left", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsOutput, 0)
out_port2 = jacklib.port_register(client, "out-right", jacklib.DEFAULT_AUDIO_TYPE, jacklib.PortIsOutput, 0)

jacklib.set_process_callback(client, process_callback, 0)
jacklib.activate(client)

# TESTING!!!!!!!!!
class AudioGainW(QDialog, ui_plugin_audio_gain.Ui_AudioGainW):
    def __init__(self, parent=None):
        super(AudioGainW, self).__init__(parent)
        self.setupUi(self)

        self.dial_left.setPixmap(4)
        self.dial_right.setPixmap(4)

        self.connect(self.dial_left, SIGNAL("valueChanged(int)"), self.checkValueL)
        self.connect(self.dial_right, SIGNAL("valueChanged(int)"), self.checkValueR)

        self.resize(250, 0)

    def checkValueL(self, value):
        global x_left
        x_left = (float(value+50)/100)*2

    def checkValueR(self, value):
        global x_right
        x_right = (float(value+50)/100)*2

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)

    # Show GUI
    gui = AudioGainW()
    gui.show()

    # App-Loop
    ret = app.exec_()

    jacklib.deactivate(client)
    jacklib.client_close(client)

    sys.exit(ret)

