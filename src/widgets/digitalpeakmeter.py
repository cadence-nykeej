#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QRectF
from PyQt4.QtGui import QColor, QLinearGradient, QPainter, QWidget

# Simple Audio Meter
class DigitalPeakMeter(QWidget):
    def __init__(self, parent=None):
        super(DigitalPeakMeter, self).__init__(parent)

        self.HORIZONTAL = 0
        self.VERTICAL = 1

        self.n_channels = 0

        self.meter_gradient = QLinearGradient(0.0, 0, 0, 10)
        self.meter_gradient.setColorAt(0.0, Qt.red)
        self.meter_gradient.setColorAt(0.2, Qt.yellow)
        self.meter_gradient.setColorAt(0.6, Qt.green)
        self.meter_gradient.setColorAt(1.0, Qt.green)

        self.checkSizes()
        self.setOrientation(self.HORIZONTAL)
        self.setChannels(2)

        self.resize(50, 500)

    def displayMeter(self, meter_n, level):
        if (self.n_channels < meter_n):
          print "Invalid meter number"
          return

        level = abs(float(level))

        if (level < 0.0):
          level = 0.0
        elif (level > 0.5):
          level = 0.5

        self.channels_data[meter_n-1] = level
        self.update()

    def checkSizes(self):
        self.width_ = self.width()
        self.height_ = self.height()

        self.meter_gradient.setFinalStop(self.width_, self.height_)

        if (self.n_channels > 0):
          self.meter_size = self.width_/self.n_channels

    def setOrientation(self, orientation_):
        self.orientiation = orientation_

    def setChannels(self, channels):
        self.n_channels = channels
        self.channels_data = []
        self.last_max_data = []

        for i in range(channels):
          self.channels_data.append(0.0)
          self.last_max_data.append(self.height_)

    def paintEvent(self, event):
        painter = QPainter(self)

        painter.setPen(Qt.black)
        painter.setBrush(Qt.black)
        painter.drawRect(0, 0, self.width_, self.height_)

        meter_x = 0

        for i in range(self.n_channels):
          level = self.channels_data[i]
          value = self.height_-(self.height_*level*2)

          # Don't bounce the meter so much
          value = (self.last_max_data[i]+self.last_max_data[i]+value)/3

          if (value < 0):
            value = 0

          painter.setPen(QColor("#111111"))
          painter.setBrush(self.meter_gradient)
          painter.drawRect(meter_x, value, self.meter_size, self.height_)

          meter_x += self.meter_size
          self.last_max_data[i] = value

    def resizeEvent(self, event):
        QTimer.singleShot(0, self.checkSizes)
        return QWidget.resizeEvent(self, event)

