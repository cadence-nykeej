#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import os, signal, sys
from PyQt4.QtCore import Qt, QSettings, QTimer, SIGNAL
from PyQt4.QtGui import QApplication, QCursor, QFileDialog, QFontMetrics, QIcon, QImage
from PyQt4.QtGui import QMessageBox, QMainWindow, QMenu, QPainter, QPrintDialog, QPrinter

# Imports (Custom Stuff)
import jacklib, jacksettings, icons_rc, logs, patchcanvas, render

# Set Version
VERSION = "0.0.10"

# Set Home dir
HOME = os.getenv("HOME")

# Set Path
PATH = os.getenv("PATH").split(":")

# Set Current Working Directory (where 'shared.py' is located)
PWD = sys.path[0]

# Set Platform
if ("linux" in sys.platform):
  LINUX = True
  WINDOWS = False
elif ("win" in sys.platform):
  LINUX = False
  WINDOWS = True
else:
  LINUX = False
  WINDOWS = False

# Have JACK2 ?
try:
  jacklib.get_version_string()
  JACK2 = True
except:
  JACK2 = False

# Can Render ?
for i in range(len(PATH)):
  if (os.path.exists(os.path.join(PATH[i], "jack_capture"))):
    canRender = True
    break
else:
  canRender = False

# Do not close on SIGUSR1
if (not WINDOWS):
  signal.signal(signal.SIGUSR1, signal.SIG_IGN)

# Variables
TRANSPORT_VIEW_HMS = 0
TRANSPORT_VIEW_BBT = 1
TRANSPORT_VIEW_FRAMES = 2

sample_rates = (22050, 32000, 44100, 48000, 88200, 96000, 192000)

# DBus object
class DBus(object):
    __slots__ = [
            'loop',
            'bus',
            'a2j',
            'jack',
            'patchbay',
            'ladish_control',
            'ladish_studio',
            'ladish_room',
            'ladish_graph',
            'ladish_app_iface',
            'ladish_app_daemon'
    ]
DBus = DBus()

# Jack object
class Jack(object):
    __slots__ = [
            'client',
            'midi_in_data',
            'midi_in_port',
            'midi_out_port',
            'midi_out_data'
    ]
jack = Jack()

# Init objects
DBus.loop = None
DBus.bus = None
DBus.a2j = None
DBus.jack = None
DBus.patchbay = None
DBus.ladish_control = None
DBus.ladish_studio = None
DBus.ladish_room = None
DBus.ladish_graph = None
DBus.ladish_app_iface = None
DBus.ladish_app_daemon = None

jack.client = None
jack.midi_in_data = None
jack.midi_in_port = None
jack.midi_out_data = None
jack.midi_out_port = None

# Custom MessageBox
def CustomMessageBox(self, icon, title, text, extra_text=""):
    msgBox = QMessageBox(self)
    msgBox.setIcon(icon)
    msgBox.setWindowTitle(title)
    msgBox.setText(text)
    msgBox.setInformativeText(extra_text)
    msgBox.setStandardButtons(QMessageBox.Yes|QMessageBox.No)
    msgBox.setDefaultButton(QMessageBox.No)
    return msgBox.exec_()

# Properly convert QString to str
def QStringStr(string):
    return str(unicode(string).encode('utf-8'))

# Raster Graphics Application
QRasterApplication = QApplication
QRasterApplication.setGraphicsSystem("raster")

# Cadence Global Settings
GlobalSettings = QSettings("GlobalSettings", "KXStudio")

# QLineEdit and QPushButtom combo
def getAndSetPath(parent, cur_path, lineEdit):
    new_path = QFileDialog.getExistingDirectory(parent, parent.tr("Set Path"), cur_path, QFileDialog.ShowDirsOnly)

    if not new_path.isEmpty():
      lineEdit.setText(new_path)

    return new_path

# Get Icon from user theme, using our own as backup (Oxygen)
def getIcon(icon, size=16):
    return QIcon.fromTheme(icon, QIcon(":/%ix%i/.png" % (size,size)))

# Shared Icons
def setIcons(self, modes):
  if ("canvas" in modes):
    self.act_canvas_arrange.setIcon(getIcon("view-sort-ascending"))
    self.act_canvas_refresh.setIcon(getIcon("view-refresh"))
    self.act_canvas_zoom_fit.setIcon(getIcon("zoom-fit-best"))
    self.act_canvas_zoom_in.setIcon(getIcon("zoom-in"))
    self.act_canvas_zoom_out.setIcon(getIcon("zoom-out"))
    self.act_canvas_zoom_100.setIcon(getIcon("zoom-original"))
    self.act_canvas_print.setIcon(getIcon("document-print"))
    self.b_canvas_zoom_fit.setIcon(getIcon("zoom-fit-best"))
    self.b_canvas_zoom_in.setIcon(getIcon("zoom-in"))
    self.b_canvas_zoom_out.setIcon(getIcon("zoom-out"))
    self.b_canvas_zoom_100.setIcon(getIcon("zoom-original"))

  if ("jack_" in modes): # TEST - change this soon
    self.act_jack_clear_xruns.setIcon(getIcon("edit-clear"))
    self.act_jack_configure.setIcon(getIcon("configure"))
    self.act_jack_render.setIcon(getIcon("media-record"))
    self.b_jack_clear_xruns.setIcon(getIcon("edit-clear"))
    self.b_jack_configure.setIcon(getIcon("configure"))
    self.b_jack_render.setIcon(getIcon("media-record"))

  if ("transport" in modes):
    self.act_transport_play.setIcon(getIcon("media-playback-start"))
    self.act_transport_stop.setIcon(getIcon("media-playback-stop"))
    self.act_transport_backwards.setIcon(getIcon("media-seek-backward"))
    self.act_transport_forwards.setIcon(getIcon("media-seek-forward"))
    self.b_transport_play.setIcon(getIcon("media-playback-start"))
    self.b_transport_stop.setIcon(getIcon("media-playback-stop"))
    self.b_transport_backwards.setIcon(getIcon("media-seek-backward"))
    self.b_transport_forwards.setIcon(getIcon("media-seek-forward"))

  if ("misc" in modes):
    self.act_quit.setIcon(getIcon("application-exit"))
    self.act_configure.setIcon(getIcon("configure"))

# Shared Connections
def setConnections(self, modes):
  if ("canvas" in modes):
    self.connect(self.act_canvas_arrange, SIGNAL("triggered()"), lambda: canvas_arrange(self))
    self.connect(self.act_canvas_refresh, SIGNAL("triggered()"), lambda: canvas_refresh(self))
    self.connect(self.act_canvas_zoom_fit, SIGNAL("triggered()"), lambda: canvas_zoom_fit(self))
    self.connect(self.act_canvas_zoom_in, SIGNAL("triggered()"), lambda: canvas_zoom_in(self))
    self.connect(self.act_canvas_zoom_out, SIGNAL("triggered()"), lambda: canvas_zoom_out(self))
    self.connect(self.act_canvas_zoom_100, SIGNAL("triggered()"), lambda: canvas_zoom_100(self))
    self.connect(self.act_canvas_print, SIGNAL("triggered()"), lambda: canvas_print(self))
    self.connect(self.act_canvas_save_image, SIGNAL("triggered()"), lambda: canvas_save_image(self))
    self.connect(self.b_canvas_zoom_fit, SIGNAL("clicked()"), lambda: canvas_zoom_fit(self))
    self.connect(self.b_canvas_zoom_in, SIGNAL("clicked()"), lambda: canvas_zoom_in(self))
    self.connect(self.b_canvas_zoom_out, SIGNAL("clicked()"), lambda: canvas_zoom_out(self))
    self.connect(self.b_canvas_zoom_100, SIGNAL("clicked()"), lambda: canvas_zoom_100(self))

  if ("jack_" in modes): #TEST - change this soon
    self.connect(self.act_jack_clear_xruns, SIGNAL("triggered()"), lambda: jack_clear_xruns(self))
    self.connect(self.act_jack_render, SIGNAL("triggered()"), lambda: jack_render(self))
    self.connect(self.act_jack_configure, SIGNAL("triggered()"), lambda: jack_configure(self))
    self.connect(self.act_jack_bf_16, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 16))
    self.connect(self.act_jack_bf_32, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 32))
    self.connect(self.act_jack_bf_64, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 64))
    self.connect(self.act_jack_bf_128, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 128))
    self.connect(self.act_jack_bf_256, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 256))
    self.connect(self.act_jack_bf_512, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 512))
    self.connect(self.act_jack_bf_1024, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 1024))
    self.connect(self.act_jack_bf_2048, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 2048))
    self.connect(self.act_jack_bf_4096, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 4096))
    self.connect(self.act_jack_bf_8192, SIGNAL("triggered(bool)"), lambda: jack_buffer_size_m(self, 8192))
    self.connect(self.b_jack_clear_xruns, SIGNAL("clicked()"), lambda: jack_clear_xruns(self))
    self.connect(self.b_jack_configure, SIGNAL("clicked()"), lambda: jack_configure(self))
    self.connect(self.b_jack_render, SIGNAL("clicked()"), lambda: jack_render(self))
    self.connect(self.cb_jack_buffer_size, SIGNAL("currentIndexChanged(QString)"), lambda: jack_buffer_size_cb(self, self.cb_buffer_size.currentText()))
    self.connect(self.cb_jack_sample_rate, SIGNAL("currentIndexChanged(QString)"), lambda: jack_sample_rate_cb(self, self.cb_sample_rate.currentText()))

  if ("transport_" in modes):
    self.connect(self.act_transport_play, SIGNAL("triggered(bool)"), lambda: transport_playpause(self, self.act_transport_play.isChecked()))
    self.connect(self.act_transport_stop, SIGNAL("triggered()"), lambda: transport_stop(self))
    self.connect(self.act_transport_backwards, SIGNAL("triggered()"), lambda: transport_backwards(self))
    self.connect(self.act_transport_forwards, SIGNAL("triggered()"), lambda: transport_forwards(self))
    self.connect(self.b_transport_play, SIGNAL("clicked(bool)"), lambda: transport_playpause(self, self.b_transport_play.isChecked()))
    self.connect(self.b_transport_stop, SIGNAL("clicked()"), lambda: transport_stop(self))
    self.connect(self.b_transport_backwards, SIGNAL("clicked()"), lambda: transport_backwards(self))
    self.connect(self.b_transport_forwards, SIGNAL("clicked()"), lambda: transport_forwards(self))
    self.connect(self.sb_transport_bpm, SIGNAL("valueChanged(double)"), lambda: transport_bpm_set(self, self.sb_bpm.value()))
    self.connect(self.label_time, SIGNAL("customContextMenuRequested(QPoint)"), lambda: transport_view_menu(self))

  if ("misc" in modes):
    self.connect(self.act_show_logs, SIGNAL("triggered()"), lambda: show_logs(self))

# Shared init code
def Init(self, app):
  self.buffer_size = 0
  self.sample_rate = 0
  self.last_buffer_size = 0
  self.last_sample_rate = 0
  self.next_sample_rate = 0

  self.last_bpm = None
  self.last_transport_state = None

  self.cb_sample_rate.clear()

  for i in range(len(sample_rates)):
    self.cb_sample_rate.addItem(str(sample_rates[i]))

  self.act_jack_bf_list = [] #(self.act_jack_bf_16, self.act_jack_bf_32, self.ac_jack_bf_64, self.act_jack_bf_128, self.act_jack_bf_256, self.act_jack_bf_512,
                           #self.act_jack_bf_1024, self.act_jack_bf_2048, self.act_jack_bf_4096, self.act_jack_bf_8192)

  self.settings = QSettings(app.applicationName(), app.organizationName())

  self.saved_settings = {
    "Main/DefaultProjectFolder": self.settings.value("Main/DefaultProjectFolder", os.path.join(HOME, "ladish-projects")).toString(),
    "Main/RefreshInterval": self.settings.value("Main/RefreshInterval", 100).toInt()[0],
    "Canvas/Theme": self.settings.value("Canvas/Theme", patchcanvas.getDefaultThemeName()).toString(),
    "Canvas/BezierLines": self.settings.value("Canvas/BezierLines", True).toBool(),
    "Canvas/AutoHideGroups": self.settings.value("Canvas/AutoHideGroups", True).toBool(),
    "Canvas/FancyEyeCandy": self.settings.value("Canvas/FancyEyeCandy", False).toBool(),
    "Canvas/Antialiasing": self.settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0],
    "Canvas/TextAntialiasing": self.settings.value("Canvas/TextAntialiasing", True).toBool(),
    "Apps/Database": self.settings.value("Apps/Database", "LADISH").toString()
  }


# Shared Canvas code
def canvas_arrange(self):
    patchcanvas.Arrange()

    if ("miniCanvasPreview" in dir(self)):
      self.miniCanvasPreview.update()

def canvas_refresh(self):
    patchcanvas.clear()
    self.init_ports()

def canvas_zoom_fit(self):
    min_x = min_y = max_x = max_y = None
    items = self.graphicsView.items()

    if (len(items) == 0):
      return

    for i in range(len(items)):
      if (items[i].isVisible() and type(items[i]) == patchcanvas.CanvasBox):
        pos = items[i].scenePos()
        rect = items[i].boundingRect()

        if (not min_x):
          min_x = pos.x()
        elif (pos.x() < min_x):
          min_x = pos.x()

        if (not min_y):
          min_y = pos.y()
        elif (pos.y() < min_y):
          min_y = pos.y()

        if (not max_x):
          max_x = pos.x()+rect.width()
        elif (pos.x()+rect.width() > max_x):
          max_x = pos.x()+rect.width()

        if (not max_y):
          max_y = pos.y()+rect.height()
        elif (pos.y()+rect.height() > max_y):
          max_y = pos.y()+rect.height()

    self.graphicsView.fitInView(min_x, min_y, abs(max_x-min_x), abs(max_y-min_y), Qt.KeepAspectRatio)

def canvas_zoom_in(self):
    self.graphicsView.scale(1.2, 1.2)

def canvas_zoom_out(self):
    self.graphicsView.scale(0.8, 0.8)

def canvas_zoom_100(self):
    self.graphicsView.resetTransform()

def canvas_print(self):
    self.scene.clearSelection()
    self.printer = QPrinter()
    dialog = QPrintDialog(self.printer, self)
    if (dialog.exec_()):
      painter = QPainter(self.printer)
      painter.setRenderHint(QPainter.Antialiasing)
      painter.setRenderHint(QPainter.TextAntialiasing)
      self.scene.render(painter)

def canvas_save_image(self):
    path = QFileDialog.getSaveFileName(self, self.tr("Save Image"), filter=self.tr("PNG Image (*.png);;JPEG Image (*.jpg)"))

    if (not path.isEmpty()):
      self.scene.clearSelection()
      if (path.endsWith(".jpg", Qt.CaseInsensitive)):
        img_format = "JPG"
      else:
        img_format = "PNG"

      self.image = QImage(self.scene.sceneRect().width(), self.scene.sceneRect().height(), QImage.Format_RGB32)
      painter = QPainter(self.image)
      painter.setRenderHint(QPainter.Antialiasing)
      painter.setRenderHint(QPainter.TextAntialiasing)
      self.scene.render(painter)
      self.image.save(path, img_format, 100)

# Shared Jack code
def jack_buffer_size(self, buffer_size):
    if (jack.client):
      jacklib.set_buffer_size(jack.client, int(buffer_size))
    else:
      if (jacksettings.setBufferSize(buffer_size)):
        setBufferSize(self, buffer_size)

def jack_sample_rate(self, sample_rate):
    if (jack.client):
      setSampleRate(self, sample_rate, True)
    else:
      if (jacksettings.setSampleRate(sample_rate)):
        setSampleRate(self, sample_rate)

def jack_buffer_size_cb(self, text):
    jack_buffer_size(self, int(text))

def jack_buffer_size_m(self, clicked, buffer_size):
    #buffer_size = int(self.sender().text().replace("&",""))
    jack_buffer_size(self, buffer_size)

def jack_sample_rate_cb(self, text):
    jack_sample_rate(self, int(text.replace("*","")))

# Shared Transport code
def transport_playpause(self, play):
    if (not jack.client): return
    if (play):
      jacklib.transport_start(jack.client)
    else:
      jacklib.transport_stop(jack.client)
    QTimer.singleShot(50, lambda: refreshTransport(self)) #NOTE - keep this?

def transport_stop(self):
    if (not jack.client): return
    jacklib.transport_stop(jack.client)
    jacklib.transport_locate(jack.client, 0)
    QTimer.singleShot(50, lambda: refreshTransport(self)) #NOTE - keep this?

def transport_backwards(self):
    if (not jack.client): return
    new_frame = int(jacklib.get_current_transport_frame(jack.client))-100000
    if (new_frame < 0): new_frame = 0
    jacklib.transport_locate(jack.client, new_frame)

def transport_forwards(self):
    if (not jack.client): return
    new_frame = int(jacklib.get_current_transport_frame(jack.client))+100000
    jacklib.transport_locate(jack.client, new_frame)

def transport_view_menu(self):
    menu = QMenu(self)
    act_t_hms = menu.addAction("Hours:Minutes:Seconds")
    act_t_bbt = menu.addAction("Beat:Bar:Tick")
    act_t_fr = menu.addAction("Frames")

    act_t_hms.setCheckable(True)
    act_t_bbt.setCheckable(True)
    act_t_fr.setCheckable(True)

    if (self.selected_transport_view == TRANSPORT_VIEW_HMS):
      act_t_hms.setChecked(True)
    elif (self.selected_transport_view == TRANSPORT_VIEW_BBT):
      act_t_bbt.setChecked(True)
    elif (self.selected_transport_view == TRANSPORT_VIEW_FRAMES):
      act_t_fr.setChecked(True)

    act_selected = menu.exec_(QCursor().pos())

    if (act_selected == act_t_hms):
      transport_set_view(self, TRANSPORT_VIEW_HMS)
    elif (act_selected == act_t_bbt):
      transport_set_view(self, TRANSPORT_VIEW_BBT)
    elif (act_selected == act_t_fr):
      transport_set_view(self, TRANSPORT_VIEW_FRAMES)

def transport_set_view(self, view):
    if (view == TRANSPORT_VIEW_HMS):
      self.selected_transport_view = TRANSPORT_VIEW_HMS
      self.label_time.setMinimumWidth(QFontMetrics(self.label_time.font()).width("00:00:00")+3)
    elif (view == TRANSPORT_VIEW_BBT):
      self.selected_transport_view = TRANSPORT_VIEW_BBT
      self.label_time.setMinimumWidth(QFontMetrics(self.label_time.font()).width("000|0|0000")+3)
    elif (view == TRANSPORT_VIEW_FRAMES):
      self.selected_transport_view = TRANSPORT_VIEW_FRAMES
      self.label_time.setMinimumWidth(QFontMetrics(self.label_time.font()).width("000'000'000")+3)

def transport_bpm_set(self, bpm):
    if (not jack.client): return
    pos = jacklib.jack_position_t
    jacklib.transport_query(jack.client, pos) # TODO - set 1:1:1 pos if not valid
    pos.beats_per_minute = bpm
    jacklib.transport_reposition(jack.client, pos)

# Shared GUI Code
def refreshBufferSize(self):
    if (self.last_buffer_size != self.buffer_size):
      setBufferSize(self, self.buffer_size)

    self.last_buffer_size = self.buffer_size

def refreshSampleRate(self):
    if (self.last_sample_rate != self.sample_rate):
      setSampleRate(self, self.sample_rate)

    self.last_sample_rate = self.sample_rate

def refreshDSPLoad(self):
    if (not jack.client): return
    setDSPLoad(self, int(jacklib.cpu_load(jack.client)))

def refreshTransport(self):
    if (not jack.client): return
    pos = jacklib.jack_position_t
    state = jacklib.transport_query(jack.client, pos)

    if (self.selected_transport_view == TRANSPORT_VIEW_HMS):
      frame = pos.frame
      time = frame / self.sample_rate
      secs = time % 60
      mins = (time / 60) % 60
      hrs  = (time / 3600) % 60
      secH = minH = hrsH = ""
      if secs < 10: secH = "0"
      if mins < 10: minH = "0"
      if hrs  < 10: hrsH = "0"
      self.label_time.setText(hrsH+str(hrs)+":"+minH+str(mins)+":"+secH+str(secs))

    elif (self.selected_transport_view == TRANSPORT_VIEW_BBT):
      bar  = pos.bar
      beat = pos.beat
      tick = pos.tick
      barH = beatH = tickH = ""
      # TODO - ask jack for fields size
      if (bar == 0):
        beat = 0
        tick = 0
        barH = "00"
      elif bar < 10: barH = "00"
      elif bar < 100: barH = "0"
      #if beat < 10: beatH = "0"
      if tick < 10: tickH = "000"
      elif tick < 100: tickH = "00"
      elif tick < 1000: tickH = "0"
      self.label_time.setText(barH+str(bar)+"|"+beatH+str(beat)+"|"+tickH+str(tick))

    elif (self.selected_transport_view == TRANSPORT_VIEW_FRAMES):
      frame = pos.frame
      frame1 = pos.frame % 1000
      frame2 = (pos.frame / 1000) % 1000
      frame3 = (pos.frame / 1000000) % 1000
      frame1h = frame2h = frame3h = ""
      if frame1 < 10: frame1h = "00"
      elif frame1 < 100: frame1h = "0"
      if frame2 < 10: frame2h = "00"
      elif frame2 < 100: frame2h = "0"
      if frame3 < 10: frame3h = "00"
      elif frame3 < 100: frame3h = "0"
      self.label_time.setText(frame3h+str(frame3)+"'"+frame2h+str(frame2)+"'"+frame1h+str(frame1))

    if (pos.beats_per_minute > 0.1): #FIXME - investigate this
      if (pos.beats_per_minute != self.last_bpm):
        self.sb_bpm.setValue(pos.beats_per_minute)
        self.sb_bpm.setStyleSheet("")
    else:
      self.sb_bpm.setStyleSheet("color: palette(mid);") #FIXME - inside the box font color only, or affects right click menu

    self.last_bpm = pos.beats_per_minute

    if (state != self.last_transport_state):
      if (state == jacklib.TransportStopped):
        icon = getIcon("media-playback-start")
        self.act_transport_play.setChecked(False)
        self.act_transport_play.setIcon(icon)
        self.act_transport_play.setText(self.tr("&Play"))
        self.b_transport_play.setChecked(False)
        self.b_transport_play.setIcon(icon)
      elif (state == jacklib.TransportStarting or state == jacklib.TransportRolling):
        icon = getIcon("media-playback-pause")
        self.act_transport_play.setChecked(True)
        self.act_transport_play.setIcon(icon)
        self.act_transport_play.setText(self.tr("&Pause"))
        self.b_transport_play.setChecked(True)
        self.b_transport_play.setIcon(icon)
    self.last_transport_state = state

    del pos #TODO - really needed?

def setBufferSize(self, buffer_size):
    self.buffer_size = buffer_size
    if (buffer_size):
      if (buffer_size == 16):
        self.cb_buffer_size.setCurrentIndex(0)
      elif (buffer_size == 32):
        self.cb_buffer_size.setCurrentIndex(1)
      elif (buffer_size == 64):
        self.cb_buffer_size.setCurrentIndex(2)
      elif (buffer_size == 128):
        self.cb_buffer_size.setCurrentIndex(3)
      elif (buffer_size == 256):
        self.cb_buffer_size.setCurrentIndex(4)
      elif (buffer_size == 512):
        self.cb_buffer_size.setCurrentIndex(5)
      elif (buffer_size == 1024):
        self.cb_buffer_size.setCurrentIndex(6)
      elif (buffer_size == 2048):
        self.cb_buffer_size.setCurrentIndex(7)
      elif (buffer_size == 4096):
        self.cb_buffer_size.setCurrentIndex(8)
      elif (buffer_size == 8192):
        self.cb_buffer_size.setCurrentIndex(9)
      else:
        QMessageBox.warning(self, self.tr("Warning"), self.tr("Invalid JACK Buffer Size requested"))

      #for i in range(len(self.act_jack_bf_list)):
        #self.act_jack_bf_list[i].setEnabled(True)
        #if (QStringStr(self.act_jack_bf_list[i].text().replace("&","")) == str(buffer_size)):
          ##if (not self.act_jack_bf_list[i].isChecked()):
            #self.act_jack_bf_list[i].setChecked(True)
        #else:
          #if (self.act_jack_bf_list[i].isChecked()):
            #self.act_jack_bf_list[i].setChecked(False)
    #else:
      #for i in range(len(self.act_jack_bf_list)):
        #self.act_jack_bf_list[i].setEnabled(False)
        #if (self.act_jack_bf_list[i].isChecked()):
          #self.act_jack_bf_list[i].setChecked(False)

def setSampleRate(self, sample_rate, future=False):
      if (sample_rate != self.sample_rate):
        if (future):
          ask = QMessageBox.question(self, self.tr("Change Sample Rate"), self.tr("It's not possible to change Sample Rate while Jack is running.\n"
                                        "Do you want to change as soon as Jack stops?"), QMessageBox.Ok|QMessageBox.Cancel)
          if (ask == QMessageBox.Ok):
            self.next_sample_rate = sample_rate
          else:
            self.next_sample_rate = 0

        else:
          self.sample_rate = sample_rate
          self.next_sample_rate = 0

        self.cb_sample_rate.clear()

        for i in range(len(sample_rates)):
          text = str(sample_rates[i])
          if (self.next_sample_rate and self.sample_rate != self.next_sample_rate and sample_rates[i] == self.sample_rate):
            text += "*"

          self.cb_sample_rate.addItem(text)

          if (sample_rates[i] == self.sample_rate):
            self.cb_sample_rate.setCurrentIndex(i)

def setRealTime(self, realtime):
    self.label_realtime.setText(" RT " if realtime else " <s>RT</s> ")
    self.label_realtime.setEnabled(realtime)

def setDSPLoad(self, dsp_load):
    self.pb_dsp_load.setValue(dsp_load)

def setXruns(self, xruns):
    self.b_xruns.setText("%s Xrun%s" % ((str(xruns) if (xruns >= 0) else "--"), ("" if (xruns == 1) else "s")))

# External Dialogs
def jack_render(self):
    render.RenderW(self, jack.client).exec_()

def jack_configure(self):
    jacksettings.JackSettingsW(self).exec_()

def show_logs(self):
    logs.LogsW(self).show()

