#!/usr/bin/env python
# -*- coding: utf-8 -*-

# KXStudio package database

generic_audio_icon = "audio-x-generic"
generic_midi_icon = "audio-midi"

# Package || AppName || Type || Binary || Icon || Save || Level || License || Features[ ladspa, dssi, lv2, vst, vst-mode, transport, midi, midi-mode ] || Doc[ doc, website ]
list_DAW = [
  ( "ardour", "Ardour", "DAW", "ardour2", "ardour", "Auto", 1, "OpenSource", (1, 0, 1, 0, "", 1, 0, "ALSA"), ("file:///usr/share/kxstudio/docs/FM_Ardour_22Mar10.pdf", "http://www.ardour.org/") ),
  ( "ardourvst", "ArdourVST", "DAW", "ardourvst", "ardour", "Auto", 1, "OpenSource", (1, 0, 1, 1, "Windows", 1, 0, "ALSA"), ("file:///usr/share/kxstudio/docs/FM_Ardour_22Mar10.pdf", "http://www.ardour.org/") ),
  ( "ardourvst-32bit", "ArdourVST (32bit)", "DAW", "ardourvst", "ardour", "Auto", 1, "OpenSource", (1, 0, 1, 1, "Windows", 1, 0, "ALSA"), ("file:///usr/share/kxstudio/docs/FM_Ardour_22Mar10.pdf", "http://www.ardour.org/") ),
  ( "ardour30", "Ardour 3.0 (SVN)", "DAW", "ardour3", "ardour", "Manual", 0, "OpenSource", (1, 0, 1, 0, "", 1, 1, "Jack"), ("", "http://www.ardour.org/") ),

  ( "composite", "Composite", "DAW", "composite-gui", "composite-icon", "Manual", 0, "OpenSource", (1, 0, 0, 0, "", 1, 1, "Jack"), ("/usr/share/composite/data/doc/manual.html", "") ),
  ( "composite-git", "Composite (GIT)", "DAW", "composite-gui", "composite-icon", "Manual", 0, "OpenSource", (1, 0, 0, 0, "", 1, 1, "Jack"), ("/usr/share/composite/data/doc/manual.html", "") ),

  ( "energyxt2", "EnergyXT2", "DAW", "energyxt2", "energyxt2", "Manual", 0, "Demo", (0, 0, 0, 1, "Native", 0, 1, "Jack"), ("file:///usr/share/kxstudio/docs/EnergyXT_Manual_EN.pdf", "http://www.energy-xt.com/") ),

  ( "epichord", "Epichord", "Sequencer", "epichord", generic_audio_icon, "Manual", 0, "OpenSource", (0, 0, 0, 0, "", 0, 1, "Jack"), ("", "") ),

  ( "hydrogen", "Hydrogen", "Drum Sequencer", "hydrogen", "h2-icon", "Template", 0, "OpenSource", (1, 0, 0, 0, "", 1, 1, "ALSA"), ("file:///usr/share/hydrogen/data/doc/manual_en.html", "http://www.hydrogen-music.org/") ),
  ( "hydrogen-svn", "Hydrogen (SVN)", "Drum Sequencer", "hydrogen", "h2-icon", "Auto", 1, "OpenSource", (1, 0, 0, 0, "", 1, 1, "ALSA"), ("file:///usr/share/hydrogen/data/doc/manual_en.html", "http://www.hydrogen-music.org/") ),

  ( "lmms", "LMMS", "DAW", "lmms", "lmms", "Manual", 0, "OpenSource", (1, 0, 0, 1, "Windows", 0, 1, "ALSA"), ("", "http://lmms.sourceforge.net/") ),
  ( "lmms-git", "LMMS (GIT)", "DAW", "/opt/lmms/bin/lmms", "lmms", "Manual", 0, "OpenSource", (1, 0, 1, 1, "Windows", 0, 1, "ALSA"), ("", "http://lmms.sourceforge.net/") ),

  ( "machina", "Machina", "Sequencer", "machina_gui", "machina", "Manual", 0, "OpenSource", (0, 0, 0, 0, "", 0, 1, "Jack"), ("", "") ),
  ( "machina-svn", "Machina (SVN)", "Sequencer", "machina_gui-svn", "machina-svn", "Manual", 0, "OpenSource", (0, 0, 0, 0, "", 0, 1, "Jack"), ("", "") ),

  ( "muse", "MusE", "DAW", "muse", "muse", "Template", 0, "OpenSource", (1, 1, 0, 0, "", 0, 1, "ALSA"), ("file:///usr/share/doc/muse/html/window_ref.html", "http://www.muse-sequencer.org/") ),
  ( "muse-svn", "MusE (SVN)", "DAW", "muse", "muse", "Template", 0, "OpenSource", (1, 1, 0, 0, "", 0, 1, "ALSA"), ("file:///usr/share/doc/muse/html/window_ref.html", "http://www.muse-sequencer.org/") ),

  ( "musescore", "MuseScore", "MIDI Composer", "mscore", "mscore", "Manual", 0, "OpenSource", (0, 0, 0, 0, "", 0, 1, "Jack | ALSA"), ("file:///usr/share/kxstudio/docs/MuseScore-en.pdf", "http://www.musescore.org/") ),

  ( "non-daw", "Non-DAW", "DAW", "non-daw", "non-daw", "Template", 0, "OpenSource", (0, 0, 0, 0, "", 1, 0, "CV"), ("file:///usr/share/doc/non-daw/MANUAL.html", "http://non-daw.tuxfamily.org/") ),
  ( "non-sequencer", "Non-Sequencer", "MIDI Sequencer", "non-sequencer", "non-sequencer", "Template", 0, "OpenSource", (0, 0, 0, 0, "", 1, 1, "Jack"), ("file:///usr/share/doc/non-sequencer/MANUAL.html", "http://non-sequencer.tuxfamily.org/") ),

  ( "qtractor", "Qtractor", "DAW", "qtractor", "qtractor", "Auto", 1, "OpenSource", (1, 1, 1, 1, "Native", 1, 1, "ALSA"), ("file:///usr/share/kxstudio/docs/qtractor-0.3.0-user-manual.pdf", "http://qtractor.sourceforge.net/") ),
  ( "qtractor-32bit", "Qtractor (32bit)", "DAW", "qtractor-32bit", "qtractor", "Auto", 1, "OpenSource", (1, 1, 1, 1, "Native", 1, 1, "ALSA"), ("file:///usr/share/kxstudio/docs/qtractor-0.3.0-user-manual.pdf", "http://qtractor.sourceforge.net/") ),
  ( "qtractor-svn", "Qtractor (SVN)", "DAW", "qtractor", "qtractor", "Auto", 1, "OpenSource", (1, 1, 1, 1, "Native", 1, 1, "ALSA"), ("file:///usr/share/kxstudio/docs/qtractor-0.3.0-user-manual.pdf", "http://qtractor.sourceforge.net/") ),

  ( "reaper", "REAPER", "DAW", "reaper", "reaper", "Manual", 0, "Demo", (0, 0, 0, 1, "Windows", 1, 1, "ALSA"), ("", "http://www.reaper.fm") ),

  ( "renoise", "Renoise", "Tracker", "renoise", "renoise", "Template", 0, "ShareWare", (1, 1, 0, 1, "Native", 1, 1, "ALSA"), ("file:///usr/share/kxstudio/docs/Renoise User Manual.pdf", "http://www.renoise.com/") ),
  ( "renoise-x64", "Renoise (64bit)", "Tracker", "renoise-x64", "renoise", "Template", 0, "ShareWare", (1, 1, 0, 1, "Native", 1, 1, "ALSA"), ("file:///usr/share/kxstudio/docs/Renoise User Manual.pdf", "http://www.renoise.com/") ),

  ( "rosegarden", "Rosegarden", "MIDI Sequencer", "rosegarden", "rosegarden", "Auto", 1, "OpenSource", (1, 1, 0, 0, "", 1, 1, "ALSA"), ("", "http://www.rosegardenmusic.com/") ),
  ( "rosegarden-kde3", "Rosegarden (KDE3)", "MIDI Sequencer", "rosegarden-kde3", "rosegarden", "Manual", 0, "OpenSource", (1, 1, 0, 0, "", 1, 1, "ALSA"), ("", "http://www.rosegardenmusic.com/") ),

  ( "seq24", "Seq24", "MIDI Sequencer", "seq24", "seq24", "Auto", 1, "OpenSource", (0, 0, 0, 0, "", 1, 1, "ALSA"), ("file:///usr/share/kxstudio/docs/SEQ24", "http://www.filter24.org/seq24/") ),

  ( "traverso", "Traverso", "Audio Sequencer", "traverso", "traverso", "Manual", 0, "OpenSource", (1, 0, 1, 0, "", 1, 0, ""), ("file:///usr/share/kxstudio/docs/traverso-manual-0.49.0.pdf", "http://traverso-daw.org/") ),
  ( "traverso-git", "Traverso (GIT)", "Audio Sequencer", "traverso", "traverso", "Manual", 0, "OpenSource", (1, 0, 1, 0, "", 1, 0, ""), ("file:///usr/share/kxstudio/docs/traverso-manual-0.49.0.pdf", "http://traverso-daw.org/") ),
]

# Package || AppName || Instruments? || Effects? || Binary || Icon || Save || Level || License || Features[ internal, ladspa, dssi, lv2, vst, vst-mode, midi-mode ] || Doc[ doc, website ]
list_Host = [
  ( "calf-plugins", "Calf Jack Host", "Yes", "Yes", "calfjackhost", "calf", "None", 0, "OpenSource", (1, 0, 0, 0, 0, "", "Jack"), ("", "http://calf.sourceforge.net/") ),
  ( "calf-plugins-git", "Calf Jack Host (GIT)", "Yes", "Yes", "calfjackhost", "calf", "Auto", 1, "OpenSource", (1, 0, 0, 0, 0, "", "Jack"), ("", "http://calf.sourceforge.net/") ),

  ( "festige", "FeSTige", "Yes", "Yes", "festige", "festige", "Auto", 1, "OpenSource", (0, 0, 0, 0, 1, "Windows", "Jack"), ("", "http://festige.sourceforge.net/") ),

  ( "ingen", "Ingen", "Yes", "Yes", "ingen -eg", "ingen", "Manual", 0, "OpenSource", (1, 1, 0, 1, 0, "", "Jack"), ("", "http://drobilla.net/blog/software/ingen/") ),
  ( "ingen-svn", "Ingen (SVN)", "Yes", "Yes", "ingen-svn -eg", "ingen", "Manual", 0, "OpenSource", (1, 1, 0, 1, 0, "", "Jack"), ("", "http://drobilla.net/blog/software/ingen/") ),

  ( "monobristol", "MonoBristol", "Yes", "Yes", "monobristol", "monobristol", "Manual", 0, "OpenSource", (1, 0, 0, 0, 0, "", "Jack | ALSA"), ("", "http://dacr.hu/monobristol") ),

  ( "jack-rack", "Jack Rack", "Yes", "Yes", "jack-rack", "jack-rack", "Template", 0, "OpenSource", (0, 1, 0, 0, 0, "", "Jack"), ("", "http://jack-rack.sourceforge.net/") ),

  ( "jost", "Jost", "Yes", "Yes", "jost", "jost", "Manual", 0, "OpenSource", (0, 1, 1, 0, 1, "Native", "ALSA"), ("", "http://www.anticore.org/jucetice/?page_id=4") ),

  ( "zynjacku", "LV2 Rack", "No", "Yes", "lv2rack", "lv2rack", "Manual", 0, "OpenSource", (0, 0, 0, 1, 0, "", "Jack"), ("", "http://home.gna.org/zynjacku/") ),
  ( "zynjacku", "ZynJackU", "Yes", "No", "zynjacku", "zynjacku", "Manual", 0, "OpenSource", (0, 0, 0, 1, 0, "", "Jack"), ("", "http://home.gna.org/zynjacku/") ),
]

# Package || AppName || Type || Binary || Icon || Save || Level || License || Features[ built-in-fx, audio-input, midi-mode ] || Doc[ doc, website ]
list_Instrument = [
  ( "aeolus", "Aeolus", "Synth", "aeolus", generic_audio_icon, "None", 0, "OpenSource", (0, 0, "Jack | ALSA"), ("", "http://www.kokkinizita.net/linuxaudio/aeolus/index.html") ),

  ( "amsynth", "Amsynth", "Synth", "amsynth", "amsynth", "Manual", 0, "OpenSource", (1, 0, "ALSA"), ("", "") ),

  ( "azr3-jack", "AZR3", "Synth", "azr3", "azr3", "None", 0, "OpenSource", (0, 0, "Jack"), ("", "http://ll-plugins.nongnu.org/azr3/") ),

  ( "jsampler", "JSampler Fantasia", "Sampler", "jsampler-bin", "jsampler", "Manual", 0, "OpenSource", (0, 0, "Jack + ALSA"), ("file:///usr/share/kxstudio/docs/jsampler/jsampler.html", "http://www.linuxsampler.org/") ),

  ( "juced-plugins", "DrumSynth", "Synth", "drumsynth", "juced", "Manual", 0, "OpenSource", (0, 1, "ALSA"), ("", "http://code.google.com/p/juced/") ),
  ( "juced-plugins", "HighLife", "Sampler", "highlife", "juced", "Manual", 0, "OpenSource", (1, 1, "ALSA"), ("", "http://code.google.com/p/juced/") ),
  ( "juced-plugins", "Nekobee", "Synth", "nekobee-juced", "juced", "Manual", 0, "OpenSource", (0, 1, "ALSA"), ("", "http://code.google.com/p/juced/") ),
  ( "juced-plugins", "Peggy2000", "Synth", "peggy2000", "juced", "Manual", 0, "OpenSource", (1, 1, "ALSA"), ("", "http://code.google.com/p/juced/") ),
  ( "juced-plugins", "SoundCrab", "SoundFont Player", "soundcrab", "juced", "Manual", 0, "OpenSource", (0, 1, "ALSA"), ("", "http://code.google.com/p/juced/") ),
  ( "juced-plugins", "Vex", "Synth", "vex", "juced", "Manual", 0, "OpenSource", (1, 1, "ALSA"), ("", "http://code.google.com/p/juced/") ),

  ( "loomer-plugins", "Aspect", "Synth", "Aspect", "loomer", "Manual", 0, "Demo", (1, 1, "ALSA"), ("file:///usr/share/doc/loomer-plugins/Aspect Manual.pdf.gz", "http://www.loomer.co.uk/aspect.htm") ),
  ( "loomer-plugins", "Sequent", "Synth", "Sequent", "loomer", "Manual", 0, "Demo", (1, 1, "ALSA"), ("file:///usr/share/doc/loomer-plugins/Sequent Manual.pdf.gz", "http://www.loomer.co.uk/sequent.htm") ),
  ( "loomer-plugins", "Shift", "Synth", "Shift", "loomer", "Manual", 0, "Demo", (1, 1, "ALSA"), ("file:///usr/share/doc/loomer-plugins/Shift Manual.pdf.gz", "http://www.loomer.co.uk/shift.htm") ),
  ( "loomer-plugins", "String", "Synth", "String", "loomer", "Manual", 0, "Demo", (1, 1, "ALSA"), ("file:///usr/share/doc/loomer-plugins/String Manual.pdf.gz", "http://www.loomer.co.uk/string.htm") ),

  ( "phasex", "Phasex", "Synth", "phasex", "phasex", "Manual", 0, "OpenSource", (1, 1, "ALSA"), ("file:///usr/share/phasex/help/parameters.help", "") ),

  ( "pianoteq", "Pianoteq", "Synth", "/usr/local/bin/Pianoteq", "pianoteq", "Manual", 0, "Demo", (1, 0, "Jack + ALSA"), ("file:///usr/local/bin/Documentation/pianoteq-english.pdf", "http://www.pianoteq.com/pianoteq3_standard") ),
  ( "pianoteq-x64", "Pianoteq (64bit)", "Synth", "/usr/local/bin/Pianoteq_x64", "pianoteq", "Manual", 0, "Demo", (1, 0, "Jack + ALSA"), ("file:///usr/local/bin/Documentation/pianoteq-english.pdf", "http://www.pianoteq.com/pianoteq3_standard") ),
  ( "pianoteq-play", "Pianoteq Play", "Synth", "/usr/local/bin/Pianoteq_PLAY", "pianoteq", "Manual", 0, "Demo", (1, 0, "Jack + ALSA"), ("file:///usr/local/bin/Documentation/pianoteq-english.pdf", "http://www.pianoteq.com/pianoteq3_play") ),
  ( "pianoteq-play-x64", "Pianoteq Play (64bit)", "Synth", "/usr/local/bin/Pianoteq_PLAY_x64", "pianoteq", "Manual", 0, "Demo", (1, 0, "Jack + ALSA"), ("file:///usr/local/bin/Documentation/pianoteq-english.pdf", "http://www.pianoteq.com/pianoteq3_play") ),

  ( "qsampler", "Qsampler", "Sampler", "qsampler", "qsampler", "Template", 0, "OpenSource", (0, 0, "Jack + ALSA"), ("", "http://qsampler.sourceforge.net/") ),
  ( "qsampler-svn", "Qsampler (SVN)", "Sampler", "qsampler", "qsampler", "Auto", 1, "OpenSource", (0, 0, "Jack + ALSA"), ("", "http://qsampler.sourceforge.net/") ),

  ( "qsynth", "Qsynth", "SoundFont Player", "qsynth -a jack -m jack", "qsynth", "Manual", 0, "OpenSource", (1, 0, "Jack | ALSA"), ("", "http://qsynth.sourceforge.net/") ),

  ( "wolpertinger", "Wolpertinger", "Synth", "Wolpertinger004", "wolpertinger", "Manual", 0, "OpenSource", (1, 0, "ALSA"), ("", "http://tumbetoene.tuxfamily.org") ),

  ( "yoshimi", "Yoshimi", "Synth", "yoshimi -j -J", "yoshimi", "Auto", 1, "OpenSource", (1, 0, "Jack | ALSA"), ("", "http://yoshimi.sourceforge.net/") ),

  ( "zynaddsubfx", "ZynAddSubFX", "Synth", "zynaddsubfx", "zynaddsubfx", "Manual", 0, "OpenSource", (1, 0, "Jack + ALSA"), ("", "http://zynaddsubfx.sourceforge.net/") ),
]

# Package || AppName || Type || Short name || Icon || Save || Level || License || Features[ built-in-fx, audio-input, midi-mode ] || Doc[ doc, website ]

# Need name: bit99, bit100

list_Bristol = [
  ( "bristol", "Moog Voyager", "Synth", "explorer", "bristol_explorer", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Moog Mini", "Synth", "mini", "bristol_mini", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Sequential Circuits Prophet-52", "Synth", "prophet52", "bristol_prophet52", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),

  ( "bristol", "Moog/Realistic MG-1", "Synth", "realistic", "bristol_realistic", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Memory Moog", "Synth", "memoryMoog", "bristol_memoryMoog", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Baumann BME-700", "Synth", "BME700", "bristol_BME700", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Synthi Aks (*)", "Synth", "aks", "bristol_aks", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),

  ( "bristol", "Moog Voyager Blue Ice", "Synth", "voyager", "bristol_voyager", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Moog Sonic-6", "Synth", "sonic6", "bristol_sonic6", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Hammond B3", "Synth", "hammondB3", "bristol_hammondB3", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Sequential Circuits Prophet-5", "Synth", "prophet", "bristol_prophet", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Sequential Circuits Prophet-10", "Synth", "prophet10", "bristol_prophet10", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Sequential Circuits Pro-1", "Synth", "pro1", "bristol_pro1", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Fender Rhodes Stage-73", "Synth", "rhodes", "bristol_rhodes", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Rhodes Bass Piano", "Synth", "rhodesbass", "bristol_rhodesbass", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Crumar Roadrunner", "Synth", "roadrunner", "bristol_roadrunner", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Crumar Bit-1", "Synth", "bitone", "bristol_bitone", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Crumar Stratus", "Synth", "stratus", "bristol_stratus", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Crumar Trilogy", "Synth", "trilogy", "bristol_trilogy", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Oberheim OB-X", "Synth", "obx", "bristol_obx", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Oberheim OB-Xa", "Synth", "obxa", "bristol_obxa", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "ARP Axxe", "Synth", "axxe", "bristol_axxe", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "ARP Odyssey", "Synth", "odyssey", "bristol_odyssey", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "ARP 2600", "Synth", "arp2600", "bristol_arp2600", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "ARP Solina Strings", "Synth", "solina", "bristol_solina", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Korg Poly-800", "Synth", "poly800", "bristol_poly800", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Korg Mono/Poly", "Synth", "monopoly", "bristol_monopoly", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Korg Polysix", "Synth", "poly", "bristol_poly", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Korg MS-20 (*)", "Synth", "ms20", "bristol_ms20", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "VOX Continental", "Synth", "vox", "bristol_vox", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "VOX Continental 300", "Synth", "voxM2", "bristol_voxM2", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Roland Juno-6", "Synth", "juno", "bristol_juno", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Roland Jupiter 8", "Synth", "jupiter8", "bristol_jupiter8", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Bristol BassMaker (*)", "Synth", "bassmaker", "bristol_bassmaker", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Yamaha DX", "Synth", "dx", "bristol_dx", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Yamaha CS-80 (*)", "Synth", "cs80", "bristol_cs80", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Bristol SID Softsynth", "Synth", "sidney", "bristol_sidney", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Commodore-64 SID polysynth (*)", "Synth", "melbourne", "bristol_sidney", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ), #FIXME - needs icon
  ( "bristol", "Bristol Granular Synthesiser (*)", "Synth", "granular", "bristol_granular", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
  ( "bristol", "Bristol Realtime Mixer (*)", "Synth", "mixer", "bristol_mixer", "Auto", 1, "OpenSource", (1, 1, "Jack | ALSA"), ("", "") ),
]

# Package || AppName || Type || Binary || Icon || Save || Level || License || Features[ stereo, midi-mode ] || Doc[ doc, website ]
list_Effect = [
  ( "ambdec", "AmbDec", "Ambisonic Decoder", "ambdec", generic_audio_icon, "None", 0, "OpenSource", (1, "---"), ("", "") ),

  ( "guitarix", "Guitarix", "Guitar FX", "guitarix", "guitarix", "Manual", 0, "OpenSource", (0, "Jack"), ("", "http://guitarix.sourceforge.net/") ),

  ( "jamin", "Jamin", "Mastering", "jamin", "jamin", "Template", 0, "OpenSource", (1, "---"), ("", "http://jamin.sourceforge.net/") ),

  ( "jcgui", "Jc_Gui", "Convolver", "Jc_Gui", "Jc_Gui", "Manual", 0, "OpenSource", (1, "---"), ("", "") ),

  ( "juced-plugins", "BitMangler", "Unknown", "bitmangler", "juced", "Manual", 0, "OpenSource", (1, "ALSA"), ("", "http://code.google.com/p/juced/") ),
  ( "juced-plugins", "EQinox", "EQ", "eqinox", "juced", "Manual", 0, "OpenSource", (1, "ALSA"), ("", "http://code.google.com/p/juced/") ),
  ( "juced-plugins", "Tal-Filter", "Filter", "tal-filter", "juced", "Manual", 0, "OpenSource", (1, "ALSA"), ("", "http://code.google.com/p/juced/") ),
  ( "juced-plugins", "Tal-Reverb", "Reverb", "tal-reverb", "juced", "Manual", 0, "OpenSource", (1, "ALSA"), ("", "http://code.google.com/p/juced/") ),

  ( "linuxdsp-plugins", "Channel Equaliser", "EQ", "ch-eq1_i686", "linuxdsp", "Manual", 0, "FreeWare", (0, "---"), ("file:///usr/share/doc/linuxdsp-plugins/CH-EQ1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/ch_eq1/index.html") ),
  ( "linuxdsp-plugins", "Channel Equaliser (Stereo)", "EQ", "ch-eq2_i686", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/CH-EQ2/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/ch_eq2/index.html") ),
  ( "linuxdsp-plugins", "Guitar Distortion", "Distortion", "dt1_i686", "linuxdsp", "Manual", 0, "FreeWare", (0, "---"), ("file:///usr/share/doc/linuxdsp-plugins/DT-1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/guitar_fx/dt1/index.html") ),
  ( "linuxdsp-plugins", "Graphic Equaliser", "EQ", "gr-eq2_i686", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/GR-EQ2/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/gr_eq2/index.html") ),
  ( "linuxdsp-plugins", "Valve Overdrive", "Amplifier", "odv2_i686", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/ODV2/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/odv2/index.html") ),
  ( "linuxdsp-plugins", "Guitar Phaser", "Phaser", "ph1_i686", "linuxdsp", "Manual", 0, "FreeWare", (0, "---"), ("file:///usr/share/doc/linuxdsp-plugins/PH-1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/guitar_fx/ph1/index.html") ),
  ( "linuxdsp-plugins", "Reverb", "Reverb", "sr-2a_i686", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/SR-2A/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/sr_2a/index.html") ),
  ( "linuxdsp-plugins", "Guitar Sustainer", "Sustainer", "st1_i686", "linuxdsp", "Manual", 0, "FreeWare", (0, "---"), ("file:///usr/share/doc/linuxdsp-plugins/ST-1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/guitar_fx/st1/index.html") ),
  ( "linuxdsp-plugins", "Vintage Compressor", "Compressor", "vc2_i686", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/VC2/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/vc2/index.html") ),

  ( "linuxdsp-plugins-x64", "Channel Equaliser (64bit)", "EQ", "ch-eq1_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (0, "---"), ("file:///usr/share/doc/linuxdsp-plugins/CH-EQ1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/ch_eq1/index.html") ),
  ( "linuxdsp-plugins-x64", "Channel Equaliser (Stereo, 64bit)", "EQ", "ch-eq2_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/CH-EQ2/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/ch_eq2/index.html") ),
  #( "linuxdsp-plugins-x64", "Guitar Distortion (64bit)", "Distortion", "dt1_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (0, "---"), ("file:///usr/share/doc/linuxdsp-plugins/DT-1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/guitar_fx/dt1/index.html") ),
  ( "linuxdsp-plugins-x64", "Graphic Equaliser (64bit)", "EQ", "gr-eq2_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/GR-EQ2/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/gr_eq2/index.html") ),
  ( "linuxdsp-plugins-x64", "Valve Overdrive (64bit)", "Amplifier", "odv2_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/ODV2/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/odv2/index.html") ),
  #( "linuxdsp-plugins-x64", "Guitar Phaser (64bit)", "Phaser", "ph1_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (0, "---"), ("file:///usr/share/doc/linuxdsp-plugins/PH-1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/guitar_fx/ph1/index.html") ),
  ( "linuxdsp-plugins-x64", "Reverb (64bit)", "Reverb", "sr-2a_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/SR-2A/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/sr_2a/index.html") ),
  #( "linuxdsp-plugins-x64", "Guitar Sustainer (64bit)", "Sustainer", "st1_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (0, "---"), ("file:///usr/share/doc/linuxdsp-plugins/ST-1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/guitar_fx/st1/index.html") ),
  ( "linuxdsp-plugins-x64", "Vintage Compressor (64bit)", "Compressor", "vc2_x86-64", "linuxdsp", "Manual", 0, "FreeWare", (1, "---"), ("file:///usr/share/doc/linuxdsp-plugins/VC2/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/vc2/index.html") ),

  ( "loomer-plugins", "Manifold", "Enhancer", "Manifold", "loomer", "Manual", 0, "Demo", (1, "ALSA"), ("file:///usr/share/doc/loomer-plugins/Manifold Manual.pdf.gz", "http://www.loomer.co.uk/manifold.htm") ),
  ( "loomer-plugins", "Resound", "Delay", "Resound", "loomer", "Manual", 0, "Demo", (1, "ALSA"), ("file:///usr/share/doc/loomer-plugins/Resound Manual.pdf.gz", "http://www.loomer.co.uk/resound.htm") ),
  ( "loomer-plugins", "String (FX)", "Bundle", "String_FX", "loomer", "Manual", 0, "Demo", (1, "ALSA"), ("file:///usr/share/doc/loomer-plugins/String Manual.pdf.gz", "http://www.loomer.co.uk/string.htm") ),

  ( "rakarrack", "Rakarrack", "Guitar FX", "rakarrack", "rakarrack", "Manual", 0, "OpenSource", (1, "Jack + ALSA"), ("file:///usr/share/doc/rakarrack/html/help.html", "http://rakarrack.sourceforge.net") ),
]

# Package || AppName || Type || Binary || Icon || Save || Level || License || Features[ midi-mode, tranport ] || Doc[ doc, website ]
list_Tool = [
  ( "a2jmidid", "ALSA -> Jack MIDI", "Bridge", "a2j -e", generic_midi_icon, "None", 0, "OpenSource", ("Jack", 0), ("", "http://home.gna.org/a2jmidid/") ),

  ( "arpage", "Arpage", "MIDI Arpeggiator", "arpage", "arpage", "None", 0, "OpenSource", ("Jack", 1), ("", "") ),
  ( "arpage", "Zonage", "MIDI Mapper", "zonage", "zonage", "None", 0, "OpenSource", ("Jack", 0), ("", "") ),

  ( "audacity", "Audacity", "Audio Editor", "audacity", "audacity", "Manual", 0, "OpenSource", ("---", 0), ("", "http://audacity.sourceforge.net/") ),
  ( "audacity-32bit", "Audacity (32bit)", "Audio Editor", "audacity-32bit", "audacity", "Manual", 0, "OpenSource", ("---", 0), ("", "http://audacity.sourceforge.net/") ),

  ( "catia", "Catia", "Patch Bay", "catia", generic_audio_icon, "None", 0, "OpenSource", ("Jack", 1), ("", "") ),
  ( "claudia", "Claudia", "Session Handler", "claudia", generic_audio_icon, "None", 0, "OpenSource", ("Jack", 1), ("", "") ),
  ( "j2sc", "Jack2 Simple Config", "Jack Config", "j2sc", "j2sc", "None", 0, "OpenSource", ("---", 0), ("", "") ),

  ( "drumstick", "Drumstick Virtual Piano", "Virtual Keyboard", "drumstick-vpiano", "drumstick", "None", 0, "OpenSource", ("ALSA", 0), ("", "http://drumstick.sourceforge.net/") ),

  ( "fmit", "Free Music Instrument Tuner", "Instrument Tuner", "fmit", generic_audio_icon, "None", 0, "OpenSource", ("---", 0), ("", "") ),

  ( "gigedit", "Gigedit", "Instrument Editor", "gigedit", generic_audio_icon, "None", 0, "OpenSource", ("---", 0), ("/usr/share/doc/gigedit/gigedit_quickstart.html", "") ),

  ( "gjacktransport", "GJackClock", "Transport Tool", "gjackclock", "gjackclock", "None", 0, "OpenSource", ("---", 1), ("", "") ),
  ( "gjacktransport", "GJackTransport", "Transport Tool", "gjacktransport", "gjacktransport", "None", 0, "OpenSource", ("---", 1), ("", "") ),

  ( "gninjam", "Gtk NINJAM client", "Music Collaboration", "gninjam", generic_audio_icon, "None", 0, "OpenSource", ("---", 1), ("", "") ),

  ( "jack-capture", "Jack Capture", "Recorder", "jack_capture_gui2", "media-record", "None", 0, "OpenSource", ("---", 0), ("", "") ),

  ( "jack-keyboard", "Jack Keyboard", "Virtual Keyboard", "jack-keyboard", "jack-keyboard", "None", 0, "OpenSource", ("Jack", 0), ("file:///usr/share/kxstudio/docs/jack-keyboard/manual.html", "http://jack-keyboard.sourceforge.net/") ),

  ( "jack-mixer", "Jack Mixer", "Mixer", "jack_mixer", "jack_mixer", "Auto", 1, "OpenSource", ("Jack", 0), ("", "http://home.gna.org/jackmixer/") ),

  ( "jack-trans2midi", "Jack-Transport -> MIDI Clock", "Bridge", "trans2midi", generic_midi_icon, "None", 0, "OpenSource", ("Jack", 1), ("", "") ),

  ( "kmetronome", "KMetronome", "Metronome", "kmetronome", "kmetronome", "None", 0, "OpenSource", ("ALSA", 0), ("", "http://kmetronome.sourceforge.net/kmetronome.shtml") ),
  ( "kmidimon", "KMidimon", "Monitor", "kmidimon", "kmidimon", "None", 0, "OpenSource", ("ALSA", 0), ("", "http://kmidimon.sourceforge.net/") ),

  ( "ladish", "LADI Session Handler", "Session Handler", "gladish", "gladish", "---", 0, "OpenSource", ("Jack", 0), ("", "http://www.ladish.org") ),
  ( "ladish-git", "LADI Session Handler (GIT)", "Session Handler", "gladish", "gladish", "---", 0, "OpenSource", ("Jack", 0), ("", "http://www.ladish.org") ),

  ( "laditools", "LADI Log", "Log Viewer", "ladilog", "ladilog", "None", 0, "OpenSource", ("---", 0), ("", "") ),
  ( "laditools", "LADI Tray", "Session Handler", "laditray", "laditray", "---", 0, "OpenSource", ("---", 0), ("", "") ),
  ( "laditools-git", "LADI Log (GIT)", "Log Viewer", "ladilog", "ladilog", "None", 0, "OpenSource", ("---", 0), ("", "") ),
  ( "laditools-git", "LADI Tray (GIT)", "Session Handler", "laditray", "laditray", "---", 0, "OpenSource", ("---", 0), ("", "") ),

  ( "linuxdsp-plugins", "JACK Patch Bay", "Patch Bay", "jp1_i686", "linuxdsp", "Manual", 0, "FreeWare", ("Jack", 0), ("file:///usr/share/doc/linuxdsp-plugins/JP1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/jp1/index.html") ),
  ( "linuxdsp-plugins-x64", "JACK Patch Bay (64bit)", "Patch Bay", "jp1_x86-64", "linuxdsp", "Manual", 0, "FreeWare", ("Jack", 0), ("file:///usr/share/doc/linuxdsp-plugins/JP1/manual.pdf.gz", "http://www.linuxdsp.co.uk/download/jp1/index.html") ),

  ( "lives", "LiVES", "VJ / Video Editor", "lives", "lives", "Manual", 0, "OpenSource", ("---", 1), ("", "http://lives.sourceforge.net/") ),

  ( "lpatchage", "LADI Patchage", "Patch Bay", "lpatchage", "lpatchage", "None", 0, "OpenSource", ("Jack", 0), ("", "http://ladish.org/wiki/lpatchage") ),

  ( "ltc2amidi", "LTC -> MIDI Clock", "Bridge", "ltc2amidi", generic_midi_icon, "None", 0, "OpenSource", ("ALSA", 0), ("", "") ),

  ( "mhwaveedit", "MhWaveEdit", "Audio Editor", "mhwaveedit", "mhwaveedit", "None", 0, "OpenSource", ("---", 0), ("", "http://gna.org/projects/mhwaveedit/") ),

  ( "mixxx", "Mixxx", "DJ", "mixxx", "mixxx", "None", 0, "OpenSource", ("ALSA", 0), ("file:///usr/share/kxstudio/docs/Mixxx-Manual.pdf", "http://mixxx.sourceforge.net/") ),

  ( "non-mixer", "Non-Mixer", "Mixer", "non-mixer", "non-mixer", "Manual", 0, "OpenSource", ("CV", 0), ("file:///usr/share/doc/non-mixer/MANUAL.html", "http://non-daw.tuxfamily.org/") ),

  ( "openmovieeditor", "OpenMovieEditor", "Video Editor", "openmovieeditor", "openmovieeditor", "Manual", 0, "OpenSource", ("---", 1), ("file:///usr/share/kxstudio/docs/openmovieeditor/tutorial.html", "http://www.openmovieeditor.org/") ),

  ( "patchage", "Patchage", "Patch Bay", "patchage", "patchage", "None", 0, "OpenSource", ("Jack + ALSA", 0), ("", "http://drobilla.net/blog/software/patchage/") ),
  ( "patchage", "Patchage (ALSA Only)", "Patch Bay", "patchage -J", "patchage", "None", 0, "OpenSource", ("ALSA", 0), ("", "http://drobilla.net/blog/software/patchage/") ),
  ( "patchage-svn", "Patchage (SVN)", "Patch Bay", "patchage-svn", "patchage-svn", "None", 0, "OpenSource", ("Jack + ALSA", 0), ("", "http://drobilla.net/blog/software/patchage/") ),
  ( "patchage-svn", "Patchage (SVN, ALSA Only)", "Patch Bay", "patchage-svn -J", "patchage-svn", "None", 0, "OpenSource", ("Jack + ALSA", 0), ("", "http://drobilla.net/blog/software/patchage/") ),

  ( "qjackctl", "Jack Control", "Jack Control", "qjackctl", "qjackctl", "None", 0, "OpenSource", ("Jack + ALSA", 1), ("", "") ),

  ( "qamix", "QAMix", "Mixer", "qamix", "qamix", "None", 0, "OpenSource", ("ALSA", 0), ("", "") ),
  ( "qarecord", "QARecord", "Recorder", "qarecord --jack", "qarecord_48", "None", 0, "OpenSource", ("ALSA", 0), ("", "") ),
  ( "qmidiarp", "QMidiArp", "MIDI Arpeggiator", "qmidiarp", generic_midi_icon, "None", 0, "OpenSource", ("ALSA", 0), ("", "") ),

  ( "timemachine", "TimeMachine", "Recorder", "timemachine", "timemachine", "None", 0, "OpenSource", ("---", 0), ("", "http://plugin.org.uk/timemachine/") ),

  ( "vmpk", "Virtual MIDI Piano Keyboard", "Virtual Keyboard", "vmpk", "vmpk", "None", 0, "OpenSource", ("ALSA", 0), ("file:///usr/share/vmpk/help.html", "http://vmpk.sourceforge.net/") ),
  ( "xjadeo", "XJadeo", "Video Player", "qjadeo", "qjadeo", "None", 0, "OpenSource", ("---", 1), ("", "http://xjadeo.sourceforge.net/") ),
]

