#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import QString, QVariant, SLOT
from PyQt4.QtGui import QDialog, QDialogButtonBox, QFileDialog, QGraphicsView
from PyQt4.QtGui import QMainWindow, QTableWidgetItem
from PyQt4.QtXml import QDomDocument

# Imports (Plugins and Resources)
import ui_catarina, ui_settings_app
import ui_catarina_addgroup, ui_catarina_removegroup, ui_catarina_renamegroup
import ui_catarina_addport, ui_catarina_removeport, ui_catarina_renameport
import ui_catarina_connectports, ui_catarina_disconnectports
from shared import *

# Configure Catarina Dialog
class CatarinaSettingsW(QDialog, ui_settings_app.Ui_SettingsW):
    def __init__(self, parent=None):
        super(CatarinaSettingsW, self).__init__(parent)
        self.setupUi(self)

        self.loadSettings()

        self.lw_page.hideRow(0)
        self.lw_page.hideRow(2)
        self.lw_page.hideRow(3)
        self.lw_page.hideRow(4)
        self.lw_page.setCurrentCell(1, 0)
        #self.lw_page.item(0, 0).setIcon(QIcon(":/48x48/catarina.png"))

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL("clicked()"), self.resetSettings)
        self.connect(self, SIGNAL("accepted()"), self.saveSettings)

    def saveSettings(self):
        settings = QSettings()
        settings.setValue(QString("Canvas/Theme"), self.cb_canvas_theme.currentText())
        settings.setValue(QString("Canvas/BezierLines"), self.cb_canvas_bezier_lines.isChecked())
        settings.setValue(QString("Canvas/AutoHideGroups"), self.cb_canvas_hide_groups.isChecked())
        settings.setValue(QString("Canvas/FancyEyeCandy"), self.cb_canvas_eyecandy.isChecked())
        settings.setValue(QString("Canvas/Antialiasing"), self.cb_canvas_render_aa.checkState())
        settings.setValue(QString("Canvas/TextAntialiasing"), self.cb_canvas_render_text_aa.isChecked())

    def loadSettings(self):
        settings = QSettings()
        self.cb_canvas_bezier_lines.setChecked(settings.value("Canvas/BezierLines", True).toBool())
        self.cb_canvas_hide_groups.setChecked(settings.value("Canvas/AutoHideGroups", False).toBool())
        self.cb_canvas_eyecandy.setChecked(settings.value("Canvas/FancyEyeCandy", False).toBool())
        self.cb_canvas_render_aa.setCheckState(settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0])
        self.cb_canvas_render_text_aa.setChecked(settings.value("Canvas/TextAntialiasing", True).toBool())

        theme_name = settings.value("Canvas/Theme", patchcanvas.getDefaultThemeName()).toString()

        for i in range(len(patchcanvas.theme_list)):
          self.cb_canvas_theme.addItem(patchcanvas.theme_list[i]['name'])
          if (patchcanvas.theme_list[i]['name'] == theme_name):
            self.cb_canvas_theme.setCurrentIndex(i)

    def resetSettings(self):
        self.cb_canvas_theme.setCurrentIndex(0)
        self.cb_canvas_bezier_lines.setChecked(True)
        self.cb_canvas_hide_groups.setChecked(False)
        self.cb_canvas_eyecandy.setChecked(False)
        self.cb_canvas_render_aa.setCheckState(Qt.PartiallyChecked)
        self.cb_canvas_render_text_aa.setChecked(True)

# Add Group Dialog
class CatarinaAddGroupW(QDialog, ui_catarina_addgroup.Ui_CatarinaAddGroupW):
    def __init__(self, parent=None):
        super(CatarinaAddGroupW, self).__init__(parent)
        self.setupUi(self)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.le_group_name, SIGNAL("textChanged(QString)"), self.checkText)

    def setReturn(self):
        self.ret_name  = self.le_group_name.text()
        self.ret_split = bool(self.cb_split.isChecked())

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if text.isEmpty() else True)

# Remove Group Dialog
class CatarinaRemoveGroupW(QDialog, ui_catarina_removegroup.Ui_CatarinaRemoveGroupW):
    def __init__(self, parent=None, group_list=[]):
        super(CatarinaRemoveGroupW, self).__init__(parent)
        self.setupUi(self)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.addGroups()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.tw_group_list, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkCell)
        self.connect(self.tw_group_list, SIGNAL("cellDoubleClicked(int, int)"), self.accept)

    def addGroups(self):
        for i in range(len(self.group_list)):
          twi_group_id    = QTableWidgetItem(str(self.group_list[i][0]))
          twi_group_name  = QTableWidgetItem(self.group_list[i][1])
          twi_group_split = QTableWidgetItem("Yes" if (self.group_list[i][2]) else "No")
          self.tw_group_list.insertRow(i)
          self.tw_group_list.setItem(i, 0, twi_group_id)
          self.tw_group_list.setItem(i, 1, twi_group_name)
          self.tw_group_list.setItem(i, 2, twi_group_split)

    def setReturn(self):
        self.ret_group_id = int(self.tw_group_list.item(self.tw_group_list.currentRow(), 0).text().toInt()[0])

    def checkCell(self, row, column, old_row, old_column):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (row >= 0) else False)

# Rename Group Dialog
class CatarinaRenameGroupW(QDialog, ui_catarina_renamegroup.Ui_CatarinaRenameGroupW):
    def __init__(self, parent=None, group_list=[]):
        super(CatarinaRenameGroupW, self).__init__(parent)
        self.setupUi(self)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.addGroups()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.le_new_group_name, SIGNAL("textChanged(QString)"), self.checkText)

    def addGroups(self):
        for i in range(len(self.group_list)):
          self.cb_group_to_rename.addItem(str(self.group_list[i][0])+" - "+self.group_list[i][1])

    def setReturn(self):
        self.ret_group_id = int(self.cb_group_to_rename.currentText().split(" - ")[0])
        self.ret_new_group_name = self.le_new_group_name.text()

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if (text.isEmpty() or text == self.cb_group_to_rename.currentText()) else True)

# Add Port Dialog
class CatarinaAddPortW(QDialog, ui_catarina_addport.Ui_CatarinaAddPortW):
    def __init__(self, parent=None, group_list=[], port_id=0):
        super(CatarinaAddPortW, self).__init__(parent)
        self.setupUi(self)

        self.sb_port_id.setValue(port_id)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.addGroups()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.le_port_name, SIGNAL("textChanged(QString)"), self.checkText)

    def addGroups(self):
        for i in range(len(self.group_list)):
          self.cb_group.addItem(QString("%1 - %2").arg(self.group_list[i][0]).arg(self.group_list[i][1]))

    def setReturn(self):
        self.ret_port_group = self.cb_group.currentText().split(" ")[0].toInt()[0]
        self.ret_port_id = self.sb_port_id.value()
        self.ret_name = self.le_port_name.text()
        self.ret_port_mode = 1 if (self.rb_flags_input.isChecked()) else 2
        self.ret_port_type = self.cb_port_type.currentIndex()
        self.ret_port_split = True if (self.cb_flags_physical.isChecked()) else False

    def checkText(self, text):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False if text.isEmpty() else True)

# Remove Port Dialog
class CatarinaRemovePortW(QDialog, ui_catarina_removeport.Ui_CatarinaRemovePortW):
    def __init__(self, parent=None, group_list=[], port_list=[]):
        super(CatarinaRemovePortW, self).__init__(parent)
        self.setupUi(self)

        self.tw_port_list.setColumnWidth(0, 25)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.port_list = port_list
        self.reAddPorts()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.tw_port_list, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkCell)
        self.connect(self.tw_port_list, SIGNAL("cellDoubleClicked(int, int)"), self.accept)
        self.connect(self.rb_input, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_output, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_audio, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_midi, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_outro, SIGNAL("clicked()"), self.reAddPorts)

    def reAddPorts(self):
        self.tw_port_list.clearContents()
        for i in range(self.tw_port_list.rowCount()):
          self.tw_port_list.removeRow(0)

        port_mode = 1 if (self.rb_input.isChecked()) else 2

        if (self.rb_audio.isChecked()):
          port_type = 0
        elif (self.rb_midi.isChecked()):
          port_type = 1
        elif (self.rb_outro.isChecked()):
          port_type = 2
        else:
          print "Invalid Port Type, GUI error"
          return

        h = 0
        for i in range(len(self.port_list)):
          if (self.port_list[i][3] == port_mode and self.port_list[i][4] == port_type):
            port_id = self.port_list[i][1]
            port_name = self.port_list[i][2]
            group_name = self.findPortGroupName(self.port_list[i][0])
            tw_port_id   = QTableWidgetItem(str(port_id))
            tw_port_name = QTableWidgetItem(group_name+":"+port_name)
            self.tw_port_list.insertRow(h)
            self.tw_port_list.setItem(h, 0, tw_port_id)
            self.tw_port_list.setItem(h, 1, tw_port_name)
            h += 1

    def findPortGroupName(self, group_id):
      for i in range(len(self.group_list)):
        if (group_id == self.group_list[i][0]):
          return self.group_list[i][1]
          break
      else:
        return ""

    def setReturn(self):
        self.ret_port_id = self.tw_port_list.item(self.tw_port_list.currentRow(), 0).text().toInt()[0]

    def checkCell(self, row, column, old_row, old_column):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (row >= 0) else False)

# Rename Port Dialog
class CatarinaRenamePortW(QDialog, ui_catarina_renameport.Ui_CatarinaRenamePortW):
    def __init__(self, parent=None, group_list=[], port_list=[]):
        super(CatarinaRenamePortW, self).__init__(parent)
        self.setupUi(self)

        self.tw_port_list.setColumnWidth(0, 25)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.port_list = port_list
        self.reAddPorts()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.tw_port_list, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkCell)
        self.connect(self.le_new_name, SIGNAL("textChanged(QString)"), self.checkText)

        self.connect(self.rb_input, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_output, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_audio, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_midi, SIGNAL("clicked()"), self.reAddPorts)
        self.connect(self.rb_outro, SIGNAL("clicked()"), self.reAddPorts)

    def reAddPorts(self):
        self.tw_port_list.clearContents()
        for i in range(self.tw_port_list.rowCount()):
          self.tw_port_list.removeRow(0)

        port_mode = 1 if (self.rb_input.isChecked()) else 2

        if (self.rb_audio.isChecked()):
          port_type = 0
        elif (self.rb_midi.isChecked()):
          port_type = 1
        elif (self.rb_outro.isChecked()):
          port_type = 2
        else:
          print "Invalid Port Type, GUI error"
          return

        h = 0
        for i in range(len(self.port_list)):
          if (self.port_list[i][3] == port_mode and self.port_list[i][4] == port_type):
            port_id = self.port_list[i][1]
            port_name = self.port_list[i][2]
            group_name = self.findPortGroupName(self.port_list[i][0])
            tw_port_id   = QTableWidgetItem(str(port_id))
            tw_port_name = QTableWidgetItem(group_name+":"+port_name)
            self.tw_port_list.insertRow(h)
            self.tw_port_list.setItem(h, 0, tw_port_id)
            self.tw_port_list.setItem(h, 1, tw_port_name)
            h += 1

    def findPortGroupName(self, group_id):
      for i in range(len(self.group_list)):
        if (group_id == self.group_list[i][0]):
          return self.group_list[i][1]
          break
      else:
        return ""

    def setReturn(self):
        self.ret_new_port_name = self.le_new_name.text()

    def checkCell(self):
        self.checkText(self.le_new_name.text())

    def checkText(self, text=None):
        item = self.tw_port_list.item(self.tw_port_list.currentRow(), 0)
        if (item and not text.isEmpty()):
          self.ret_port_id = item.text().toInt()[0]
          self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)
        else:
          self.ret_port_id = None
          self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

# Connect Ports Dialog
class CatarinaConnectPortsW(QDialog, ui_catarina_connectports.Ui_CatarinaConnectPortsW):
    def __init__(self, parent=None, group_list=[], port_list=[]):
        super(CatarinaConnectPortsW, self).__init__(parent)
        self.setupUi(self)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.ports_audio = []
        self.ports_midi  = []
        self.ports_outro = []
        self.group_list = group_list
        self.port_list = port_list

        for i in range(len(self.port_list)):
          if (self.port_list[i][4] == 0): #Audio
            self.ports_audio.append(self.port_list[i])
          elif (self.port_list[i][4] == 1): #MIDI
            self.ports_midi.append(self.port_list[i])
          elif (self.port_list[i][4] == 2): #Outro
            self.ports_outro.append(self.port_list[i])

        self.portModeChanged()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.rb_audio, SIGNAL("clicked()"), self.portModeChanged)
        self.connect(self.rb_midi, SIGNAL("clicked()"), self.portModeChanged)
        self.connect(self.rb_outro, SIGNAL("clicked()"), self.portModeChanged)
        self.connect(self.lw_outputs, SIGNAL("currentRowChanged(int)"), self.checkOutSelection)
        self.connect(self.lw_inputs, SIGNAL("currentRowChanged(int)"), self.checkInSelection)

    def portModeChanged(self):
        if (self.rb_audio.isChecked()):
          ports = self.ports_audio
        elif (self.rb_midi.isChecked()):
          ports = self.ports_midi
        elif (self.rb_outro.isChecked()):
          ports = self.ports_outro
        else:
          return
        self.showPorts(ports)

    def showPorts(self, ports):
        self.lw_outputs.clear()
        self.lw_inputs.clear()

        for i in range(len(ports)):
          if (ports[i][3] == 1): #Input
            self.lw_inputs.addItem(str(ports[i][1])+" - '"+self.findGroupName(ports[i][0])+":"+ports[i][2]+"'")
          elif (ports[i][3] == 2): #Output
            self.lw_outputs.addItem(str(ports[i][1])+" - '"+self.findGroupName(ports[i][0])+":"+ports[i][2]+"'")

    def findGroupName(self, group_id):
        for i in range(len(self.group_list)):
          if (self.group_list[i][0] == group_id):
            return self.group_list[i][1]
            break
        else:
          return None

    def setReturn(self):
        self.ret_port_ids = (self.lw_outputs.currentItem().text().split(" - ")[0].toInt()[0], self.lw_inputs.currentItem().text().split(" - ")[0].toInt()[0])

    def checkOutSelection(self, row):
        self.checkSelection(row, self.lw_inputs.currentRow())

    def checkInSelection(self, row):
        self.checkSelection(self.lw_outputs.currentRow(), row)

    def checkSelection(self, out_row, in_row):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (out_row >= 0 and in_row >= 0) else False)

# Disconnect Ports Dialog
class CatarinaDisconnectPortsW(QDialog, ui_catarina_disconnectports.Ui_CatarinaDisconnectPortsW):
    def __init__(self, parent=None, group_list=[], port_list=[], connection_list=[]):
        super(CatarinaDisconnectPortsW, self).__init__(parent)
        self.setupUi(self)

        self.tw_connections.setColumnWidth(0, 225)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        self.group_list = group_list
        self.port_list = port_list
        self.connection_list = connection_list

        self.portModeChanged()

        self.connect(self, SIGNAL("accepted()"), self.setReturn)
        self.connect(self.rb_audio, SIGNAL("clicked()"), self.portModeChanged)
        self.connect(self.rb_midi, SIGNAL("clicked()"), self.portModeChanged)
        self.connect(self.rb_outro, SIGNAL("clicked()"), self.portModeChanged)
        self.connect(self.tw_connections, SIGNAL("currentCellChanged(int, int, int, int)"), self.checkSelection)

    def portModeChanged(self):
        if (self.rb_audio.isChecked()):
          mode = 0
        elif (self.rb_midi.isChecked()):
          mode = 1
        elif (self.rb_outro.isChecked()):
          mode = 2
        else:
          return
        self.showPorts(mode)

    def showPorts(self, mode):
        self.tw_connections.clearContents()
        for i in range(self.tw_connections.rowCount()):
          self.tw_connections.removeRow(0)

        h = 0
        for i in range(len(self.connection_list)):
          if (self.findPortMode(self.connection_list[i][1]) == mode):
            port_out_id = self.connection_list[i][1]
            port_out_name = self.findPortName(port_out_id)

            port_in_id = self.connection_list[i][2]
            port_in_name = self.findPortName(port_in_id)

            tw_port_out = QTableWidgetItem(str(port_out_id)+" - '"+port_out_name+"'")
            tw_port_in  = QTableWidgetItem(str(port_in_id)+" - '"+port_in_name+"'")

            self.tw_connections.insertRow(h)
            self.tw_connections.setItem(h, 0, tw_port_out)
            self.tw_connections.setItem(h, 1, tw_port_in)
            h += 1

    def findPortName(self, port_id):
        for i in range(len(self.port_list)):
          if (self.port_list[i][1] == port_id):
            return self.findGroupName(self.port_list[i][0])+":"+self.port_list[i][2]
            break
        else:
          return None

    def findPortMode(self, port_id):
        for i in range(len(self.port_list)):
          if (self.port_list[i][1] == port_id):
            return self.port_list[i][4]
            break
        else:
          return None

    def findGroupName(self, group_id):
        for i in range(len(self.group_list)):
          if (self.group_list[i][0] == group_id):
            return self.group_list[i][1]
            break
        else:
          return None

    def setReturn(self):
        port_out_id = self.tw_connections.item(self.tw_connections.currentRow(), 0).text().split(" - ")[0].toInt()[0]
        port_in_id  = self.tw_connections.item(self.tw_connections.currentRow(), 1).text().split(" - ")[0].toInt()[0]
        self.ret_port_ids = (port_out_id, port_in_id)

    def checkSelection(self, row, column, prev_row, prev_column):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True if (row >= 0) else False)

# Main Window
class CatarinaMainW(QMainWindow, ui_catarina.Ui_CatarinaMainW):
    def __init__(self, parent=None):
        super(CatarinaMainW, self).__init__(parent)
        self.setupUi(self)

        # Load App Settings
        self.loadSettings()

        # Icons
        setIcons(self, ["canvas"])

        self.act_project_new.setIcon(getIcon("document-new"))
        self.act_project_open.setIcon(getIcon("document-open"))
        self.act_project_save.setIcon(getIcon("document-save"))
        self.act_project_save_as.setIcon(getIcon("document-save-as"))
        self.b_project_new.setIcon(getIcon("document-new"))
        self.b_project_open.setIcon(getIcon("document-open"))
        self.b_project_save.setIcon(getIcon("document-save"))
        self.b_project_save_as.setIcon(getIcon("document-save-as"))

        self.act_patchbay_add_group.setIcon(getIcon("list-add"))
        self.act_patchbay_remove_group.setIcon(getIcon("edit-delete"))
        self.act_patchbay_rename_group.setIcon(getIcon("edit-rename"))
        self.act_patchbay_add_port.setIcon(getIcon("list-add"))
        self.act_patchbay_remove_port.setIcon(getIcon("list-remove"))
        self.act_patchbay_rename_port.setIcon(getIcon("edit-rename"))
        self.act_patchbay_connect_ports.setIcon(getIcon("network-connect"))
        self.act_patchbay_disconnect_ports.setIcon(getIcon("network-disconnect"))
        self.b_group_add.setIcon(getIcon("list-add"))
        self.b_group_remove.setIcon(getIcon("edit-delete"))
        self.b_group_rename.setIcon(getIcon("edit-rename"))
        self.b_port_add.setIcon(getIcon("list-add"))
        self.b_port_remove.setIcon(getIcon("list-remove"))
        self.b_port_rename.setIcon(getIcon("edit-rename"))
        self.b_ports_connect.setIcon(getIcon("network-connect"))
        self.b_ports_disconnect.setIcon(getIcon("network-disconnect"))

        # Set Scene
        self.scene = patchcanvas.MyGraphicsScene(self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.setRenderHint(QPainter.Antialiasing, True if (self.saved_settings["Canvas/Antialiasing"] == Qt.Checked) else False)
        self.graphicsView.setRenderHint(QPainter.TextAntialiasing, self.saved_settings["Canvas/TextAntialiasing"])
        self.graphicsView.setOptimizationFlag(QGraphicsView.DontSavePainterState, True)
        self.graphicsView.setOptimizationFlag(QGraphicsView.DontAdjustForAntialiasing, not bool(self.saved_settings["Canvas/Antialiasing"]))

        # Create Canvas
        patchcanvas.options['theme_name'] = unicode(self.saved_settings["Canvas/Theme"])
        patchcanvas.options['bezier_lines'] = self.saved_settings["Canvas/BezierLines"]
        patchcanvas.options['antialiasing'] = self.saved_settings["Canvas/Antialiasing"]
        patchcanvas.options['auto_hide_groups'] = self.saved_settings["Canvas/AutoHideGroups"]
        patchcanvas.options['connect_midi2outro'] = False
        patchcanvas.options['fancy_eyecandy'] = self.saved_settings["Canvas/FancyEyeCandy"]

        patchcanvas.features['group_rename'] = True
        patchcanvas.features['port_rename'] = True
        patchcanvas.features['handle_group_pos'] = False

        patchcanvas.init(self.scene, self.canvas_callback)

        setConnections(self, ["canvas"])

        self.connect(self.act_project_new, SIGNAL("triggered()"), self.func_project_new)
        self.connect(self.act_project_open, SIGNAL("triggered()"), self.func_project_open)
        self.connect(self.act_project_save, SIGNAL("triggered()"), self.func_project_save)
        self.connect(self.act_project_save_as, SIGNAL("triggered()"), self.func_project_save_as)
        self.connect(self.b_project_new, SIGNAL("clicked()"), self.func_project_new)
        self.connect(self.b_project_open, SIGNAL("clicked()"), self.func_project_open)
        self.connect(self.b_project_save, SIGNAL("clicked()"), self.func_project_save)
        self.connect(self.b_project_save_as, SIGNAL("clicked()"), self.func_project_save_as)
        self.connect(self.act_patchbay_add_group, SIGNAL("triggered()"), self.func_group_add)
        self.connect(self.act_patchbay_remove_group, SIGNAL("triggered()"), self.func_group_remove)
        self.connect(self.act_patchbay_rename_group, SIGNAL("triggered()"), self.func_group_rename)
        self.connect(self.act_patchbay_add_port, SIGNAL("triggered()"), self.func_port_add)
        self.connect(self.act_patchbay_remove_port, SIGNAL("triggered()"), self.func_port_remove)
        self.connect(self.act_patchbay_rename_port, SIGNAL("triggered()"), self.func_port_rename)
        self.connect(self.act_patchbay_connect_ports, SIGNAL("triggered()"), self.func_connect_ports)
        self.connect(self.act_patchbay_disconnect_ports, SIGNAL("triggered()"), self.func_disconnect_ports)
        self.connect(self.b_group_add, SIGNAL("clicked()"), self.func_group_add)
        self.connect(self.b_group_remove, SIGNAL("clicked()"), self.func_group_remove)
        self.connect(self.b_group_rename, SIGNAL("clicked()"), self.func_group_rename)
        self.connect(self.b_port_add, SIGNAL("clicked()"), self.func_port_add)
        self.connect(self.b_port_remove, SIGNAL("clicked()"), self.func_port_remove)
        self.connect(self.b_port_rename, SIGNAL("clicked()"), self.func_port_rename)
        self.connect(self.b_ports_connect, SIGNAL("clicked()"), self.func_connect_ports)
        self.connect(self.b_ports_disconnect, SIGNAL("clicked()"), self.func_disconnect_ports)

        self.connect(self.act_settings_configure, SIGNAL("triggered()"), self.configureCatarina)

        self.connect(self.act_help_about, SIGNAL("triggered()"), self.aboutCatarina)
        self.connect(self.act_help_about_qt, SIGNAL("triggered()"), app, SLOT("aboutQt()"))

        # Start Empty Project
        self.func_project_new()

    def canvas_callback(self, action, *args):
        if (action == patchcanvas.ACTION_PORT_DISCONNECT_ALL):
          port_id = args[0]

          h = 0
          for i in range(len(self.connection_list)):
            if (self.connection_list[i-h][1] == port_id or self.connection_list[i-h][2] == port_id):
              patchcanvas.disconnectPorts(self.connection_list[i-h][0])
              self.connection_list.pop(i-h)
              h += 1

        elif (action == patchcanvas.ACTION_PORT_RENAME):
          port_id = args[0]
          new_port_name = args[1]
          patchcanvas.renamePort(port_id, new_port_name)

          for i in range(len(self.port_list)):
            if (port_id == self.port_list[i][1]):
              self.port_list[i][2] = new_port_name
              break

        elif (action == patchcanvas.ACTION_PORT_INFO):
          port_id = args[0]

          for i in range(len(self.port_list)):
            if (self.port_list[i][1] == port_id):
              group_id  = self.port_list[i][0]
              port_name = self.port_list[i][2]
              port_mode = self.port_list[i][3]
              port_type = self.port_list[i][4]
              break

          for i in range(len(self.group_list)):
            if (self.group_list[i][0] == group_id):
              group_name  = self.group_list[i][1]
              break

          if (port_mode == patchcanvas.PORT_MODE_INPUT):
            mode_text = self.tr("Input")
          elif (port_mode == patchcanvas.PORT_MODE_OUTPUT):
            mode_text = self.tr("Output")
          else:
            mode_text = self.tr("Unknown")

          if (port_type == patchcanvas.PORT_TYPE_AUDIO):
            type_text = self.tr("Audio")
          elif (port_type == patchcanvas.PORT_TYPE_MIDI):
            type_text = self.tr("MIDI")
          elif (port_type == patchcanvas.PORT_TYPE_OUTRO):
            type_text = self.tr("Outro")
          else:
            type_text = self.tr("Unknown")

          port_full_name = group_name+":"+port_name

          info = self.tr(""
                  "<table>"
                  "<tr><td align='right'><b>Group Name:</b></td><td>&nbsp;%1</td></tr>"
                  "<tr><td align='right'><b>Group ID:</b></td><td>&nbsp;%2</td></tr>"
                  "<tr><td align='right'><b>Port Name:</b></td><td>&nbsp;%3</td></tr>"
                  "<tr><td align='right'><b>Port ID:</b></td><td>&nbsp;%4</i></td></tr>"
                  "<tr><td align='right'><b>Full Port Name:</b></td><td>&nbsp;%5</td></tr>"
                  "<tr><td colspan='2'>&nbsp;</td></tr>"
                  "<tr><td align='right'><b>Port Mode:</b></td><td>&nbsp;%6</td></tr>"
                  "<tr><td align='right'><b>Port Type:</b></td><td>&nbsp;%7</td></tr>"
                  "</table>"
                  ).arg(group_name).arg(group_id).arg(port_name).arg(port_id).arg(port_full_name).arg(mode_text).arg(type_text)

          QMessageBox.information(self, self.tr("Port Information"), info)

        elif (action == patchcanvas.ACTION_PORTS_CONNECT):
          connection_id = self.last_connection_id
          port_out_id = args[0]
          port_in_id  = args[2]
          patchcanvas.connectPorts(connection_id, port_out_id, port_in_id)

          self.connection_list.append([connection_id, port_out_id, port_in_id])
          self.last_connection_id += 1

        elif (action == patchcanvas.ACTION_PORTS_DISCONNECT):
          connection_id = args[0]
          patchcanvas.disconnectPorts(connection_id)

          for i in range(len(self.connection_list)):
            if (connection_id == self.connection_list[i][0]):
              self.connection_list.pop(i)
              break

        elif (action == patchcanvas.ACTION_GROUP_DISCONNECT_ALL):
          group_id = args[0]
          group_port_list = []

          for i in range(len(self.port_list)):
            if (self.port_list[i][0] == group_id):
              group_port_list.append(self.port_list[i][1])

          h = 0
          for i in range(len(self.connection_list)):
            if (self.connection_list[i-h][1] in group_port_list or self.connection_list[i-h][2] in group_port_list):
              patchcanvas.disconnectPorts(self.connection_list[i-h][0])
              self.connection_list.pop(i-h)
              h += 1

        elif (action == patchcanvas.ACTION_GROUP_RENAME):
          group_id = args[0]
          new_group_name = args[1]
          patchcanvas.renameGroup(group_id, new_group_name)

          for i in range(len(self.group_list)):
            if (self.group_list[i][0] == group_id):
              self.group_list[i][1] = new_group_name
              break

        elif (action == patchcanvas.ACTION_GROUP_JOIN):
          group_id = args[0]
          patchcanvas.joinGroup(group_id)

          for i in range(len(self.group_list)):
            if (self.group_list[i][0] == group_id):
              self.group_list[i][2] = False
              break

        elif (action == patchcanvas.ACTION_GROUP_SPLIT):
          group_id = args[0]
          patchcanvas.splitGroup(group_id)

          for i in range(len(self.group_list)):
            if (self.group_list[i][0] == group_id):
              self.group_list[i][2] = True
              break

        elif (action == patchcanvas.ACTION_REQUEST_PORT_CONNECTION_LIST):
          port_id = args[0]
          connections = []

          those_port_ids = []

          for i in range(len(self.connection_list)):
            if (self.connection_list[i][1] == port_id):
              those_port_ids.append(self.connection_list[i][2])
            elif (self.connection_list[i][2] == port_id):
              those_port_ids.append(self.connection_list[i][1])

          for i in range(len(those_port_ids)):
            target_port_id = those_port_ids[i]

            for j in range(len(self.port_list)):
              if (self.port_list[j][1] == target_port_id):
                target_group_id = self.port_list[j][0]
                target_port_name = self.port_list[j][2]
                break
            else:
              return []

            for j in range(len(self.group_list)):
              if (self.group_list[j][0] == target_group_id):
                target_group_name = self.group_list[j][1]
                break
            else:
              return []

            connections.append((target_port_id, target_group_name+":"+target_port_name))

          return connections

        elif (action == patchcanvas.ACTION_REQUEST_GROUP_CONNECTION_LIST):
          group_id = args[0]
          port_modes = args[2]
          connections = []

          this_port_ids = []
          those_port_ids = []

          for i in range(len(self.port_list)):
            if (self.port_list[i][0] == group_id):
              port_id = self.port_list[i][1]
              port_mode = self.port_list[i][3]

              if ( (port_mode == patchcanvas.PORT_MODE_OUTPUT and port_modes & patchcanvas.PORT_MODE_OUTPUT) or
                   (port_mode == patchcanvas.PORT_MODE_INPUT and port_modes & patchcanvas.PORT_MODE_INPUT) ):
                this_port_ids.append(port_id)

          for i in range(len(self.connection_list)):
            if (self.connection_list[i][1] in this_port_ids):
              those_port_ids.append(self.connection_list[i][2])
            elif (self.connection_list[i][2] in this_port_ids):
              those_port_ids.append(self.connection_list[i][1])

          for i in range(len(those_port_ids)):
            port_id = those_port_ids[i]

            for j in range(len(self.port_list)):
              if (self.port_list[j][1] == port_id):
                group_id = self.port_list[j][0]
                port_name = self.port_list[j][2]
                break
            else:
              return []

            for j in range(len(self.group_list)):
              if (self.group_list[j][0] == group_id):
                group_name = self.group_list[j][1]
                break
            else:
              return []

            connections.append((port_id, group_name+":"+port_name))

          return connections

    def init_ports(self):
        for i in range(len(self.group_list)):
          patchcanvas.addGroup(self.group_list[i][0], self.group_list[i][1], self.group_list[i][2], self.group_list[i][3])

        for i in range(len(self.group_list_pos)):
          patchcanvas.setGroupPos(self.group_list_pos[i][0], self.group_list_pos[i][1], self.group_list_pos[i][2], self.group_list_pos[i][3], self.group_list_pos[i][4])

        for i in range(len(self.port_list)):
          patchcanvas.addPort(self.port_list[i][0], self.port_list[i][1], self.port_list[i][2], self.port_list[i][3], self.port_list[i][4])

        for i in range(len(self.connection_list)):
          patchcanvas.connectPorts(self.connection_list[i][0], self.connection_list[i][1], self.connection_list[i][2])

        self.group_list_pos = []

    def func_project_new(self):
        self.group_list = []
        self.group_list_pos = []
        self.port_list = []
        self.connection_list = []
        self.last_group_id = 1
        self.last_port_id = 1
        self.last_connection_id = 1
        self.save_path = None
        patchcanvas.clear()

    def func_project_open(self):
        path = QFileDialog.getOpenFileName(self, self.tr("Load State"), filter=self.tr("Catarina XML Document (*.xml)"))
        if (not path.isEmpty()):
          patchcanvas.clear()
          self.save_path = path
          self.loadFile(path)
          self.init_ports()

    def func_project_save(self):
        if (self.save_path):
          self.saveFile(self.save_path)
        else:
          self.func_project_save_as()

    def func_project_save_as(self):
        path = QFileDialog.getSaveFileName(self, self.tr("Save State"), filter=self.tr("Catarina XML Document (*.xml)"))
        if (not path.isEmpty()):
          self.save_path = path
          self.saveFile(path)

    def func_group_add(self):
        dialog = CatarinaAddGroupW(self)
        if (dialog.exec_()):
          group_id    = self.last_group_id
          group_name  = dialog.ret_name
          group_split = dialog.ret_split
          group_icon  = patchcanvas.ICON_HARDWARE if (group_split) else patchcanvas.ICON_APPLICATION
          patchcanvas.addGroup(group_id, group_name, group_split, group_icon)
          self.group_list.append([group_id, group_name, group_split, group_icon])
          self.last_group_id += 1

    def func_group_remove(self):
        dialog = CatarinaRemoveGroupW(self, self.group_list)
        if (dialog.exec_()):
          group_id = dialog.ret_group_id

          for i in range(len(self.port_list)):
            if (group_id == self.port_list[i][0]):
              port_id = self.port_list[i][1]

              h = 0
              for j in range(len(self.connection_list)):
                if (self.connection_list[j-h][1] == port_id or self.connection_list[j-h][2] == port_id):
                  patchcanvas.disconnectPorts(self.connection_list[j-h][0])
                  self.connection_list.pop(j-h)
                  h += 1

          h = 0
          for i in range(len(self.port_list)):
            if (self.port_list[i-h][0] == group_id):
              port_id = self.port_list[i-h][1]
              patchcanvas.removePort(port_id)
              self.port_list.pop(i-h)
              h += 1

          patchcanvas.removeGroup(group_id)

          for i in range(len(self.group_list)):
            if (self.group_list[i][0] == group_id):
              self.group_list.pop(i)
              break

    def func_group_rename(self):
        dialog = CatarinaRenameGroupW(self, self.group_list)
        if (dialog.exec_()):
          group_id = dialog.ret_group_id
          new_group_name = dialog.ret_new_group_name
          patchcanvas.renameGroup(group_id, new_group_name)

          for i in range(len(self.group_list)):
            if (self.group_list[i][0] == group_id):
              self.group_list[i][1] = new_group_name
              break

    def func_port_add(self):
        if (len(self.group_list) > 0):
          dialog = CatarinaAddPortW(self, self.group_list, self.last_port_id)
          if (dialog.exec_()):
            port_group = dialog.ret_port_group
            port_id = dialog.ret_port_id
            port_name = dialog.ret_name
            port_mode = dialog.ret_port_mode
            port_type = dialog.ret_port_type
            patchcanvas.addPort(port_group, port_id, port_name, port_mode, port_type)

            self.port_list.append([port_group, port_id, port_name, port_mode, port_type])
            self.last_port_id += 1

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please add a Group first!"))

    def func_port_remove(self):
        if (len(self.port_list) > 0):
          dialog = CatarinaRemovePortW(self, self.group_list, self.port_list)
          if (dialog.exec_()):
            port_id = dialog.ret_port_id
            patchcanvas.removePort(port_id)

            h = 0
            for i in range(len(self.connection_list)):
              if (self.connection_list[i-h][1] == port_id or self.connection_list[i-h][2] == port_id):
                patchcanvas.disconnectPorts(self.connection_list[i-h][0])
                self.connection_list.pop(i-h)
                h += 1

            for i in range(len(self.port_list)):
              if (self.port_list[i][1] == port_id):
                self.port_list.pop(i)
                break

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please add a Port first!"))

    def func_port_rename(self):
        if (len(self.port_list) > 0):
          dialog = CatarinaRenamePortW(self, self.group_list, self.port_list)
          if (dialog.exec_()):
            port_id = dialog.ret_port_id
            new_port_name = dialog.ret_new_port_name
            patchcanvas.renamePort(port_id, new_port_name)

            for i in range(len(self.port_list)):
              if (self.port_list[i][1] == port_id):
                self.port_list[i][2] = new_port_name
                break

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please add a Port first!"))

    def func_connect_ports(self):
        if (len(self.port_list) > 0):
          dialog = CatarinaConnectPortsW(self, self.group_list, self.port_list)
          if (dialog.exec_()):
            connection_id = self.last_connection_id
            port_out_id = dialog.ret_port_ids[0]
            port_in_id  = dialog.ret_port_ids[1]

            for i in range(len(self.connection_list)):
              if (self.connection_list[i][1] == port_out_id and self.connection_list[i][2] == port_in_id):
                QMessageBox.warning(self, self.tr("Warning"), self.tr("Ports already connected!"))
                return

            patchcanvas.connectPorts(connection_id, port_out_id, port_in_id)

            self.connection_list.append([connection_id, port_out_id, port_in_id])
            self.last_connection_id += 1

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please add some Ports first!"))

    def func_disconnect_ports(self):
        if (len(self.connection_list) > 0):
          dialog = CatarinaDisconnectPortsW(self, self.group_list, self.port_list, self.connection_list)
          if (dialog.exec_()):
            connection_id = None
            port_out_id = dialog.ret_port_ids[0]
            port_in_id  = dialog.ret_port_ids[1]

            for i in range(len(self.connection_list)):
              if (self.connection_list[i][1] == port_out_id and self.connection_list[i][2] == port_in_id):
                connection_id = self.connection_list[i][0]
                self.connection_list.pop(i)
                break
            else:
              print "could not find the connection"
              return

            patchcanvas.disconnectPorts(connection_id)

        else:
          QMessageBox.warning(self, self.tr("Warning"), self.tr("Please make some Connections first!"))

    def configureCatarina(self):
        CatarinaSettingsW(self).exec_()

    def aboutCatarina(self):
        QMessageBox.about(self, self.tr("About Catarina"), self.tr("<h3>Catarina</h3>"
            "<br>Version %1"
            "<br>Catarina is a testing ground for the 'PatchCanvas' module.<br>"
            "<br>Copyright (C) 2010 falkTX").arg(VERSION))

    def saveSettings(self):
        settings = QSettings()
        settings.setValue("Geometry", QVariant(self.saveGeometry()))
        settings.setValue("ShowToolbar", self.frame_toolbar.isVisible())

    def loadSettings(self):
        settings = QSettings()
        self.restoreGeometry(settings.value("Geometry").toByteArray())

        show_toolbar = settings.value("ShowToolbar", True).toBool()
        self.act_settings_show_toolbar.setChecked(show_toolbar)
        self.frame_toolbar.setVisible(show_toolbar)

        self.saved_settings = {
          "Canvas/Theme": settings.value("Canvas/Theme", patchcanvas.options['theme_name']).toString(),
          "Canvas/BezierLines": settings.value("Canvas/BezierLines", patchcanvas.options['bezier_lines']).toBool(),
          "Canvas/AutoHideGroups": settings.value("Canvas/AutoHideGroups", False).toBool(),
          "Canvas/FancyEyeCandy": settings.value("Canvas/FancyEyeCandy", patchcanvas.options['fancy_eyecandy']).toBool(),
          "Canvas/Antialiasing": settings.value("Canvas/Antialiasing", Qt.PartiallyChecked).toInt()[0],
          "Canvas/TextAntialiasing": settings.value("Canvas/TextAntialiasing", True).toBool()
        }

    def getGroupPos(self, group_id):
        for i in range(len(self.group_list)):
          if (self.group_list[i][0] == group_id):
            group_name  = self.group_list[i][1]
            group_split = self.group_list[i][2]
            group_icon  = self.group_list[i][3]
            break
        else:
          return [0.0, 0.0, 0.0, 0.0]

        for i in range(len(patchcanvas.Canvas.group_list)):
          if (patchcanvas.Canvas.group_list[i]['id'] == group_id):
            pos = patchcanvas.Canvas.group_list[i]['widgets'][0].scenePos()

            if (group_split):
              poss = patchcanvas.Canvas.group_list[i]['widgets'][1].scenePos()
            else:
              poss = pos

            return [pos.x(), pos.y(), poss.x(), poss.y()]

    def saveFile(self, path):
        content = ("<?xml version='1.0' encoding='UTF-8'?>\n"
                   "<!DOCTYPE CATARINA>\n"
                   "<CATARINA VERSION='%s'>\n") % (VERSION)

        content += " <Groups>\n"
        for i in range(len(self.group_list)):
          group_id = self.group_list[i][0]
          group_name  = self.group_list[i][1]
          group_split = self.group_list[i][2]
          group_icon  = self.group_list[i][3]
          group_pos = self.getGroupPos(group_id)
          content += "  <g%i> <name>%s</name> <data>%i:%i:%i:%f:%f:%f:%f</data> </g%i>\n" % (i, group_name, group_id, group_split, group_icon,
                      group_pos[0], group_pos[1], group_pos[2], group_pos[3], i)
        content += " </Groups>\n"

        content += " <Ports>\n"
        for i in range(len(self.port_list)):
          content += "  <p%i> <name>%s</name> <data>%i:%i:%i:%i</data> </p%i>\n" % (i,
            self.port_list[i][2], self.port_list[i][0], self.port_list[i][1], self.port_list[i][3], self.port_list[i][4], i)
        content += " </Ports>\n"

        content += " <Connections>\n"
        for i in range(len(self.connection_list)):
          content += "  <c%i>%i:%i:%i</c%i>\n" % (i, self.connection_list[i][0], self.connection_list[i][1], self.connection_list[i][2], i)
        content += " </Connections>\n"

        content += "</CATARINA>\n"

        if (open(path, "w").write(content)):
          QMessageBox.critical(self, self.tr("Error"), self.tr("Failed to save file"))

    def loadFile(self, path):
        if not os.path.exists(path):
          QMessageBox.critical(self, self.tr("Error"), self.tr("The file '%1' does not exist").arg(path))
          return

        read = open(path, "r").read()

        if not read:
          QMessageBox.critical(self, self.tr("Error"), self.tr("Failed to load file"))
          return

        self.group_list = []
        self.group_list_pos = []
        self.port_list = []
        self.connection_list = []
        self.last_group_id = 1
        self.last_port_id = 1
        self.last_connection_id = 1

        xml = QDomDocument()
        xml.setContent(read)

        content = xml.documentElement()
        if (content.tagName() != "CATARINA"):
          QMessageBox.critical(self, self.tr("Error"), self.tr("Not a valid Catarina file"))
          return

        # Get values from XML - the big code
        node = content.firstChild()
        while not node.isNull():
          if (node.toElement().tagName() == "Groups"):
            group_id = group_name = group_split = None
            groups = node.toElement().firstChild()
            while not groups.isNull():
              group = groups.toElement().firstChild()
              while not group.isNull():
                tag  = group.toElement().tagName()
                text = group.toElement().text()
                if (tag == "name"):
                  group_name = unicode(text)
                elif (tag == "data"):
                  group_data  = text.split(":")
                  group_id    = int(group_data[0])
                  group_split = int(group_data[1])
                  group_icon = int(group_data[2])
                  group_pos_x = float(group_data[3])
                  group_pos_y = float(group_data[4])
                  group_pos_xs = float(group_data[5])
                  group_pos_ys = float(group_data[6])
                  self.group_list.append([group_id, group_name, group_split, group_icon])
                  self.group_list_pos.append([group_id, group_pos_x, group_pos_y, group_pos_xs, group_pos_ys])
                  if (group_id > self.last_group_id):
                    self.last_group_id = group_id+1
                group = group.nextSibling()
              groups = groups.nextSibling()

          elif (node.toElement().tagName() == "Ports"):
            group_id = port_id = port_name = port_mode = port_type = None
            ports = node.toElement().firstChild()
            while not ports.isNull():
              port = ports.toElement().firstChild()
              while not port.isNull():
                tag  = port.toElement().tagName()
                text = port.toElement().text()
                if (tag == "name"):
                  port_name = unicode(text)
                elif (tag == "data"):
                  port_data = text.split(":")
                  group_id  = int(port_data[0])
                  port_id   = int(port_data[1])
                  port_mode = int(port_data[2])
                  port_type = int(port_data[3])
                  self.port_list.append([group_id, port_id, port_name, port_mode, port_type])
                  if (port_id > self.last_port_id):
                    self.last_port_id = port_id+1
                port = port.nextSibling()
              ports = ports.nextSibling()

          elif (node.toElement().tagName() == "Connections"):
            conns = node.toElement().firstChild()
            while not conns.isNull():
              conn_data = conns.toElement().text().split(":")
              if (not conn_data[0].toInt()[0]):
                conns = conns.nextSibling()
                continue
              connection_id = int(conn_data[0])
              conn_source_id = int(conn_data[1])
              conn_target_id = int(conn_data[2])
              self.connection_list.append([connection_id, conn_source_id, conn_target_id])
              if (connection_id > self.last_connection_id):
                self.last_connection_id = connection_id+1
              conns = conns.nextSibling()
          node = node.nextSibling()

    def closeEvent(self, event):
        self.saveSettings()
        patchcanvas.clear()
        return QMainWindow.closeEvent(self, event)

#--------------- main ------------------
if __name__ == '__main__':

    # Raster Graphics engine
    QRasterApplication = QApplication
    QRasterApplication.setGraphicsSystem("raster")

    # App initialization
    app = QRasterApplication(sys.argv)
    app.setApplicationName("Catarina")
    app.setApplicationVersion(VERSION)
    app.setOrganizationName("falkTX")
    #app.setWindowIcon(QIcon(":/48x48/catarina.png"))

    # Show GUI
    gui = CatarinaMainW()

    if (app.arguments().count() > 1):
      gui.save_path = unicode(app.arguments()[1])
      gui.loadFile(gui.save_path)
      gui.init_ports()

    gui.show()

    # App-Loop
    sys.exit(app.exec_())
