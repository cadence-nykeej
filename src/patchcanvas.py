#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
from PyQt4.QtCore import Qt, QLineF, QPointF, QRectF, QSettings, QString, QTimer, QVariant
from PyQt4.QtGui import QColor, QCursor, QIcon, QInputDialog, QFont, QFontMetrics, QGraphicsItem, QGraphicsLineItem, QGraphicsPathItem
from PyQt4.QtGui import QLinearGradient, QMenu, QPainter, QPainterPath, QPen, QPolygonF
from PyQt4.QtSvg import QGraphicsSvgItem, QSvgRenderer

# For FX
from PyQt4.QtCore import QObject, QAbstractAnimation, SIGNAL
from PyQt4.QtGui import QGraphicsColorizeEffect, QGraphicsDropShadowEffect

# Only used for MyGraphicsScene
from PyQt4.QtGui import QGraphicsScene
from PyQt4.QtCore import QPointF, QRectF

# Imports (Plugins and Resources)
from themes import *

# Properly convert QString to str
def QStringStr(string):
    return str(unicode(string).encode('utf-8'))

# Port Mode
PORT_MODE_INPUT  = 1
PORT_MODE_OUTPUT = 2

# Port Type
PORT_TYPE_AUDIO = 0
PORT_TYPE_MIDI  = 1
PORT_TYPE_OUTRO = 2

# Callback Action
ACTION_PORT_DISCONNECT_ALL = 0
ACTION_PORT_RENAME = 1
ACTION_PORT_INFO = 2
ACTION_PORTS_CONNECT = 3
ACTION_PORTS_DISCONNECT = 4
ACTION_GROUP_DISCONNECT_ALL = 5
ACTION_GROUP_RENAME = 6
ACTION_GROUP_INFO = 7
ACTION_GROUP_SPLIT = 8
ACTION_GROUP_JOIN = 9
ACTION_REQUEST_PORT_CONNECTION_LIST = 10
ACTION_REQUEST_GROUP_CONNECTION_LIST = 11

# Icon
ICON_HARDWARE = 0
ICON_APPLICATION = 1
ICON_LADISH_ROOM = 2

# Animation list
global animations
animations = []

# Canvas options
options = {
    'theme_name': u"",
    'bezier_lines': True,
    'antialiasing': 1,
    'auto_hide_groups': True,
    'connect_midi2outro': False,
    'fancy_eyecandy': False
}

# Canvas features
features = {
    'group_rename': True,
    'port_rename': True,
    'handle_group_pos': False
}

# Main Canvas object
class Canvas(object):
    __slots__ = [
      'scene',
      'callback',
      'debug',
      'last_z_value',
      'last_group_id',
      'last_connection_id',
      'initial_pos',
      'group_list',
      'port_list',
      'connection_list',
      'postponed_groups',
      'postponed_timer',
      'settings',
      'theme',
      'size_rect'
      #'icons'
    ]
Canvas = Canvas()

def init(scene, callback, debug=True):
  if (debug):
    print "patchcanvas::init(scene, callback, %i)" % (debug)

  Canvas.scene = scene
  Canvas.callback = callback
  Canvas.debug = debug

  Canvas.last_z_value = 0
  Canvas.last_group_id = 0
  Canvas.last_connection_id = 0
  Canvas.initial_pos = QPointF(0, 0)

  Canvas.group_list = []
  Canvas.port_list = []
  Canvas.connection_list = []

  Canvas.postponed_groups = []
  Canvas.postponed_timer = QTimer()
  Canvas.postponed_timer.setInterval(100)
  QObject.connect(Canvas.postponed_timer, SIGNAL("timeout()"), CanvasPostponedGroups)

  Canvas.settings = QSettings()

  for i in range(len(theme_list)):
    if (theme_list[i]['name'] == options['theme_name']):
      Canvas.theme = theme_list[i]
      break
  else:
    Canvas.theme = getDefaultTheme()

  Canvas.size_rect = QRectF()

  Canvas.scene.setBackgroundBrush(Canvas.theme['canvas_bg'])

  #Canvas.icons = XIcon()
  #Canvas.icons.addIconPath("/usr/share/cadence/icons/")
  #Canvas.icons.addThemeName(str(unicode(QIcon.themeName()).encode('utf-8')))
  #Canvas.icons.addThemeName("oxygen") #Just in case...

def setInitialPos(x, y):
  if (Canvas.debug):
    print "patchcanvas::setInitialPos(%i, %i)" % (x, y)

  Canvas.initial_pos = QPointF(x, y)

def setCanvasSize(x, y, width, height):
  if (Canvas.debug):
    print "patchcanvas::setCanvasSize(%i, %i, %i, %i)" % (x, y, width, height)

  Canvas.size_rect = QRectF(x, y, width, height)

def clear():
  if (Canvas.debug):
    print "patchcanvas::clear()"

  h = 0
  for i in range(len(Canvas.connection_list)):
    disconnectPorts(Canvas.connection_list[i-h][0])
    h += 1

  h = 0
  for i in range(len(Canvas.port_list)):
    removePort(Canvas.port_list[i-h]['id'])
    h += 1

  h = 0
  for i in range(len(Canvas.group_list)):
    removeGroup(Canvas.group_list[i-h]['id'])
    h += 1

  global animations
  for i in range(len(animations)):
    if (animations[0][0].state() == QAbstractAnimation.Running):
      animations[0][0].stop()
      RemoveItemFX(animations[0][1])
    animations.pop(0)

  Canvas.last_z_value = 0
  Canvas.last_group_id = 0
  Canvas.last_connection_id = 0

  Canvas.group_list = []
  Canvas.port_list = []
  Canvas.connection_list = []
  #Canvas.postponed_groups = [] # TESTING - force remove, or wait for removal?
  #Canvas.postponed_timer.stop()

def addGroup(group_id, group_name, split=False, icon=ICON_APPLICATION):
  if (Canvas.debug):
    print "patchcanvas::addGroup(%i, %s, %i, %i)" % (group_id, group_name, split, icon)

  group_box = CanvasBox(group_id, group_name, icon, Canvas.scene)
  group_dict = {
    'id': group_id,
    'name': group_name,
    'split': split,
    'icon': icon,
    'widgets': [group_box]
  }

  if (split):
    group_box.setSplit(True, PORT_MODE_OUTPUT)

    if (features['handle_group_pos']):
      group_box.setPos(Canvas.settings.value("CanvasPositions/%s_o" % (group_name), CanvasGetNewGroupPos()).toPointF())
    else:
      group_box.setPos(CanvasGetNewGroupPos())

    group_sbox = CanvasBox(group_id, group_name, icon, Canvas.scene)
    group_dict['widgets'].append(group_sbox)

    group_sbox.setSplit(True, PORT_MODE_INPUT)

    if (features['handle_group_pos']):
      group_sbox.setPos(Canvas.settings.value("CanvasPositions/%s_i" % (group_name), CanvasGetNewGroupPos(True)).toPointF())
    else:
      group_sbox.setPos(CanvasGetNewGroupPos(True))

    #if (not options['auto_hide_groups'] and options['fancy_eyecandy']):
      #ItemFX(group_sbox, True)

    Canvas.last_z_value+=2
    group_sbox.setZValue(Canvas.last_z_value)

  else:
    if (group_name == "Hardware Capture" or group_name == "Hardware Playback" or group_name == "Capture" or group_name == "Playback"):
      horizontal = True
    else:
      horizontal = False

    group_box.setSplit(False)

    if (features['handle_group_pos']):
      group_box.setPos(Canvas.settings.value("CanvasPositions/%s" % (group_name), CanvasGetNewGroupPos()).toPointF())
    else:
      group_box.setPos(CanvasGetNewGroupPos(horizontal))

  #if (not options['auto_hide_groups'] and options['fancy_eyecandy']):
    #ItemFX(group_box, True)

  Canvas.last_z_value+=1
  group_box.setZValue(Canvas.last_z_value)

  Canvas.group_list.append(group_dict)

  QTimer.singleShot(0, Canvas.scene.update)

def removeGroup(group_id):
  if (Canvas.debug):
    print "patchcanvas::removeGroup(%i)" % (group_id)

  for i in range(len(Canvas.group_list)):
    if (Canvas.group_list[i]['id'] == group_id):
      item = Canvas.group_list[i]['widgets'][0]
      group_name = Canvas.group_list[i]['name']

      if (len(item.port_list) > 0):
        if (Canvas.debug):
          print "patchcanvas::removeGroup - This group still has ports, postpone it's removal"
        Canvas.postponed_groups.append(group_id)
        Canvas.postponed_timer.start()
        return

      if (Canvas.group_list[i]['split']):
        s_item = Canvas.group_list[i]['widgets'][1]
        if (features['handle_group_pos']): Canvas.settings.setValue("CanvasPositions/%s_o" % (group_name), item.pos())
        if (features['handle_group_pos']): Canvas.settings.setValue("CanvasPositions/%s_i" % (group_name), s_item.pos())

        if (options['fancy_eyecandy'] and s_item.isVisible()):
          ItemFX(s_item, False)
        else:
          Canvas.scene.removeItem(s_item.icon_svg)
          Canvas.scene.removeItem(s_item)

      else:
        if (features['handle_group_pos']): Canvas.settings.setValue("CanvasPositions/%s" % (group_name), item.pos())

      if (options['fancy_eyecandy'] and item.isVisible()):
        ItemFX(item, False)
      else:
        Canvas.scene.removeItem(item.icon_svg)
        Canvas.scene.removeItem(item)

      Canvas.group_list.pop(i)
      break

  else:
    print "patchcanvas::removeGroup - Unable to find group to remove"
    return

  QTimer.singleShot(0, Canvas.scene.update)

def renameGroup(group_id, new_name):
  if (Canvas.debug):
    print "patchcanvas::renameGroup(%i, %s)" % (group_id, new_name)

  for i in range(len(Canvas.group_list)):
    if (Canvas.group_list[i]['id'] == group_id):
      Canvas.group_list[i]['widgets'][0].setText(new_name)
      Canvas.group_list[i]['name'] = new_name

      if (Canvas.group_list[i]['split']):
        Canvas.group_list[i]['widgets'][1].setText(new_name)
        Canvas.group_list[i]['name'] = new_name

      break

  else:
    print "patchcanvas::renameGroup - Unable to find group to rename"
    return

  QTimer.singleShot(0, Canvas.scene.update)

def splitGroup(group_id):
  if (Canvas.debug):
    print "patchcanvas::splitGroup(%i)" % (group_id)

  item = None
  group_name = None
  group_icon = None
  ports_data = []
  ports_list_ids = []
  conns_data = []

  # Step 1 - Store all Item data
  for i in range(len(Canvas.group_list)):
    if (Canvas.group_list[i]['id'] == group_id):
      item_dict = Canvas.group_list[i]
      item = item_dict['widgets'][0]
      group_name = item_dict['name']
      group_icon = item_dict['icon']
      break
  else:
    print "patchcanvas::splitGroup - Unable to find group to split"
    return

  for i in range(len(item.port_list)):
    port_id = item.port_list[i]['id']
    port_name = item.port_list[i]['name']
    port_mode = item.port_list[i]['mode']
    port_type = item.port_list[i]['type']

    ports_data.append((port_id, port_name, port_mode, port_type))
    ports_list_ids.append(port_id)

  for i in range(len(Canvas.connection_list)):
    if (Canvas.connection_list[i][1] in ports_list_ids or Canvas.connection_list[i][2] in ports_list_ids):
      conns_data.append(Canvas.connection_list[i])

  # Step 2 - Remove Item and Children
  for i in range(len(conns_data)):
    disconnectPorts(conn_data[i][0])

  for i in range(len(ports_data)):
    removePort(ports_data[i][0])

  removeGroup(group_id)

  # Step 3 - Re-create Item, now splitted
  addGroup(group_id, group_name, True, group_icon)

  for i in range(len(ports_data)):
    addPort(group_id, ports_data[i][0], ports_data[i][1], ports_data[i][2], ports_data[i][3])

  for i in range(len(conns_data)):
    connectPorts(conns_data[i][0], conns_data[i][1], conns_data[i][2])

  QTimer.singleShot(0, Canvas.scene.update)

def joinGroup(group_id):
  if (Canvas.debug):
    print "patchcanvas::joinGroup(%i)" % (group_id)

  item = None
  s_item = None
  group_name = None
  group_icon = None
  ports_data = []
  ports_list_ids = []
  conns_data = []

  # Step 1 - Store all Item data
  for i in range(len(Canvas.group_list)):
    if (Canvas.group_list[i]['id'] == group_id):
      item_dict = Canvas.group_list[i]
      item = item_dict['widgets'][0]
      s_item = item_dict['widgets'][1]
      group_name = item_dict['name']
      group_icon = item_dict['icon']
      break
  else:
    print "patchcanvas::joinGroup - Unable to find group to join"
    return

  for i in range(len(item.port_list)):
    port_id = item.port_list[i]['id']
    port_name = item.port_list[i]['name']
    port_mode = item.port_list[i]['mode']
    port_type = item.port_list[i]['type']

    ports_data.append((port_id, port_name, port_mode, port_type))
    ports_list_ids.append(port_id)

  for i in range(len(s_item.port_list)):
    port_id = s_item.port_list[i]['id']
    port_name = s_item.port_list[i]['name']
    port_mode = s_item.port_list[i]['mode']
    port_type = s_item.port_list[i]['type']

    ports_data.append((port_id, port_name, port_mode, port_type))
    ports_list_ids.append(port_id)

  for i in range(len(Canvas.connection_list)):
    if (Canvas.connection_list[i][1] in ports_list_ids or Canvas.connection_list[i][2] in ports_list_ids):
      conns_data.append(Canvas.connection_list[i])

  # Step 2 - Remove Item and Children
  for i in range(len(conns_data)):
    disconnectPorts(conns_data[i][0])

  for i in range(len(ports_data)):
    removePort(ports_data[i][0])

  removeGroup(group_id)

  # Step 3 - Re-create Item, now together
  addGroup(group_id, group_name, False, group_icon)

  for i in range(len(ports_data)):
    addPort(group_id, ports_data[i][0], ports_data[i][1], ports_data[i][2], ports_data[i][3])

  for i in range(len(conns_data)):
    connectPorts(conns_data[i][0], conns_data[i][1], conns_data[i][2])

  QTimer.singleShot(0, Canvas.scene.update)

def setGroupPos(group_id, group_pos_x, group_pos_y, group_pos_xs, group_pos_ys):
  if (Canvas.debug):
    print "patchcanvas::setGroupPos(%i, %f, %f, %f, %f)" % (group_id, group_pos_x, group_pos_y, group_pos_xs, group_pos_ys)

  for i in range(len(Canvas.group_list)):
    if (group_id == Canvas.group_list[i]['id']):
      Canvas.group_list[i]['widgets'][0].setPos(group_pos_x, group_pos_y)

      if (Canvas.group_list[i]['split']):
        Canvas.group_list[i]['widgets'][1].setPos(group_pos_xs, group_pos_ys)

      break

  else:
    print "patchcanvas::setGroupPos - Unable to find group to reposition"
    return

  QTimer.singleShot(0, Canvas.scene.update)

def setGroupIcon(group_id, icon):
  if (Canvas.debug):
    print "patchcanvas::setGroupIcon(%i, %i)" % (group_id, icon)

  for i in range(len(Canvas.group_list)):
    if (group_id == Canvas.group_list[i]['id']):
      Canvas.group_list[i]['widgets'][0].setIcon(icon)

      if (Canvas.group_list[i]['split']):
        Canvas.group_list[i]['widgets'][1].setIcon(icon)

      break

  else:
    print "patchcanvas::setGroupIcon - Unable to find group to change icon"
    return

  QTimer.singleShot(0, Canvas.scene.update)

def addPort(group_id, port_id, port_name, port_mode, port_type):
  if (Canvas.debug):
    print "patchcanvas::addPort(%i, %i, %s, %i, %i)" % (group_id, port_id, port_name, port_mode, port_type)

  for i in range(len(Canvas.group_list)):
    if (group_id == Canvas.group_list[i]['id']):

      if (Canvas.group_list[i]['split']):
        if (Canvas.group_list[i]['widgets'][0].splitted_mode == port_mode):
          widget = 0
        else:
          widget = 1
        new_port = Canvas.group_list[i]['widgets'][widget].addPort(port_id, port_name, port_mode, port_type)

      else:
        new_port = Canvas.group_list[i]['widgets'][0].addPort(port_id, port_name, port_mode, port_type)

      break

  else:
    print "patchcanvas::addPort - Unable to find parent group"
    return

  if (options['fancy_eyecandy']):
    ItemFX(new_port['widget'], True)

  Canvas.port_list.append(new_port)

  QTimer.singleShot(0, Canvas.scene.update)

def removePort(port_id):
  if (Canvas.debug):
    print "patchcanvas::removePort(%i)" % (port_id)

  for i in range(len(Canvas.port_list)):
    if (port_id == Canvas.port_list[i]['id']):
      item = Canvas.port_list[i]['widget']
      item.parentItem().removePort(port_id)
      if (options['fancy_eyecandy']):
        ItemFX(item, False, True)
      else:
        Canvas.scene.removeItem(item)
      Canvas.port_list.pop(i)
      break

  else:
    print "patchcanvas::removePort - Unable to find port to remove"
    return

  QTimer.singleShot(0, Canvas.scene.update)

def renamePort(port_id, new_port_name):
  if (Canvas.debug):
    print "patchcanvas::renamePort(%i, %s)" % (port_id, new_port_name)

  for i in range(len(Canvas.port_list)):
    if (port_id == Canvas.port_list[i]['id']):
      Canvas.port_list[i]['widget'].setText(new_port_name)
      Canvas.port_list[i]['widget'].parentItem().renamePort(port_id, new_port_name)
      break

  else:
    print "patchcanvas::renamePort - Unable to find port to rename"
    return

  QTimer.singleShot(0, Canvas.scene.update)

def connectPorts(connection_id, port_out_id, port_in_id):
  if (Canvas.debug):
    print "patchcanvas::connectPorts(%i, %i, %i)" % (connection_id, port_out_id, port_in_id)

  port_out = port_in = None
  for i in range(len(Canvas.port_list)):
    if (port_out_id == Canvas.port_list[i]['id']):
      port_out = Canvas.port_list[i]['widget']
      port_out_parent = port_out.parentItem()
    elif (port_in_id == Canvas.port_list[i]['id']):
      port_in = Canvas.port_list[i]['widget']
      port_in_parent = port_in.parentItem()

  if (not port_out or not port_in):
    print "patchcanvas::connectPorts - Unable to find ports to connect"
    return

  if (options['bezier_lines']):
    line = CanvasBezierLine(port_out, port_in, Canvas.scene)
  else:
    line = CanvasLine(port_out, port_in, Canvas.scene)

  port_out_parent.addLine(line, connection_id)
  port_in_parent.addLine(line, connection_id)

  Canvas.last_z_value+=1
  port_out_parent.setZValue(Canvas.last_z_value)
  port_in_parent.setZValue(Canvas.last_z_value)

  Canvas.last_z_value+=1
  line.setZValue(Canvas.last_z_value)

  if (options['fancy_eyecandy']):
    ItemFX(line, True)

  Canvas.connection_list.append([connection_id, port_out_id, port_in_id, line])

  QTimer.singleShot(0, Canvas.scene.update)

def disconnectPorts(connection_id):
  if (Canvas.debug):
    print "patchcanvas::disconnectPorts(%i)" % (connection_id)

  for i in range(len(Canvas.connection_list)):
    if (Canvas.connection_list[i][0] == connection_id):
      port_1_id = Canvas.connection_list[i][1]
      port_2_id = Canvas.connection_list[i][2]
      line = Canvas.connection_list[i][3]
      Canvas.connection_list.pop(i)
      break
  else:
    print "patchcanvas::disconnectPorts - Unable to find connection ports"
    return

  for i in range(len(Canvas.port_list)):
    if (Canvas.port_list[i]['id'] == port_1_id):
      item1 = Canvas.port_list[i]['widget']
      break
  else:
    print "patchcanvas::disconnectPorts - Unable to find output port"
    return

  for i in range(len(Canvas.port_list)):
    if (Canvas.port_list[i]['id'] == port_2_id):
      item2 = Canvas.port_list[i]['widget']
      break
  else:
    print "patchcanvas::disconnectPorts - Unable to find input port"
    return

  item1.parentItem().removeLine(connection_id)
  item2.parentItem().removeLine(connection_id)

  if (options['fancy_eyecandy']):
    ItemFX(line, False, True)
  else:
    Canvas.scene.removeItem(line)

  QTimer.singleShot(0, Canvas.scene.update)

def CanvasGetNewGroupPos(horizontal=False):
  if (Canvas.debug):
    print "patchcanvas::CanvasGetNewGroupPos(%i)" % (int(horizontal))

  new_pos = QPointF(Canvas.initial_pos.x(), Canvas.initial_pos.y())
  items = Canvas.scene.items()

  while (True):
    for i in range(len(items)):
      if (type(items[i]) == CanvasBox):
        #item_rect = QRectF(items[i].scenePos().x(), items[i].scenePos().y(), items[i].boundingRect().width(), items[i].boundingRect().height())
        #item_rect = items[i].sceneBoundingRect()
        if (items[i].sceneBoundingRect().contains(new_pos)):
          new_pos += QPointF(items[i].boundingRect().width()+15, 0) if horizontal else QPointF(0, items[i].boundingRect().height()+15)
          break
    else:
      break

  return new_pos

def CanvasPostponedGroups():
  if (Canvas.debug):
    print "patchcanvas::CanvasPostponedGroups()"

  for i in range(len(Canvas.postponed_groups)):
    group_id = Canvas.postponed_groups[i]

    for j in range(len(Canvas.group_list)):
      if (Canvas.group_list[j]['id'] == group_id):
        item = Canvas.group_list[j]['widgets'][0]

        if (Canvas.group_list[j]['split']):
          s_item = Canvas.group_list[j]['widgets'][1]
        else:
          s_item = None

        if (len(item.port_list) == 0 and (not s_item or len(s_item.port_list) == 0)):
          removeGroup(group_id)
          Canvas.postponed_groups.pop(i)

        break

  if (len(Canvas.postponed_groups) == 0):
    Canvas.postponed_timer.stop()

def Arrange():
  if (Canvas.debug):
    print "patchcanvas::Arrange()"

  # Make graph
  graph = []
  items = Canvas.scene.items()
  for i in range(len(items)):
    if (type(items[i]) == CanvasBox):
      ports = []
      conns = []
      port_flags = 0
      for j in range(len(items[i].port_list)):
        port_flags |= items[i].port_list[j]['mode']
        ports.append(items[i].port_list[j]['id'])
      for k in range(len(items[i].connection_lines)):
        conns.append(items[i].connection_lines[k][1])
      graph.append((items[i], ports, port_flags, conns))

  # Initial X values
  initial_x = Canvas.initial_pos.x()-(Canvas.initial_pos.x()*1/3)
  min_x_out = initial_x
  min_x_both = initial_x
  min_x_in = initial_x

  # Get width of boxes
  for i in range(len(graph)):
    if (graph[i][2] == PORT_MODE_INPUT+PORT_MODE_OUTPUT):
      item_width = graph[i][0].box_width
      if (item_width > min_x_in-initial_x):
        min_x_in = item_width+initial_x
    elif (graph[i][2] == PORT_MODE_OUTPUT):
      item_width = graph[i][0].box_width
      if (item_width > min_x_both-initial_x):
        min_x_both = item_width+initial_x

  # Make some space between ports
  if (min_x_both > initial_x):
    min_x_both += 100.0

  if (min_x_in > initial_x):
    min_x_in += 100.0

  min_x_in += min_x_both-initial_x

  # Initial Y values
  last_y_out = last_y_both = last_y_in = Canvas.initial_pos.y() #-(Canvas.initial_pos.y()*1/3)

  # List of groups that have already been repositioned
  items_ready = []

  # Move boxes with connections
  for i in range(len(graph)):
    item = graph[i][0]
    ports = graph[i][1]
    port_flags = graph[i][2]
    conns = graph[i][3]

    if (port_flags == PORT_MODE_INPUT+PORT_MODE_OUTPUT and conns):
      item.setPos(min_x_both, last_y_both)
      items_ready.append(item.group_id)
      last_y_both += item.box_height + 20
    elif (port_flags == PORT_MODE_INPUT and conns):
      item.setPos(min_x_in, last_y_in)
      items_ready.append(item.group_id)
      last_y_in += item.box_height + 20
    elif (port_flags == PORT_MODE_OUTPUT and conns):
      item.setPos(min_x_out, last_y_out)
      items_ready.append(item.group_id)
      last_y_out += item.box_height + 20

  # Move boxes without connections
  for i in range(len(graph)):
    item = graph[i][0]
    ports = graph[i][1]
    port_flags = graph[i][2]
    conns = graph[i][3]

    if (port_flags == PORT_MODE_INPUT+PORT_MODE_OUTPUT and not conns and not item.group_id in items_ready):
      item.setPos(min_x_both, last_y_both)
      items_ready.append(item.group_id)
      last_y_both += item.box_height + 20
    elif (port_flags == PORT_MODE_INPUT and not conns and not item.group_id in items_ready):
      item.setPos(min_x_in, last_y_in)
      items_ready.append(item.group_id)
      last_y_in += item.box_height + 20
    elif (port_flags == PORT_MODE_OUTPUT and not conns and not item.group_id in items_ready):
      item.setPos(min_x_out, last_y_out)
      items_ready.append(item.group_id)
      last_y_out += item.box_height + 20

  # Move empty boxes
  for i in range(len(graph)):
    item = graph[i][0]

    if (port_flags == PORT_MODE_INPUT+PORT_MODE_OUTPUT and not item.group_id in items_ready):
      item.setPos(min_x_both, last_y_both)
      items_ready.append(item.group_id)
      last_y_both += item.box_height + 20
    elif (port_flags == PORT_MODE_INPUT and not item.group_id in items_ready):
      item.setPos(min_x_in, last_y_in)
      items_ready.append(item.group_id)
      last_y_in += item.box_height + 20
    elif (port_flags == PORT_MODE_OUTPUT and not item.group_id in items_ready):
      item.setPos(min_x_out, last_y_out)
      items_ready.append(item.group_id)
      last_y_out += item.box_height + 20

  QTimer.singleShot(0, Canvas.scene.update)

def ItemFX(item, show, destroy=True):
  if (Canvas.debug):
    print "patchcanvas::ItemFX(%s, %i, %i)" % (type(item), int(show), int(destroy))

  global animations

  # Check if item already has an animationItemFX
  for i in range(len(animations)):
    if (animations[i][1] == item):
      animations[i][0].stop()
      animations.pop(i)
      break

  animation = CanvasFadeAnimation(item, show)
  animation.setDuration(750 if (show) else 500)
  animation.start()
  animations.append((animation, item))
  if (not show):
    if (destroy):
      QObject.connect(animation, SIGNAL("finished()"), lambda: RemoveItemFX(item))
    else:
      QObject.connect(animation, SIGNAL("finished()"), lambda vis=False: item.setVisible(False))

def RemoveItemFX(item):
  if (Canvas.debug):
    print "patchcanvas::RemoveItemFX(%s)" % (type(item))

  if (type(item) == CanvasBox):
    #for i in range(len(item.connection_lines)):
      #Canvas.scene.removeItem(item.connection_lines[i][0])
    #for i in range(len(item.port_list)):
      #Canvas.scene.removeItem(item.port_list[i]['widget'])
    Canvas.scene.removeItem(item.icon_svg)

  # TESTING - should be fixed by now
  #if (item.scene()): # May already be removed...
  Canvas.scene.removeItem(item)

# Custom GraphicsScene
class MyGraphicsScene(QGraphicsScene):
    def __init__(self, parent=None):
        super(MyGraphicsScene, self).__init__(parent)

        self.fake_selection = False
        self.fake_rubberband = self.addRect(QRectF(0, 0, 0, 0), Qt.green)
        self.fake_rubberband.hide()

        self.mouse_down = False
        self.ctrl_down = False

    def keyPressEvent(self, event):
        if (event.key() == Qt.Key_Control):
          self.ctrl_down = True
        return QGraphicsScene.keyPressEvent(self, event)

    def keyReleaseEvent(self, event):
        self.ctrl_down = False
        return QGraphicsScene.keyReleaseEvent(self, event)

    def wheelEvent(self, event):
        if (self.ctrl_down):
          factor = 1.41 ** (event.delta()/240.0)
          self.views()[0].scale(factor, factor)
          return event.accept()
        else:
          return QGraphicsScene.wheelEvent(self, event)

    def mousePressEvent(self, event):
        if (event.button() == Qt.LeftButton):
          self.mouse_down = True
        else:
          self.mouse_down = False

        return QGraphicsScene.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if (len(self.selectedItems()) > 0):
          return QGraphicsScene.mouseMoveEvent(self, event)
        elif (self.mouse_down):
          if (not self.fake_selection):
            self.origin = event.scenePos()
            self.fake_rubberband.show()
            self.fake_rubberband.setZValue(Canvas.last_z_value+1)
            self.fake_selection = True

          pos = event.scenePos()

          if (pos.x() > self.origin.x()): x = self.origin.x()
          else: x = pos.x()

          if (pos.y() > self.origin.y()): y = self.origin.y()
          else: y = pos.y()

          self.fake_rubberband.setRect(x, y, abs(pos.x()-self.origin.x()), abs(pos.y()-self.origin.y()))
          return event.accept()

    def mouseReleaseEvent(self, event):
        if (self.fake_selection):
          items = self.items()
          if (len(items) > 0):
            for i in range(len(items)):
              if (items[i].isVisible() and type(items[i]) == CanvasBox):
                item_rect = items[i].sceneBoundingRect()
                if ( self.fake_rubberband.contains(QPointF(item_rect.x(), item_rect.y())) and
                     self.fake_rubberband.contains(QPointF(item_rect.x()+item_rect.width(), item_rect.y()+item_rect.height())) ):
                  items[i].setSelected(True)

            self.fake_rubberband.hide()
            self.fake_rubberband.setRect(0, 0, 0, 0)
            self.fake_selection = False

        else:
          items = self.selectedItems()
          update = False
          ret_items = []
          if (len(items) > 0):
            for i in range(len(items)):
              if (items[i].isVisible() and type(items[i]) == CanvasBox):
                items[i].checkItemPos()
                ret_items.append((items[i].group_id, items[i].scenePos()))
                update = True

          if (update):
            self.emit(SIGNAL("sceneItemsMoved"), ret_items)

        self.mouse_down = False
        return QGraphicsScene.mouseReleaseEvent(self, event)

# Fade in/out animation
class CanvasFadeAnimation(QAbstractAnimation):
    def __init__(self, item, show, parent=None):
        super(CanvasFadeAnimation, self).__init__(parent)

        self.item = item
        self.show = show
        self._duration = 0

        self.item.show()

    def setDuration(self, time):
      self._duration = time

    def duration(self):
      return self._duration

    def totalDuration(self):
      return self._duration

    def updateCurrentTime(self, time):
      if (self.show):
        value = float(time)/self._duration
      else:
        value = 1.0-(float(time)/self._duration)
      self.item.setOpacity(value)

    def updateDirection(self, direction):
      pass

    def updateState(self, oldState, newState):
      pass

# Animate Port Connection (New Line) - FIXME: not working since line gradients have been implemented
class CanvasPortConnectAnimation(QAbstractAnimation):
    def __init__(self, line, parent=None):
        super(CanvasPortConnectAnimation, self).__init__(parent)

        self.line = line
        self._duration = 0
        self.setDirection(QAbstractAnimation.Backward)

        self.mode = 1 if (Canvas.theme['bg_color'].black() < 100) else 0

        colors = self.line.pen().color()
        self.red = colors.red()
        self.green = colors.green()
        self.blue = colors.blue()
        self.width = self.line.pen().width()

    def maxRGB(self, value):
      if (value > 255):
        return 255
      elif (value < 0):
        return 0
      else:
        return value

    def setDuration(self, time):
      self._duration = time

    def duration(self):
      return self._duration

    def totalDuration(self):
      return self._duration

    def updateCurrentTime(self, time):
      value = 255*time/self._duration
      if (self.mode == 0):
        color = QColor(self.maxRGB(self.red+value), self.maxRGB(self.green+value), self.maxRGB(self.blue+value))
      else:
        color = QColor(self.maxRGB(self.red-value), self.maxRGB(self.green-value), self.maxRGB(self.blue-value))
      self.line.setPen(QPen(color, self.width))

      value = 1-(float(time)/self._duration)
      self.line.setOpacity(value)

    def updateDirection(self, direction):
      pass

    def updateState(self, oldState, newState):
      pass

# Animate Glow of Port's Lines
class CanvasLineAnimation(QAbstractAnimation):
    def __init__(self, line, port_type, parent=None):
        super(CanvasLineAnimation, self).__init__(parent)

        self.line = line
        self.port_type = port_type
        self._duration = 0

        self.setLoopCount(-1)
        self.direction_just_changed = False
        self.n = 0

    def setDuration(self, time):
      self._duration = time

    def duration(self):
      return self._duration

    def totalDuration(self):
      return self._duration

    def updateCurrentTime(self, time):
      # FIXME - need a better way to check this...
      if (time > 475 and self.direction() == QAbstractAnimation.Forward and not self.direction_just_changed):
        self.setDirection(QAbstractAnimation.Backward)
        self.direction_just_changed = True
      elif (time < 25 and self.direction() == QAbstractAnimation.Backward and not self.direction_just_changed):
        self.setDirection(QAbstractAnimation.Forward)
        self.direction_just_changed = True
      else:
        self.direction_just_changed = False

      # CPU Saver:
      if (self.n != 5):
        self.n += 1
        return
      else:
        self.n = 0

      # FIXME - use 'line_***_glow'
      color = 255*time/self._duration
      if (self.port_type == PORT_TYPE_AUDIO):
        self.line.setColor(QColor(color, color, 255))
      elif (self.port_type == PORT_TYPE_MIDI):
        self.line.setColor(QColor(255, color, color))
      elif (self.port_type == PORT_TYPE_OUTRO):
        self.line.setColor(QColor(color, 255, color))

    def updateDirection(self, direction):
      pass

    def updateState(self, oldState, newState):
      pass

# Simple Line
class CanvasLine(QGraphicsLineItem):
    def __init__(self, item1, item2, parent_scene=None):
        super(CanvasLine, self).__init__(None, parent_scene)

        if (options['fancy_eyecandy']):
          self.setOpacity(0.0)

        self.port_type1 = item1.port_type
        self.port_type2 = item2.port_type
        self.item1 = item1
        self.item2 = item2
        self.updateLinePos()

        self.animation = None
        self.glow = None
        self.locked = False

    def enableGlow(self, yesno):
        if self.locked: return
        if (options['fancy_eyecandy']):
          if (yesno):
            self.glow = CanvasPortGlow()
            self.glow.setPortType(self.port_type)
            self.animation = CanvasLineAnimation(self, self.port_type) #self, self.port_type
            self.animation.setDuration(500)
            self.animation.start()
          else:
            self.glow = None
            if (self.animation):
              self.animation.stop()
              self.animation = None
          self.setGraphicsEffect(self.glow)
        else:
          self.updateLineGradient(yesno)

    def setPortType(self, port_type1, port_type2):
        self.port_type1 = port_type1
        self.port_type2 = port_type2
        self.updateLineGradient()

    def updateLinePos(self):
        if (self.item1.port_mode == PORT_MODE_OUTPUT):
          line = QLineF(self.item1.scenePos().x()+self.item1.width_+12, self.item1.scenePos().y()+7.5, self.item2.scenePos().x(), self.item2.scenePos().y()+7.5)
          self.setLine(line)
          self.updateLineGradient()

    def updateLineGradient(self, selected=False):
        pos_top = self.boundingRect().top()
        pos_bot = self.boundingRect().bottom()
        if (self.item2.scenePos().y() >= self.item1.scenePos().y()):
          pos1 = 0
          pos2 = 1
        else:
          pos1 = 1
          pos2 = 0

        port_gradient = QLinearGradient(0, pos_top, 0, pos_bot)

        if (self.port_type1 == PORT_TYPE_AUDIO):
          port_gradient.setColorAt(pos1, Canvas.theme['line_audio_sel'] if (selected) else Canvas.theme['line_audio'])
        elif (self.port_type1 == PORT_TYPE_MIDI):
          port_gradient.setColorAt(pos1, Canvas.theme['line_midi_sel'] if (selected) else Canvas.theme['line_midi'])
        elif (self.port_type1 == PORT_TYPE_OUTRO):
          port_gradient.setColorAt(pos1, Canvas.theme['line_outro_sel'] if (selected) else Canvas.theme['line_outro'])
        else:
          print "Error: Invalid Port1 Type!"
          return

        if (self.port_type2 == PORT_TYPE_AUDIO):
          port_gradient.setColorAt(pos2, Canvas.theme['line_audio_sel'] if (selected) else Canvas.theme['line_audio'])
        elif (self.port_type2 == PORT_TYPE_MIDI):
          port_gradient.setColorAt(pos2, Canvas.theme['line_midi_sel'] if (selected) else Canvas.theme['line_midi'])
        elif (self.port_type2 == PORT_TYPE_OUTRO):
          port_gradient.setColorAt(pos2, Canvas.theme['line_outro_sel'] if (selected) else Canvas.theme['line_outro'])
        else:
          print "Error: Invalid Port2 Type!"
          return

        self.setPen(QPen(port_gradient, 2))

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, bool(options['antialiasing']))
        return QGraphicsLineItem.paint(self, painter, option, widget)

# Canvas Line, Bezier style
class CanvasBezierLine(QGraphicsPathItem):
    def __init__(self, item1, item2, parent_scene=None):
        super(CanvasBezierLine, self).__init__(None, parent_scene)

        if (options['fancy_eyecandy']):
          self.setOpacity(0.0)

        self.port_type1 = item1.port_type
        self.port_type2 = item2.port_type
        self.item1 = item1
        self.item2 = item2
        self.updateLinePos()

        self.animation = None
        self.glow = None
        self.locked = False

        self.setBrush(QColor(0,0,0,0))

    def enableGlow(self, yesno):
        if self.locked: return
        if (options['fancy_eyecandy']):
          if (yesno):
            self.glow = CanvasPortGlow()
            self.glow.setPortType(self.port_type1)
            self.animation = CanvasLineAnimation(self, self.port_type1) #self.glow, self.port_type1
            self.animation.setDuration(500)
            self.animation.start()
          else:
            self.glow = None
            if (self.animation):
              self.animation.stop()
              self.animation = None
          self.setGraphicsEffect(self.glow)
        else:
          self.updateLineGradient(yesno)

    def setPortType(self, port_type1, port_type2):
        self.port_type1 = port_type1
        self.port_type2 = port_type2
        self.updateLineGradient()

    def updateLinePos(self):
        if (self.item1.port_mode == PORT_MODE_OUTPUT):
          item1_x = self.item1.scenePos().x()+self.item1.width_+12
          item1_y = self.item1.scenePos().y()+7.5

          item2_x = self.item2.scenePos().x()
          item2_y = self.item2.scenePos().y()+7.5

          item1_mid_x = abs(item1_x-item2_x)/2
          item1_new_x = item1_x+item1_mid_x

          item2_mid_x = abs(item1_x-item2_x)/2
          item2_new_x = item2_x-item2_mid_x

          path = QPainterPath(QPointF(item1_x, item1_y))
          path.cubicTo(item1_new_x, item1_y, item2_new_x, item2_y, item2_x, item2_y)
          self.setPath(path)
          self.updateLineGradient()

    def updateLineGradient(self, selected=False):
        pos_top = self.boundingRect().top()
        pos_bot = self.boundingRect().bottom()
        if (self.item2.scenePos().y() >= self.item1.scenePos().y()):
          pos1 = 0
          pos2 = 1
        else:
          pos1 = 1
          pos2 = 0

        port_gradient = QLinearGradient(0, pos_top, 0, pos_bot)

        if (self.port_type1 == PORT_TYPE_AUDIO):
          port_gradient.setColorAt(pos1, Canvas.theme['line_audio_sel'] if (selected) else Canvas.theme['line_audio'])
        elif (self.port_type1 == PORT_TYPE_MIDI):
          port_gradient.setColorAt(pos1, Canvas.theme['line_midi_sel'] if (selected) else Canvas.theme['line_midi'])
        elif (self.port_type1 == PORT_TYPE_OUTRO):
          port_gradient.setColorAt(pos1, Canvas.theme['line_outro_sel'] if (selected) else Canvas.theme['line_outro'])
        else:
          print "Error: Invalid Port1 Type!"
          return

        if (self.port_type2 == PORT_TYPE_AUDIO):
          port_gradient.setColorAt(pos2, Canvas.theme['line_audio_sel'] if (selected) else Canvas.theme['line_audio'])
        elif (self.port_type2 == PORT_TYPE_MIDI):
          port_gradient.setColorAt(pos2, Canvas.theme['line_midi_sel'] if (selected) else Canvas.theme['line_midi'])
        elif (self.port_type2 == PORT_TYPE_OUTRO):
          port_gradient.setColorAt(pos2, Canvas.theme['line_outro_sel'] if (selected) else Canvas.theme['line_outro'])
        else:
          print "Error: Invalid Port2 Type!"
          return

        self.setPen(QPen(port_gradient, 2))

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, bool(options['antialiasing']))
        return QGraphicsPathItem.paint(self, painter, option, widget)

# Fake Line, make QTimer happy
class FakeLineMov(object):
    def __init__(self):
      self.is_line = False

    def updateLinePos(self, scenePos):
        pass

# Canvas Line, moving cursor
class CanvasLineMov(QGraphicsLineItem):
    def __init__(self, parent=None):
        super(CanvasLineMov, self).__init__(parent)

        self.port_mode = None
        self.port_type = None
        self.item = None
        self.is_line = True

        # Port position doesn't change while moving around line
        self.item_width = self.parentItem().width_
        self.line_x = self.scenePos().x()
        self.line_y = self.scenePos().y()

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, bool(options['antialiasing']))
        QGraphicsLineItem.paint(self, painter, option, widget)

    def updateLinePos(self, scenePos):
        if (self.port_mode == PORT_MODE_INPUT):
          item_pos = (0, 7.5)
        elif (self.port_mode == PORT_MODE_OUTPUT):
          item_pos = (self.item_width+12, 7.5)
        else:
          return

        line = QLineF(item_pos[0], item_pos[1], scenePos.x()-self.line_x, scenePos.y()-self.line_y)
        self.setLine(line)

    def setPortMode(self, port_mode):
        self.port_mode = port_mode

    def setPortType(self, port_type):
        self.port_type = port_type

        if (self.port_type == PORT_TYPE_AUDIO):
          pen = QPen(Canvas.theme['line_audio'], 2)
        elif (self.port_type == PORT_TYPE_MIDI):
          pen = QPen(Canvas.theme['line_midi'], 2)
        elif (self.port_type == PORT_TYPE_OUTRO):
          pen = QPen(Canvas.theme['line_outro'], 2)
        else:
          print "Error: Invalid Port Type!"
          return

        self.setPen(pen)

# Canvas Line, Bezier style, moving cursor
class CanvasBezierLineMov(QGraphicsPathItem):
    def __init__(self, parent=None):
        super(CanvasBezierLineMov, self).__init__(parent)

        self.port_mode = None
        self.port_type = None
        self.item = None
        self.is_line = True

        # Port position doesn't change while moving around line
        self.item_x = self.parentItem().scenePos().x()
        self.item_y = self.parentItem().scenePos().y()
        self.item_width = self.parentItem().width_

    def paint(self, painter, option, widget):
        painter.setRenderHint(QPainter.Antialiasing, bool(options['antialiasing']))
        QGraphicsPathItem.paint(self, painter, option, widget)

    def updateLinePos(self, scenePos):
        if (self.port_mode == PORT_MODE_INPUT):
          old_x = 0
          old_y = 7.5
          mid_x = abs(scenePos.x()-self.item_x)/2
          new_x = old_x-mid_x
        elif (self.port_mode == PORT_MODE_OUTPUT):
          old_x = self.item_width+12
          old_y = 7.5
          mid_x = abs(scenePos.x()-(self.item_x+old_x))/2
          new_x = old_x+mid_x
        else:
          return

        #mid_fx = abs(old_x-scenePos.x())/2
        #new_fx = scenePos.x()-mid_x
        final_x = scenePos.x()-self.item_x
        final_y = scenePos.y()-self.item_y

        path = QPainterPath(QPointF(old_x, old_y))
        path.cubicTo(new_x, old_y, new_x, final_y, final_x, final_y)
        self.setPath(path)

    def setPortMode(self, port_mode):
        self.port_mode = port_mode

    def setPortType(self, port_type):
        self.port_type = port_type

        if (self.port_type == PORT_TYPE_AUDIO):
          pen = QPen(Canvas.theme['line_audio'], 2)
        elif (self.port_type == PORT_TYPE_MIDI):
          pen = QPen(Canvas.theme['line_midi'], 2)
        elif (self.port_type == PORT_TYPE_OUTRO):
          pen = QPen(Canvas.theme['line_outro'], 2)
        else:
          print "Error: Invalid Port Type!"
          return

        color = QColor(0,0,0)
        color.setAlpha(0)
        self.setBrush(color)
        self.setPen(pen)

# Input/Output Port
class CanvasPort(QGraphicsItem):
    def __init__(self, port_id, port_name, port_mode, port_type, parent=None):
        super(CanvasPort, self).__init__(parent)

        # Save Variables, useful for later
        self.port_id = port_id
        self.port_mode = port_mode
        self.port_type = port_type
        self.text = port_name

        # Base Variables
        self.font_port = QFont(Canvas.theme['port_font_name'], Canvas.theme['port_font_size'], Canvas.theme['port_font_state'])
        self.height_ = 15
        self.width_  = 15

        self.mov_line = FakeLineMov()
        self.hover_item = None
        self.last_selected_state = False

        self.mouse_down = False
        self.moving_cursor = False

        self.setFlags(QGraphicsItem.ItemIsSelectable)

    def setPortMode(self, port_mode):
        self.port_mode = port_mode

    def setPortType(self, port_type):
        self.port_type = port_type
        self.update()

    def setText(self, text):
        self.text = text
        self.update()

        # Fix garbage when name size decreases
        #QTimer.singleShot(0, Canvas.scene.update) # FIXME - REALLY?

    def setWidth(self, width):
        self.width_ = width

    def mousePressEvent(self, event):
        if (event.button() == Qt.LeftButton):
          self.mouse_down = True
        else:
          self.mouse_down = False

        self.moving_cursor = False

        return QGraphicsItem.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if (self.mouse_down):

          if (not self.moving_cursor):
            self.setCursor(QCursor(Qt.CrossCursor))
            self.moving_cursor = True

            for i in range(len(Canvas.connection_list)):
              if (Canvas.connection_list[i][1] == self.port_id or Canvas.connection_list[i][2] == self.port_id):
                Canvas.connection_list[i][3].locked = True

          if (not self.mov_line.is_line):
            if (options['bezier_lines']):
              self.mov_line = CanvasBezierLineMov(self)
            else:
              self.mov_line = CanvasLineMov(self)

            #self.mov_line.setBasedItem(self)
            self.mov_line.setPortMode(self.port_mode)
            self.mov_line.setPortType(self.port_type)

            self.mov_line.setZValue(Canvas.last_z_value)
            self.parentItem().setZValue(Canvas.last_z_value+1)
            Canvas.last_z_value += 2

          item = None
          items = Canvas.scene.items(event.scenePos(), Qt.ContainsItemShape, Qt.AscendingOrder)
          for i in range(len(items)):
            if (type(items[i]) == CanvasPort):
              if (items[i] != self):
                if not item:
                  item = items[i]
                elif (items[i].parentItem().zValue() > item.parentItem().zValue()):
                  item = items[i]

          if (self.hover_item and self.hover_item != item and self.hover_item):
            self.hover_item.setSelected(False)

          if (item):
            if ( item.port_mode != self.port_mode and
                ( item.port_type == self.port_type or (options['connect_midi2outro'] and (
                                                (item.port_type == PORT_TYPE_MIDI and self.port_type == PORT_TYPE_OUTRO) or
                                                (item.port_type == PORT_TYPE_OUTRO and self.port_type == PORT_TYPE_MIDI)
                                                ))
                ) ):
              item.setSelected(True)
              self.hover_item = item

            else:
              self.hover_item = None
          else:
            self.hover_item = None

          self.mov_line.updateLinePos(event.scenePos())
          #QTimer.singleShot(0, lambda scenePos=event.scenePos(): self.mov_line.updateLinePos(scenePos))

        return QGraphicsItem.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        if (self.mouse_down):

          if (self.mov_line.is_line):
            Canvas.scene.removeItem(self.mov_line)
            self.mov_line = FakeLineMov()

          for i in range(len(Canvas.connection_list)):
            if (Canvas.connection_list[i][1] == self.port_id or Canvas.connection_list[i][2] == self.port_id):
              Canvas.connection_list[i][3].locked = False

          if (self.hover_item):
            for i in range(len(Canvas.connection_list)):
              if ( (Canvas.connection_list[i][1] == self.port_id and Canvas.connection_list[i][2] == self.hover_item.port_id) or
                  (Canvas.connection_list[i][1] == self.hover_item.port_id and Canvas.connection_list[i][2] == self.port_id) ):
                Canvas.callback(ACTION_PORTS_DISCONNECT, Canvas.connection_list[i][0])
                break
            else:
              source_port_name = self.parentItem().text+":"+self.text
              target_port_name = self.hover_item.parentItem().text+":"+self.hover_item.text
              if (self.port_mode == PORT_MODE_OUTPUT):
                Canvas.callback(ACTION_PORTS_CONNECT, self.port_id, source_port_name, self.hover_item.port_id, target_port_name)
              else:
                Canvas.callback(ACTION_PORTS_CONNECT, self.hover_item.port_id, target_port_name, self.port_id, source_port_name)

            Canvas.scene.clearSelection()

        if (self.moving_cursor):
          self.setCursor(QCursor(Qt.ArrowCursor))

        self.mouse_down = False
        self.moving_cursor = False

        return QGraphicsItem.mouseReleaseEvent(self, event)

    def contextMenuEvent(self, event):
        #TEST
        Canvas.scene.clearSelection()
        self.setSelected(True)

        menu = QMenu()

        port_list = Canvas.callback(ACTION_REQUEST_PORT_CONNECTION_LIST, self.port_id, self.parentItem().text+":"+self.text)
        discMenu = QMenu("Disconnect", menu)
        if (len(port_list) > 0):
          for i in range(len(port_list)):
            act_x_disc = discMenu.addAction(port_list[i][1])
            QObject.connect(act_x_disc, SIGNAL("triggered()"), lambda port_id=port_list[i][0]: self.contextMenuDisconnect(port_id))
        else:
          act_x_disc = discMenu.addAction("No connections")
          act_x_disc.setEnabled(False)

        menu.addMenu(discMenu)
        act_x_disc_all = menu.addAction("Disconnect &All")
        act_x_sep_1 = menu.addSeparator()
        act_x_info = menu.addAction("Get &Info")
        act_x_rename = menu.addAction("&Rename")

        if (not features['port_rename']):
          act_x_rename.setVisible(False)

        act_selected = menu.exec_(event.screenPos())

        if (act_selected == act_x_info):
          Canvas.callback(ACTION_PORT_INFO, self.port_id)
        elif (act_selected == act_x_rename):
          new_name = QInputDialog.getText(None, "Rename Port", "New name:", text=self.text)
          if (new_name[1] and not new_name[0].isEmpty()):
            if (Canvas.callback):
              Canvas.callback(ACTION_PORT_RENAME, self.port_id, new_name[0])
        elif (act_selected == act_x_disc_all):
          Canvas.callback(ACTION_PORT_DISCONNECT_ALL, self.port_id, self.parentItem().text+":"+self.text)

        return event.accept()

    def contextMenuDisconnect(self, port_idx):
        for i in range(len(Canvas.connection_list)):
          if ( (Canvas.connection_list[i][1] == self.port_id and Canvas.connection_list[i][2] == port_idx) or
               (Canvas.connection_list[i][2] == self.port_id and Canvas.connection_list[i][1] == port_idx) ):
            Canvas.callback(ACTION_PORTS_DISCONNECT, Canvas.connection_list[i][0])
            break

    def boundingRect(self):
        return QRectF(0, 0, self.width_+12, self.height_)

    def paint(self, painter, option, widget):
        painter.setRenderHints(QPainter.Antialiasing, (options['antialiasing'] == 2))

        if (self.port_mode == PORT_MODE_INPUT):
          text_pos = QPointF(3, 12)
          if (Canvas.theme['port_mode'] == THEME_PORT_POLYGON):
            poly_locx = [0, self.width_+5, self.width_+12, self.width_+5, 0]
          elif (Canvas.theme['port_mode'] == THEME_PORT_SQUARE):
            poly_locx = [0, self.width_+5, self.width_+5, self.width_+5, 0]
          else:
            print "Invalid THEME_PORT mode"
            return
        elif (self.port_mode == PORT_MODE_OUTPUT):
          text_pos = QPointF(9, 12)
          if (Canvas.theme['port_mode'] == THEME_PORT_POLYGON):
            poly_locx = [self.width_+12, 7, 0, 7, self.width_+12]
          elif (Canvas.theme['port_mode'] == THEME_PORT_SQUARE):
            poly_locx = [self.width_+12, 5, 5, 5, self.width_+12]
          else:
            print "Invalid THEME_PORT mode"
            return
        else:
          print "Error: Invalid Port Mode!"
          return

        if (self.port_type == PORT_TYPE_AUDIO):
          poly_color = Canvas.theme['port_audio_bg_sel'] if (self.isSelected()) else Canvas.theme['port_audio_bg']
          poly_pen = Canvas.theme['port_audio_pen_sel'] if (self.isSelected()) else Canvas.theme['port_audio_pen']
        elif (self.port_type == PORT_TYPE_MIDI):
          poly_color = Canvas.theme['port_midi_bg_sel'] if (self.isSelected()) else Canvas.theme['port_midi_bg']
          poly_pen = Canvas.theme['port_midi_pen_sel'] if (self.isSelected()) else Canvas.theme['port_midi_pen']
        elif (self.port_type == PORT_TYPE_OUTRO):
          poly_color = Canvas.theme['port_outro_bg_sel'] if (self.isSelected()) else Canvas.theme['port_outro_bg']
          poly_pen = Canvas.theme['port_outro_pen_sel'] if (self.isSelected()) else Canvas.theme['port_outro_pen']
        else:
          print "Error: Invalid Port Type!"
          return

        polygon = QPolygonF()
        polygon += QPointF(poly_locx[0], 0)
        polygon += QPointF(poly_locx[1], 0)
        polygon += QPointF(poly_locx[2], 7.5)
        polygon += QPointF(poly_locx[3], 15)
        polygon += QPointF(poly_locx[4], 15)

        painter.setBrush(poly_color)
        painter.setPen(poly_pen)
        painter.drawPolygon(polygon)

        painter.setPen(Canvas.theme['port_text'])
        painter.setFont(self.font_port)
        painter.drawText(text_pos, self.text)

        if (self.isSelected() != self.last_selected_state):
          self.parentItem().makeItGlow(self.port_id, self.isSelected())

        self.last_selected_state = self.isSelected()

# Rectangle Box (which contains the icon, name and ports)
class CanvasBox(QGraphicsItem):
    def __init__(self, group_id, text, icon=ICON_APPLICATION, parent=None):
        super(CanvasBox, self).__init__(None, parent)

        # Save Variables, useful for later
        self.group_id = group_id
        self.text = text
        #self.icon = None

        # Base Variables
        self.box_width  = 50
        self.box_height = 25
        self.port_list = []
        self.port_list_ids = []
        self.connection_lines = []

        self.last_pos = None
        self.splitted = False
        self.splitted_mode = None
        self.forced_split = False
        self.repositioned = False
        self.moving_cursor = False

        # Set Font
        self.font_name = QFont(Canvas.theme['box_font_name'], Canvas.theme['box_font_size'], Canvas.theme['box_font_state'])
        self.font_port = QFont(Canvas.theme['port_font_name'], Canvas.theme['port_font_size'], Canvas.theme['port_font_state'])

        # Icon
        #self.setIcon(icon)
        self.icon_svg = CanvasIcon(icon, text, self)

        # Shadow
        if (options['fancy_eyecandy']):
          self.shadow = CanvasBoxShadow(self.toGraphicsObject())
          self.shadow.setFakeParent(self)
          self.setGraphicsEffect(self.shadow)

        # Final touches
        self.setFlags(QGraphicsItem.ItemIsMovable|QGraphicsItem.ItemIsSelectable)

        # Initial Paint
        if (options['auto_hide_groups'] or options['fancy_eyecandy']):
          self.setVisible(False) # Wait for at least 1 port

        self.relocateAll()

    def setIcon(self, icon):
        self.icon_svg.setIcon(icon, self.text)
        #size = 16
        #text = self.text.lower()
        #if (icon == ICON_APPLICATION):
          #if (text == "jucejack"):
            #icon_text = "juced"
          #elif (text == "tritium"):
            #icon_text = "composite-icon"
          #else:
            #icon_text = self.text.lower()
          #self.icon = QIcon(Canvas.icons.getIconPath(icon_text, size)).pixmap(16, 16)
          #if not self.icon:
            #self.icon = QIcon(Canvas.icons.getIconPath("application-x-executable", size)).pixmap(16, 16)
        #elif (icon == ICON_HARDWARE):
          #self.icon = QIcon(Canvas.icons.getIconPath("audio-card", size)).pixmap(16, 16)
        #else:
          #print "Invalid icon requested"
          #self.icon = ""
        #self.update()

    def setSplit(self, split, mode=None):
        self.splitted = split
        self.splitted_mode = mode

    def setText(self, text):
        self.text = text
        self.relocateAll()

    def makeItGlow(self, port_id, glow):
        lines_to_glow = []
        for i in range(len(Canvas.connection_list)):
          if (Canvas.connection_list[i][1] == port_id or Canvas.connection_list[i][2] == port_id):
            Canvas.connection_list[i][3].enableGlow(glow)

    def addLine(self, line, connection_id):
        self.connection_lines.append([line, connection_id])

    def removeLine(self, connection_id):
        for i in range(len(self.connection_lines)):
          if (self.connection_lines[i][1] == connection_id):
            self.connection_lines.pop(i)
            return True
        else:
          print "patchcanvas::CanvasBox.removeLine - Unable to find line to remove"
          return False

    def addPort(self, port_id, port_name, port_mode, port_type):
        if (len(self.port_list) == 0):
          if (options['fancy_eyecandy']):
            #self.setOpacity(0.0)
            #self.setVisible(True)
            ItemFX(self, True)
          if (options['auto_hide_groups']):
            self.setVisible(True)

        new_port  = CanvasPort(port_id, port_name, port_mode, port_type, self)

        port_dict = {
          'id': port_id,
          'name': port_name,
          'mode': port_mode,
          'type': port_type,
          'widget': new_port,
        }

        self.port_list.append(port_dict)
        self.port_list_ids.append(port_id)
        self.relocateAll()

        return port_dict

    def removePort(self, port_id):
        for i in range(len(self.port_list)):
          if (port_id == self.port_list[i]['id']):
            self.port_list.pop(i)
            self.port_list_ids.pop(i)
            break
        else:
          print "patchcanvas::CanvasBox.removePort - Unable to find port to remove"

        self.relocateAll()

        if (len(self.port_list) == 0 and self.isVisible()):
          if (Canvas.debug):
            print "patchcanvas::CanvasBox.removePort - This group has no more ports, hide it"
          if (options['fancy_eyecandy']):
            ItemFX(self, False, False)
          elif (options['auto_hide_groups']):
            self.setVisible(False)

    def renamePort(self, port_id, new_port_name):
        for i in range(len(self.port_list)):
          if (port_id == self.port_list[i]['id']):
            new_port_dict = {
              'id': self.port_list[i]['id'],
              'name': new_port_name,
              'mode': self.port_list[i]['mode'],
              'type': self.port_list[i]['type'],
              'widget': self.port_list[i]['widget'],
            }
            self.port_list[i] = new_port_dict
            break
        else:
          print "patchcanvas::CanvasBox.renamePort - Unable to find port to rename"

        self.relocateAll()

    def relocateAll(self):
        self.prepareGeometryChange()

        max_in_width  = 0
        max_in_height = 24
        max_out_width = 0
        max_out_height = 24
        have_audio_in = have_audio_out = have_midi_in = have_midi_out = have_outro_in =  have_outro_out = False

        # reset box size
        self.box_width  = 50
        self.box_height = 25

        # Check Text Name size
        app_name_size = QFontMetrics(self.font_name).width(self.text)+30
        if (app_name_size > self.box_width):
          self.box_width = app_name_size

        # Get Max Box Width/Height
        for i in range(len(self.port_list)):
          if (self.port_list[i]['mode'] == PORT_MODE_INPUT):
            max_in_height += 18

            size = QFontMetrics(self.font_port).width(self.port_list[i]['name'])
            if (size > max_in_width):
              max_in_width = size

            if (self.port_list[i]['type'] == PORT_TYPE_AUDIO and not have_audio_in):
              have_audio_in = True
              max_in_height += 2
            elif (self.port_list[i]['type'] == PORT_TYPE_MIDI and not have_midi_in):
              have_midi_in = True
              max_in_height += 2
            elif (self.port_list[i]['type'] == PORT_TYPE_OUTRO and not have_outro_in):
              have_outro_in = True
              max_in_height += 2

          elif (self.port_list[i]['mode'] == PORT_MODE_OUTPUT):
            max_out_height += 18

            size = QFontMetrics(self.font_port).width(self.port_list[i]['name'])
            if (size > max_out_width):
              max_out_width = size

            if (self.port_list[i]['type'] == PORT_TYPE_AUDIO and not have_audio_out):
              have_audio_out = True
              max_out_height += 2
            elif (self.port_list[i]['type'] == PORT_TYPE_MIDI and not have_midi_out):
              have_midi_out = True
              max_out_height += 2
            elif (self.port_list[i]['type'] == PORT_TYPE_OUTRO and not have_outro_out):
              have_outro_out = True
              max_out_height += 2

        final_width = 30 + max_in_width + max_out_width
        if (final_width > self.box_width):
          self.box_width = final_width

        if (max_in_height > self.box_height):
          self.box_height = max_in_height

        if (max_out_height > self.box_height):
          self.box_height = max_out_height

        self.box_height -= 2

        last_in_pos  = 24
        last_out_pos = 24
        last_in_type  = None
        last_out_type = None

        # Re-position ports, AUDIO
        for i in range(len(self.port_list)):
          if (self.port_list[i]['mode'] == PORT_MODE_INPUT and self.port_list[i]['type'] == PORT_TYPE_AUDIO):

            self.port_list[i]['widget'].setPos(QPointF(1, last_in_pos))
            self.port_list[i]['widget'].setWidth(max_in_width)

            last_in_pos += 18
            last_in_type = self.port_list[i]['type']

          elif (self.port_list[i]['mode'] == PORT_MODE_OUTPUT and self.port_list[i]['type'] == PORT_TYPE_AUDIO):

            self.port_list[i]['widget'].setPos(QPointF(self.box_width-max_out_width-13, last_out_pos))
            self.port_list[i]['widget'].setWidth(max_out_width)

            last_out_pos += 18
            last_out_type = self.port_list[i]['type']

        # Re-position ports, MIDI
        for i in range(len(self.port_list)):
          if (self.port_list[i]['mode'] == PORT_MODE_INPUT and self.port_list[i]['type'] == PORT_TYPE_MIDI):

            if (last_in_type != None and self.port_list[i]['type'] != last_in_type):
              last_in_pos += 2

            self.port_list[i]['widget'].setPos(QPointF(1, last_in_pos))
            self.port_list[i]['widget'].setWidth(max_in_width)

            last_in_pos += 18
            last_in_type = self.port_list[i]['type']

          elif (self.port_list[i]['mode'] == PORT_MODE_OUTPUT and self.port_list[i]['type'] == PORT_TYPE_MIDI):

            if (last_out_type != None and self.port_list[i]['type'] != last_out_type):
              last_out_pos += 2

            self.port_list[i]['widget'].setPos(QPointF(self.box_width-max_out_width-13, last_out_pos))
            self.port_list[i]['widget'].setWidth(max_out_width)

            last_out_pos += 18
            last_out_type = self.port_list[i]['type']

        # Re-position ports, Outro
        for i in range(len(self.port_list)):
          if (self.port_list[i]['mode'] == PORT_MODE_INPUT and self.port_list[i]['type'] == PORT_TYPE_OUTRO):

            if (last_in_type != None and self.port_list[i]['type'] != last_in_type):
              last_in_pos += 2

            self.port_list[i]['widget'].setPos(QPointF(1, last_in_pos))
            self.port_list[i]['widget'].setWidth(max_in_width)

            last_in_pos += 18
            last_in_type = self.port_list[i]['type']

          elif (self.port_list[i]['mode'] == PORT_MODE_OUTPUT and self.port_list[i]['type'] == PORT_TYPE_OUTRO):

            if (last_out_type != None and self.port_list[i]['type'] != last_out_type):
              last_out_pos += 2

            self.port_list[i]['widget'].setPos(QPointF(self.box_width-max_out_width-13, last_out_pos))
            self.port_list[i]['widget'].setWidth(max_out_width)

            last_out_pos += 18
            last_out_type = self.port_list[i]['type']

        self.update()

    def resetLinesZValue(self):
        for i in range(len(Canvas.connection_list)):
          if (Canvas.connection_list[i][1] in self.port_list_ids and Canvas.connection_list[i][2] in self.port_list_ids):
            z_value = Canvas.last_z_value
          else:
            z_value = Canvas.last_z_value-1

          Canvas.connection_list[i][3].setZValue(z_value)

    def repaintLines(self):
        if (self.pos() != self.last_pos):
          for i in range(len(self.connection_lines)):
            self.connection_lines[i][0].updateLinePos()
            #QTimer.singleShot(0, self.connection_lines[i][0].updateLinePos)

        self.last_pos = self.pos()

    def mousePressEvent(self, event):
        Canvas.last_z_value += 1
        self.setZValue(Canvas.last_z_value)
        self.resetLinesZValue()
        self.moving_cursor = False

        if (event.button() == Qt.RightButton):
          Canvas.scene.clearSelection()
          self.setSelected(True)
          self.mouse_down = False
          return event.accept()
        elif (event.button() == Qt.LeftButton): # FIXME - there's a bug laying around here, this code fixes it:
          if (self.sceneBoundingRect().contains(event.scenePos())):
            self.mouse_down = True
            return QGraphicsItem.mousePressEvent(self, event)
          else:
            self.mouse_down = False
            return event.ignore()
        else:
          self.mouse_down = False
          return QGraphicsItem.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if (self.mouse_down):
          if (not self.moving_cursor):
            self.setCursor(QCursor(Qt.SizeAllCursor))
            self.moving_cursor = True
          return QGraphicsItem.mouseMoveEvent(self, event)
        else:
          return event.accept()

    def mouseReleaseEvent(self, event):
        if (self.moving_cursor):
          self.setCursor(QCursor(Qt.ArrowCursor))

        self.mouse_down = False
        self.moving_cursor = False
        return QGraphicsItem.mouseReleaseEvent(self, event)

    def checkItemPos(self):
        if not Canvas.size_rect.isNull():
          pos = self.scenePos()
          if (Canvas.size_rect.contains(pos)):
            pass
          else:
            if (pos.x() < Canvas.size_rect.x()):
              self.setPos(Canvas.size_rect.x(), pos.y())
            elif (pos.y() < Canvas.size_rect.y()):
              self.setPos(pos.x(), Canvas.size_rect.y())
            elif (pos.x() > Canvas.size_rect.width()):
              self.setPos(Canvas.size_rect.width(), pos.y())
            elif (pos.y() > Canvas.size_rect.height()):
              self.setPos(pos.x(), Canvas.size_rect.height())

    def contextMenuEvent(self, event):
        menu = QMenu()

        port_list = Canvas.callback(ACTION_REQUEST_GROUP_CONNECTION_LIST, self.group_id, self.text, self.splitted_mode if (self.splitted) else PORT_MODE_INPUT|PORT_MODE_OUTPUT)
        port_list_names = []
        discMenu = QMenu("Disconnect", menu)
        if (len(port_list) > 0):
          for i in range(len(port_list)):
            if (port_list[i][1] not in port_list_names):
              act_x_disc = discMenu.addAction(port_list[i][1])
              QObject.connect(act_x_disc, SIGNAL("triggered()"), lambda port_id=port_list[i][0]: self.contextMenuDisconnect(port_id))
              port_list_names.append(port_list[i][1])
        else:
          act_x_disc = discMenu.addAction("No connections")
          act_x_disc.setEnabled(False)

        menu.addMenu(discMenu)
        act_x_disc_all = menu.addAction("Disconnect &All")
        act_x_sep1 = menu.addSeparator()
        act_x_rename = menu.addAction("&Rename")
        act_x_sep2 = menu.addSeparator()
        act_x_split_join = menu.addAction("Join" if self.splitted else "Split")

        if (not features['group_rename']):
          act_x_rename.setVisible(False)
          act_x_sep1.setVisible(False)

        for i in range(len(Canvas.group_list)):
          if (Canvas.group_list[i]['id'] == self.group_id):
            if (self.splitted):
              if (len(Canvas.group_list[i]['widgets'][1].port_list) == 0):
                act_x_split_join.setVisible(False)
                act_x_sep2.setVisible(False)
            else:
              have_in = have_out = False
              for j in range(len(self.port_list)):
                if (self.port_list[j]['mode'] == PORT_MODE_INPUT):
                  have_in = True
                elif (self.port_list[j]['mode'] == PORT_MODE_OUTPUT):
                  have_out = True

                if (have_in and have_out):
                  break
              else:
                act_x_split_join.setVisible(False)
                act_x_sep2.setVisible(False)
            break

        act_selected = menu.exec_(event.screenPos())

        if (act_selected == act_x_split_join):
          if (self.splitted):
            Canvas.callback(ACTION_GROUP_JOIN, self.group_id)
          else:
            Canvas.callback(ACTION_GROUP_SPLIT, self.group_id)

        elif (act_selected == act_x_rename):
          new_name = QInputDialog.getText(None, "Rename Group", "New name:", text=self.text)
          if (new_name[1] and not new_name[0].isEmpty()):
            Canvas.callback(ACTION_GROUP_RENAME, self.group_id, new_name[0])

        elif (act_selected == act_x_disc_all):
          Canvas.callback(ACTION_GROUP_DISCONNECT_ALL, self.group_id, self.text)

        return event.accept()

    def contextMenuDisconnect(self, port_id):
        conns_todo = []

        for i in range(len(Canvas.connection_list)):
          if (Canvas.connection_list[i][1] in self.port_list_ids and Canvas.connection_list[i][2] == port_id):
            conns_todo.append(Canvas.connection_list[i][0])
          elif (Canvas.connection_list[i][2] in self.port_list_ids and Canvas.connection_list[i][1] == port_id):
            conns_todo.append(Canvas.connection_list[i][0])

        for i in range(len(conns_todo)):
          Canvas.callback(ACTION_PORTS_DISCONNECT, conns_todo[i])

    def boundingRect(self):
        return QRectF(0, 0, self.box_width, self.box_height)

    def paint(self, painter, option, widget):
        painter.setRenderHints(QPainter.Antialiasing, (options['antialiasing'] == 2))

        self.repaintLines()

        if (self.isSelected()):
          painter.setPen(Canvas.theme['box_pen_sel'])
        else:
          painter.setPen(Canvas.theme['box_pen'])

        box_gradient = QLinearGradient(0, 0, 0, self.box_height)
        box_gradient.setColorAt(0, Canvas.theme['box_bg_1'])
        box_gradient.setColorAt(1, Canvas.theme['box_bg_2'])

        painter.setBrush(box_gradient)
        painter.drawRect(0, 0, self.box_width, self.box_height)

        painter.setFont(self.font_name)
        painter.setPen(Canvas.theme['box_text'])
        painter.drawText(QPointF(25, 16), self.text)

        #painter.drawPixmap(QRectF(4, 3, 16, 16), self.icon, QRectF(0, 0, 16, 16))


# Canvas Icon
class CanvasIcon(QGraphicsSvgItem):
    def __init__(self, icon, name, parent=None):
        super(CanvasIcon, self).__init__(parent)

        self.setIcon(icon, name)

        self.colorFX = QGraphicsColorizeEffect(self)
        self.colorFX.setColor(Canvas.theme['box_text'].color())
        self.setGraphicsEffect(self.colorFX)

    def setIcon(self, icon, name):
        if (type(name) == QString):
          name = QStringStr(name)

        name = name.lower()

        if (icon == ICON_APPLICATION):
          self.size = QRectF(3, 2, 19, 18)

          if ("audacious" in name):
            self.size = QRectF(5, 4, 16, 16)
            icon_path = ":/svg/audacious.svg"
          elif ("jamin" in name):
            self.size = QRectF(5, 3, 16, 16)
            icon_path = ":/svg/jamin.svg"
          elif ("mplayer" in name):
            self.size = QRectF(5, 4, 16, 16)
            icon_path = ":/svg/mplayer.svg"
          elif ("vlc" in name):
            self.size = QRectF(5, 3, 16, 16)
            icon_path = ":/svg/vlc.svg"

          else:
            self.size = QRectF(5, 3, 16, 16)
            icon_path = ":/svg/generic_app.svg"

        elif (icon == ICON_HARDWARE):
            self.size = QRectF(5, 2, 16, 16)
            icon_path = ":/svg/generic_hw.svg"

        elif (icon == ICON_LADISH_ROOM):
            self.size = QRectF(5, 2, 16, 16)
            icon_path = ":/svg/generic_hw.svg"

        else:
          self.size = QRectF(0, 0, 0, 0)
          print "patchcanvas::CanvasIcon.setIcon - Unsupported Icon requested"
          return

        self.renderer_ = QSvgRenderer(icon_path, Canvas.scene)
        self.setSharedRenderer(self.renderer_)
        self.update()

    def boundingRect(self):
        return QRectF(self.size)

    def paint(self, painter, option, widget):
        painter.setRenderHints(QPainter.Antialiasing, (options['antialiasing'] == 2))
        #painter.setRenderHints(QPainter.Antialiasing|QPainter.SmoothPixmapTransform)
        self.renderer_.render(painter, QRectF(self.size))

# Port Glow Effect
class CanvasPortGlow(QGraphicsDropShadowEffect):
    def __init__(self, parent=None):
        super(CanvasPortGlow, self).__init__(parent)

        self.setBlurRadius(12)
        self.setOffset(0, 0)

    def setPortType(self, port_type):
        if (port_type == PORT_TYPE_AUDIO):
          self.setColor(Canvas.theme['line_audio_glow'])
        elif (port_type == PORT_TYPE_MIDI):
          self.setColor(Canvas.theme['line_midi_glow'])
        elif (port_type == PORT_TYPE_OUTRO):
          self.setColor(Canvas.theme['line_outro_glow'])

# Box Shadow Effect
class CanvasBoxShadow(QGraphicsDropShadowEffect):
    def __init__(self, parent=None):
        super(CanvasBoxShadow, self).__init__(parent)

        self.setBlurRadius(20)
        self.setColor(Canvas.theme['box_shadow'])
        self.setOffset(0, 0)
        self.fake_parent = None

    def setFakeParent(self, parent):
        self.fake_parent = parent

    def draw(self, painter):
        self.fake_parent.repaintLines()
        return QGraphicsDropShadowEffect.draw(self, painter)

