#!/usr/bin/make -f
# Makefile for Cadence #
# ---------------------- #
# Created by falkTX
#

DESTDIR =
PYUIC = pyuic4
PYRCC = pyrcc4


all: build

build: UI RC

UI: cadence catarina catia claudia tools

cadence: src/ui_cadence.py

catarina: src/ui_catarina.py \
	src/ui_catarina_addgroup.py src/ui_catarina_removegroup.py src/ui_catarina_renamegroup.py \
	src/ui_catarina_addport.py src/ui_catarina_removeport.py src/ui_catarina_renameport.py \
	src/ui_catarina_connectports.py src/ui_catarina_disconnectports.py

catia: src/ui_catia.py

claudia: src/ui_claudia.py \
	src/ui_claudia_createroom.py src/ui_claudia_addnew.py src/ui_claudia_addnew_kxstudio.py \
	src/ui_claudia_runcustom.py src/ui_claudia_saveproject.py src/ui_claudia_projectproperties.py \
	src/ui_claudia_studiolist.py src/ui_claudia_studioname.py

tools: \
	src/ui_logs.py src/ui_render.py src/ui_xycontroller.py \
	src/ui_settings_app.py src/ui_settings_jack.py

src/ui_cadence.py: src/ui/cadence.ui
	$(PYUIC) -o ./src/ui_cadence.py $<

src/ui_catarina.py: src/ui/catarina.ui
	$(PYUIC) -o ./src/ui_catarina.py $<

src/ui_catarina_addgroup.py: src/ui/catarina_addgroup.ui
	$(PYUIC) -o ./src/ui_catarina_addgroup.py $<

src/ui_catarina_removegroup.py: src/ui/catarina_removegroup.ui
	$(PYUIC) -o ./src/ui_catarina_removegroup.py $<

src/ui_catarina_renamegroup.py: src/ui/catarina_renamegroup.ui
	$(PYUIC) -o ./src/ui_catarina_renamegroup.py $<

src/ui_catarina_addport.py: src/ui/catarina_addport.ui
	$(PYUIC) -o ./src/ui_catarina_addport.py $<

src/ui_catarina_removeport.py: src/ui/catarina_removeport.ui
	$(PYUIC) -o ./src/ui_catarina_removeport.py $<

src/ui_catarina_renameport.py: src/ui/catarina_renameport.ui
	$(PYUIC) -o ./src/ui_catarina_renameport.py $<

src/ui_catarina_connectports.py: src/ui/catarina_connectports.ui
	$(PYUIC) -o ./src/ui_catarina_connectports.py $<

src/ui_catarina_disconnectports.py: src/ui/catarina_disconnectports.ui
	$(PYUIC) -o ./src/ui_catarina_disconnectports.py $<

src/ui_catia.py: src/ui/catia.ui
	$(PYUIC) -o ./src/ui_catia.py $<

src/ui_claudia.py: src/ui/claudia.ui
	$(PYUIC) -o ./src/ui_claudia.py $<

src/ui_claudia_createroom.py: src/ui/claudia_createroom.ui
	$(PYUIC) -o ./src/ui_claudia_createroom.py $<

src/ui_claudia_addnew.py: src/ui/claudia_addnew.ui
	$(PYUIC) -o ./src/ui_claudia_addnew.py $<

src/ui_claudia_addnew_kxstudio.py: src/ui/claudia_addnew_kxstudio.ui
	$(PYUIC) -o ./src/ui_claudia_addnew_kxstudio.py $<

src/ui_claudia_runcustom.py: src/ui/claudia_runcustom.ui
	$(PYUIC) -o ./src/ui_claudia_runcustom.py $<

src/ui_claudia_saveproject.py: src/ui/claudia_saveproject.ui
	$(PYUIC) -o ./src/ui_claudia_saveproject.py $<

src/ui_claudia_projectproperties.py: src/ui/claudia_projectproperties.ui
	$(PYUIC) -o ./src/ui_claudia_projectproperties.py $<

src/ui_claudia_studiolist.py: src/ui/claudia_studiolist.ui
	$(PYUIC) -o ./src/ui_claudia_studiolist.py $<

src/ui_claudia_studioname.py: src/ui/claudia_studioname.ui
	$(PYUIC) -o ./src/ui_claudia_studioname.py $<

src/ui_logs.py: src/ui/logs.ui
	$(PYUIC) -o ./src/ui_logs.py $<

src/ui_render.py: src/ui/render.ui
	$(PYUIC) -o ./src/ui_render.py $<

src/ui_xycontroller.py: src/ui/xycontroller.ui
	$(PYUIC) -o ./src/ui_xycontroller.py $<

src/ui_settings_app.py: src/ui/settings_app.ui
	$(PYUIC) -o ./src/ui_settings_app.py $<

src/ui_settings_jack.py: src/ui/settings_jack.ui
	$(PYUIC) -o ./src/ui_settings_jack.py $<

RC: src/icons_rc.py

src/icons_rc.py: src/icons/icons.qrc
	$(PYRCC) -o ./src/icons_rc.py $<

# LANG:
#	pylupdate4 -verbose src/lang/lang.pro
#	lrelease src/lang/lang.pro


clean:
	rm -f src/icons_rc.py src/ui_*.py src/*.pyc src/*~ *~

distclean: clean


install:
	install -d $(DESTDIR)/usr/bin/
	install -d $(DESTDIR)/usr/share/applications/
	install -d $(DESTDIR)/usr/share/pixmaps/
	install -d $(DESTDIR)/usr/share/cadence/
	install -d $(DESTDIR)/usr/share/cadence/src/
	install -d $(DESTDIR)/usr/share/cadence/icons/
	install -d $(DESTDIR)/usr/share/cadence/pulse2jack/
	install -d $(DESTDIR)/usr/share/cadence/templates/
	install -m 755 data/cadence data/cadence-session-start data/catarina data/catia data/claudia pulse2jack/pulse2jack $(DESTDIR)/usr/bin/
# 	install -m 644 data/*.desktop $(DESTDIR)/usr/share/applications/
	install -m 644 pulse2jack/profiles/* $(DESTDIR)/usr/share/cadence/pulse2jack/
	install -m 644 src/icons/svg/j2sc.svg $(DESTDIR)/usr/share/pixmaps/
	install -m 755 src/*.py $(DESTDIR)/usr/share/cadence/src/
	cp -r data/icons/* $(DESTDIR)/usr/share/cadence/icons/
	cp -r templates/* $(DESTDIR)/usr/share/cadence/templates/

uninstall:
	rm -f $(DESTDIR)/usr/bin/cadence
	rm -f $(DESTDIR)/usr/bin/cadence-session-start
	rm -f $(DESTDIR)/usr/bin/catarina
	rm -f $(DESTDIR)/usr/bin/catia
	rm -f $(DESTDIR)/usr/bin/claudia
	rm -f $(DESTDIR)/usr/bin/pulse2jack
	rm -rf $(DESTDIR)/usr/share/cadence/
