/*
 * Stereo Audio Gain plugin, by falkTX
 *
 */

const float gain = 1.5;

// just for a test
#define PLUGIN_STANDALONE

#ifdef PLUGIN_STANDALONE
#include <jack/jack.h>
#include <jack/midiport.h>

#include <stdio.h>
#include <string.h>
#include <unistd.h>

jack_port_t *in_port_1, *in_port_2;
jack_port_t *out_port_1, *out_port_2;
jack_client_t *client;

int process_jack(jack_nframes_t nframes, void *arg)
{
    jack_default_audio_sample_t *in1, *in2, *out1, *out2;
    in1 = (jack_default_audio_sample_t*)jack_port_get_buffer(in_port_1, nframes);
    in2 = (jack_default_audio_sample_t*)jack_port_get_buffer(in_port_2, nframes);
    out1 = (jack_default_audio_sample_t*)jack_port_get_buffer(out_port_1, nframes);
    out2 = (jack_default_audio_sample_t*)jack_port_get_buffer(out_port_2, nframes);

    int i;
    for (i=0; i < nframes; ++i) {
      out1[i] = in1[i]*gain;
      out2[i] = in2[i]*gain;
    }

    return 0;
}

void shutdown_by_jack(void *arg)
{
    exit(1);
}

int main (int argc, char *argv[])
{
    client = jack_client_open("audio-gain", JackNullOption, 0);
    jack_set_process_callback(client, process_jack, 0);
    jack_on_shutdown(client, shutdown_by_jack, 0);
    in_port_1 = jack_port_register(client, "input-01", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    in_port_2 = jack_port_register(client, "input-02", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    out_port_1 = jack_port_register(client, "output-01", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    out_port_2 = jack_port_register(client, "output-02", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    jack_activate(client);

    sleep(-1);
    jack_client_close (client);

    return 0;
}

#endif

